﻿using CryDownloader.Plugin;
using System;
using System.Diagnostics;

namespace CryDownloader.Logic
{
    public class LogManager : ILogManager
    {
        public void WriteLog(LogLevel logLevel, string message)
        {
            Debug.WriteLine(message);
        }

        public void WriteException(LogLevel logLevel, string message, Exception ex)
        {

        }
    }
}
