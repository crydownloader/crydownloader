﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    static class StreamExtension
    {
        private static byte[] Read(Stream stream, int readSize)
        {
            byte[] buf = new byte[readSize];

            bool result = stream.Read(buf, 0, buf.Length) == buf.Length;

            if (result)
                return buf;

            return null;
        }

        public static bool Read(this Stream stream, out sbyte output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = (sbyte)buf[0];
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out byte output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = buf[0];
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out short output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = (short)((buf[1] << 8) | buf[0]);
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out ushort output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = (ushort)((buf[1] << 8) | buf[0]);
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out int output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = ((buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0]);
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out uint output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = (uint)((buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0]);
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out long output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = ((buf[7] << 56) | (buf[6] << 48) | (buf[5] << 40) | 
                    (buf[4] << 32) | (buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0]);
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out ulong output)
        {
            byte[] buf = Read(stream, 1);

            if (buf != null)
            {
                output = (ulong)((buf[7] << 56) | (buf[6] << 48) | (buf[5] << 40) | 
                    (buf[4] << 32) | (buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0]);
                return true;
            }
            else
                output = 0;

            return false;
        }

        public static bool Read(this Stream stream, out string output)
        {
            if (Read(stream, out int stringSize))
            {
                byte[] buf = Read(stream, stringSize * sizeof(char));

                if (buf != null)
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < stringSize; ++i)
                    {
                        char c = (char)((buf[i + 8] << 8) | buf[i]);
                        sb.Append(c);
                    }

                    output = sb.ToString();
                    return true;
                }
            }

            output = string.Empty;
            return false;
        }
    }
}
