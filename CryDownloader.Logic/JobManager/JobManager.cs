﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    public class JobManager
    {
        private class ThreadInfo : IChildJobManager
        {
            private readonly object m_lockObject;
            private readonly Queue<IChildJob> m_childJobs;
            private readonly List<Task> m_childJobTasks;

            public Thread Thread { get; }
            public bool IsPaused { get; set; }
            public ManualResetEvent ResumeEvent { get; }
            public ManualResetEvent ExitEvent { get; }
            public CancellationTokenSource CancellationTokenSource { get; }

            public IJob CurrentJob { get; set; }

            public int MaxChildJobProcessCount { get; }

            public ThreadInfo(int maxChildJobCount, ParameterizedThreadStart action)
            {
                m_lockObject = new object();
                m_childJobs = new Queue<IChildJob>();
                m_childJobTasks = new List<Task>();

                ResumeEvent = new ManualResetEvent(false);
                ExitEvent = new ManualResetEvent(false);
                CancellationTokenSource = new CancellationTokenSource();
                MaxChildJobProcessCount = maxChildJobCount;

                Thread = new Thread(action);
                Thread.Start(this);
            }

            public void ClearChildJobTasks()
            {
                m_childJobTasks.Clear();
            }

            public void AddChildJob(IChildJob childJob)
            {
                lock (m_lockObject)
                    m_childJobs.Enqueue(childJob);

                if (m_childJobTasks.Count < MaxChildJobProcessCount)
                    m_childJobTasks.Add(Task.Run(ProcessChildJob));
            }

            public IChildJob CollectChildJob()
            {
                lock (m_lockObject)
                    return m_childJobs.Dequeue();
            }

            public bool WaitForChilds()
            {
                if (m_childJobTasks.Count == 0)
                    return true;

                foreach (var child in m_childJobTasks)
                {
                    if (!child.Wait(5000))
                        return false;
                }

                return true;
            }

            private void ProcessChildJob()
            {
                IChildJob childJob;

                while ((childJob = CollectChildJob()) != null)
                    childJob.Process(CurrentJob, CancellationTokenSource.Token);
            }
        }

        private readonly Dictionary<JobPriority, Queue<IJob>> m_jobs;
        private readonly List<ThreadInfo> m_jobTasks;
        private readonly object m_lockObject;
        private readonly ManualResetEvent m_pauseEvent;

        public int MaxJobProcessCount { get; set; }

        public int MaxChildJobProcessCount { get; set; }

        public bool IsRunning { get; private set; }
        public bool IsPaused { get; private set; }

        public JobManager()
        {
            m_jobs = new Dictionary<JobPriority, Queue<IJob>>();
            MaxJobProcessCount = 4;
            MaxChildJobProcessCount = 4;
            m_jobTasks = new List<ThreadInfo>();
            m_lockObject = new object();
            m_pauseEvent = new ManualResetEvent(false);
        }

        public void AddJob(JobPriority priority, IJob job)
        {
            lock (m_lockObject)
            {
                if (!m_jobs.TryGetValue(priority, out Queue<IJob> queue))
                {
                    queue = new Queue<IJob>();
                    m_jobs.Add(priority, queue);
                }

                queue.Enqueue(job);
            }
        }

        public void Start()
        {
            IsRunning = true;
            m_jobTasks.Clear();

            for (int i = 0; i < MaxJobProcessCount; i++)
                m_jobTasks.Add(new ThreadInfo(MaxChildJobProcessCount, ProcessJobTask));
        }

        public void Pause()
        {
            IsPaused = true;
        }

        public bool Resume()
        {
            IsPaused = false;
            m_pauseEvent.Set();

            if (!WaitHandle.WaitAll(m_jobTasks.Select((x) => x.ResumeEvent).ToArray(), 100))
            {
                m_pauseEvent.Reset();
                return false;
            }

            m_pauseEvent.Reset();
            return true;
        }

        public bool Stop()
        {
            IsRunning = false;

            foreach (var job in m_jobTasks)
            {
                // wait for exit loop signal
                if (!WaitHandle.WaitAll(m_jobTasks.Select((x) => x.ResumeEvent).ToArray(), 5000))
                    return false;

                job.Thread.Join(); // wait for real thread exit
            }

            return true;
        }

        private void ProcessJobTask(object param)
        {
            var threadInfo = (ThreadInfo)param;

            while (IsRunning)
            {
                if (IsPaused)
                {
                    threadInfo.IsPaused = true;
                    m_pauseEvent.WaitOne();
                    threadInfo.IsPaused = false;
                    threadInfo.ResumeEvent.Set();
                }

                var currJob = CollectJob();

                if (currJob != null)
                {
                    threadInfo.CurrentJob = currJob;
                    currJob.Process(threadInfo, threadInfo.CancellationTokenSource.Token);

                    threadInfo.WaitForChilds();
                    threadInfo.ClearChildJobTasks();
                }
            }

            threadInfo.ExitEvent.Set();
        }

        private IJob CollectJob()
        {
            if (m_jobs.TryGetValue(JobPriority.High, out Queue<IJob> queue))
            {
                lock (m_lockObject)
                {
                    if (queue.Count > 0)
                        return queue.Dequeue();
                }
            }

            if (m_jobs.TryGetValue(JobPriority.Normal, out queue))
            {
                lock (m_lockObject)
                {
                    if (queue.Count > 0)
                        return queue.Dequeue();
                }
            }

            if (m_jobs.TryGetValue(JobPriority.Low, out queue))
            {
                lock (m_lockObject)
                {
                    if (queue.Count > 0)
                        return queue.Dequeue();
                }
            }

            return null;
        }
    }
}
