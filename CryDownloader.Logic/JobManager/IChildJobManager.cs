﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    public interface IChildJobManager
    {
        void AddChildJob(IChildJob childJob);
    }
}
