﻿using System.Threading;

namespace CryDownloader.Logic
{
    public interface IChildJob
    {
        void Process(IJob job, CancellationToken cancellationToken);
    }
}
