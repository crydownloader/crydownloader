﻿using System.Threading;

namespace CryDownloader.Logic
{
    public interface IJob
    {
        void Process(IChildJobManager jobManager, CancellationToken cancellationToken);
    }

}
