﻿using CryDownloader.Plugin;
using Newtonsoft.Json;
using System;

namespace CryDownloader.Logic
{
    class JsonWriterWrapper : IJsonWriter
    {
        public JsonWriter OriginalWriter { get; }

        public JsonWriterWrapper(JsonWriter writer)
        {
            OriginalWriter = writer;
        }

        public void WriteStartObject()
        {
            OriginalWriter.WriteStartObject();
        }

        public void WriteEndObject()
        {
            OriginalWriter.WriteEndObject();
        }

        public void WriteStartArray()
        {
            OriginalWriter.WriteStartArray();
        }

        public void WriteEndArray()
        {
            OriginalWriter.WriteEndArray();
        }

        public void WriteStartConstructor(string name)
        {
            OriginalWriter.WriteStartConstructor(name);
        }

        public void WriteEndConstructor()
        {
            OriginalWriter.WriteEndConstructor();
        }

        public void WritePropertyName(string name)
        {
            OriginalWriter.WritePropertyName(name);
        }

        public void WritePropertyName(string name, bool escape)
        {
            OriginalWriter.WritePropertyName(name, escape);
        }

        public void WriteEnd()
        {
            OriginalWriter.WriteEnd();
        }

        public void WriteNull()
        {
            OriginalWriter.WriteNull();
        }

        public void WriteUndefined()
        {
            OriginalWriter.WriteUndefined();
        }

        public void WriteRaw(string json)
        {
            OriginalWriter.WriteRaw(json);
        }

        public void WriteRawValue(string json)
        {
            OriginalWriter.WriteRawValue(json);
        }

        public void WriteValue(string value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(int value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(uint value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(long value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(ulong value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(float value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(double value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(bool value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(short value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(ushort value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(char value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(byte value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(sbyte value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(decimal value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(DateTime value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(DateTimeOffset value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(Guid value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(TimeSpan value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(byte[] value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(Uri value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteValue(object value)
        {
            OriginalWriter.WriteValue(value);
        }

        public void WriteComment(string text)
        {
            OriginalWriter.WriteComment(text);
        }

        public void WriteWhitespace(string ws)
        {
            OriginalWriter.WriteWhitespace(ws);
        }
    }
}
