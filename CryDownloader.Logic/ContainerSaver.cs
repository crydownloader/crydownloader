﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CryDownloader.Logic
{
    class ContainerSaver
    {
        public ContainerSaver()
        { }

        public virtual bool Save(string filename, IEnumerable<DownloadContainer> containers)
        {
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };

            using (XmlWriter writer = XmlWriter.Create(filename, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("containers");

                foreach (var container in containers)
                {
                    writer.WriteStartElement("container");
                    writer.WriteAttributeString("name", container.Name);

                    if (container.Url != null)
                        writer.WriteAttributeString("url", container.Url.ToString());

                    foreach (var entry in container)
                    {
                        if (entry.FileState == FileState.FileNotFound)
                            continue;

                        writer.WriteStartElement("entry");
                        writer.WriteAttributeString("name", entry.FileName);
                        writer.WriteString(entry.Url.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            return true;
        }
    }
}
