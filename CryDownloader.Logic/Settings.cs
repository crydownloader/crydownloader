﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace CryDownloader.Logic
{
    public class Settings : MarshalByRefObject
    {
        private int m_scanCount;

        public int DownloadCountPerHost { get; set; }
        public int DownloadCount { get; set; }
        public int ChunkDownloadCount { get; set; }
        public string DownloadDirectory { get; set; }
        public ulong MaxDownloadSpeed { get; set; }

        public int ScanCount { get => m_scanCount; set { m_scanCount = value; ScanCountChanged?.Invoke(this, EventArgs.Empty); } }
        public int RetryCount { get; set; }
        public string Language { get; set; }
        public string Accent { get; set; }
        public string Style { get; set; }
        public bool UseSystemStyle { get; set; }
        public FileSizeFormatType FileSizeFormat { get; set; }
        public int LastWidth { get; set; }
        public int LastHeight { get; set; }
        public WindowState LastWindowState { get; set; }
        public bool ShowMilliseconds { get; set; }
        public bool PreferApplicationFileIcons { get; set; }
        public bool ShowNotifications { get; set; }
        public bool LoadingEntry { get; set; }

        public Dictionary<string, bool> ArchivExtensions { get; set; }
        public Dictionary<string, bool> ImageExtensions { get; set; }
        public Dictionary<string, bool> AudioExtensions { get; set; }
        public Dictionary<string, bool> VideoExtensions { get; set; }

        public Dictionary<string, Dictionary<string, string>> Properties { get; }

        public event EventHandler ScanCountChanged;

        public Settings()
        {
            Properties = new Dictionary<string, Dictionary<string, string>>();
            ArchivExtensions = new Dictionary<string, bool>();
            ImageExtensions = new Dictionary<string, bool>();
            AudioExtensions = new Dictionary<string, bool>();
            VideoExtensions = new Dictionary<string, bool>();
            ScanCount = 4;
            DownloadCount = 4;
            DownloadCountPerHost = 1;
        }

        public void SaveToStream(Stream stream)
        {
            using (var writer = new JsonTextWriter(new StreamWriter(stream)))
            {
                var serializer = new JsonSerializer();

                writer.Formatting = Formatting.Indented;
                serializer.Serialize(writer, this);
            }
        }

        public static Settings LoadFromStream(Stream stream)
        {
            if (stream == null)
                return new Settings();

            using (var writer = new JsonTextReader(new StreamReader(stream)))
            {
                var serializer = new JsonSerializer();

                return serializer.Deserialize<Settings>(writer);
            }
        }
    }
}
