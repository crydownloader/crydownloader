﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    class WebClientEx : WebClient
    {
        public int ConnectionLimit { get; set; }

        public WebClientEx()
        {
            ConnectionLimit = int.MaxValue;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address) as HttpWebRequest;

            if (request != null)
            {
                request.ServicePoint.ConnectionLimit = this.ConnectionLimit;
                request.UserAgent = Hoster.UserAgent;
            }

            return request;
        }
    }
}
