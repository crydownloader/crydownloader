﻿using CryDownloader.Plugin;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;

namespace CryDownloader.Logic
{
    public class DownloadEntryAddedEventArgs : EventArgs
    {
        public DownloadEntry Entry { get; }

        public DownloadEntryAddedEventArgs(DownloadEntry entry)
        {
            Entry = entry;
        }
    }

    public class DownloadContainer : ICollection<DownloadEntry>
    {
        class DownloadSessionContainer
        {
            public string Name { get; set; }
            public List<DownloadSessionEntry> Entries { get; }

            public DownloadSessionContainer()
            {
                Entries = new List<DownloadSessionEntry>();
            }
        }

        class DownloadSessionEntry
        {
            public string Name { get; set; }
            public string DownloadPath { get; set; }
            public Uri Url { get; set; }
        }

        public event EventHandler<DownloadEntryAddedEventArgs> EntryAdded;

        private List<DownloadEntry> m_list;

        public string Name { get; set; }
        public string Directory { get; set; }

        public int Count => m_list.Count;

        public int UrlCount => Urls.Count;

        public Uri Url { get; set; }

        public bool IsReadOnly => ((ICollection<DownloadEntry>)m_list).IsReadOnly;

        IContainerCreatorInternal m_Creator;
        public IContainer Container { get; private set; }

        public Dictionary<Uri, string> Urls { get; }

        public DownloadContainer()
        {
            m_list = new List<DownloadEntry>();
            Urls = new Dictionary<Uri, string>();
        }

        public virtual void Load(IEntryCreator creator,
            IDownloadContainerView containerView,
            Action<IContainer, DownloadEntry> entryAddedCallback, 
            Action<Uri> scanUrlChangedCallback, 
            CancellationToken? token)
        {
            creator.CreateEntries(Urls.Keys, containerView, entryAddedCallback, scanUrlChangedCallback, token);

            foreach (var entry in m_list)
            {
                if (Urls.TryGetValue(entry.Url, out var strName))
                    entry.FileName = strName;
            }
        }

        internal void SetContainer(IContainerCreatorInternal creator, IContainer container)
        {
            Container = container;
            m_Creator = creator;
        }

        public void Add(DownloadEntry item)
        {
            if (m_list.Count == 0 && m_Creator != null && Container != null)
                m_Creator.AddContainer(Container);

            m_list.Add(item);
            EntryAdded?.Invoke(this, new DownloadEntryAddedEventArgs(item));
        }

        public void Clear()
        {
            m_list.Clear();
        }

        public bool Contains(DownloadEntry item)
        {
            return m_list.Contains(item);
        }

        public void CopyTo(DownloadEntry[] array, int arrayIndex)
        {
            m_list.CopyTo(array, arrayIndex);
        }

        public IEnumerator<DownloadEntry> GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        public bool Remove(DownloadEntry item)
        {
            return m_list.Remove(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual void SaveToStream(Stream stream)
        {
            throw new NotSupportedException();
        }

        public static void SaveToStream(Stream stream, ICollection<DownloadContainer> containers)
        {
            List<DownloadSessionContainer> lstEntries = new List<DownloadSessionContainer>();

            foreach (var container in containers)
            {
                DownloadSessionContainer sessionContainer = new DownloadSessionContainer
                {
                    Name = container.Name
                };

                foreach (var entry in container)
                {
                    DownloadSessionEntry sessionEntry = new DownloadSessionEntry
                    {
                        Name = entry.FileName,
                        Url = entry.Url,
                        DownloadPath = entry.DownloadPath,
                    };

                    sessionContainer.Entries.Add(sessionEntry);
                }

                lstEntries.Add(sessionContainer);
            }

            SaveEntriesToStream(stream, lstEntries);
        }

        public static ICollection<DownloadContainer> LoadFromStream(Stream stream, HosterManager manager, Dictionary<string, IContainer> dictContainer)
        {
            List<DownloadSessionContainer> lstEntries = new List<DownloadSessionContainer>();

            LoadEntriesFromStream(stream, lstEntries);

            List<DownloadContainer> result = new List<DownloadContainer>();

            foreach (var entry in lstEntries)
            {
                DownloadContainer container = new DownloadContainer
                {
                    Name = entry.Name
                };

              // ContainerCreator2 creator2 = new ContainerCreator2(container, dictContainer, null);
              //
              // foreach (var file in entry.Entries)
              //     manager.CreateDownloadEntry(file.Url, creator2, null);
              //
              // result.Add(container);
            }

            return result;
        }

        private static void SaveEntriesToStream(Stream stream, IEnumerable<DownloadSessionContainer> containers)
        {
            using (var writer = new JsonTextWriter(new StreamWriter(stream)))
            {
                var serializer = new JsonSerializer();

                writer.Formatting = Newtonsoft.Json.Formatting.Indented;
                serializer.Serialize(writer, containers);
            }
        }

        private static void LoadEntriesFromStream(Stream stream, ICollection<DownloadSessionContainer> containers)
        {
            using (var writer = new JsonTextReader(new StreamReader(stream)))
            {
                var serializer = new JsonSerializer();

                List<DownloadSessionContainer> serializedContainers = serializer.Deserialize<List<DownloadSessionContainer>>(writer);

                if (serializedContainers != null)
                {
                    foreach (var container in serializedContainers)
                        containers.Add(container);
                }
            }
        }
    }
}
