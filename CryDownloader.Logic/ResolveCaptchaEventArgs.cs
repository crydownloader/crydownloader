﻿using CryDownloader.Plugin;
using System;
using System.Windows;
using System.Windows.Threading;

namespace CryDownloader.Logic
{
    public sealed class OpenBrowserEventArgs : EventArgs
    {
        public CaptchaType CaptchaType { get; internal set; }
        public IInternWebBrowser Browser { get; }

        public IManualResolveElement Result { get; set; }

        public Visibility Visibility { get; }

        public OpenBrowserEventArgs(CaptchaType captchaType, IInternWebBrowser browser, Visibility visibility)
        {
            CaptchaType = captchaType;
            Browser = browser;
            Visibility = visibility;
        }

        internal void ResolveCaptcha(Dispatcher dispatcher, Application app, IHtmlNode captchaNode, Func<bool> checkCallback, Action successCallback)
        {
            app.CaptchaManager.ResolveCaptcha(dispatcher, CaptchaType, Result, Browser, captchaNode, checkCallback, successCallback);
        }
    }
}
