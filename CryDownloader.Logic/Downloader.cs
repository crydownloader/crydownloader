﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CryDownloader.Logic
{
    public class Downloader
    {
        class DownloadThread
        {
            public event EventHandler ThreadFinished;

            public Thread Thread { get; }
            public bool IsRunning { get; set; }

            public int ThreadIndex { get; }

            private readonly LinkedList<DownloadEntry>[] m_queue;
            private readonly Application m_application;
            private readonly Dictionary<Hoster, int> m_hosterDownloadCount;

            private DownloadEntry m_currentDownload;

            public DownloadThread(LinkedList<DownloadEntry>[] queue, Dictionary<Hoster, int> dictHosterDownloadCount, Application app, int threadIndex)
            {
                m_queue = queue;
                m_application = app;
                m_hosterDownloadCount = dictHosterDownloadCount;
                ThreadIndex = threadIndex;
                Thread = new Thread(ProcessThread);
            }

            public void Start()
            {
                Thread.Start();
            }

            public void Stop()
            {
                IsRunning = false;
                Thread.Join();
            }

            private void ProcessThread()
            {
                IsRunning = true;

                while (IsRunning)
                {
                    DownloadEntry entry = GetNextDownloadEntry();

                    if (entry == null)
                        IsRunning = false;
                    else
                    {
                        lock (m_currentDownload)
                        {
                            m_currentDownload = entry;
                        }

                        if (m_currentDownload == null)
                            IsRunning = false;
                        else
                        {
                            m_application.DownloadEntry(m_currentDownload);
                        }

                        lock (m_hosterDownloadCount)
                        {
                            m_hosterDownloadCount[m_currentDownload.Hoster] -= 1;
                        }
                    }
                }

                ThreadFinished?.Invoke(this, EventArgs.Empty);
            }

            private DownloadEntry GetNextDownloadEntry()
            {
                lock (m_queue)
                {
                    lock (m_hosterDownloadCount)
                    {
                        for (int i = 0; i < m_queue.Length; ++i)
                        {
                            lock (m_queue[i])
                            {
                                if (m_queue[i].Count > 0)
                                {
                                    var entry = m_queue[i].First;
                                    var result = BeginDownloadNode(entry);

                                    if (result != null)
                                        return result;
                                }
                            }
                        }
                    }
                }

                return null;
            }

            private DownloadEntry BeginDownloadNode(LinkedListNode<DownloadEntry> node)
            {
                if (m_hosterDownloadCount.TryGetValue(node.Value.Hoster, out var downloadCount))
                {
                    if (downloadCount >= m_application.Settings.DownloadCountPerHost)
                        return BeginDownloadNode(node.Next);
                }
                else
                    m_hosterDownloadCount.Add(node.Value.Hoster, 1);

                return node.Value;
            }
        }

        private int m_threadCount;

        private DownloadThread[] m_downloadThreads;
        private LinkedList<DownloadEntry>[] m_queue;
        private bool m_bIsRunning;
        private readonly Dictionary<Hoster, int> m_hosterDownloadCount;

        public int ThreadCount
        {
            get => m_threadCount;
            set
            {
                int oldThread = m_threadCount;
                m_threadCount = value;
                OnThreadCountChanged(oldThread, m_threadCount);
            }
        }

        public int PriorityCount { get; set; }

        public Application Application { get; }

        public Downloader(Application app)
        {
            Application = app;
            ThreadCount = app.Settings.DownloadCount;

            m_hosterDownloadCount = new Dictionary<Hoster, int>();
        }

        public void Destroy()
        {
            m_bIsRunning = false;

            lock (m_downloadThreads)
            {
                for (int i = 0; i < m_downloadThreads.Length; ++i)
                    m_downloadThreads[i].Stop();
            }
        }

        public void Start()
        {
            if (m_queue != null)
            {
                if (m_queue.Length != PriorityCount)
                    m_queue = new LinkedList<DownloadEntry>[PriorityCount];
            }
            else
                m_queue = new LinkedList<DownloadEntry>[PriorityCount];

            lock (m_downloadThreads)
            {
                for (int i = 0; i < m_downloadThreads.Length; ++i)
                    m_downloadThreads[i].Start();
            }
        }

        public void Stop()
        {
            Application.CancelDownload();
        }

        public void Pause()
        {
            Application.PauseDownload();
        }

        public void AddEntry(IDownloadEntryView entry)
        {
            lock (m_queue)
            {
                if (entry.Priority >= PriorityCount)
                    m_queue[m_queue.Length - 1].AddFirst(entry.Data);
                else
                    m_queue[entry.Priority].AddFirst(entry.Data);
            }
        }

        public void AddEntries(IEnumerable<IDownloadEntryView> entries)
        {
            List<DownloadEntry>[] priorityEntries = new List<DownloadEntry>[4];

            foreach (var entry in entries)
            {
                if (entry.Priority >= priorityEntries.Length)
                    priorityEntries[priorityEntries.Length - 1].Add(entry.Data);
                else
                    priorityEntries[entry.Priority].Add(entry.Data);
            }

            lock (m_queue)
            {
                for (int i = 0; i < priorityEntries.Length; ++i)
                {
                    foreach (var entry in priorityEntries[i])
                        m_queue[i].AddFirst(entry);
                }
            }
        }

        protected virtual void OnThreadCountChanged(int oldThreadCount, int newThreadCount)
        {
            int dif = newThreadCount - oldThreadCount;

            if (dif < 0)
            {
                dif = -dif;

                lock (m_downloadThreads)
                {
                    for (int i = 1; i < dif + 1; ++i)
                    {
                        int index = m_downloadThreads.Length - i;
                        m_downloadThreads[index].IsRunning = false;
                        m_downloadThreads[index].ThreadFinished -= Downloader_ThreadFinished;
                        m_downloadThreads[index].ThreadFinished += Downloader_ThreadFinished;
                    }
                }
            }
            else
            {
                lock (m_downloadThreads)
                {
                    Array.Resize(ref m_downloadThreads, ThreadCount);

                    for (int i = 1; i < dif + 1; ++i)
                    {
                        int index = m_downloadThreads.Length - i;
                        m_downloadThreads[m_downloadThreads.Length - i] = new DownloadThread(m_queue, m_hosterDownloadCount, Application, index);

                        if (m_bIsRunning)
                            m_downloadThreads[m_downloadThreads.Length - i].Start();
                    }
                }
            }
        }

        private void Downloader_ThreadFinished(object sender, EventArgs e)
        {
            lock (m_downloadThreads)
            {
                Array.Resize(ref m_downloadThreads, ThreadCount);
            }
        }
    }
}
