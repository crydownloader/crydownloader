﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CryDownloader.Logic
{
    abstract class DownloadContainerLoader<T>
    {
        public List<T> Container { get; }

        public DownloadContainerLoader()
        {
            Container = new List<T>();
        }

        public abstract bool Load(Stream stream);
    }

    class DownloadContainerLoader : DownloadContainerLoader<DownloadContainer>
    {
        private bool ReadNode(DownloadContainer container, XmlNode node)
        {
            if (node.Name != "container")
                return false;

            XmlAttribute nameAttr = node.Attributes["name"];

            if (nameAttr == null)
                return false;

            container.Name = nameAttr.Value;

            nameAttr = node.Attributes["url"];

            if (nameAttr == null)
                return false;

            container.Url = new Uri(nameAttr.Value);

            XmlNodeList nodes = node.SelectNodes("./entry");

            foreach (XmlNode node2 in nodes)
            {
                nameAttr = node2.Attributes["name"];

                if (nameAttr == null)
                    return false;

                container.Urls.Add(new Uri(node2.Value ?? node2.InnerText), nameAttr.Value);
            }

            return true;
        }

        public override bool Load(Stream stream)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(stream);

            if (doc.DocumentElement.Name == "containers")
            {
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    var downloadContainer = new DownloadContainer();

                    if (!ReadNode(downloadContainer, node))
                        return false;

                    Container.Add(downloadContainer);
                }
            }

            return true;
        }
    }
}
