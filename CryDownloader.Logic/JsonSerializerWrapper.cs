﻿using CryDownloader.Plugin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    class JsonSerializerWrapper : IJsonSerializer
    {
        public JsonSerializer Serializer { get; }

        public JsonSerializerWrapper(JsonSerializer serializer)
        {
            Serializer = serializer;
        }


        [DebuggerStepThrough]
        public void Populate(TextReader reader, object target)
        {
            Serializer.Populate(new JsonTextReader(reader), target);
        }

        [DebuggerStepThrough]
        public void Populate(IJsonReader reader, object target)
        {
            Serializer.Populate(((JsonReaderWrapper)reader).OriginalReader, target);
        }

        [DebuggerStepThrough]
        public object Deserialize(IJsonReader reader)
        {
            return Serializer.Deserialize(((JsonReaderWrapper)reader).OriginalReader, null);
        }

        [DebuggerStepThrough]
        public object Deserialize(TextReader reader, Type objectType)
        {
            return Serializer.Deserialize(new JsonTextReader(reader), objectType);
        }

        [DebuggerStepThrough]
        public T Deserialize<T>(IJsonReader reader)
        {
            return Serializer.Deserialize<T>(((JsonReaderWrapper)reader).OriginalReader);
        }

        [DebuggerStepThrough]
        public object Deserialize(IJsonReader reader, Type objectType)
        {
            return Serializer.Deserialize(((JsonReaderWrapper)reader).OriginalReader, objectType);
        }

        public void Serialize(TextWriter textWriter, object value)
        {
            Serializer.Serialize(new JsonTextWriter(textWriter), value);
        }

        public void Serialize(IJsonWriter jsonWriter, object value, Type objectType)
        {
            Serializer.Serialize(((JsonWriterWrapper)jsonWriter).OriginalWriter, value, objectType);
        }

        public void Serialize(TextWriter textWriter, object value, Type objectType)
        {
            Serializer.Serialize(new JsonTextWriter(textWriter), value, objectType);
        }

        public void Serialize(IJsonWriter jsonWriter, object value)
        {
            Serializer.Serialize(((JsonWriterWrapper)jsonWriter).OriginalWriter, value, null);
        }
    }
}
