﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    class NotAvailUpdatePackage : UpdatePackage
    {
        public NotAvailUpdatePackage()
            : base(null, string.Empty, UpdateChannel.None)
        { }
    }
}
