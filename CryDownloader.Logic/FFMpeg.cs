﻿using System;
using System.Diagnostics;

namespace CryDownloader.Logic
{
    class FFMpeg
    {
        public bool Muxing(LogManager logManager, string fileListPath, string outputFile)
        {
            string commandline = $"-protocol_whitelist file,http,https,tcp,tls -i {fileListPath} -c copy {outputFile}";

            return StartProcess(logManager, commandline);
        }

        private bool StartProcess(LogManager logManager, string commandLine)
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo("", commandLine);


            processStartInfo.UseShellExecute = false;

            try
            {
                using (var process = Process.Start(processStartInfo))
                {
                    process.ErrorDataReceived += Process_ErrorDataReceived;
                }

                return true;
            }
            catch (Exception ex)
            {
                logManager.WriteException(Plugin.LogLevel.Normal, "failed to start ffmpeg", ex);
                return false;
            }
        }

        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null)
                return;

            if (IsProgressData(e.Data))
            {
                OnProgressChanged();
            }
            else if (IsCompleteData(e.Data))
            {
                OnCompleted();
            }
        }

        protected void OnProgressChanged()
        {

        }

        protected void OnCompleted()
        {

        }

        private bool IsProgressData(string data)
        {
            return false;
        }

        private bool IsCompleteData(string data)
        {
            return false;
        }
    }
}
