﻿using CefSharp;
using CryDownloader.Plugin;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using System.Windows.Threading;
using IRequest = CefSharp.IRequest;

namespace CryDownloader.Logic
{
    public interface IInternWebBrowser : Plugin.IWebBrowser
    {
        FrameworkElement Control { get; }

        void SetBackground(SolidColorBrush brush);
    }

    class WebBrowser : MarshalByRefObject, IInternWebBrowser
    {
        class DownloadHandler : IDownloadHandler
        {
            public Uri DownloadUrl { get; private set; }

            public void OnBeforeDownload(CefSharp.IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
            {
                DownloadUrl = new Uri(downloadItem.Url);

                if (!callback.IsDisposed)
                    callback.Dispose();
            }

            public void OnDownloadUpdated(CefSharp.IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IDownloadItemCallback callback)
            {
                if (!callback.IsDisposed)
                    callback.Dispose();
            }
        }

        class RequestWrapper : Plugin.IRequest
        {
            class RequestStream : System.IO.Stream
            {
                private readonly MemoryStream m_memoryStream;

                public byte[] Content { get; private set; }

                public override bool CanRead => m_memoryStream.CanRead;

                public override bool CanSeek => m_memoryStream.CanSeek;

                public override bool CanWrite => m_memoryStream.CanWrite;

                public override long Length => m_memoryStream.Length;

                public override long Position { get => m_memoryStream.Position; set => m_memoryStream.Position = value; }

                public RequestStream()
                {
                    m_memoryStream = new MemoryStream();
                }

                public override void Flush()
                {
                    m_memoryStream.Flush();
                }

                public override int Read(byte[] buffer, int offset, int count)
                {
                    return m_memoryStream.Read(buffer, offset, count);
                }

                public override long Seek(long offset, SeekOrigin origin)
                {
                    return m_memoryStream.Seek(offset, origin);
                }

                public override void SetLength(long value)
                {
                    m_memoryStream.SetLength(value);
                }

                public override void Write(byte[] buffer, int offset, int count)
                {
                    m_memoryStream.Write(buffer, offset, count);
                }

                protected override void Dispose(bool disposing)
                {
                    Content = m_memoryStream.ToArray();
                    m_memoryStream.Dispose();
                    base.Dispose(disposing);
                }
            }

            public IRequest Data { get; }
            public NameValueCollection Headers { get => Data.Headers; set => Data.Headers = value; }
            public string Method { get => Data.Method; set => Data.Method = value; }
            public string Url { get => Data.Url; set => Data.Url = value; }

            private RequestStream m_stream;

            public byte[] Content => m_stream.Content;

            public RequestWrapper(IRequest data)
            {
                Data = data;
                Headers = new NameValueCollection();
            }

            public Stream GetRequestStream()
            {
                m_stream = new RequestStream();

                return m_stream;
            }
        }

        class CookieVisitor : ICookieVisitor
        {
            public CookieContainer CookieContainer { get; }

            public ManualResetEvent ManualResetEvent { get; }

            public CookieVisitor()
            {
                CookieContainer = new CookieContainer();
                ManualResetEvent = new ManualResetEvent(false);
            }

            public void Dispose()
            {
                
            }

            public bool Visit(CefSharp.Cookie cookie, int count, int total, ref bool deleteCookie)
            {
                var myCookie = new System.Net.Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain);

                if (cookie.Expires != null)
                    myCookie.Expires = cookie.Expires.Value;

                myCookie.HttpOnly = cookie.HttpOnly;
                myCookie.Secure = cookie.Secure;

                CookieContainer.Add(myCookie);

                if (count == total - 1)
                    ManualResetEvent.Set();

                return true;
            }
        }

        public event EventHandler<OpenBrowserEventArgs> OpeningBrowser;

        public FrameworkElement Control => new WindowsFormsHost() { Child = m_webBrowser };
        public IManualResolveElement Parent { get; set; }

        public string UserAgent => m_webBrowser.UserAgent;

        private MyCefWebBrowser m_webBrowser;

        private readonly DownloadHandler m_myDownloadHandler;
        private readonly ManualResetEvent m_pageLoading;
        private readonly IApplication m_application;

        public WebBrowser(IApplication app, Dispatcher dispatcher)
        {
            m_myDownloadHandler = new DownloadHandler();
            m_application = app;

            ManualResetEvent resetEvent = new ManualResetEvent(false);
            m_pageLoading = new ManualResetEvent(false);

            dispatcher.Invoke(() =>
            {
                m_webBrowser = new MyCefWebBrowser(string.Empty)
                {
                    DownloadHandler = m_myDownloadHandler
                };
            });

            m_webBrowser.FrameLoadEnd += WebBrowser_FrameLoadEnd;
            m_webBrowser.IsBrowserInitializedChanged += (x, y) =>
            {
                resetEvent.Set();
            };

           

            resetEvent.WaitOne();
        }

        public void Navigate(Uri url)
        {
            m_webBrowser.Load(url.ToString());
            m_pageLoading.WaitOne();
            m_pageLoading.Reset();
            var cookieManager = Cef.GetGlobalCookieManager();

        }

        public CookieContainer GetAllCookies()
        {
            var cookieManager = Cef.GetGlobalCookieManager();
            var visitor = new CookieVisitor();
            cookieManager.VisitAllCookies(visitor);
            visitor.ManualResetEvent.WaitOne();
            return visitor.CookieContainer;
        }

        public CookieContainer GetAllCookies(Uri url)
        {
            var cookieManager = Cef.GetGlobalCookieManager();
            var visitor = new CookieVisitor();
            cookieManager.VisitUrlCookies(url.ToString(), true, visitor);

            visitor.ManualResetEvent.WaitOne();
            return visitor.CookieContainer;
        }

        public Plugin.IRequest CreateRequest()
        {
            IRequest request = m_webBrowser.GetMainFrame().CreateRequest();

            return new RequestWrapper(request);
        }

        public void LoadRequest(Plugin.IRequest request)
        {
            var req = (RequestWrapper)request;

            IRequest r = req.Data;

            if (r.Method == "POST")
            {
                r.InitializePostData();
                var element = r.PostData.CreatePostDataElement();
                element.Bytes = req.Content;
                r.PostData.AddElement(element);
            }

            m_webBrowser.GetMainFrame().LoadRequest(r);
            m_pageLoading.WaitOne();
            m_pageLoading.Reset();
        }

        public IHtmlNode GetElementById(string identifer)
        {
            var frame = m_webBrowser.GetBrowser().MainFrame;
            var t = frame.EvaluateScriptAsync($"(function() {{ return document.getElementById('{identifer}').outerHTML; }})();", null).Result;

            string EvaluateJavaScriptResult = t.Success ? (t.Result?.ToString() ?? string.Empty) : t.Message;

            if (!string.IsNullOrEmpty(EvaluateJavaScriptResult) && t.Success)
            {
                var node = HtmlAgilityPack.HtmlNode.CreateNode(EvaluateJavaScriptResult);

                return new HtmlNodeWrapper(node);
            }

            return null;
        }

        public void SendClickEvent(IHtmlNode node)
        {
            string xpath = CreateXPath(node);

            var frame = m_webBrowser.GetBrowser().MainFrame;
            frame.ExecuteJavaScriptAsync($"(function() {{ document.evaluate('{xpath}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click(); }})();", null);
        }

        public void SetValue(IHtmlNode node, string value)
        {
            string xpath = CreateXPath(node);
            var frame = m_webBrowser.GetBrowser().MainFrame;
            frame.ExecuteJavaScriptAsync($"(function() {{ document.evaluate('{xpath}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value= '{value}'; }})();", null);
        }

        public void RemoveElements(IEnumerable<IHtmlNode> visibleNodes, IEnumerable<IHtmlNode> hiddenNodes)
        {
            var frame = m_webBrowser.GetBrowser().MainFrame;
            var doc = ((HtmlDocumentWrapper)GetDocument()).Impl;

            if (visibleNodes != null)
            {
                foreach (var node in visibleNodes)
                {
                    string xpath = CreateXPath(node);

                    var elements = doc.DocumentNode.SelectNodes(xpath);

                    foreach (var element in elements)
                        element.AddClass("crydownloader-element-visible");
                }
            }

            if (hiddenNodes != null)
            {
                foreach (var node in hiddenNodes)
                {
                    string xpath = CreateXPath(node);

                    var elements = doc.DocumentNode.SelectNodes(xpath);

                    foreach (var element in elements)
                        element.AddClass("crydownloader-element-hide");

                    frame.ExecuteJavaScriptAsync($"(function() {{ var node = document.evaluate('{xpath}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; node.classList.add('crydownloader-element-hide'); }})();", null);
                }
            }

            var htmlNode = doc.DocumentNode.Element("html");

            foreach (var node in htmlNode.ChildNodes)
            {
                if ((HasScriptNode(node) && !HasMyClass(node)))
                {
                    node.AddClass("crydownloader-element-hide");

                    string xpath = CreateXPath(node);

                    frame.ExecuteJavaScriptAsync($"(function() {{ var node = document.evaluate('{xpath}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; node.classList.add('crydownloader-element-hide'); }})();", null);
                }
            }

            List<HtmlNode> removedNodes = FindElementsWithoutMyClass(htmlNode);

            foreach (var node in removedNodes)
            {
                var xpath = CreateXPath(node);
                node.Remove();

                string script = $"(function() {{ var node = document.evaluate('{xpath}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; node.parentNode.removeChild(node); }})();";

                frame.ExecuteJavaScriptAsync(script, null);
            }

            var stylesheets = doc.DocumentNode.SelectNodes("//head/link[@rel='stylesheet']");

            foreach (var node in stylesheets)
            {
                var xpath = CreateXPath(node);
                node.Remove();

                string script = $"(function() {{ var node = document.evaluate('{xpath}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; node.parentNode.removeChild(node); }})();";

                frame.ExecuteJavaScriptAsync(script, null);
            }


            frame.ExecuteJavaScriptAsync("(function() { var styleElement = document.createElement('style'); styleElement.setAttribute('type', 'text/css'); styleElement.innerHTML='.crydownloader-element-hide, .crydownloader-element-hide *, .crydownloader-element-hide::before, .crydownloader-element-hide::after{visibility: hidden !important;}'; document.head.appendChild(styleElement); })();", null);
        }

        public string GetElementValue(string id)
        {
            var frame = m_webBrowser.GetBrowser().MainFrame;
            var t = frame.EvaluateScriptAsync($"(function() {{ return document.getElementById('{id}').value; }})();", null).Result;

            string EvaluateJavaScriptResult = t.Success ? t.Result?.ToString() ?? string.Empty : string.Empty;
            return EvaluateJavaScriptResult;
        }

        public void HideScrollbars()
        {
            var frame = m_webBrowser.GetBrowser().MainFrame;
            frame.ExecuteJavaScriptAsync("document.body.style.overflow = 'hidden'", null);
        }

        public void FitToContent()
        {

        }

        public void SetBackground(SolidColorBrush brush)
        {
            var c = brush.Color;
            var colorStr = "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
            var frame = m_webBrowser.GetBrowser().MainFrame;
            frame.ExecuteJavaScriptAsync($"(function() {{ var styleElement = document.createElement('style'); styleElement.setAttribute('type', 'text/css'); styleElement.innerHTML='body {{ background-color: {colorStr}; }}'; document.head.appendChild(styleElement); }})();", null);
        }

        public void LoadHTML(string html, string url)
        {
            m_webBrowser.LoadHtml(html, url);
            m_pageLoading.WaitOne();
            m_pageLoading.Reset();
        }

        public Uri GetDownloadUrl()
        {
            return m_myDownloadHandler.DownloadUrl;
        }

        public IHtmlDocument GetDocument()
        {
            var frame = m_webBrowser.GetBrowser().MainFrame;
            var t = frame.EvaluateScriptAsync("(function() { return document.documentElement.outerHTML; })();", null).Result;

            string EvaluateJavaScriptResult = t.Success ? (t.Result?.ToString() ?? string.Empty) : t.Message;

            if (!string.IsNullOrEmpty(EvaluateJavaScriptResult) && t.Success)
            {
                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(EvaluateJavaScriptResult);
                return new HtmlDocumentWrapper(doc);
            }

            return null;
        }

        private bool HasElementNodes(HtmlNode node)
        {
            foreach (var n in node.ChildNodes)
            {
                if (n.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                    return true;
            }

            return false;
        }

        private int GetElementNodeCount(HtmlNode node)
        {
            int result = 0;

            foreach (var n in node.ChildNodes)
            {
                if (n.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                    ++result;
            }

            return result;
        }

        private bool HasMyClass(HtmlNode mainNode)
        {
            foreach (var node in mainNode.ChildNodes)
            {
                bool isElementVisible = node.HasClass("crydownloader-element-visible");
                bool isElementHidden = node.HasClass("crydownloader-element-hide");

                if (isElementHidden || isElementVisible)
                    return true;

                if (HasMyClass(node))
                    return true;
            }

            return false;
        }

        private bool HasScriptNode(HtmlNode mainNode)
        {
            foreach (var node in mainNode.ChildNodes)
            {
                if (node.Name == "script")
                    return true;

                if (HasScriptNode(node))
                    return true;
            }

            return false;
        }

        private List<HtmlNode> FindElementsWithoutMyClass(HtmlNode mainNode)
        {
            List<HtmlNode> result = new List<HtmlNode>();

            foreach (var node in mainNode.ChildNodes)
            {
                bool isElementVisible = node.HasClass("crydownloader-element-visible");
                bool isElementHidden = node.HasClass("crydownloader-element-hide");

                if (!isElementHidden)
                {
                    if (!isElementVisible)
                    {
                        if (HasElementNodes(node))
                        {
                            var lst = FindElementsWithoutMyClass(node);

                            if (lst.Count > 0)
                            {
                                if (GetElementNodeCount(node) == lst.Count)
                                    result.Add(node);
                                else
                                    result.AddRange(lst);
                            }
                            else
                                result.AddRange(lst);
                        }
                        else if (node.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                            result.Add(node);
                    }
                }
            }

            return result;
        }

        private IHtmlNode GetFirstElement(IHtmlNode node)
        {
            IHtmlNode result = node;

            while (result.ParentNode != null && result.ParentNode.NodeType == Plugin.HtmlNodeType.Element)
                result = result.ParentNode;

            return result;
        }

        private HtmlNode GetFirstElement(HtmlNode node)
        {
            HtmlNode result = node;

            while (result.ParentNode != null && result.NodeType == HtmlAgilityPack.HtmlNodeType.Element)
                result = result.ParentNode;

            return result;
        }

        private string CreateXPath(IHtmlNode node)
        {
            if (!string.IsNullOrEmpty(node.Id))
                return $"//*[@id=\"{node.Id}\"]";

            string xpath = node.XPath;
            IHtmlNode firstElement = GetFirstElement(node);

            if (!string.IsNullOrEmpty(firstElement.Id))
            {
                int startPos = xpath.IndexOf(firstElement.Name);
                int endPos = xpath.IndexOf('/', startPos);

                if (endPos < 0)
                    return $"//*[@id=\"{firstElement.Id}\"]";

                return $"//*[@id=\"{firstElement.Id}\"]" + xpath.Remove(0, endPos);
            }
            else
            {
                // TODO: find a way without id
            }

            return xpath;
        }

        private string CreateXPath(HtmlNode node)
        {
            if (!string.IsNullOrEmpty(node.Id))
                return $"//*[@id=\"{node.Id}\"]";

            string xpath = node.XPath;
            HtmlNode firstElement = GetFirstElement(node);

            if (!string.IsNullOrEmpty(firstElement.Id))
            {
                int startPos = xpath.IndexOf(firstElement.Name);
                int endPos = xpath.IndexOf('/', startPos);

                if (endPos < 0)
                    return $"//*[@id=\"{firstElement.Id}\"]";

                return $"//*[@id=\"{firstElement.Id}\"]" + xpath.Remove(0, endPos);
            }
            else
            {
                // TODO: find a way without id
            }

            return xpath;
        }

        private void WebBrowser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            m_pageLoading.Set();
        }

        public void Show(Visibility visibility)
        {
            var args = new OpenBrowserEventArgs(null, this, visibility);

            OpeningBrowser?.Invoke(this, args);

            if (args.Result != null && Parent == null)
            {
                Parent = args.Result;
                args.Result.Show(args.Visibility);
            }
        }

        public void Dispose()
        {
            m_webBrowser.Dispose();
        }

        public Task<object> EvaluateJavaScriptAsync(string strScript)
        {
            return m_webBrowser.EvaluateScriptAsync(strScript).ContinueWith((x) => x.Result.Result);
        }

        public void ExecuteJavaScriptAsync(string strScript)
        {
            m_webBrowser.ExecuteScriptAsync(strScript);
        }

        public void ExecuteJavaScriptAsync(string strMethodName, params object[] args)
        {
            m_webBrowser.ExecuteScriptAsync(strMethodName, args);
        }
    }
}
