﻿using CryDownloader.Plugin;
using Jint.Native;
using Jint.Native.Boolean;
using Jint.Native.String;
using System;

namespace CryDownloader.Logic
{
    class JavaScriptValueWrapper : IJavaScriptValue
    {
        public JsValue Handle { get; }

        public JavaScriptValueWrapper(JsValue handle)
        {
            Handle = handle;
        }

        public override string ToString()
        {
            return Handle.ToString();
        }

        public bool IsPrimitive()
        {
            return Handle.IsPrimitive();
        }

        public bool IsUndefined()
        {
            return Handle.IsUndefined();
        }

        public bool IsArray()
        {
            return Handle.IsArray();
        }

        public bool IsDate()
        {
            return Handle.IsDate();
        }

        public bool IsRegExp()
        {
            return Handle.IsRegExp();
        }

        public bool IsObject()
        {
            return Handle.IsObject();
        }

        public bool IsString()
        {
            return Handle.IsString();
        }

        public bool IsNumber()
        {
            return Handle.IsNumber();
        }

        public bool IsBoolean()
        {
            return Handle.IsBoolean();
        }

        public bool IsNull()
        {
            return Handle.IsNull();
        }

        public object AsObject()
        {
            return (bool)Handle.AsObject().ToObject();
        }

        public object AsArray()
        {
            return Handle.AsArray().ToObject();
        }

        public object AsDate()
        {
            return Handle.AsDate().ToObject();
        }

        public object AsRegExp()
        {
            return Handle.AsRegExp().ToObject();
        }

        public T TryCast<T>(Action<IJavaScriptValue> fail = null) where T : class
        {
            return Handle.TryCast<T>(value => fail(new JavaScriptValueWrapper(value)));
        }

        public bool AsBoolean()
        {
            return (bool)Handle.As<BooleanInstance>().PrimitiveValue.ToObject();
        }

        public string AsString()
        {
            return Handle.ToString();
        }

        public double AsNumber()
        {
            return (double)Handle.As<Jint.Native.Number.NumberInstance>().NumberData.ToObject();
        }

        public object ToObject()
        {
            return Handle.ToObject();
        }

        public IJavaScriptValue Invoke(params IJavaScriptValue[] arguments)
        {
            throw new NotImplementedException();
        }

        public IJavaScriptValue Invoke(IJavaScriptValue thisObj, IJavaScriptValue[] arguments)
        {
            throw new NotImplementedException();
        }

        public bool Equals(IJavaScriptValue other)
        {
            throw new NotImplementedException();
        }
    }
}
