﻿
namespace CryDownloader.Logic
{
    interface IJavaScriptWrapperHandle<T>
    {
        T Handle { get; }
    }
}
