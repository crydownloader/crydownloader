﻿using CryDownloader.Plugin;
using Esprima.Ast;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryDownloader.Logic
{
    class JavaScriptWrapper : IJavaScript
    {
        public Esprima.Ast.Script Script { get; }

        public JavaScriptWrapper(Esprima.Ast.Script script)
        { 
            Script = script;
            script.Body.ToList();
        }

        public IList<IJavaScriptStatement> GetStatements()
        {
            return Script.Body.Select((x) => (IJavaScriptStatement)new JavaScriptStatementWrapper(x)).ToList();
        }
    }

    class JavaScriptPositionWrapper : IJavaScriptPosition
    {
        public Esprima.Position Position { get; }

        public int Line => Position.Line;

        public int Column => Position.Column;

        public JavaScriptPositionWrapper(Esprima.Position position)
        {
            Position = position;
        }
    }

    class JavaScriptLocationWrapper : IJavaScriptLocation
    {
        public IJavaScriptPosition Start
        {
            get
            {
                if (m_startPos == null)
                    m_startPos = new JavaScriptPositionWrapper(Location.Start);

                return m_startPos;
            }
        }

        public IJavaScriptPosition End
        {
            get
            {
                if (m_endPos == null)
                    m_endPos = new JavaScriptPositionWrapper(Location.End);

                return m_endPos;
            }
        }

        public string Source => Location.Source ?? string.Empty;

        public Esprima.Location Location { get; }

        private IJavaScriptPosition m_endPos;
        private IJavaScriptPosition m_startPos;

        public JavaScriptLocationWrapper(Esprima.Location location)
        {
            Location = location;
        }
    }

    class JavaScriptStatementWrapper : IJavaScriptStatement
    {
        public JavaScriptSyntaxNodes Type => (JavaScriptSyntaxNodes)Statement.Type;

        public IJavaScriptLocation Location
        {
            get
            {
                if (m_location == null)
                    m_location = new JavaScriptLocationWrapper(Statement.Location);

                return m_location;
            }
        }

        private IJavaScriptLocation m_location;

        public Esprima.Ast.Statement Statement { get; }

        public JavaScriptStatementWrapper(Statement statement)
        {
            Statement = statement;
        }
    }

    class JavaScriptProgramWrapper : IJavaScriptProgram
    {
        private Jint.Engine m_engine;

        public JavaScriptProgramWrapper()
        {
            m_engine = new Jint.Engine();
        }

        public IJavaScript CreateScript()
        {
            return null; //new ScriptWrapper(new Esprima.Ast.Script());
        }

        public IJavaScript LoadScript(string strScript)
        {
            var s = new Esprima.JavaScriptParser(strScript, new Esprima.ParserOptions() {  AdaptRegexp = true, Tolerant = true }).ParseScript();

            return new JavaScriptWrapper(s);
        }

        public IJavaScriptValue EvaluateScript(IJavaScript script)
        {
            if (script is JavaScriptWrapper s)
                return new JavaScriptValueWrapper(m_engine.Evaluate(s.Script));

            return null;
        }

        public void SetValue(string strName, IJavaScriptValue value)
        {
            if (value is JavaScriptValueWrapper valueWrapper)
                m_engine.SetValue(strName, valueWrapper.Handle);
        }

        public IJavaScriptValue GetValue(string strName)
        {
            return new JavaScriptValueWrapper(m_engine.GetValue(strName));
        }

        public IJavaScriptValue Invoke(string propertyName, object thisObject, params object[] arguments)
        {
            return new JavaScriptValueWrapper(m_engine.Invoke(propertyName, thisObject, arguments));
        }

        public IJavaScriptValue Invoke(string propertyName, params object[] arguments)
        {
            return new JavaScriptValueWrapper(m_engine.Invoke(propertyName, arguments));
        }

        public IJavaScriptValue Invoke(IJavaScriptValue value, params object[] arguments)
        {
            return new JavaScriptValueWrapper(m_engine.Invoke(((JavaScriptValueWrapper)value).Handle, arguments));
        }

        public IJavaScriptValue Invoke(IJavaScriptValue value, object thisObject, params object[] arguments)
        {
            return new JavaScriptValueWrapper(m_engine.Invoke(((JavaScriptValueWrapper)value).Handle, thisObject, arguments));
        }

        public IJavaScript LoadScript(IList<IJavaScriptStatement> statements)
        {
            Esprima.Ast.NodeList<Statement> nodeList;

            var ctor = typeof(Esprima.Ast.NodeList<Statement>).GetConstructor(
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null,
                new Type[] { typeof(ICollection<Statement>) }, null);

            var enumerator = statements.Where((x) => x is JavaScriptStatementWrapper).Select((x) =>
            {
                if (x is JavaScriptStatementWrapper s)
                    return s.Statement;

                return null;
            });

            nodeList = (Esprima.Ast.NodeList<Statement>)ctor.Invoke(new object[] { enumerator.ToList() });

            return new JavaScriptWrapper(new Script(nodeList, false));
        }
    }
}
