﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CryDownloader.Logic
{
    static class RsdfFormat
    {
        public static readonly byte[] Key = { 0x8C, 0x35, 0x19, 0x2D, 0x96, 0x4D, 0xC3, 0x18, 0x2C, 0x6F, 0x84, 0xF3, 0x25, 0x22, 0x39, 0xEB, 0x4A, 0x32, 0x0D, 0x25, 0x00, 0x00, 0x00, 0x00 };
        public static readonly byte[] IV = { 0xA3, 0xD5, 0xA3, 0x3C, 0xB9, 0x5A, 0xC1, 0xF5, 0xCB, 0xDB, 0x1A, 0xD2, 0x5C, 0xB0, 0xA7, 0xAA };

        public static SymmetricAlgorithm Algorithm = new RijndaelManaged
        {
            Padding = PaddingMode.None,
            FeedbackSize = 8,
            Mode = CipherMode.CFB,
            BlockSize = 128,
            KeySize = 128,
            IV = IV,
            Key = Key,
        };
    }

    class RsdfContainer : DownloadContainer
    {
        public RsdfContainer(string filename, IEntryCreator creator)
        {
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                ReadContainer(fs, creator);
            }
        }

        public RsdfContainer(Stream stream, IEntryCreator creator)
        {
            ReadContainer(stream, creator);
        }

        public override void SaveToStream(Stream stream)
        {
            string strContainer = GetContainerString(this);
            byte[] buffer = Encoding.UTF8.GetBytes(strContainer);
            stream.Write(buffer, 0, buffer.Length);
        }

        private void ReadContainer(Stream stream, IEntryCreator creator)
        {
            byte[] data = new byte[stream.Length];

            stream.Read(data, 0, data.Length);

            string dataStr = Encoding.UTF8.GetString(data);

            string[] lines = dataStr.Contains('�') ? dataStr.Split('�') : dataStr.Split('\n');
            List<Uri> urls = new List<Uri>();

            using (var dec = RsdfFormat.Algorithm.CreateDecryptor())
            {
                for (int i = 0; i < lines.Length; ++i)
                {
                    var line = lines[i].Trim();
                    if (string.IsNullOrWhiteSpace(line))
                        continue;

                    var linkData = Convert.FromBase64String(line);

                    var outputBuffer = new byte[linkData.Length];
                    dec.TransformBlock(linkData, 0, linkData.Length, outputBuffer, 0);

                    var link = Encoding.UTF8.GetString(outputBuffer);

                    const string ccfPrefix = "CCF:";

                    if (link.StartsWith(ccfPrefix))
                        link = link.Substring(ccfPrefix.Length).Trim();

                    urls.Add(new Uri(link, UriKind.Absolute));
                }
            }

          //  creator.CreateEntries(urls, this);
        }

        private static string GetContainerString(RsdfContainer container)
        {
            using (var enc = RsdfFormat.Algorithm.CreateEncryptor(RsdfFormat.Key, RsdfFormat.IV))
            {
                var lines = container.Select(entry => entry.Url.ToString()).ToArray();

                var sb = new StringBuilder();
                var outputBuffer = new byte[4096];

                for (int i = 0; i < lines.Length; ++i)
                {
                    var currentLink = lines[i];

                    if (string.IsNullOrEmpty(currentLink))
                        continue;

                    var linkBytes = Encoding.UTF8.GetBytes(currentLink);
                    var written = enc.TransformBlock(linkBytes, 0, linkBytes.Length, outputBuffer, 0);
                    var sizedOutput = new byte[written];
                    Buffer.BlockCopy(outputBuffer, 0, sizedOutput, 0, written);

                    var b64 = Convert.ToBase64String(sizedOutput);

                    sb.Append(b64).Append("\r\n");
                }

                return BitConverter.ToString(Encoding.UTF8.GetBytes(sb.ToString())).Replace("-", "");
            }
        }
    }
}
