﻿using System.ComponentModel;

namespace CryDownloader.Logic
{
    public interface IDownloadContainerView : INotifyPropertyChanged, Plugin.IContainer
    {
        void Load();
    }
}
