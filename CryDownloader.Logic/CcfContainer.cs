﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CryDownloader.Logic
{
    static class CcfFormat
    {
        public static readonly ReadOnlyCollection<byte[]> Keys = new ReadOnlyCollection<byte[]>(new[]
        {
            new byte[] {0x5F, 0x67, 0x9C, 0x00, 0x54, 0x87, 0x37, 0xE1, 0x20, 0xE6, 0x51, 0x8A, 0x98, 0x1B, 0xD0, 0xBA, 0x11, 0xAF, 0x5C, 0x71, 0x9E, 0x97, 0x50, 0x29, 0x83, 0xAD, 0x6A, 0xA3, 0x8E, 0xD7, 0x21, 0xC3},
            new byte[] {0x17, 0x1B, 0xF8, 0xE3, 0x4C, 0x3D, 0x0C, 0x0C, 0x26, 0x93, 0xFD, 0xD2, 0xB0, 0x80, 0x42, 0x3A, 0x5B, 0x98, 0xF4, 0xD0, 0x28, 0xA0, 0xAF, 0x4D, 0x82, 0xA3, 0x85, 0xD8, 0x37, 0xA8, 0xF9, 0x5F},
            new byte[] {0x02, 0x69, 0x00, 0xE9, 0x77, 0xC6, 0x40, 0x24, 0x42, 0xB6, 0x61, 0x32, 0x9C, 0xFE, 0x62, 0xD6, 0xED, 0x21, 0xBD, 0xEB, 0x0C, 0xD6, 0x32, 0x13, 0x18, 0xA8, 0xED, 0xC7, 0xBC, 0x5A, 0x6C, 0x86}
        });

        public static readonly ReadOnlyCollection<byte[]> IVs = new ReadOnlyCollection<byte[]>(new[]
        {
            new byte[] {0xE3, 0xD1, 0x53, 0xAD, 0x60, 0x9E, 0xF7, 0x35, 0x8D, 0x66, 0x68, 0x41, 0x80, 0xC7, 0x33, 0x1A},
            new byte[] {0x9F, 0xE9, 0x5F, 0xFF, 0x7C, 0xA4, 0xFC, 0x0F, 0xCE, 0xF2, 0x5E, 0x4F, 0x74, 0x44, 0xAE, 0x67},
            new byte[] {0x8C, 0xE1, 0x17, 0x3E, 0xBA, 0xD7, 0x6E, 0x08, 0x58, 0x4B, 0x94, 0x57, 0x39, 0x26, 0x23, 0x1E}
        });

        public static readonly ReadOnlyCollection<Version> Versions = new ReadOnlyCollection<Version>(new[]
        {
            new Version(1, 0),
            new Version(0, 8),
            new Version(0, 7)
        });

        public static SymmetricAlgorithm CreateSymmetricAlgorithm() => new RijndaelManaged
        {
            Mode = CipherMode.CBC,
            Padding = PaddingMode.Zeros
        };

        public static XmlWriterSettings WriterSettings = new XmlWriterSettings
        {
            Encoding = new UTF8Encoding(false),
            Indent = false,
            OmitXmlDeclaration = true
        };

    }

    class CcfContainer : DownloadContainer
    {
        [XmlRoot(ElementName = "Options", Namespace = "")]
        [DataContract(Name = "Options", Namespace = "")]
        class CcfOptions
        {
            [XmlElement(ElementName = "Kommentar", Namespace = "")]
            [DataMember]
            public string Kommentar { get; set; }
            [XmlElement(ElementName = "Passwort", Namespace = "")]
            [DataMember]
            public string Passwort { get; set; }
        }

        [XmlRoot(ElementName = "Download", Namespace = "")]
        [DataContract(Name = "Download", Namespace = "")]
        class CcfDownload
        {
            [XmlElement(ElementName = "FileSize", Namespace = "")]
            [DataMember]
            public string FileSize { get; set; }

            [XmlElement(ElementName = "Url", Namespace = "")]
            [DataMember]
            public string Url { get; set; }

            [XmlAttribute(AttributeName = "Url", Namespace = "")]
            public string UrlAttribute { get; set; }

            [XmlElement(ElementName = "FileName", Namespace = "")]
            [DataMember]
            public string FileName { get; set; }

            public static CcfDownload FromDownloadEntry(DownloadEntry entry)
            {
                var result = new CcfDownload();
                result.FileName = entry.FileName;
                result.FileSize = entry.FileSize.ToString();
                result.Url = entry.Url.ToString();

                return result;
            }
        }

        [XmlRoot(ElementName = "Package", Namespace = "")]
        [DataContract(Name = "Package", Namespace = "")]
        class CcfPackageItem
        {
            [XmlElement(ElementName = "Options", Namespace = "")]
            public CcfOptions Options { get; set; }

            [XmlElement(ElementName = "Download", Namespace = "")]
            public List<CcfDownload> Downloads { get; set; }

            [XmlAttribute(AttributeName = "service", Namespace = "")]
            public string Service { get; set; }

            [XmlAttribute(AttributeName = "name", Namespace = "")]
            public string Name { get; set; }

            [XmlAttribute(AttributeName = "url", Namespace = "")]
            public string Url { get; set; }
        }

        [XmlRoot(ElementName = "CryptLoad", Namespace = "")]
        [DataContract(Name = "CryptLoad", Namespace = "")]
        class CryptLoadContainer
        {
            [XmlElement(ElementName = "Package", Namespace = "")]
            [DataMember]
            public List<CcfPackageItem> Packages { get; set; }

            public static CryptLoadContainer FromCcfContainer(CcfContainer container, string password)
            {
                CcfPackageItem item = new CcfPackageItem
                {
                    Options = new CcfOptions
                    {
                        Kommentar = "CCF Container created by CryDownloader",
                        Passwort = password
                    },
                    Service = "CryDownloader",
                    Url = "https://cryfact.com",
                    Downloads = new List<CcfDownload>()
                };

                foreach (var package in container)
                    item.Downloads.Add(CcfDownload.FromDownloadEntry(package));

                return new CryptLoadContainer
                {
                    Packages = new List<CcfPackageItem> { item }
                };
            }
        }

        public string Password { get; set; }

        public CcfContainer(string filename, IEntryCreator creator)
        {
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                ReadContainer(fs, creator);
            }
        }

        public CcfContainer(Stream stream, IEntryCreator creator)
        {
            ReadContainer(stream, creator);
        }

        private void ReadContainer(Stream stream, IEntryCreator creator)
        {
            byte[] data = new byte[stream.Length];

            stream.Read(data, 0, data.Length);

            string xml = DecryptXml(data, out var ccfVersion);

            if (string.IsNullOrEmpty(xml))
                throw new NotSupportedException("The CCF version is not supported");

            CryptLoadContainer content = SerializeFromXml(xml);

            if (string.IsNullOrEmpty(xml))
                throw new NotSupportedException("The CCF version is not supported");

            var ps = content.Packages;
            List<Uri> urls = new List<Uri>();

            for (int i = 0; i < ps.Count; ++i)
            {
                var currentPackage = ps[i];

                if (currentPackage != null)
                {
                    foreach (var entry in currentPackage.Downloads)
                        urls.Add(new Uri(entry.Url, UriKind.Absolute));
                }
            }

         //   creator.CreateEntries(urls, this);
        }

        public override void SaveToStream(Stream stream)
        {
            const int version10KeyIndex = 0;

            var dataBytes = SerializeToXml(CryptLoadContainer.FromCcfContainer(this, Password));

            using (var rij = CcfFormat.CreateSymmetricAlgorithm())
            {
                rij.IV = CcfFormat.IVs[version10KeyIndex];
                rij.Key = CcfFormat.Keys[version10KeyIndex];

                using (var enc = rij.CreateEncryptor())
                {
                    var outputResized = enc.TransformFinalBlock(dataBytes, 0, dataBytes.Length);
                    stream.Write(outputResized, 0, outputResized.Length);
                }
            }
        }

        private static bool IsValidXmlContainer(string data)
        {
            return data.IndexOf("<CryptLoad", StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        private static string DecryptXml(byte[] data, out Version usedVersion)
        {
            usedVersion = null;

            using (var rij = CcfFormat.CreateSymmetricAlgorithm())
            {
                var output = new byte[4096];

                for (int i = 0; i < CcfFormat.Keys.Count; ++i)
                {
                    rij.IV = CcfFormat.IVs[i];
                    rij.Key = CcfFormat.Keys[i];

                    using (var dec = rij.CreateDecryptor())
                    {
                        var written = dec.TransformBlock(data, 0, data.Length, output, 0);
                        var outputResized = new byte[written];
                        Buffer.BlockCopy(output, 0, outputResized, 0, written);

                        var xmlData = Encoding.UTF8.GetString(outputResized);

                        if (xmlData[xmlData.Length - 1] == '\0')
                            xmlData = xmlData.Remove(xmlData.Length - 1);

                        if (IsValidXmlContainer(xmlData))
                        {
                            usedVersion = CcfFormat.Versions[i];
                            return xmlData;
                        }

                    }
                }
            }

            return string.Empty;
        }

        private static CryptLoadContainer SerializeFromXml(string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CryptLoadContainer));

            xml = xml.Trim();

            using (var reader = new StringReader(xml))
            {
                using (var xmlReader = XmlReader.Create(reader))
                {
                    return serializer.Deserialize(xmlReader) as CryptLoadContainer;
                }
            }
        }

        private static byte[] SerializeToXml(CryptLoadContainer container)
        {
            using (var ms = new MemoryStream())
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                using (var writer = XmlWriter.Create(ms, CcfFormat.WriterSettings))
                {
                    var serializer = new XmlSerializer(typeof(CryptLoadContainer));
                    serializer.Serialize(writer, container, namespaces);

                    return ms.ToArray();
                }
            }
        }
    }
}
