﻿using CryDownloader.Plugin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace CryDownloader.Logic
{
    class CustomJsonConverter : JsonConverter
    {
        public IJsonConverter OriginalConverter { get; }

        public CustomJsonConverter(IJsonConverter origConverter)
        {
            OriginalConverter = origConverter;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            OriginalConverter.WriteJson(new JsonWriterWrapper(writer), value, new JsonSerializerWrapper(serializer));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return OriginalConverter.ReadJson(new JsonReaderWrapper(reader), objectType, existingValue, new JsonSerializerWrapper(serializer));
        }

        public override bool CanConvert(Type objectType)
        {
            return OriginalConverter.CanConvert(objectType);
        }
    }
}
