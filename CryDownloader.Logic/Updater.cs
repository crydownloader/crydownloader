﻿using Renci.SshNet;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    public class Updater
    {
        internal const string SftpHost = "update.cryfact.com";
        internal const string SftpUser = "updateCryDownloader";
        internal const string SftpPassword = "KugWQ36Zv0OIfCiKDp6C";

        public UpdateChannel UpdateChannel { get; }

        public Updater()
        { }

        public Task<UpdatePackage> FindUpdateAsync()
        {
            return Task.Run(() =>
            {
                Assembly thisAssembly = null;
                try
                {
                    thisAssembly = Assembly.GetEntryAssembly();
                }
                finally
                {
                    if (thisAssembly is null)
                        thisAssembly = Assembly.GetExecutingAssembly();
                }

                string FullAssemblyName = thisAssembly.Location;
                var versionInfo = FileVersionInfo.GetVersionInfo(FullAssemblyName);
                Version fileVersion = new Version(versionInfo.FileVersion);

                using (var sftp = new SftpClient(SftpHost, SftpUser, SftpPassword))
                {
                    try
                    {
                        sftp.Connect();
                    }
                    catch (Exception)
                    {
                        return new NotAvailUpdatePackage();
                    }

                    string remoteFilePath = $"{UpdateChannel.ToString()}/index";

                    using (var memoryStream = new MemoryStream())
                    {
                        sftp.DownloadFile(remoteFilePath, memoryStream);

                        memoryStream.Seek(0, SeekOrigin.Begin);

                        if (!memoryStream.Read(out ulong indexCount))
                            return null;

                        for (ulong i = 0; i < indexCount; ++i)
                        {
                            if (!memoryStream.Read(out int major))
                                return null;

                            if (!memoryStream.Read(out int minor))
                                return null;

                            if (!memoryStream.Read(out int build))
                                return null;

                            if (!memoryStream.Read(out int rev))
                                return null;

                            Version ver = new Version(major, minor, build, rev);

                            if (ver > fileVersion)
                            {
                                remoteFilePath = $"{UpdateChannel.ToString()}/{major}.{minor}.{build}.{rev}/changelog.txt";

                                if (!sftp.Exists(remoteFilePath))
                                    return new UpdatePackage(ver, string.Empty, UpdateChannel);

                                string changelog;

                                using (MemoryStream changelogStream = new MemoryStream())
                                {
                                    sftp.DownloadFile(remoteFilePath, changelogStream);

                                    changelogStream.Seek(0, SeekOrigin.Begin);

                                    using (var reader = new StreamReader(changelogStream))
                                    {
                                        changelog = reader.ReadToEnd();
                                    }
                                }

                                return new UpdatePackage(ver, changelog, UpdateChannel);
                            }
                        }
                    }
                }

                return new ActualUpdatePackage(fileVersion);
            });
        }
    }
}
