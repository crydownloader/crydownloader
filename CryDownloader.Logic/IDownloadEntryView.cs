﻿using CryDownloader.Plugin;
using System.ComponentModel;
using System.Net;

namespace CryDownloader.Logic
{
    public interface IDownloadEntryView : INotifyPropertyChanged
    {
        event DownloadProgressChangedEventHandler DownloadProgressChanged;

        DownloadEntry Data { get; }
        string Name { get; set; }
        string Filename { get; set; }

        int Priority { get; set; }
    }
}
