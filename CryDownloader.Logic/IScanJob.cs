﻿using System;

namespace CryDownloader.Logic
{
    public enum ScanJobState
    {
        None,
        Scan,
        Wait,
        Loading,
    }

    public interface IScanJobBase
    {
        void Scan(JobTaskManager taskManager);
        void Abort();
    }

    public interface IChildScanJob : IScanJobBase
    {
        IScanJob Parent { get; }
    }

    public interface IScanJob : IScanJobBase
    {
        ScanJobState State { get; set; }
    }
}
