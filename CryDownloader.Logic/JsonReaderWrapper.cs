﻿using CryDownloader.Plugin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    class JsonReaderWrapper : IJsonReader
    {
        public JsonReader OriginalReader { get; }

        public Plugin.JsonToken TokenType => (Plugin.JsonToken)OriginalReader.TokenType;

        public object Value => OriginalReader.Value;

        public Type ValueType => OriginalReader.ValueType;

        public int Depth { get => OriginalReader.Depth; }
        public string Path { get => OriginalReader.Path; }
        public CultureInfo Culture { get => OriginalReader.Culture; set => OriginalReader.Culture = value; }

        public JsonReaderWrapper(JsonReader reader)
        {
            OriginalReader = reader;
        }

        public Task<bool> ReadAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsync(cancellationToken);
        }

        public Task SkipAsync(CancellationToken cancellationToken = default)
        {
             return OriginalReader.ReadAsync(cancellationToken);
        }

        public Task<bool> ReadAsBooleanAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsBooleanAsync(cancellationToken).ContinueWith((x) => x.Result.Value);
        }

        public Task<byte[]> ReadAsBytesAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsBytesAsync(cancellationToken);
        }

        public Task<DateTime> ReadAsDateTimeAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsDateTimeAsync(cancellationToken).ContinueWith((x) => x.Result.Value);
        }

        public Task<DateTimeOffset> ReadAsDateTimeOffsetAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsDateTimeOffsetAsync(cancellationToken).ContinueWith((x) => x.Result.Value);
        }

        public Task<decimal> ReadAsDecimalAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsDecimalAsync(cancellationToken).ContinueWith((x) => x.Result.Value);
        }

        public Task<double> ReadAsDoubleAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsDoubleAsync(cancellationToken).ContinueWith((x) => x.Result.Value);
        }

        public Task<int> ReadAsInt32Async(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsInt32Async(cancellationToken).ContinueWith((x) => x.Result.Value);
        }

        public Task<string> ReadAsStringAsync(CancellationToken cancellationToken = default)
        {
            return OriginalReader.ReadAsStringAsync(cancellationToken);
        }

        public int ReadAsInt32()
        {
            return OriginalReader.ReadAsInt32().Value;
        }

        public string ReadAsString()
        {
            return OriginalReader.ReadAsString();
        }

        public byte[] ReadAsBytes()
        {
            return OriginalReader.ReadAsBytes();
        }

        public double ReadAsDouble()
        {
            return OriginalReader.ReadAsDouble().Value;
        }

        public bool ReadAsBoolean()
        {
            return OriginalReader.ReadAsBoolean().Value;
        }

        public decimal ReadAsDecimal()
        {
            return OriginalReader.ReadAsDecimal().Value;
        }

        public DateTime ReadAsDateTime()
        {
            return OriginalReader.ReadAsDateTime().Value;
        }

        public bool Read()
        {
            return OriginalReader.Read();
        }

        public void Skip()
        {
            OriginalReader.Skip();
        }
    }
}
