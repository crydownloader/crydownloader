﻿
using System;

namespace CryDownloader.Logic
{
    public static class FileSize
    {
        static readonly string[] BinarySizeTable = new string[] { "Bytes", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB" };
        static readonly string[] BinarySizeNameTable = new string[] { "Bytes", "kibibyte", "mebibyte", "gibibyte", "tebibyte", "pebibyte", "exbibyte", "zebibyte", "yobibyte" };
        static readonly string[] DecimalSizeTable = new string[] { "Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        static readonly string[] DecimalSizeNameTable = new string[] { "Bytes", "kilobyte", "megabyte", "gigabyte", "terabyte", "petabyte", "exabyte", "zettabyte", "yottabyte" };

        public static string ConvertSizeStringToFullSizeString(string sizeString, FileSizeFormatType fileSizeFormatType)
        {
            string[] currentFullSizeTable;
            string[] currentSizeTable;

            switch (fileSizeFormatType)
            {
                case FileSizeFormatType.Decimal:
                    currentFullSizeTable = DecimalSizeNameTable;
                    currentSizeTable = DecimalSizeTable;
                    break;
                case FileSizeFormatType.Binary:
                default:
                    currentFullSizeTable = BinarySizeNameTable;
                    currentSizeTable = BinarySizeTable;
                    break;
            }

            for (int i = 0; i < currentSizeTable.Length; ++i)
            {
                if (sizeString.Contains(currentSizeTable[i]))
                    sizeString = sizeString.Replace(currentSizeTable[i], currentFullSizeTable[i]);
            }

            return sizeString;
        }

        public static string GetSizeString(ulong size, FileSizeFormatType fileSizeFormatType, bool fullName = false)
        {
            int currIndex = 0;
            string[] currentTable;
            ulong currentDivisor;

            switch (fileSizeFormatType)
            {
                case Logic.FileSizeFormatType.Decimal:
                    currentDivisor = 1000;
                    currentTable = fullName ? DecimalSizeNameTable : DecimalSizeTable;
                    break;
                case Logic.FileSizeFormatType.Binary:
                default:
                    currentDivisor = 1024;
                    currentTable = fullName ? BinarySizeNameTable : BinarySizeTable;
                    break;
            }

            double dSize = (double)size;
            GetSizeString(currentTable.Length, currentDivisor, ref currIndex, ref dSize);

            return $"{Math.Round(dSize, 2, MidpointRounding.AwayFromZero):N2} {currentTable[currIndex]}";
        }

        private static void GetSizeString(int tableSize, ulong divisor, ref int currIndex, ref double size)
        {
            if (size >= divisor)
            {
                if (currIndex >= tableSize)
                    return;

                size /= divisor;
                ++currIndex;

                GetSizeString(tableSize, divisor, ref currIndex, ref size);
            }
        }
    }
}
