﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace CryDownloader.Logic
{
    public class ClipboardManager
    {
        // See http://msdn.microsoft.com/en-us/library/ms649021%28v=vs.85%29.aspx
        public const int WM_CLIPBOARDUPDATE = 0x031D;
        public static IntPtr HWND_MESSAGE = new IntPtr(-3);

        // See http://msdn.microsoft.com/en-us/library/ms632599%28VS.85%29.aspx#message_only
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool AddClipboardFormatListener(IntPtr hwnd);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool RemoveClipboardFormatListener(IntPtr hwnd);

        public event EventHandler ClipboardChanged;

        public bool IsActive
        {
            get => m_bIsActive;
            set
            {
                GetHwndSourceAndHandle(out var source, out var handle);

                if (value)
                {
                    source.AddHook(WndProc);
                    AddClipboardFormatListener(handle);
                }
                else if (m_bIsActive)
                {
                    source.RemoveHook(WndProc);
                    RemoveClipboardFormatListener(handle);
                }

                m_bIsActive = value;
            }
        }

        private bool m_bIsActive;
        private Window m_window;

        public ClipboardManager(Window windowSource)
        {
            if (!(PresentationSource.FromVisual(windowSource) is HwndSource source))
            {
                throw new ArgumentException(
                    "Window source MUST be initialized first, such as in the Window's OnSourceInitialized handler.",
                    nameof(windowSource));
            }

            m_window = windowSource;
        }

        protected virtual void OnClipboardChanged()
        {
            ClipboardChanged?.Invoke(this, EventArgs.Empty);
        }

        private void GetHwndSourceAndHandle(out HwndSource source, out IntPtr handle)
        {
            if (!(PresentationSource.FromVisual(m_window) is HwndSource s))
            {
                throw new ArgumentException(
                    "Window source MUST be initialized first, such as in the Window's OnSourceInitialized handler.",
                    nameof(m_window));
            }

            source = s;
            handle = new WindowInteropHelper(m_window).Handle;
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case WM_CLIPBOARDUPDATE:
                    OnClipboardChanged();
                    handled = true;
                    break;
                default:
                    break;
            }

            return IntPtr.Zero;
        }
    }
}
