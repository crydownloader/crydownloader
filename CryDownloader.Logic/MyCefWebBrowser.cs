﻿using CefSharp;
using CefSharp.Internals;
using CefSharp.WinForms;
using CefSharp.WinForms.Internals;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;

namespace CryDownloader.Logic
{
    internal static class IntPtrExtensions
    {
        //
        // Zusammenfassung:
        //     /// Do an unchecked conversion from IntPtr to int /// so overflow exceptions
        //     don't get thrown. ///
        //
        // Parameter:
        //   intPtr:
        //     the IntPtr to cast
        //
        // Rückgabewerte:
        //     a 32-bit signed integer
        public static int CastToInt32(this IntPtr intPtr)
        {
            return (int)intPtr.ToInt64();
        }
    }

    internal static class InternalWebBrowserExtensions
    {
        internal static void SetHandlersToNullExceptLifeSpan(this IWebBrowserInternal browser)
        {
            browser.DialogHandler = null;
            browser.RequestHandler = null;
            browser.DisplayHandler = null;
            browser.LoadHandler = null;
            browser.KeyboardHandler = null;
            browser.JsDialogHandler = null;
            browser.DragHandler = null;
            browser.DownloadHandler = null;
            browser.MenuHandler = null;
            browser.FocusHandler = null;
            browser.ResourceHandlerFactory = null;
            browser.RenderProcessMessageHandler = null;
        }
    }

    [Docking(DockingBehavior.AutoDock)]
    [DefaultEvent("LoadingStateChanged")]
    [ToolboxBitmap(typeof(MyCefWebBrowser))]
    [Description("CefSharp ChromiumWebBrowser - Chromium Embedded Framework .Net wrapper. https://github.com/cefsharp/CefSharp")]
    [Designer(typeof(ChromiumWebBrowserDesigner))]
    internal class MyCefWebBrowser : Control, IWebBrowserInternal, IWebBrowser, IDisposable, IWinFormsWebBrowser
    {
        internal class ParentFormMessageInterceptor : NativeWindow, IDisposable
        {
            //
            // Zusammenfassung:
            //     /// Keep track of whether a move is in progress. ///
            private bool isMoving;

            //
            // Zusammenfassung:
            //     /// Used to determine the coordinates involved in the move ///
            private Rectangle movingRectangle;

            //
            // Zusammenfassung:
            //     /// Gets or sets the browser. ///
            private MyCefWebBrowser Browser
            {
                get;
                set;
            }

            //
            // Zusammenfassung:
            //     /// Gets or sets the parent form. ///
            private Form ParentForm
            {
                get;
                set;
            }

            //
            // Zusammenfassung:
            //     /// Initializes a new instance of the CefSharp.WinForms.Internals.ParentFormMessageInterceptor
            //     class. ///
            //
            // Parameter:
            //   browser:
            //     The browser.
            public ParentFormMessageInterceptor(MyCefWebBrowser browser)
            {
                Browser = browser;
                Browser.ParentChanged += ParentParentChanged;
                RefindParentForm();
            }

            //
            // Zusammenfassung:
            //     /// Call to force refinding of the parent Form. /// (i.e. top level window that
            //     owns the ChromiumWebBrowserControl) ///
            public void RefindParentForm()
            {
                ParentParentChanged(Browser, null);
            }

            //
            // Zusammenfassung:
            //     /// Adjust the form to listen to if the ChromiumWebBrowserControl's parent changes.
            //     ///
            //
            // Parameter:
            //   sender:
            //     The ChromiumWebBrowser whose parent has changed.
            //
            //   e:
            //     The System.EventArgs instance containing the event data.
            private void ParentParentChanged(object sender, EventArgs e)
            {
                Control control = (Control)sender;
                Form parentForm = ParentForm;
                Form form = control.FindForm();
                if (parentForm == null || form == null || parentForm.Handle != form.Handle)
                {
                    if (base.Handle != IntPtr.Zero)
                    {
                        ReleaseHandle();
                    }
                    if (parentForm != null)
                    {
                        parentForm.HandleCreated -= OnHandleCreated;
                        parentForm.HandleDestroyed -= OnHandleDestroyed;
                    }
                    ParentForm = form;
                    if (form != null)
                    {
                        form.HandleCreated += OnHandleCreated;
                        form.HandleDestroyed += OnHandleDestroyed;
                        if (form.IsHandleCreated)
                        {
                            OnHandleCreated(form, null);
                        }
                    }
                }
            }

            //
            // Zusammenfassung:
            //     /// Handles the HandleCreated event. ///
            //
            // Parameter:
            //   sender:
            //     The sender.
            //
            //   e:
            //     The System.EventArgs instance containing the event data.
            private void OnHandleCreated(object sender, EventArgs e)
            {
                AssignHandle(((Form)sender).Handle);
            }

            //
            // Zusammenfassung:
            //     /// Handles the HandleDestroyed event. ///
            //
            // Parameter:
            //   sender:
            //     The sender.
            //
            //   e:
            //     The System.EventArgs instance containing the event data.
            private void OnHandleDestroyed(object sender, EventArgs e)
            {
                ReleaseHandle();
            }

            //
            // Zusammenfassung:
            //     /// Invokes the default window procedure associated with this window. ///
            //
            // Parameter:
            //   m:
            //     A System.Windows.Forms.Message that is associated with the current Windows message.
            protected override void WndProc(ref Message m)
            {
                bool flag = false;
                int num = -1;
                int num2 = -1;
                switch (m.Msg)
                {
                    case 6:
                        {
                            MyCefWebBrowser browser = Browser;
                            if ((int)m.WParam == 0)
                            {
                                browser.IsActivating = false;
                                DefWndProc(ref m);
                            }
                            else
                            {
                                browser.IsActivating = browser.IsActiveControl();
                                DefWndProc(ref m);
                            }
                            return;
                        }
                    case 534:
                        movingRectangle = (Rectangle)Marshal.PtrToStructure(m.LParam, typeof(Rectangle));
                        num = movingRectangle.Left;
                        num2 = movingRectangle.Top;
                        flag = true;
                        break;
                    case 3:
                        {
                            int num3 = IntPtrExtensions.CastToInt32(m.LParam);
                            num = (num3 & 0xFFFF);
                            num2 = ((num3 >> 16) & 0xFFFF);
                            flag = true;
                            break;
                        }
                }
                if (flag && Browser.IsHandleCreated && ParentForm.WindowState == FormWindowState.Normal && (ParentForm.Left != num || ParentForm.Top != num2) && !isMoving && ParentForm.Left >= 0 && ParentForm.Right >= 0)
                {
                    OnMoving();
                }
                DefWndProc(ref m);
            }

            //
            // Zusammenfassung:
            //     /// Called when [moving]. ///
            protected virtual void OnMoving()
            {
                isMoving = true;
                if (Browser.IsBrowserInitialized)
                {
                    Browser.GetBrowser().GetHost().NotifyMoveOrResizeStarted();
                }
                isMoving = false;
            }

            //
            // Zusammenfassung:
            //     /// Performs application-defined tasks associated with freeing, releasing, or
            //     resetting unmanaged resources. ///
            public void Dispose()
            {
                Dispose(disposing: true);
            }

            //
            // Zusammenfassung:
            //     /// Releases unmanaged and - optionally - managed resources. ///
            //
            // Parameter:
            //   disposing:
            //     true to release both managed and unmanaged resources; false to release only unmanaged
            //     resources.
            protected virtual void Dispose(bool disposing)
            {
                if (disposing)
                {
                    if (ParentForm != null)
                    {
                        ParentForm.HandleCreated -= OnHandleCreated;
                        ParentForm.HandleDestroyed -= OnHandleDestroyed;
                        ParentForm = null;
                    }
                    if (base.Handle != IntPtr.Zero)
                    {
                        ReleaseHandle();
                    }
                    if (Browser != null)
                    {
                        Browser.ParentChanged -= ParentParentChanged;
                        Browser = null;
                    }
                }
            }

            //
            // Zusammenfassung:
            //     /// When overridden in a derived class, manages an unhandled thread exception.
            //     ///
            //
            // Parameter:
            //   e:
            //     An System.Exception that specifies the unhandled thread exception.
            protected override void OnThreadException(Exception e)
            {
                base.OnThreadException(e);
            }
        }

        internal class DefaultFocusHandler : IFocusHandler
        {
            /// <summary>
            /// Called when the browser component has received focus.
            /// </summary>
            /// <param name="chromiumWebBrowser">the ChromiumWebBrowser control</param>
            /// <param name="browser">the browser object</param>
            /// <remarks>Try to avoid needing to override this logic in a subclass. The implementation in
            /// DefaultFocusHandler relies on very detailed behavior of how WinForms and
            /// Windows interact during window activation.</remarks>
            public virtual void OnGotFocus(IWebBrowser chromiumWebBrowser, IBrowser browser)
            {
                //We don't deal with popups as they're rendered by default entirely by CEF
                //For print dialogs the browser will be null, we don't want to deal with that either.
                if (browser == null || browser.IsPopup)
                {
                    return;
                }

                var winFormsChromiumWebBrowser = (MyCefWebBrowser)chromiumWebBrowser;
                // During application activation, CEF receives a WM_SETFOCUS
                // message from Windows because it is the top window 
                // on the CEF UI thread.
                //
                // If the WinForm ChromiumWebBrowser control is the 
                // current .ActiveControl before app activation 
                // then we MUST NOT try to reactivate the WinForm
                // control during activation because that will 
                // start a race condition between reactivating
                // the CEF control AND having another control 
                // that should be the new .ActiveControl.
                //
                // For example:
                // * CEF control has focus, and thus ChromiumWebBrowser
                //   is the current .ActiveControl
                // * Alt-Tab to another application
                // * Click a non CEF control in the WinForms application.
                // * This begins the Windows activation process.
                // * The WM_ACTIVATE process on the WinForm UI thread
                //   will update .ActiveControl to the clicked control.
                //   The clicked control will receive WM_SETFOCUS as well. 
                //   (i.e. OnGotFocus)
                //   If the ChromiumWebBrowser was the previous .ActiveControl,
                //   then we set .Activating = true.
                // * The WM_ACTIVATE process on the CEF thread will
                //   send WM_SETFOCUS to CEF thus staring the race of
                //   which will end first, the WndProc WM_ACTIVATE process
                //   on the WinForm UI thread or the WM_ACTIVATE process
                //   on the CEF UI thread.
                // * CEF will then call this method on the CEF UI thread
                //   due to WM_SETFOCUS.
                // * This method will clear the activation state (if any)
                //   on the ChromiumWebBrowser control, due to the race
                //   condition the WinForm UI thread cannot.
                if (winFormsChromiumWebBrowser.IsActivating)
                {
                    winFormsChromiumWebBrowser.IsActivating = false;
                }
                else
                {
                    // Otherwise, we're not being activated
                    // so we must activate the ChromiumWebBrowser control
                    // for WinForms focus tracking.
                    winFormsChromiumWebBrowser.InvokeOnUiThreadIfRequired(() =>
                    {
                        winFormsChromiumWebBrowser.Activate();
                    });
                }
            }

            /// <summary>
            /// Called when the browser component is requesting focus.
            /// </summary>
            /// <param name="chromiumWebBrowser">the ChromiumWebBrowser control</param>
            /// <param name="browser">the browser object</param>
            /// <param name="source">Indicates where the focus request is originating from.</param>
            /// <returns>Return false to allow the focus to be set or true to cancel setting the focus.</returns>
            public virtual bool OnSetFocus(IWebBrowser chromiumWebBrowser, IBrowser browser, CefFocusSource source)
            {
                //We don't deal with popups as they're rendered by default entirely by CEF
                if (browser.IsPopup)
                {
                    return false;
                }
                // Do not let the browser take focus when a Load method has been called
                return source == CefFocusSource.FocusSourceNavigation;
            }

            /// <summary>
            /// Called when the browser component is about to lose focus.
            /// For instance, if focus was on the last HTML element and the user pressed the TAB key.
            /// </summary>
            /// <param name="chromiumWebBrowser">the ChromiumWebBrowser control</param>
            /// <param name="browser">the browser object</param>
            /// <param name="next">Will be true if the browser is giving focus to the next component
            /// and false if the browser is giving focus to the previous component.</param>
            public virtual void OnTakeFocus(IWebBrowser chromiumWebBrowser, IBrowser browser, bool next)
            {
                //We don't deal with popups as they're rendered by default entirely by CEF
                if (browser.IsPopup)
                {
                    return;
                }

                var winFormsChromiumWebBrowser = (MyCefWebBrowser)chromiumWebBrowser;

                // NOTE: OnTakeFocus means leaving focus / not taking focus
                winFormsChromiumWebBrowser.InvokeOnUiThreadIfRequired(() => winFormsChromiumWebBrowser.SelectNextControl(next));
            }
        }

        //
        // Zusammenfassung:
        //     /// The managed cef browser adapter ///
        private ManagedCefBrowserAdapter managedCefBrowserAdapter;

        //
        // Zusammenfassung:
        //     /// The parent form message interceptor ///
        private ParentFormMessageInterceptor parentFormMessageInterceptor;

        //
        // Zusammenfassung:
        //     /// The browser ///
        private IBrowser browser;

        //
        // Zusammenfassung:
        //     /// A flag that indicates whether or not the designer is active /// NOTE: DesignMode
        //     becomes false by the time we get to the destructor/dispose so it gets stored
        //     here ///
        private bool designMode;

        //
        // Zusammenfassung:
        //     /// A flag that indicates whether or not CefSharp.WinForms.ChromiumWebBrowser.InitializeFieldsAndCefIfRequired
        //     has been called. ///
        private bool initialized;

        //
        // Zusammenfassung:
        //     /// Has the underlying Cef Browser been created (slightly different to initliazed
        //     in that /// the browser is initialized in an async fashion) ///
        private bool browserCreated;

        //
        // Zusammenfassung:
        //     /// Browser initialization settings ///
        private IBrowserSettings browserSettings;

        //
        // Zusammenfassung:
        //     /// The request context (we deliberately use a private variable so we can throw
        //     an exception if /// user attempts to set after browser created) ///
        private IRequestContext requestContext;

        //
        // Zusammenfassung:
        //     /// The value for disposal, if it's 1 (one) then this instance is either disposed
        //     /// or in the process of getting disposed ///
        private int disposeSignaled;

        //
        // Zusammenfassung:
        //     /// Gets a value indicating whether this instance is disposed. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public new bool IsDisposed
        {
            get
            {
                return Interlocked.CompareExchange(ref disposeSignaled, 1, 1) == 1;
            }
        }

        //
        // Zusammenfassung:
        //     /// Set to true while handing an activating WM_ACTIVATE message. /// MUST ONLY
        //     be cleared by DefaultFocusHandler. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public bool IsActivating
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Gets or sets the browser settings. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IBrowserSettings BrowserSettings
        {
            get
            {
                return browserSettings;
            }
            set
            {
                if (browserCreated)
                {
                    throw new Exception("Browser has already been created. BrowserSettings must be set before the underlying CEF browser is created.");
                }
                if (value != null && value.GetType() != typeof(BrowserSettings))
                {
                    throw new Exception($"BrowserSettings can only be of type {typeof(BrowserSettings)} or null");
                }
                browserSettings = value;
            }
        }

        //
        // Zusammenfassung:
        //     /// Gets or sets the request context. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IRequestContext RequestContext
        {
            get
            {
                return requestContext;
            }
            set
            {
                if (browserCreated)
                {
                    throw new Exception("Browser has already been created. RequestContext must be set before the underlying CEF browser is created.");
                }
                if (value != null && value.GetType() != typeof(RequestContext))
                {
                    throw new Exception($"RequestContxt can only be of type {typeof(RequestContext)} or null");
                }
                requestContext = value;
            }
        }

        //
        // Zusammenfassung:
        //     /// A flag that indicates whether the control is currently loading one or more
        //     web pages (true) or not (false). ///
        //
        // Hinweise:
        //     In the WPF control, this property is implemented as a Dependency Property and
        //     fully supports data /// binding.
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public bool IsLoading
        {
            get;
            private set;
        }

        //
        // Zusammenfassung:
        //     /// The text that will be displayed as a ToolTip ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public string TooltipText
        {
            get;
            private set;
        }

        //
        // Zusammenfassung:
        //     /// The address (URL) which the browser control is currently displaying. ///
        //     Will automatically be updated as the user navigates to another page (e.g. by
        //     clicking on a link). ///
        //
        // Hinweise:
        //     In the WPF control, this property is implemented as a Dependency Property and
        //     fully supports data /// binding.
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public string Address
        {
            get;
            private set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IDialogHandler and assign to handle dialog events. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IDialogHandler DialogHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IJsDialogHandler and assign to handle events related to
        //     JavaScript Dialogs. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IJsDialogHandler JsDialogHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IKeyboardHandler and assign to handle events related to
        //     key press. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IKeyboardHandler KeyboardHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IRequestHandler and assign to handle events related to
        //     browser requests. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IRequestHandler RequestHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IDownloadHandler and assign to handle events related to
        //     downloading files. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IDownloadHandler DownloadHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.ILoadHandler and assign to handle events related to browser
        //     load status. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public ILoadHandler LoadHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.ILifeSpanHandler and assign to handle events related to
        //     popups. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public ILifeSpanHandler LifeSpanHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IDisplayHandler and assign to handle events related to
        //     browser display state. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IDisplayHandler DisplayHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IContextMenuHandler and assign to handle events related
        //     to the browser context menu ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IContextMenuHandler MenuHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IRenderProcessMessageHandler and assign to handle messages
        //     from the render process. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IRenderProcessMessageHandler RenderProcessMessageHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IFindHandler to handle events related to find results.
        //     ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IFindHandler FindHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IAudioHandler to handle audio events. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IAudioHandler AudioHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// The CefSharp.IFocusHandler for this ChromiumWebBrowser. ///
        //
        // Hinweise:
        //     If you need customized focus handling behavior for WinForms, the suggested ///
        //     best practice would be to inherit from DefaultFocusHandler and try to avoid ///
        //     needing to override the logic in OnGotFocus. The implementation in /// DefaultFocusHandler
        //     relies on very detailed behavior of how WinForms and /// Windows interact during
        //     window activation.
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IFocusHandler FocusHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IDragHandler and assign to handle events related to dragging.
        //     ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IDragHandler DragHandler
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Implement CefSharp.IResourceHandlerFactory and control the loading of resources
        //     ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public IResourceHandlerFactory ResourceHandlerFactory
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// A flag that indicates whether the state of the control currently supports
        //     the GoForward action (true) or not (false). ///
        //
        // Hinweise:
        //     In the WPF control, this property is implemented as a Dependency Property and
        //     fully supports data /// binding.
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public bool CanGoForward
        {
            get;
            private set;
        }

        //
        // Zusammenfassung:
        //     /// A flag that indicates whether the state of the control current supports the
        //     GoBack action (true) or not (false). ///
        //
        // Hinweise:
        //     In the WPF control, this property is implemented as a Dependency Property and
        //     fully supports data /// binding.
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public bool CanGoBack
        {
            get;
            private set;
        }

        //
        // Zusammenfassung:
        //     /// A flag that indicates whether the WebBrowser is initialized (true) or not
        //     (false). ///
        //
        // Hinweise:
        //     In the WPF control, this property is implemented as a Dependency Property and
        //     fully supports data /// binding.
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public bool IsBrowserInitialized
        {
            get;
            private set;
        }

        //
        // Zusammenfassung:
        //     /// A flag that indicates if you can execute javascript in the main frame. ///
        //     Flag is set to true in IRenderProcessMessageHandler.OnContextCreated. /// and
        //     false in IRenderProcessMessageHandler.OnContextReleased ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public bool CanExecuteJavascriptInMainFrame
        {
            get;
            private set;
        }

        //
        // Zusammenfassung:
        //     /// ParentFormMessageInterceptor hooks the Form handle and forwards /// the move/active
        //     messages to the browser, the default is true /// and should only be required
        //     when using CefSettings.MultiThreadedMessageLoop /// set to true. ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(true)]
        public bool UseParentFormMessageInterceptor
        {
            get;
            set;
        } = true;


        //
        // Zusammenfassung:
        //     /// The javascript object repository, one repository per ChromiumWebBrowser instance.
        //     ///
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public IJavascriptObjectRepository JavascriptObjectRepository
        {
            get
            {
                InitializeFieldsAndCefIfRequired();
                if (managedCefBrowserAdapter != null)
                {
                    return managedCefBrowserAdapter.JavascriptObjectRepository;
                }
                return null;
            }
        }

        //
        // Zusammenfassung:
        //     /// Gets the browser adapter. ///
        IBrowserAdapter IWebBrowserInternal.BrowserAdapter
        {
            get
            {
                return managedCefBrowserAdapter;
            }
        }

        //
        // Zusammenfassung:
        //     /// Gets or sets a value indicating whether this instance has parent. ///
        bool IWebBrowserInternal.HasParent
        {
            get;
            set;
        }

        //
        // Zusammenfassung:
        //     /// Manually implement Focused because cef does not implement it. ///
        //
        // Hinweise:
        //     This is also how the Microsoft's WebBrowserControl implements the Focused property.
        public override bool Focused
        {
            get
            {
                if (base.Focused)
                {
                    return true;
                }
                if (!base.IsHandleCreated)
                {
                    return false;
                }
                return NativeMethodWrapper.IsFocused(base.Handle);
            }
        }

        public string UserAgent { get; private set; }

        //
        // Zusammenfassung:
        //     /// Event handler that will get called when the resource load for a navigation
        //     fails or is canceled. /// It's important to note this event is fired on a CEF
        //     UI thread, which by default is not the same as your application UI /// thread.
        //     It is unwise to block on this thread for any length of time as your browser will
        //     become unresponsive and/or hang.. /// To access UI elements you'll need to Invoke/Dispatch
        //     onto the UI Thread. ///
        public event EventHandler<LoadErrorEventArgs> LoadError;

        //
        // Zusammenfassung:
        //     /// Event handler that will get called when the browser begins loading a frame.
        //     Multiple frames may be loading at the same /// time. Sub-frames may start or
        //     continue loading after the main frame load has ended. This method may not be
        //     called for a /// particular frame if the load request for that frame fails. For
        //     notification of overall browser load status use /// OnLoadingStateChange instead.
        //     /// It's important to note this event is fired on a CEF UI thread, which by default
        //     is not the same as your application UI /// thread. It is unwise to block on this
        //     thread for any length of time as your browser will become unresponsive and/or
        //     hang.. /// To access UI elements you'll need to Invoke/Dispatch onto the UI Thread.
        //     ///
        //
        // Hinweise:
        //     Whilst this may seem like a logical place to execute js, it's called before the
        //     DOM has been loaded, implement /// CefSharp.IRenderProcessMessageHandler.OnContextCreated(CefSharp.IWebBrowser,CefSharp.IBrowser,CefSharp.IFrame)
        //     as it's called when the underlying V8Context is created ///
        public event EventHandler<FrameLoadStartEventArgs> FrameLoadStart;

        //
        // Zusammenfassung:
        //     /// Event handler that will get called when the browser is done loading a frame.
        //     Multiple frames may be loading at the same /// time. Sub-frames may start or
        //     continue loading after the main frame load has ended. This method will always
        //     be called /// for all frames irrespective of whether the request completes successfully.
        //     /// It's important to note this event is fired on a CEF UI thread, which by default
        //     is not the same as your application UI /// thread. It is unwise to block on this
        //     thread for any length of time as your browser will become unresponsive and/or
        //     hang.. /// To access UI elements you'll need to Invoke/Dispatch onto the UI Thread.
        //     ///
        public event EventHandler<FrameLoadEndEventArgs> FrameLoadEnd;

        //
        // Zusammenfassung:
        //     /// Event handler that will get called when the Loading state has changed. ///
        //     This event will be fired twice. Once when loading is initiated either programmatically
        //     or /// by user action, and once when loading is terminated due to completion,
        //     cancellation of failure. /// It's important to note this event is fired on a
        //     CEF UI thread, which by default is not the same as your application UI /// thread.
        //     It is unwise to block on this thread for any length of time as your browser will
        //     become unresponsive and/or hang.. /// To access UI elements you'll need to Invoke/Dispatch
        //     onto the UI Thread. ///
        public event EventHandler<LoadingStateChangedEventArgs> LoadingStateChanged;

        //
        // Zusammenfassung:
        //     /// Event handler for receiving Javascript console messages being sent from web
        //     pages. /// It's important to note this event is fired on a CEF UI thread, which
        //     by default is not the same as your application UI /// thread. It is unwise to
        //     block on this thread for any length of time as your browser will become unresponsive
        //     and/or hang.. /// To access UI elements you'll need to Invoke/Dispatch onto the
        //     UI Thread. /// (The exception to this is when your running with settings.MultiThreadedMessageLoop
        //     = false, then they'll be the same thread). ///
        public event EventHandler<ConsoleMessageEventArgs> ConsoleMessage;

        //
        // Zusammenfassung:
        //     /// Event handler for changes to the status message. /// It's important to note
        //     this event is fired on a CEF UI thread, which by default is not the same as your
        //     application UI /// thread. It is unwise to block on this thread for any length
        //     of time as your browser will become unresponsive and/or hang. /// To access UI
        //     elements you'll need to Invoke/Dispatch onto the UI Thread. /// (The exception
        //     to this is when your running with settings.MultiThreadedMessageLoop = false,
        //     then they'll be the same thread). ///
        public event EventHandler<StatusMessageEventArgs> StatusMessage;

        //
        // Zusammenfassung:
        //     /// Occurs when the browser address changed. /// It's important to note this
        //     event is fired on a CEF UI thread, which by default is not the same as your application
        //     UI /// thread. It is unwise to block on this thread for any length of time as
        //     your browser will become unresponsive and/or hang.. /// To access UI elements
        //     you'll need to Invoke/Dispatch onto the UI Thread. ///
        public event EventHandler<AddressChangedEventArgs> AddressChanged;

        //
        // Zusammenfassung:
        //     /// Occurs when the browser title changed. /// It's important to note this event
        //     is fired on a CEF UI thread, which by default is not the same as your application
        //     UI /// thread. It is unwise to block on this thread for any length of time as
        //     your browser will become unresponsive and/or hang.. /// To access UI elements
        //     you'll need to Invoke/Dispatch onto the UI Thread. ///
        public event EventHandler<TitleChangedEventArgs> TitleChanged;

        //
        // Zusammenfassung:
        //     /// Event called after the underlying CEF browser instance has been created.
        //     /// It's important to note this event is fired on a CEF UI thread, which by default
        //     is not the same as your application UI /// thread. It is unwise to block on this
        //     thread for any length of time as your browser will become unresponsive and/or
        //     hang.. /// To access UI elements you'll need to Invoke/Dispatch onto the UI Thread.
        //     ///
        public event EventHandler<IsBrowserInitializedChangedEventArgs> IsBrowserInitializedChanged;

        //
        // Zusammenfassung:
        //     /// Initializes static members of the CefSharp.WinForms.ChromiumWebBrowser class.
        //     ///
        static MyCefWebBrowser()
        {
            if (CefSharpSettings.ShutdownOnExit)
            {
                System.Windows.Forms.Application.ApplicationExit += OnApplicationExit;
            }
        }

        //
        // Zusammenfassung:
        //     /// Handles the ApplicationExit event. ///
        //
        // Parameter:
        //   sender:
        //     The sender.
        //
        //   e:
        //     The System.EventArgs instance containing the event data.
        private static void OnApplicationExit(object sender, EventArgs e)
        {
            Cef.Shutdown();
        }

        //
        // Zusammenfassung:
        //     /// This constructor exists as the WinForms designer requires a parameterless
        //     constructor, if you are instantiating /// an instance of this class in code then
        //     use the CefSharp.WinForms.ChromiumWebBrowser.#ctor(System.String,CefSharp.IRequestContext)
        //     /// constructor overload instead. Using this constructor in code is unsupported
        //     and you may experience System.NullReferenceException's /// when attempting to
        //     access some of the properties immediately after instantiation. ///
        [Obsolete("Should only be used by the WinForms Designer. Use the ChromiumWebBrowser(string, IRequestContext) constructor overload instead.")]
        public MyCefWebBrowser()
        {
        }

        //
        // Zusammenfassung:
        //     /// Initializes a new instance of the CefSharp.WinForms.ChromiumWebBrowser class.
        //     ///
        //
        // Parameter:
        //   address:
        //     The address.
        //
        //   requestContext:
        //     Request context that will be used for this browser instance, /// if null the
        //     Global Request Context will be used
        public MyCefWebBrowser(string address, IRequestContext requestContext = null)
        {
            Dock = DockStyle.Top;
            Address = address;
            RequestContext = requestContext;
            InitializeFieldsAndCefIfRequired();
        }

        //
        // Zusammenfassung:
        //     /// Required for designer support - this method cannot be inlined as the designer
        //     /// will attempt to load libcef.dll and will subsiquently throw an exception.
        //     /// TODO: Still not happy with this method name, need something better ///
        [MethodImpl(MethodImplOptions.NoInlining)]
        private void InitializeFieldsAndCefIfRequired()
        {
            if (!initialized)
            {
                var setttings = new CefSettings();

                setttings.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36";
                UserAgent = setttings.UserAgent;

                if (!Cef.IsInitialized && !Cef.Initialize(setttings))
                {
                    throw new InvalidOperationException("Cef::Initialize() failed");
                }
                Cef.AddDisposable(this);
                if (FocusHandler == null)
                {
                    FocusHandler = new DefaultFocusHandler();
                }
                if (ResourceHandlerFactory == null)
                {
                    ResourceHandlerFactory = new DefaultResourceHandlerFactory();
                }
                if (browserSettings == null)
                {
                    browserSettings = new BrowserSettings();
                }
                managedCefBrowserAdapter = new ManagedCefBrowserAdapter(this, offScreenRendering: true);
                initialized = true;
                CreateBrowser();
            }
        }

        //
        // Zusammenfassung:
        //     /// If not in design mode; Releases unmanaged and - optionally - managed resources
        //     for the CefSharp.WinForms.ChromiumWebBrowser ///
        //
        // Parameter:
        //   disposing:
        //     true to release both managed and unmanaged resources; false to release only unmanaged
        //     resources.
        protected override void Dispose(bool disposing)
        {
           System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                if (Interlocked.CompareExchange(ref disposeSignaled, 1, 0) == 0)
                {
                    if (!designMode)
                    {
                        InternalDispose(disposing);
                    }
                    base.Dispose(disposing);
                }
            });
        }

        //
        // Zusammenfassung:
        //     /// Releases unmanaged and - optionally - managed resources for the CefSharp.WinForms.ChromiumWebBrowser
        //     ///
        //
        // Parameter:
        //   disposing:
        //     true to release both managed and unmanaged resources; false to release only unmanaged
        //     resources.
        //
        // Hinweise:
        //     /// This method cannot be inlined as the designer will attempt to load libcef.dll
        //     and will subsiquently throw an exception. ///
        [MethodImpl(MethodImplOptions.NoInlining)]
        private void InternalDispose(bool disposing)
        {
            if (disposing)
            {
                IsBrowserInitialized = false;
                this.AddressChanged = null;
                this.ConsoleMessage = null;
                this.FrameLoadEnd = null;
                this.FrameLoadStart = null;
                this.IsBrowserInitializedChanged = null;
                this.LoadError = null;
                this.LoadingStateChanged = null;
                this.StatusMessage = null;
                this.TitleChanged = null;
                InternalWebBrowserExtensions.SetHandlersToNullExceptLifeSpan((IWebBrowserInternal)this);
                browser = null;
                if (parentFormMessageInterceptor != null)
                {
                    parentFormMessageInterceptor.Dispose();
                    parentFormMessageInterceptor = null;
                }
                if (managedCefBrowserAdapter != null)
                {
                    managedCefBrowserAdapter.Dispose();
                    managedCefBrowserAdapter = null;
                }
                LifeSpanHandler = null;
            }
            Cef.RemoveDisposable(this);
        }

        //
        // Zusammenfassung:
        //     /// Loads the specified URL. ///
        //
        // Parameter:
        //   url:
        //     The URL to be loaded.
        public void Load(string url)
        {
            if (IsBrowserInitialized)
            {
                using (IFrame frame = this.GetMainFrame())
                {
                    frame.LoadUrl(url);
                }
            }
            else
            {
                Address = url;
            }
        }

        //
        // Zusammenfassung:
        //     /// Raises the System.Windows.Forms.Control.HandleCreated event. ///
        //
        // Parameter:
        //   e:
        //     An System.EventArgs that contains the event data.
        protected override void OnHandleCreated(EventArgs e)
        {
            designMode = base.DesignMode;
            if (!designMode)
            {
                InitializeFieldsAndCefIfRequired();
                //CreateBrowser();
                ResizeBrowser();
            }
            base.OnHandleCreated(e);
        }

        protected virtual IWindowInfo CreateBrowserWindowInfo(IntPtr handle)
        {
            WindowInfo windowInfo = new WindowInfo();
            windowInfo.SetAsChild(handle);
            windowInfo.ExStyle |= 134217728u;
            return windowInfo;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private void CreateBrowser()
        {
            browserCreated = true;
            if (!((IWebBrowserInternal)this).HasParent)
            {
                if (!IsBrowserInitialized || browser == null)
                {
                    IWindowInfo windowInfo = CreateBrowserWindowInfo(base.Handle);
                    managedCefBrowserAdapter.CreateBrowser(windowInfo, browserSettings as BrowserSettings, requestContext as RequestContext, Address);
                    browserSettings = null;
                }
                else
                {
                    IntPtr windowHandle = browser.GetHost().GetWindowHandle();
                    NativeMethodWrapper.SetWindowParent(windowHandle, base.Handle);
                }
            }
        }

        //
        // Zusammenfassung:
        //     /// Called after browser created. ///
        //
        // Parameter:
        //   browser:
        //     The browser.
        void IWebBrowserInternal.OnAfterBrowserCreated(IBrowser browser)
        {
            this.browser = browser;
            IsBrowserInitialized = true;
            if (UseParentFormMessageInterceptor)
            {
                this.InvokeOnUiThreadIfRequired(delegate
                {
                    parentFormMessageInterceptor = new ParentFormMessageInterceptor(this);
                });
            }
            ResizeBrowser();
            this.IsBrowserInitializedChanged?.Invoke(this, new IsBrowserInitializedChangedEventArgs(IsBrowserInitialized));
        }

        //
        // Zusammenfassung:
        //     /// Sets the address. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.AddressChangedEventArgs instance containing the event data.
        void IWebBrowserInternal.SetAddress(AddressChangedEventArgs args)
        {
            Address = args.Address;
            this.AddressChanged?.Invoke(this, args);
        }

        //
        // Zusammenfassung:
        //     /// Sets the loading state change. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.LoadingStateChangedEventArgs instance containing the event data.
        void IWebBrowserInternal.SetLoadingStateChange(LoadingStateChangedEventArgs args)
        {
            CanGoBack = args.CanGoBack;
            CanGoForward = args.CanGoForward;
            IsLoading = args.IsLoading;
            this.LoadingStateChanged?.Invoke(this, args);
        }

        //
        // Zusammenfassung:
        //     /// Sets the title. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.TitleChangedEventArgs instance containing the event data.
        void IWebBrowserInternal.SetTitle(TitleChangedEventArgs args)
        {
            this.TitleChanged?.Invoke(this, args);
        }

        //
        // Zusammenfassung:
        //     /// Sets the tooltip text. ///
        //
        // Parameter:
        //   tooltipText:
        //     The tooltip text.
        void IWebBrowserInternal.SetTooltipText(string tooltipText)
        {
            TooltipText = tooltipText;
        }

        //
        // Zusammenfassung:
        //     /// Handles the FrameLoadStart event. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.FrameLoadStartEventArgs instance containing the event data.
        void IWebBrowserInternal.OnFrameLoadStart(FrameLoadStartEventArgs args)
        {
            this.FrameLoadStart?.Invoke(this, args);
        }

        //
        // Zusammenfassung:
        //     /// Handles the FrameLoadEnd event. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.FrameLoadEndEventArgs instance containing the event data.
        void IWebBrowserInternal.OnFrameLoadEnd(FrameLoadEndEventArgs args)
        {
            this.FrameLoadEnd?.Invoke(this, args);
        }

        //
        // Zusammenfassung:
        //     /// Handles the ConsoleMessage event. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.ConsoleMessageEventArgs instance containing the event data.
        void IWebBrowserInternal.OnConsoleMessage(ConsoleMessageEventArgs args)
        {
            this.ConsoleMessage?.Invoke(this, args);
        }

        //
        // Zusammenfassung:
        //     /// Handles the StatusMessage event. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.StatusMessageEventArgs instance containing the event data.
        void IWebBrowserInternal.OnStatusMessage(StatusMessageEventArgs args)
        {
            this.StatusMessage?.Invoke(this, args);
        }

        //
        // Zusammenfassung:
        //     /// Handles the LoadError event. ///
        //
        // Parameter:
        //   args:
        //     The CefSharp.LoadErrorEventArgs instance containing the event data.
        void IWebBrowserInternal.OnLoadError(LoadErrorEventArgs args)
        {
            this.LoadError?.Invoke(this, args);
        }

        void IWebBrowserInternal.SetCanExecuteJavascriptOnMainFrame(bool canExecute)
        {
            CanExecuteJavascriptInMainFrame = canExecute;
        }

        //
        // Zusammenfassung:
        //     /// Raises the System.Windows.Forms.Control.SizeChanged event. ///
        //
        // Parameter:
        //   e:
        //     An System.EventArgs that contains the event data.
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (!designMode && initialized)
            {
                ResizeBrowser();
            }
        }

        //
        // Zusammenfassung:
        //     /// Resizes the browser. ///
        private void ResizeBrowser()
        {
            if (IsBrowserInitialized)
            {
                managedCefBrowserAdapter.Resize(base.Width, base.Height);
            }
        }

        //
        // Zusammenfassung:
        //     /// Raises the System.Windows.Forms.Control.GotFocus event. ///
        //
        // Parameter:
        //   e:
        //     An System.EventArgs that contains the event data.
        protected override void OnGotFocus(EventArgs e)
        {
            if (IsBrowserInitialized)
            {
                browser.GetHost().SetFocus(focus: true);
            }
            base.OnGotFocus(e);
        }

        //
        // Zusammenfassung:
        //     /// Returns the current IBrowser Instance ///
        //
        // Rückgabewerte:
        //     browser instance or null
        public IBrowser GetBrowser()
        {
            this.ThrowExceptionIfBrowserNotInitialized();
            return browser;
        }

        //
        // Zusammenfassung:
        //     /// Makes certain keys as Input keys when CefSettings.MultiThreadedMessageLoop
        //     = false ///
        //
        // Parameter:
        //   keyData:
        //     key data
        //
        // Rückgabewerte:
        //     true for a select list of keys otherwise defers to base.IsInputKey
        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Tab:
                case Keys.Left:
                case Keys.Up:
                case Keys.Right:
                case Keys.Down:
                    return true;
                case Keys.LButton | Keys.MButton | Keys.Space | Keys.Shift:
                case Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
                case Keys.LButton | Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
                case Keys.Back | Keys.Space | Keys.Shift:
                    return true;
                default:
                    return base.IsInputKey(keyData);
            }
        }

        bool IWebBrowser.Focus()
        {
            return Focus();
        }
    }
}
