﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    public enum UpdateChannel
    {
        None,
        Stable,
        Beta,
        Alpha
    }
}
