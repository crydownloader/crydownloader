﻿using CryDownloader.Plugin;

namespace CryDownloader.Logic
{
    public sealed class FileState : IFileState
    {
        public static readonly FileState FileInformationsNotAvailable = new FileState(FileDownloadState.None, FileStateIcon.QuestionMark, Language.NoFileInfo);
        public static readonly FileState FileAvailable = new FileState(FileDownloadState.None, FileStateIcon.CheckMark, Language.FileAvail);
        public static readonly FileState FileDeleted = new FileState(FileDownloadState.None, FileStateIcon.Error, Language.FileDeleted);
        public static readonly FileState FileUnpack = new FileState(FileDownloadState.Unpack, FileStateIcon.Unpack, Language.FileUnpack);
        public static readonly FileState FileMuxing = new FileState(FileDownloadState.Muxing, FileStateIcon.None, Language.FileMuxing);
        public static readonly FileState FileExists = new FileState(FileDownloadState.None, FileStateIcon.Error, Language.FileExists);
        public static readonly FileState DownloadFailed = new FileState(FileDownloadState.Failed, FileStateIcon.Error, Language.DownloadFailed);
        public static readonly FileState FileNotFound = new FileState(FileDownloadState.None, FileStateIcon.Error, Language.FileNotFound);
        public static readonly FileState FileDownloading = new FileState(FileDownloadState.Downloading, FileStateIcon.None, Language.Downloading);
        public static readonly FileState FileResolving = new FileState(FileDownloadState.Resolving, FileStateIcon.None, Language.FileResolving);
        public static readonly FileState FileDownloadFinished = new FileState(FileDownloadState.Finished, FileStateIcon.None, Language.FileDownloadFinished);
        public static readonly FileState FileWaiting = new FileState(FileDownloadState.Waiting, FileStateIcon.None, string.Empty);
        public static readonly FileState FileDownloadCancelled = new FileState(FileDownloadState.Cancelled, FileStateIcon.None, Language.FileDownloadCancelled);
        public FileDownloadState DownloadState { get; }
        public FileStateIcon Icon { get; }
        public string Description { get; }

        public FileState(FileStateIcon icon, string description)
        {
            Icon = icon;
            Description = description;
            DownloadState = FileDownloadState.None;
        }

        public FileState(FileDownloadState downloadState, FileStateIcon icon, string description)
        {
            Icon = icon;
            Description = description;
            DownloadState = downloadState;
        }

        public FileState CreateCopy(FileStateIcon newIcon, string newDescription)
        {
            bool bEmptyString = string.IsNullOrEmpty(newDescription);

            if (bEmptyString && newIcon == Icon)
                return this;
            else if (bEmptyString && newIcon == Icon)
                return new FileState(DownloadState, Icon, Description);
            else if (!bEmptyString && newIcon == Icon)
                return new FileState(DownloadState, Icon, newDescription);
            else if (bEmptyString && newIcon != Icon)
                return new FileState(DownloadState, newIcon, Description);
            else
                return new FileState(DownloadState, newIcon, newDescription);
        }
    }
}
