﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;

namespace CryDownloader.Logic
{
    class ContainerCreator2 : MarshalByRefObject, IContainerCreator
    {
        class DownloadContainerView : IContainer
        {
            public IDownloadContainerView Container { get; }

            public string Name => throw new NotImplementedException();

            private Action<IContainer, DownloadEntry> m_entryAddedCallback;

            public DownloadContainerView(IDownloadContainerView container, Action<IContainer, DownloadEntry> entryAddedCallback)
            {
                Container = container;
                m_entryAddedCallback = entryAddedCallback;
            }

            public bool AddEntry(string url, DownloadEntry entry, bool loadingEntry = true)
            {
                bool result = Container.AddEntry(entry.FileName, entry, loadingEntry);
                m_entryAddedCallback(Container, entry);
                return result;
            }
        }

        private readonly DownloadContainerView m_container;

        public Dictionary<string, IContainer> CreatedContainer { get; }
        public Uri CurrentUrl { get; set; }

        public ContainerCreator2(IDownloadContainerView container, Dictionary<string, IContainer> dictContainer, Action<IContainer, DownloadEntry> entryAddedCallback)
        {
            m_container = new DownloadContainerView(container, entryAddedCallback);
            CreatedContainer = dictContainer;
            CreatedContainer?.Add(container.Name, m_container);
        }

        public IContainer CreateContainer(string name)
        {
            return m_container;
        }
    }
}
