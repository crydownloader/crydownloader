﻿using CryDownloader.Plugin;
using System;
using System.IO;
using System.Threading;
using System.Windows.Threading;

namespace CryDownloader.Logic
{
    public class CaptchaManager : PluginManager<CaptchaType>
    {
        private readonly object m_manualCaptchaSync;

        public CaptchaManager(IApplication app)
            : base(app)
        {
            if (!Directory.Exists("Plugins/Captcha"))
            {
                EnablePluginInitialization();
                return;
            }

            string[] directories = Directory.GetDirectories("Plugins/Captcha", "*.*", SearchOption.TopDirectoryOnly);

            LoadPlugins(directories);
            EnablePluginInitialization();
            m_manualCaptchaSync = new object();
        }

        public CaptchaType GetSupportedCaptchaType(IHtmlNode htmlNode)
        {
            foreach (var captchaType in Plugins)
            {
                if (captchaType.Value.CanResolveCaptcha(htmlNode))
                    return captchaType.Value;
            }

            return null;
        }

        public bool ResolveCaptcha(Dispatcher dispatcher, CaptchaType captchaType,
            IManualResolveElement manualResolveElement, IInternWebBrowser browser,
            IHtmlNode captchaNode, Func<bool> checkCallback, Action successCallback)
        {
            if (captchaType == null)
            {
                lock (m_manualCaptchaSync)
                {
                    ManualResetEvent manualResetEvent = new ManualResetEvent(false);
                    manualResolveElement.ConditionVariable = manualResetEvent;

                    if (!manualResolveElement.IsOpen)
                    {
                        dispatcher.Invoke(() =>
                        {
                            manualResolveElement.ShowDialog();
                        });
                    }

                    manualResetEvent.WaitOne();

                    return !manualResolveElement.IsCancelled;
                }
            }
            else
                return captchaType.ResolveCaptcha(captchaNode, browser, manualResolveElement, successCallback, m_manualCaptchaSync);
        }
    }
}
