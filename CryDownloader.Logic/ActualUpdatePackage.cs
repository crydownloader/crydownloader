﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    class ActualUpdatePackage : UpdatePackage
    {
        public ActualUpdatePackage(Version version)
            : base(version, string.Empty, UpdateChannel.None)
        { }
    }
}
