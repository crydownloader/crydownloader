﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryDownloader.Logic
{
    class HtmlNodeWrapper : MarshalByRefObject, IHtmlNode
    {
        private IHtmlNode m_endNode;
        private IHtmlNode m_firstChild;
        private IHtmlNode m_lastChild;
        private IHtmlNode m_nextSibling;
        private IHtmlNode m_parentNode;
        private IHtmlNode m_previousSibling;
        private IHtmlAttributeCollection m_closingAttributes;
        private IHtmlAttributeCollection m_attributes;
        private IHtmlNodeCollection m_childNodes;

        public HtmlAgilityPack.HtmlNode Impl { get; private set; }

        public string InnerText => Impl.InnerText;

        public string InnerHtml => Impl.InnerHtml;
        public string Id => Impl.Id;

        public bool HasClosingAttributes => Impl.HasClosingAttributes;

        public bool HasChildNodes => Impl.HasChildNodes;

        public IHtmlNode EndNode
        {
            get
            {
                if (m_endNode == null && Impl.EndNode != null)
                    m_endNode = new HtmlNodeWrapper(Impl.EndNode);

                return m_endNode;
            }
        }

        public IHtmlNode FirstChild
        {
            get
            {
                if (m_firstChild == null && Impl.FirstChild != null)
                    m_firstChild = new HtmlNodeWrapper(Impl.FirstChild);

                return m_firstChild;
            }
        }

        public IHtmlNode LastChild
        {
            get
            {
                if (m_lastChild == null && Impl.LastChild != null)
                    m_lastChild = new HtmlNodeWrapper(Impl.LastChild);

                return m_lastChild;
            }
        }

        public IHtmlAttributeCollection ClosingAttributes
        {
            get
            {
                if (m_closingAttributes == null && Impl.ClosingAttributes != null)
                    m_closingAttributes = new HtmlAttributeCollectionWrapper(Impl.ClosingAttributes);

                return m_closingAttributes;
            }
        }

        public bool Closed => Impl.Closed;

        public IHtmlNodeCollection ChildNodes
        {
            get
            {
                if (m_childNodes == null && Impl.ChildNodes != null)
                    m_childNodes = new HtmlNodeCollectionWrapper(Impl.ChildNodes);

                return m_childNodes;
            }
        }

        public bool HasAttributes => Impl.HasAttributes;

        public int Line => Impl.Line;

        public int InnerStartIndex => Impl.InnerStartIndex;

        public IHtmlAttributeCollection Attributes
        {
            get
            {
                if (m_attributes == null && Impl.Attributes != null)
                    m_attributes = new HtmlAttributeCollectionWrapper(Impl.Attributes);

                return m_attributes;
            }
        }

        public int InnerLength => Impl.InnerLength;

        public int OuterLength => Impl.OuterLength;

        public string Name => Impl.Name;

        public IHtmlNode NextSibling
        {
            get
            {
                if (m_nextSibling == null && Impl.NextSibling != null)
                    m_nextSibling = new HtmlNodeWrapper(Impl.NextSibling);

                return m_nextSibling;
            }
        }

        public HtmlNodeType NodeType => (HtmlNodeType)Impl.NodeType;

        public string OriginalName => Impl.OriginalName;

        public string OuterHtml => Impl.OuterHtml;

        public IHtmlNode ParentNode
        {
            get
            {
                if (m_parentNode == null && Impl.ParentNode != null)
                    m_parentNode = new HtmlNodeWrapper(Impl.ParentNode);

                return m_parentNode;
            }
        }

        public IHtmlNode PreviousSibling
        {
            get
            {
                if (m_previousSibling == null && Impl.PreviousSibling != null)
                    m_previousSibling = new HtmlNodeWrapper(Impl.PreviousSibling);

                return m_previousSibling;
            }
        }

        public int StreamPosition => Impl.StreamPosition;

        public int LinePosition => Impl.LinePosition;

        public string XPath => Impl.XPath;

        public int Depth => Impl.Depth;

        public HtmlNodeWrapper(HtmlAgilityPack.HtmlNode node)
        {
            Impl = node;
        }
        ~HtmlNodeWrapper()
        {
            Impl = null;
            m_endNode = null;
            m_firstChild = null;
            m_lastChild = null;
            m_closingAttributes = null;
            m_childNodes = null;
            m_attributes = null;
            m_nextSibling = null;
            m_parentNode = null;
            m_previousSibling = null;
        }

        public override string ToString()
        {
            return Name;
        }

        public IEnumerable<IHtmlNode> Ancestors(string name)
        {
            return Impl.Ancestors(name).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> Ancestors()
        {
            return Impl.Ancestors().Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> AncestorsAndSelf()
        {
            return Impl.AncestorsAndSelf().Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> AncestorsAndSelf(string name)
        {
            return Impl.AncestorsAndSelf(name).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlAttribute> ChildAttributes(string name)
        {
            return Impl.ChildAttributes(name).Select((x) => (IHtmlAttribute)new HtmlAttributeWrapper(x));
        }

        public IEnumerable<IHtmlNode> Descendants()
        {
            return Impl.Descendants().Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> Descendants(int level)
        {
            return Impl.Descendants(level).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> Descendants(string name)
        {
            return Impl.Descendants(name).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> DescendantsAndSelf(string name)
        {
            return Impl.AncestorsAndSelf(name).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> DescendantsAndSelf()
        {
            return Impl.DescendantsAndSelf().Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IHtmlNode Element(string name)
        {
            var tmp = Impl.Element(name);

            if (tmp == null)
                return null;

            return new HtmlNodeWrapper(tmp);
        }

        public IEnumerable<IHtmlNode> Elements(string name)
        {
            return Impl.Elements(name).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public string GetAttributeValue(string name, string def)
        {
            return Impl.GetAttributeValue(name, def);
        }

        public bool GetAttributeValue(string name, bool def)
        {
            return Impl.GetAttributeValue(name, def);
        }

        public int GetAttributeValue(string name, int def)
        {
            return Impl.GetAttributeValue(name, def);
        }

        public IEnumerable<string> GetClasses()
        {
            return Impl.GetClasses();
        }

        public string GetDirectInnerText()
        {
            return Impl.GetDirectInnerText();
        }

        public bool HasClass(string className)
        {
            return Impl.HasClass(className);
        }

        public IHtmlNodeCollection SelectNodes(string xpath)
        {
            var result = Impl.SelectNodes(xpath);

            if (result == null)
                return null;

            return new HtmlNodeCollectionWrapper(result);
        }

        public IHtmlNode SelectSingleNode(string xpath)
        {
            var result = Impl.SelectSingleNode(xpath);

            if (result == null)
                return null;

            return new HtmlNodeWrapper(result);
        }
    }
}
