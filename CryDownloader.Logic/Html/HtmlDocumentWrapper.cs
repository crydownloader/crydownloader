﻿using CryDownloader.Plugin;
using System.Text;

namespace CryDownloader.Logic
{
    class HtmlDocumentWrapper : IHtmlDocument
    {
        public string ParsedText => Impl.ParsedText;
        public string Remainder => Impl.Remainder;
        public Encoding DeclaredEncoding => Impl.DeclaredEncoding;
        public IHtmlNode DocumentNode
        {
            get
            {
                if (m_documentNode == null)
                    m_documentNode = new HtmlNodeWrapper(Impl.DocumentNode);

                return m_documentNode;
            }
        }
        public Encoding Encoding => Impl.Encoding;
        public int CheckSum => Impl.CheckSum;
        public int RemainderOffset => Impl.RemainderOffset;
        public Encoding StreamEncoding => Impl.StreamEncoding;

        public HtmlAgilityPack.HtmlDocument Impl { get; private set; }

        public string Text => Impl.Text;

        private IHtmlNode m_documentNode;

        public HtmlDocumentWrapper(HtmlAgilityPack.HtmlDocument document)
        {
            Impl = document;
        }

        ~HtmlDocumentWrapper()
        {
            Impl = null;
            m_documentNode = null;
        }

        public IHtmlNode GetElementbyId(string id)
        {
            var result = Impl.GetElementbyId(id);

            if (result == null)
                return null;

            return new HtmlNodeWrapper(result);
        }
    }
}
