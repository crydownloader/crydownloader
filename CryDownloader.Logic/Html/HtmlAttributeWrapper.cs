﻿using CryDownloader.Plugin;
using System;

namespace CryDownloader.Logic
{
    class HtmlAttributeWrapper : MarshalByRefObject, IHtmlAttribute
    {
        private IHtmlNode m_ownerNode;

        public HtmlAgilityPack.HtmlAttribute Impl { get; private set; }
        public string Name => Impl.Name;
        public string Value => Impl.Value;

        public int StreamPosition => Impl.StreamPosition;

        public AttributeValueQuote QuoteType => (AttributeValueQuote)Impl.QuoteType;

        public IHtmlNode OwnerNode
        {
            get
            {
                if (m_ownerNode == null && Impl.OwnerNode != null)
                    m_ownerNode = new HtmlNodeWrapper(Impl.OwnerNode);

                return m_ownerNode;
            }
        }

        public string OriginalName => Impl.OriginalName;

        public string DeEntitizeValue => Impl.DeEntitizeValue;

        public string XPath => Impl.XPath;

        public int ValueLength => Impl.ValueLength;

        public int ValueStartIndex => Impl.ValueStartIndex;

        public int LinePosition => Impl.LinePosition;

        public int Line => Impl.Line;

        public bool UseOriginalName => Impl.UseOriginalName;

        public HtmlAttributeWrapper(HtmlAgilityPack.HtmlAttribute attr)
        {
            Impl = attr;
        }

        ~HtmlAttributeWrapper()
        {
            Impl = null;
            m_ownerNode = null;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
