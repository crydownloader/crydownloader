﻿using CryDownloader.Plugin;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CryDownloader.Logic
{
    class HtmlNodeCollectionWrapper : MarshalByRefObject, IHtmlNodeCollection
    {
        class Enumerator : MarshalByRefObject, IEnumerator<IHtmlNode>
        {
            private IEnumerator<HtmlAgilityPack.HtmlNode> m_enumerator;

            public Enumerator(IEnumerator<HtmlAgilityPack.HtmlNode> enumerator)
            {
                m_enumerator = enumerator;
            }

            public IHtmlNode Current => new HtmlNodeWrapper(m_enumerator.Current);

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                m_enumerator.Dispose();
            }

            public bool MoveNext()
            {
                return m_enumerator.MoveNext();
            }

            public void Reset()
            {
                m_enumerator.Reset();
            }
        }
        public IHtmlNode this[string nodeName] => new HtmlNodeWrapper(Impl[nodeName]);

        public int this[IHtmlNode node] => Impl[((HtmlNodeWrapper)node).Impl];

        public IHtmlNode this[int index] => new HtmlNodeWrapper(Impl[index]);

        public HtmlAgilityPack.HtmlNodeCollection Impl { get; private set; }

        public int Count => Impl.Count;

        public bool IsReadOnly => Impl.IsReadOnly;

        public HtmlNodeCollectionWrapper(HtmlAgilityPack.HtmlNodeCollection collection)
        {
            Impl = collection;
        }

        ~HtmlNodeCollectionWrapper()
        {
            Impl = null;
        }

        public void Add(IHtmlNode item)
        {
            Impl.Add(((HtmlNodeWrapper)item).Impl);
        }

        public void Clear()
        {
            ((ICollection<HtmlAgilityPack.HtmlNode>)Impl).Clear();
        }

        public bool Contains(IHtmlNode item)
        {
            return Impl.Contains(((HtmlNodeWrapper)item).Impl);
        }

        public void CopyTo(IHtmlNode[] array, int arrayIndex)
        {
            for (int i = arrayIndex; i < array.Length; ++i)
                array[i] = this[i];
        }

        public IEnumerable<IHtmlNode> Descendants(string name)
        {
            return Impl.Descendants(name).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> Descendants()
        {
            return Impl.Descendants().Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> Elements(string name)
        {
            return Impl.Elements(name).Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IEnumerable<IHtmlNode> Elements()
        {
            return Impl.Elements().Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public IHtmlNode FindFirst(string name)
        {
            return new HtmlNodeWrapper(Impl.FindFirst(name));
        }

        public IEnumerator<IHtmlNode> GetEnumerator()
        {
            return new Enumerator(((ICollection<HtmlAgilityPack.HtmlNode>)Impl).GetEnumerator());
        }

        public int GetNodeIndex(IHtmlNode node)
        {
            return Impl.GetNodeIndex(((HtmlNodeWrapper)node).Impl);
        }

        public int IndexOf(IHtmlNode item)
        {
            return Impl.IndexOf(((HtmlNodeWrapper)item).Impl);
        }

        public IEnumerable<IHtmlNode> Nodes()
        {
            return Impl.Nodes().Select((x) => (IHtmlNode)new HtmlNodeWrapper(x));
        }

        public bool Remove(IHtmlNode item)
        {
            return ((ICollection<HtmlAgilityPack.HtmlNode>)Impl).Remove(((HtmlNodeWrapper)item).Impl);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
