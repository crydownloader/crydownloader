﻿using CryDownloader.Plugin;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CryDownloader.Logic
{
    class HtmlAttributeCollectionWrapper : MarshalByRefObject, IHtmlAttributeCollection
    {
        class Enumerator : MarshalByRefObject, IEnumerator<IHtmlAttribute>
        {
            private IEnumerator<HtmlAgilityPack.HtmlAttribute> m_enumerator;

            public Enumerator(IEnumerator<HtmlAgilityPack.HtmlAttribute> enumerator)
            {
                m_enumerator = enumerator;
            }

            public IHtmlAttribute Current => new HtmlAttributeWrapper(m_enumerator.Current);

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                m_enumerator.Dispose();
            }

            public bool MoveNext()
            {
                return m_enumerator.MoveNext();
            }

            public void Reset()
            {
                m_enumerator.Reset();
            }
        }


        public HtmlAgilityPack.HtmlAttributeCollection Impl { get; private set; }

        public HtmlAttributeCollectionWrapper(HtmlAgilityPack.HtmlAttributeCollection collection)
        {
            Impl = collection;
        }
        ~HtmlAttributeCollectionWrapper()
        {
            Impl = null;
        }

        public IHtmlAttribute this[string name] => new HtmlAttributeWrapper(Impl[name]);
        public IHtmlAttribute this[int index] => new HtmlAttributeWrapper(Impl[index]);

        public bool IsReadOnly => Impl.IsReadOnly;

        public int Count => Impl.Count;

        public IEnumerable<IHtmlAttribute> AttributesWithName(string attributeName)
        {
            return Impl.AttributesWithName(attributeName).Select((x) => (IHtmlAttribute)new HtmlAttributeWrapper(x));
        }

        public bool Contains(string name)
        {
            return Impl.Contains(name);
        }

        public bool Contains(IHtmlAttribute item)
        {
            return Impl.Contains(((HtmlAttributeWrapper)item).Impl);
        }

        public int IndexOf(IHtmlAttribute item)
        {
            return Impl.IndexOf(((HtmlAttributeWrapper)item).Impl);
        }

        public void Add(IHtmlAttribute item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            ((ICollection<HtmlAgilityPack.HtmlAttribute>)Impl).Clear();
        }

        public void CopyTo(IHtmlAttribute[] array, int arrayIndex)
        {
            for (int i = arrayIndex; i < array.Length; ++i)
                array[i] = this[i];
        }

        public bool Remove(IHtmlAttribute item)
        {
            return ((ICollection<HtmlAgilityPack.HtmlAttribute>)Impl).Remove(((HtmlAttributeWrapper)item).Impl);
        }

        public IEnumerator<IHtmlAttribute> GetEnumerator()
        {
            return new Enumerator(((ICollection<HtmlAgilityPack.HtmlAttribute>)Impl).GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
