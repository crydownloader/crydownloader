﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace CryDownloader.Logic
{
    public class HosterManager : PluginManager<Hoster>
    {
        private readonly Settings m_settings;

        public HosterManager(IApplication app, Settings settings)
            : base(app)
        {
            m_settings = settings;

            if (!Directory.Exists("Plugins/Hoster"))
            {
                EnablePluginInitialization();
                return;
            }

            string[] directories = Directory.GetDirectories("Plugins/Hoster", "*.*", SearchOption.TopDirectoryOnly);

            LoadPlugins(directories);
            EnablePluginInitialization();
        }

        public void CreateDownloadEntry(Uri url, IContainerCreator creator, CancellationToken? cancellationToken)
        {
            foreach (var host in Plugins)
            {
                if (host.Value.IsValidUrl(url))
                    host.Value.CreateEntry(url, creator, cancellationToken);
            }
        }

        public bool CreateDownloadEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            bool bCreated = false;
            foreach (var host in Plugins)
            {
                IShareHoster hoster =  host.Value as IShareHoster;

                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return false;
                }

                if (hoster == null)
                    continue;

                if (host.Value.IsValidUrl(url))
                    return hoster.CreateEntry(url, container, cancellationToken);
            }

            if (!bCreated)
            {
                Application.LogManager.WriteLog(LogLevel.Normal, $"No Plugin for '{url.ToString()}' found");
            }

            return false;
        }

        public void CreateDownloadEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {

            Dictionary<Hoster, List<Uri>> dictUrls = new Dictionary<Hoster, List<Uri>>();

            foreach (var url in urls)
            {
                bool bIsValid = false;

                foreach (var host in Plugins)
                {
                    if (host.Value.IsValidUrl(url))
                    {
                        if (!dictUrls.TryGetValue(host.Value, out var lst))
                            dictUrls.Add(host.Value, new List<Uri>() { url });
                        else
                            lst.Add(url);

                        bIsValid = true;
                        break;
                    }
                }

                if (!bIsValid)
                    Application.LogManager.WriteLog(LogLevel.Normal, $"No Plugin for '{url}' found");
            }

            if (cancellationToken.HasValue)
            {
                if (cancellationToken.Value.IsCancellationRequested)
                    return;
            }

            foreach (var HosterUrls in dictUrls)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                HosterUrls.Key.CreateEntries(HosterUrls.Value, creator, scanUrlChangedCallback, cancellationToken);
            }
        }

        public bool ResolveDownloadUrl(Uri url, CancellationToken? cancellationToken, out Uri resolvedUrl)
        {
            resolvedUrl = null;

            foreach (var host in Plugins)
            {
                IUrlResolver resolver = host.Value as IUrlResolver;

                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return false;
                }

                if (resolver == null)
                    continue;

                if (host.Value.IsValidUrl(url))
                {
                    resolvedUrl = resolver.ResolveUrl(url, cancellationToken);
                    return true;
                }
            }

            Application.LogManager.WriteLog(LogLevel.Normal, $"No Plugin for '{url.ToString()}' found");

            return false;
        }

        protected override void OnLoadPlugin(Hoster plugin)
        {
            if (m_settings.Properties.TryGetValue(plugin.Name, out var value))
                plugin.LoadSettings(value);
        }
    }
}
