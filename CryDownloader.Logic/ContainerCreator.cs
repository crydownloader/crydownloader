﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Windows.Threading;

namespace CryDownloader.Logic
{
    interface IContainerCreatorInternal : IContainerCreator
    {
        void AddContainer(IContainer container);
    }

    class ContainerCreator<T> : MarshalByRefObject, IContainerCreatorInternal where T : IDownloadContainerView
    {
        public ObservableCollection<T> Entries { get; }
        public Dispatcher Dispatcher { get; }

        public Type ContainerType { get; }

        public string DownloadDirectory { get; }

        public Uri CurrentUrl { get; set; }

        public Dictionary<string, IContainer> CreatedContainer { get; }

        private Application m_application;
        private Action<IContainer, DownloadEntry> m_entryAddedCallback;

        public ContainerCreator(Application application, ObservableCollection<T> entries,
            Dictionary<string, IContainer> dictContainer,
            Action<IContainer, DownloadEntry> entryAddedCallback,
            Dispatcher dispatcher, string downloadDirectory)
        {
            Entries = entries;
            ContainerType = typeof(T);
            Dispatcher = dispatcher;
            DownloadDirectory = downloadDirectory;
            CreatedContainer = dictContainer;
            m_application = application;
            m_entryAddedCallback = entryAddedCallback;
        }

        public IContainer CreateContainer(string name)
        {
            name = m_application.RemoveIllegalCharacters(name);
            string lowerName = name.ToLower();

            lock (CreatedContainer)
            {
                if (CreatedContainer.TryGetValue(lowerName, out var containerResult))
                    return containerResult;

                DownloadContainer container = new DownloadContainer
                {
                    Name = name,
                    Directory = Path.Combine(DownloadDirectory, name),
                    Url = CurrentUrl
                };

                var result = (T)Activator.CreateInstance(ContainerType, container, Dispatcher);

                container.SetContainer(this, result);
                container.EntryAdded += (object sender, DownloadEntryAddedEventArgs e) => { m_entryAddedCallback(((DownloadContainer)sender).Container, e.Entry); };
                CreatedContainer.Add(lowerName, result);

                if (Entries != null)
                {
                    Dispatcher.Invoke(() => Entries.Add((T)result), DispatcherPriority.Send, CancellationToken.None, TimeSpan.FromSeconds(1));
                }

                return result;
            }
        }

        public void AddContainer(IContainer container)
        {
           // string lowerName = container.Name.ToLower();
           //
           // CreatedContainer.Add(lowerName, container);
           //
           //if (Entries != null)
           //{
           //    Dispatcher.Invoke(() => Entries.Add((T)container), DispatcherPriority.Send, CancellationToken.None, TimeSpan.FromSeconds(1));
           //}
        }
    }
}
