﻿using CefSharp;
using CefSharp.WinForms;
using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Threading;

namespace CryDownloader.Logic
{
    public class Application : MarshalByRefObject, IApplication, IEntryCreator
    {
        private const string SessionDownloadEntryContainerName = "download.container";
        private const string SessionScanEntryContainerName = "scan.container";

        private const int MAX_PATH = 256;
        private const uint SHGFI_ICON = 0x000000100;     // get icon
        private const uint SHGFI_LARGEICON = 0x000000000;     // get large icon
        private const uint SHGFI_USEFILEATTRIBUTES = 0x000000010;     // use passed dwFileAttribute
        private const uint FILE_ATTRIBUTE_NORMAL = 0x00000080;

        [StructLayout(LayoutKind.Sequential)]
        private struct SHFILEINFO
        {
            public const int NAMESIZE = 80;
            public IntPtr hIcon;
            public int iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_PATH)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = NAMESIZE)]
            public string szTypeName;
        };

        [DllImport("Shell32.dll")]
        private static extern int SHGetKnownFolderPath([MarshalAs(UnmanagedType.LPStruct)]Guid rfid, uint dwFlags, IntPtr hToken, out IntPtr ppszPath);

        [DllImport("Shell32.dll")]
        private static extern int SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbFileInfo, uint uFlags);

        [DllImport("User32.dll")]
        private static extern int DestroyIcon(IntPtr hIcon);

        public event EventHandler<OpenBrowserEventArgs> OpeningBrowser;

        private readonly List<WebBrowser> m_webBrowsers;

        public HosterManager HosterManager { get; }
        public Settings Settings { get; }
        public ILogManager LogManager { get; }
        public Dispatcher Dispatcher { get; }

        public JobTaskManager JobTaskManagaer { get; }

        public CaptchaManager CaptchaManager { get; }

        public List<string> SupportedFormats { get; }

        private bool m_bIsCancelDownload;

        private readonly Dictionary<string, IContainer> m_createdContainer;

        public Application(Dispatcher dispatcher)
        {
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;

            string tempDirectory = GetApplicationDirectory() + "temp/";

            if (Directory.Exists(tempDirectory))
                Directory.Delete(tempDirectory, true);

            m_createdContainer = new Dictionary<string, IContainer>();

            Dispatcher = dispatcher;
            AppDomain.CurrentDomain.AssemblyResolve += AssemblyResolver;

            LogManager = new LogManager();
            CaptchaManager = new CaptchaManager(this);

            string strPath = Path.Combine(GetApplicationDirectory(), "settings.cfg");

            if (File.Exists(strPath))
            {
                using (var fs = new FileStream(strPath, FileMode.Open, FileAccess.Read))
                {
                    Settings = Settings.LoadFromStream(fs);
                }
            }
            else
            {
                Settings = Settings.LoadFromStream(null); // create new settings
                SetDefaultSettingValues(Settings);
            }

            if (string.IsNullOrEmpty(Settings.DownloadDirectory))
                Settings.DownloadDirectory = GetDownloadDirectory();

            HosterManager = new HosterManager(this, Settings);
            m_webBrowsers = new List<WebBrowser>();
            m_bIsCancelDownload = false;
            JobTaskManagaer = new JobTaskManager(Settings);

            SupportedFormats = new List<string>();
            SupportedFormats.Add("CryDownloader Container|*.crydlc");

            //SupportedFormats.Add("Download Link Container|*.dlc");
            //SupportedFormats.Add("RapidShare Download Settings Format|*.rsdf");
            //SupportedFormats.Add("CryptLoad Container Format|*.ccf");
            //SupportedFormats.Add("File with download urls|*.txt");
        }

        public void Initialize()
        {
            InitializeCef();
        }

        public void LoadSessionContainer(out ICollection<DownloadContainer> scannedEntries, out ICollection<DownloadContainer> downloadEntries)
        {
            var strPath = Path.Combine(GetApplicationDirectory(), SessionDownloadEntryContainerName);

            LoadContainer(strPath, out downloadEntries);

            strPath = Path.Combine(GetApplicationDirectory(), SessionScanEntryContainerName);

            LoadContainer(strPath, out scannedEntries);
        }

        public void SaveSessionContainer(ICollection<DownloadContainer> scannedEntries, ICollection<DownloadContainer> downloadEntries)
        {
            //var strPath = Path.Combine(GetApplicationDirectory(), SessionDownloadEntryContainerName);

            //SaveContainer(strPath, downloadEntries);

            //strPath = Path.Combine(GetApplicationDirectory(), SessionScanEntryContainerName);

            //SaveContainer(strPath, scannedEntries);
        }

        public void SaveSettings()
        {
            if (HosterManager.Plugins != null)
            {
                foreach (var hoster in HosterManager.Plugins)
                    Settings.Properties[hoster.Value.Name] = hoster.Value.GetSettings();
            }

            string strPath = Path.Combine(GetApplicationDirectory(), "settings.cfg");

            if (File.Exists(strPath))
                File.Delete(strPath);

            string strDownloadDir = GetDownloadDirectory();

            if (Settings.DownloadDirectory == strDownloadDir)
                Settings.DownloadDirectory = null;

            using (var fs = new FileStream(Path.Combine(GetApplicationDirectory(), "settings.cfg"), FileMode.Create, FileAccess.Write))
            {
                Settings.SaveToStream(fs);
            }

            if (string.IsNullOrEmpty(Settings.DownloadDirectory))
                Settings.DownloadDirectory = strDownloadDir;
        }

        public void Destroy()
        {
            PauseDownload();

            HosterManager.Destroy();
            JobTaskManagaer.Stop();
        }

        public List<string> GetUrlsFromText(string text)
        {
            List<string> result = new List<string>();
            IEnumerable<string> urls = ParseText(text);

            foreach (var path in urls)
            {
                if (string.IsNullOrEmpty(path))
                    continue;

                if (path.StartsWith("http://") || path.StartsWith("https://") || IsValidPath(path))
                    result.Add(path);
                else if (path.StartsWith("www."))
                    result.Add("http://" + path);
                else if (IsValidPath(path))
                    result.Add(path);
            }

            return result;
        }

        public bool IsArchiv(string path)
        {
            FileInfo fileInfo = new FileInfo(path);

            if (Settings.ArchivExtensions.TryGetValue(fileInfo.Extension, out var result))
                return result;

            return false;
        }

        public bool IsImage(string path)
        {
            FileInfo fileInfo = new FileInfo(path);

            if (Settings.ImageExtensions.TryGetValue(fileInfo.Extension, out var result))
                return result;

            return false;
        }

        public bool IsAudio(string path)
        {
            FileInfo fileInfo = new FileInfo(path);

            if (Settings.AudioExtensions.TryGetValue(fileInfo.Extension, out var result))
                return result;

            return false;
        }

        public bool IsVideo(string path)
        {
            FileInfo fileInfo = new FileInfo(path);

            if (Settings.VideoExtensions.TryGetValue(fileInfo.Extension, out var result))
                return result;

            return false;
        }

        public static System.Drawing.Icon GetFileIcon(string path)
        {
            SHFILEINFO shfi = new SHFILEINFO();
            uint flags = SHGFI_ICON | SHGFI_USEFILEATTRIBUTES | SHGFI_LARGEICON;

            if (SHGetFileInfo(path, FILE_ATTRIBUTE_NORMAL, ref shfi, (uint)Marshal.SizeOf(shfi), flags) != 1)
                return null;

            if (shfi.iIcon == 0)
            {
                DestroyIcon(shfi.hIcon);
                return null;
            }

            System.Drawing.Icon icon = (System.Drawing.Icon)System.Drawing.Icon.FromHandle(shfi.hIcon).Clone();

            DestroyIcon(shfi.hIcon); // Cleanup
            return icon;
        }

        public void CreateDownloadEntries<T>(List<string> urls, CancellationToken cancellationToken, Dispatcher dispatcher,
            ObservableCollection<T> entries, Action<IContainer, DownloadEntry> entryAddedCallback, 
            Action<Uri> scanUrlChangedCallback, Action<string, int> startLoadingCallback) where T : IDownloadContainerView
        {
            Type entryType = typeof(T);
            List<Uri> lstUrls = new List<Uri>();
            Dictionary<string, T> sortedContainer = new Dictionary<string, T>();

            foreach (var entry in entries)
            {
                if (cancellationToken.IsCancellationRequested)
                    return;

                if (!sortedContainer.ContainsKey(entry.Name))
                    sortedContainer.Add(entry.Name, entry);
            }

            if (cancellationToken.IsCancellationRequested)
                return;

            foreach (var path in urls)
            {
                if (cancellationToken.IsCancellationRequested)
                    return;

                if (string.IsNullOrEmpty(path))
                    continue;

                if (path.StartsWith("http://") || path.StartsWith("https://"))
                {
                    if (Uri.TryCreate(path, UriKind.Absolute, out var uri))
                        lstUrls.Add(uri);
                }
                else if (path.StartsWith("www."))
                {
                    if (Uri.TryCreate("http://" + path, UriKind.Absolute, out var uri))
                        lstUrls.Add(uri);
                }
                else if (IsValidPath(path))
                {

                    FileInfo fileInfo = new FileInfo(path);

                    if (!fileInfo.Exists)
                        continue;

                    DownloadContainerLoader containerLoader = null;

                    /*if (fileInfo.Extension == ".dlc")
                        container = new DlcContainer(fileInfo.FullName, this);
                    else if (fileInfo.Extension == ".rsdf")
                        container = new RsdfContainer(fileInfo.FullName, this);
                    else if (fileInfo.Extension == ".ccf")
                        container = new CcfContainer(fileInfo.FullName, this);
                    else*/ if (fileInfo.Extension == ".crydlc")
                        containerLoader = new DownloadContainerLoader();

                    using (var fs = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
                        containerLoader.Load(fs);

                    foreach (var container in containerLoader.Container)
                    {
                        container.Directory = Path.Combine(GetDownloadDirectory(), container.Name);

                        IDownloadContainerView containerView = (IDownloadContainerView)Activator.CreateInstance(entryType, container, dispatcher);
                        dispatcher.Invoke(() => entries.Add((T)containerView));

                        startLoadingCallback?.Invoke(containerView.Name, container.UrlCount);

                        container.Load(this, containerView, entryAddedCallback, scanUrlChangedCallback, cancellationToken);
                    }

                    return;
                }
            }

            if (cancellationToken.IsCancellationRequested)
                return;

            ContainerCreator<T> creator = new ContainerCreator<T>(this, entries, m_createdContainer, entryAddedCallback, 
                 Dispatcher, Settings.DownloadDirectory);

            HosterManager.CreateDownloadEntries(lstUrls, creator, scanUrlChangedCallback, cancellationToken);
        }

        public bool DownloadEntry(DownloadEntry entry)
        {
            HosterManager.WaitUntilInitialized(entry.Hoster);

            if (m_bIsCancelDownload)
                return false;

            string path = entry.DownloadPath;
            FileInfo fi;

            if (entry.FileState == FileState.FileDownloadFinished)
                return true;

            if (File.Exists(path))
            {
                fi = new FileInfo(path);

                if (fi.Length == entry.FileSize)
                {
                    using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        if (entry.CheckStream(fs))
                        {
                            entry.FileState = FileState.FileExists;
                            return false;
                        }
                    }
                }

                try
                {
                    File.Delete(path);
                }
                catch
                {
                    entry.FileState = FileState.FileExists;
                    return false;
                }
            }

            if (!entry.StartDownload(path))
            {
                entry.FileState = FileState.DownloadFailed;
                return false;
            }

            fi = new FileInfo(path);

            if (entry.FileSize != 0)
            {
                if (fi.Length == entry.FileSize)
                {
                    using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        if (!entry.CheckStream(fs))
                        {
                            try
                            {
                                File.Delete(path);
                                DownloadEntry(entry); // retry
                            }
                            catch
                            {
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        File.Delete(path);
                        DownloadEntry(entry); // retry
                    }
                    catch
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void ResolveCaptcha(Plugin.IWebBrowser browser, IHtmlNode captchaNode, IEnumerable<IHtmlNode> hiddenNodes, Func<bool> checkCallback, Action successCallback)
        {
            WebBrowser webBrowser = (WebBrowser)browser;
            CaptchaType captchaType = null;

            if (captchaNode != null)
            {
                captchaType = CaptchaManager.GetSupportedCaptchaType(captchaNode);

                if (captchaType != null)
                {
                    webBrowser.RemoveElements(captchaType.GetCaptchaNodes(browser.GetDocument()), hiddenNodes);
                    webBrowser.HideScrollbars();
                    webBrowser.FitToContent();
                }
            }

            var args = new OpenBrowserEventArgs(captchaType, webBrowser, System.Windows.Visibility.Visible);

            if (webBrowser.Parent != null)
            {
                if (captchaType != null)
                {
                    if (!captchaType.NeedAction)
                        webBrowser.Parent.HideActionBar();
                }

                args.Result = webBrowser.Parent;
                args.ResolveCaptcha(args.Result.Dispatcher, this, captchaNode, checkCallback, successCallback);
            }
            else
            {
                OpeningBrowser?.Invoke(this, args);

                if (args.Result != null)
                {
                    webBrowser.Parent = args.Result;
                    args.ResolveCaptcha(args.Result.Dispatcher, this, captchaNode, checkCallback, successCallback);
                }
            }
        }

        public IJavaScriptProgram CreateJavaScriptProgram()
        {
            return new JavaScriptProgramWrapper();
        }

        public Plugin.IWebBrowser CreateBrowser()
        {
            var result = new WebBrowser(this, Dispatcher);
            result.OpeningBrowser += (x, y) => OpeningBrowser?.Invoke(this, y);

            m_webBrowsers.Add(result);
            return result;
        }

        public bool Download(Uri url, string path, Action startDownload, Action<DownloadCompleteEventArgs> downloadComplete, Action<Plugin.DownloadProgressChangedEventArgs> downloadProgressChanged)
        {
            bool result = true;

            startDownload();

            if (File.Exists(path))
                File.Delete(path);
            else
                CreateDirectories(path);

            DateTime startTime = DateTime.Now;

            long lastReceivedSize = 0;
            DateTime lastSpeedCalc = DateTime.Now;
            double speed = 0;
            Exception err = null;

            using (var client = new WebClientEx())
            {
                ManualResetEvent condition = new ManualResetEvent(false);

                client.DownloadFileCompleted += (x, y) =>
                {
                    var args = new DownloadCompleteEventArgs(y.Cancelled, y.Error);
                    downloadComplete(args);

                    if (y.Error != null)
                    {
                        err = y.Error;
                        result = false;
                    }

                    condition.Set();
                };

                double[] speedRingBuffer = new double[20];
                int currIdx = 0;

                client.DownloadProgressChanged += (x, y) =>
                {
                    Plugin.DownloadProgressChangedEventArgs args = new Plugin.DownloadProgressChangedEventArgs(y.BytesReceived, y.TotalBytesToReceive, speed, startTime);

                    var currTime = DateTime.Now;
                    if ((currTime - lastSpeedCalc).TotalSeconds >= 1)
                    {
                        lastSpeedCalc = currTime;

                        speedRingBuffer[currIdx++] = y.BytesReceived - lastReceivedSize;

                        if (currIdx >= speedRingBuffer.Length)
                            currIdx = 0;

                        double avgSpeed = 0.0;

                        foreach (var sp in speedRingBuffer)
                            avgSpeed += sp;

                        avgSpeed /= speedRingBuffer.Length;

                        speed = avgSpeed;
                        lastReceivedSize = y.BytesReceived;
                    }

                    if (m_bIsCancelDownload)
                    {
                        condition.Set();
                        client.CancelAsync();
                    }

                    downloadProgressChanged(args);
                };

                if (m_bIsCancelDownload)
                    result = false;

                client.DownloadFileAsync(url, path);
                condition.WaitOne();
            }
            if (!result)
            {
                var curEx = err;

                while (curEx != null)
                {
                    if (curEx is SocketException sEx)
                    {
                        if (sEx.SocketErrorCode == SocketError.ConnectionAborted)
                            Download(url, path, startDownload, downloadComplete, downloadProgressChanged);
                    }

                    curEx = curEx.InnerException;
                }
            }

            return result;
        }

        public bool IsValidPath(string path)
        {
            string strTheseAreInvalidFileNameChars = new string(Path.GetInvalidPathChars());
            strTheseAreInvalidFileNameChars += @":/?*" + "\"";
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");

            if (path.Length > 3)
                return !containsABadCharacter.IsMatch(path.Substring(3, path.Length - 3));

            return false;
        }

        public void Exit()
        {
            m_bIsCancelDownload = true;
        }

        public string RemoveIllegalCharacters(string path)
        {
            string strTheseAreInvalidFileNameChars = new string(Path.GetInvalidPathChars());
            strTheseAreInvalidFileNameChars += @":/?*" + "\"";
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");

            return containsABadCharacter.Replace(path, "");
        }

        public T LoadJsonObject<T>(string json, IJsonConverter converter)
        {
            try
            {
                using (var reader = new StringReader(json))
                {
                    using (var r = new Newtonsoft.Json.JsonTextReader(reader))
                    {
                        var ser = new Newtonsoft.Json.JsonSerializer();

                        if (converter != null)
                            ser.Converters.Add(new CustomJsonConverter(converter));

                        return ser.Deserialize<T>(r);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return default;
            }
        }

        public IHtmlDocument LoadHtmlDocument(string html)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);



            return new HtmlDocumentWrapper(doc);
        }

        public IFileState CreateFileState(FileDownloadState fileState, FileStateIcon icon, string description)
        {
            return new FileState(fileState, icon, description);
        }

        public IFileState CreateFileState(FileStateIcon icon, string description)
        {
            return new FileState(icon, description);
        }

        public IFileState GetFileState(FileDownloadState fileState)
        {
            switch (fileState)
            {
                default:
                case FileDownloadState.None:
                    return null;
                case FileDownloadState.Downloading:
                    return FileState.FileDownloading;
                case FileDownloadState.Resolving:
                    return FileState.FileResolving;
                case FileDownloadState.Starting:
                    return FileState.DownloadFailed;
                case FileDownloadState.Finished:
                    return FileState.FileDownloadFinished;
                case FileDownloadState.Waiting:
                    return FileState.FileWaiting;
                case FileDownloadState.Cancelled:
                    return FileState.FileDownloadCancelled;
                case FileDownloadState.Unpack:
                    return FileState.FileUnpack;
                case FileDownloadState.Muxing:
                    return FileState.FileMuxing;
            }
        }

        public IFileState GetFileState(FileDownloadState fileState, FileStateIcon icon, string strDescription)
        {
            switch (fileState)
            {
                default:
                case FileDownloadState.None:
                    return null;
                case FileDownloadState.Downloading:
                    return FileState.FileDownloading.CreateCopy(icon, strDescription);
                case FileDownloadState.Resolving:
                    return FileState.FileResolving.CreateCopy(icon, strDescription);
                case FileDownloadState.Starting:
                    return FileState.DownloadFailed.CreateCopy(icon, strDescription);
                case FileDownloadState.Finished:
                    return FileState.FileDownloadFinished.CreateCopy(icon, strDescription);
                case FileDownloadState.Waiting:
                    if (string.IsNullOrEmpty(strDescription))
                        return FileState.FileWaiting.CreateCopy(icon, strDescription);
                    else if (int.TryParse(strDescription, out var sec))
                        return FileState.FileWaiting.CreateCopy(icon, string.Format(Language.FileWaiting, sec));
                    else
                        return FileState.FileWaiting.CreateCopy(icon, strDescription);
                case FileDownloadState.Cancelled:
                    return FileState.FileDownloadCancelled.CreateCopy(icon, strDescription);
                case FileDownloadState.Unpack:
                    return FileState.FileUnpack.CreateCopy(icon, strDescription);
                case FileDownloadState.Muxing:
                    return FileState.FileMuxing.CreateCopy(icon, strDescription);
            }
        }

        public IFileState GetFileState(FileStateType type)
        {
            switch (type)
            {
                default:
                case FileStateType.None:
                    return null;
                case FileStateType.FileInfoNotAvail:
                    return FileState.FileInformationsNotAvailable;
                case FileStateType.FileInfoAvail:
                    return FileState.FileAvailable;
                case FileStateType.FileDeleted:
                    return FileState.FileDeleted;
                case FileStateType.FileExists:
                    return FileState.FileExists;
                case FileStateType.FileNotFound:
                    return FileState.FileNotFound;
                case FileStateType.Resolving:
                    return FileState.FileResolving;
            }
        }

        public T LoadJsonObject<T>(TextReader reader, IJsonConverter converter = null)
        {
            using (var r = new Newtonsoft.Json.JsonTextReader(reader))
            {
                var ser = new Newtonsoft.Json.JsonSerializer();

                if (converter != null)
                    ser.Converters.Add(new CustomJsonConverter(converter));

                return ser.Deserialize<T>(r);
            }
        }

        public bool Muxing(DownloadEntry entry, MuxingArgs args, Action startDownload, Action<DownloadCompleteEventArgs> downloadComplete, Action<Plugin.DownloadProgressChangedEventArgs> downloadProgressChanged)
        {
            startDownload();

            if (File.Exists(args.OutputPath))
                File.Delete(args.OutputPath);
            else
                CreateDirectories(args.OutputPath);

            DateTime startTime = DateTime.Now;
            string commandlineArgs = string.Empty;

            if (!string.IsNullOrEmpty(args.AudioPath) && !string.IsNullOrEmpty(args.VideoPath))
                commandlineArgs = $"-i \"{args.VideoPath}\" -i \"{args.AudioPath}\" -c copy \"{args.OutputPath}\""; // -progress pipe:1

            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo("ffmpeg.exe", commandlineArgs);
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            
            startInfo.WorkingDirectory = GetApplicationDirectory();

            LogManager.WriteLog(LogLevel.Hard, $"commandline > ffmpeg.exe {commandlineArgs}");

            var process = System.Diagnostics.Process.Start(startInfo);
            process.EnableRaisingEvents = true;

            Regex startRegex = new Regex("Duration: (.{2}):(.{2}):(.{2}).(.{2})");
            Regex progressRegex = new Regex(@"time=(.{2}):(.{2}):(.{2}).(.{2}) bitrate=\s*([0-9]*\.?[0-9]*?)kbits/s speed=\s*([0-9]*\.?[0-9]*?)x");
            Regex finishRegex = new Regex(@"Lsize=\s*([0-9]*)kB");

            TimeSpan outputTimespan = TimeSpan.Zero;

            System.Diagnostics.DataReceivedEventHandler eventHandler = (x, y) => {

                if (y.Data == null)
                    return;

                LogManager.WriteLog(LogLevel.Hard, $"commandline > {y.Data}");
                var match = startRegex.Match(y.Data);

                if (match.Success)
                {
                    if (outputTimespan == TimeSpan.Zero)
                    {
                        outputTimespan = new TimeSpan(0,
                                              int.Parse(match.Groups[1].Value),
                                              int.Parse(match.Groups[2].Value),
                                              int.Parse(match.Groups[3].Value),
                                              int.Parse(match.Groups[4].Value));
                    }
                    else
                    {
                        var duration = new TimeSpan(0,
                                             int.Parse(match.Groups[1].Value),
                                             int.Parse(match.Groups[2].Value),
                                             int.Parse(match.Groups[3].Value),
                                             int.Parse(match.Groups[4].Value));

                        if (outputTimespan < duration)
                            outputTimespan = duration;
                    }

                }
                else
                {
                    match = progressRegex.Match(y.Data);
                    var finishMatch = finishRegex.Match(y.Data);

                    if (match.Success)
                    {
                        var currentTimestamp = new TimeSpan(0,
                            int.Parse(match.Groups[1].Value),
                            int.Parse(match.Groups[2].Value),
                            int.Parse(match.Groups[3].Value),
                            int.Parse(match.Groups[4].Value));

                        double speed = double.Parse(match.Groups[5].Value);
                        speed *= 1024;

                        Plugin.DownloadProgressChangedEventArgs progressArgs;

                        var endTime = new TimeSpan((long)((outputTimespan - currentTimestamp).Ticks / double.Parse(match.Groups[6].Value)));

                        if (finishMatch.Success)
                        {
                            progressArgs = new Plugin.DownloadProgressChangedEventArgs(
                                (long)currentTimestamp.TotalMilliseconds,
                                (long)currentTimestamp.TotalMilliseconds,
                                (long)speed,
                                startTime,
                                endTime);


                            downloadProgressChanged(progressArgs);
                            downloadComplete(new DownloadCompleteEventArgs(m_bIsCancelDownload, null));
                        }
                        else
                        {
                            progressArgs = new Plugin.DownloadProgressChangedEventArgs(
                                (long)currentTimestamp.TotalMilliseconds,
                                (long)outputTimespan.TotalMilliseconds,
                                (long)speed,
                                startTime,
                                endTime);

                            downloadProgressChanged(progressArgs);
                        }
                    }
                }
            };

            process.ErrorDataReceived += eventHandler;
            process.OutputDataReceived += eventHandler;

            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            while (!process.HasExited)
            {
                if (m_bIsCancelDownload)
                {
                    process.Kill();
                    break;
                }

                Thread.Sleep(1);
            }

            process.WaitForExit();
            return !m_bIsCancelDownload;
        }

        public void RemoveContainer(IContainer container)
        {
            m_createdContainer.Remove(container.Name.ToLower());
        }

        public bool ScanLink(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return HosterManager.CreateDownloadEntry(url, container, cancellationToken);
        }

        public bool ResolveDownloadUrl(Uri url, CancellationToken? cancellationToken, out Uri resolvedUrl)
        {
            return HosterManager.ResolveDownloadUrl(url, cancellationToken, out resolvedUrl);
        }

        public bool SaveContainer(string filename, IEnumerable<DownloadContainer> container)
        {
            FileInfo fi = new FileInfo(filename);

            if (fi.Exists)
                File.Delete(filename);

            ContainerSaver containerSaver = null;

            if (fi.Extension == ".crydlc")
                containerSaver = new ContainerSaver();
            //else if (fi.Extension == ".dlc")
            //    containerSaver = new DlcContainerSaver();

            return containerSaver.Save(filename, container);
        }


        internal void PauseDownload()
        {
        }

        internal void CancelDownload()
        {
            //foreach (var browser in m_webBrowsers)
            //    browser.CancelDownload();

            m_bIsCancelDownload = true;
        }

        private void InitializeCef()
        {
            var settings = new CefSettings
            {
                BrowserSubprocessPath = Path.Combine(Environment.Is64BitProcess ? "x64" : "x86",
                                       "CefSharp.BrowserSubprocess.exe")
            };

            settings.CefCommandLineArgs.Add("disable-gpu", "1");
            settings.CefCommandLineArgs.Add("disable-gpu-vsync", "1");

            if (Cef.Initialize(settings))
                LogManager.WriteLog(LogLevel.Hard, "CEF initialized.");
            else
                LogManager.WriteLog(LogLevel.Hard, "failed to initialize CEF.");
        }

        private int GetNextIndex(string text, int index, out int length)
        {
            int startIndex = text.IndexOf("http://www.", index);
            length = 11;

            if (startIndex >= 0)
                return startIndex;

            startIndex = text.IndexOf("https://www.", index);
            length = 12;

            if (startIndex >= 0)
                return startIndex;

            startIndex = text.IndexOf("www.", index);
            length = 4;

            if (startIndex >= 0)
                return startIndex;

            startIndex = text.IndexOf("http://", index);
            length = 7;

            if (startIndex >= 0)
                return startIndex;

            startIndex = text.IndexOf("https://", index);
            length = 8;

            if (startIndex >= 0)
                return startIndex;

            startIndex = text.IndexOf("file:///", index);
            length = 8;

            if (startIndex <= 0)
            {
                string path = text.Substring(index);

                if (IsValidPath(path))
                {
                    if (File.Exists(path))
                    {
                        length = path.Length;
                        return 0;
                    }
                }

                return startIndex;
            }

            Regex regex = new Regex(@"\D:\/(?:.*)");
            var match = regex.Match(text);

            startIndex = match.Index;
            length = 3;

            if (match.Success)
                return startIndex;

            regex = new Regex(@"\D:\\(?:.*)");
            match = regex.Match(text);

            startIndex = match.Index;
            length = 3;

            if (match.Success)
                return startIndex;

            return -1;
        }

        private void ParseLine(string line, List<string> result)
        {
            int startIndex = 0;
            int startElementLength = 0;

            while ((startIndex = GetNextIndex(line, startIndex + startElementLength, out startElementLength)) >= 0)
            {
                int endIndex = GetNextIndex(line, startIndex + startElementLength, out var _);

                if (endIndex < 0)
                    result.Add(line.Substring(startIndex));
                else
                    result.Add(line.Substring(startIndex, endIndex - startIndex));
            }
        }

        private IEnumerable<string> ParseText(string text)
        {
            List<string> links = new List<string>();

            if (text.Contains("\n") || text.Contains(";"))
            {
                var lines = text.Split('\r', '\n', ';').Where((x) => !string.IsNullOrEmpty(x));

                foreach (var line in lines)
                    ParseLine(line, links);
            }
            else
                ParseLine(text, links);

            links = links.Where((x) => !string.IsNullOrEmpty(x)).ToList();
            return links;
        }

        public string GetApplicationDirectory()
        {
            return Path.GetFullPath(".");
        }

        private string GetDownloadDirectory()
        {
            int result = SHGetKnownFolderPath(new Guid("{374DE290-123F-4565-9164-39C4925E467B}"), 0x00004000, IntPtr.Zero, out IntPtr outPath);

            if (result >= 0)
            {
                string path = Marshal.PtrToStringUni(outPath);
                Marshal.FreeCoTaskMem(outPath);
                return path;
            }
            else
            {
                throw new ExternalException("Unable to retrieve the known folder path. It may not "
                    + "be available on this system.", result);
            }
        }

        private Assembly AssemblyResolver(object sender, ResolveEventArgs args)
        {
            if (args.Name.StartsWith("CefSharp"))
            {
                string assemblyName = args.Name.Split(new[] { ',' }, 2)[0] + ".dll";
                string archSpecificPath = Path.Combine(Environment.Is64BitProcess ? "x64" : "x86",
                                                       assemblyName);
                if (File.Exists(archSpecificPath))
                    return Assembly.LoadFile(Path.GetFullPath(archSpecificPath));

                return null;
            }

            return null;
        }

        private void SetDefaultSettingValues(Settings settings)
        {
            settings.DownloadDirectory = GetDownloadDirectory();
            settings.DownloadCount = 4;
            settings.DownloadCountPerHost = 4;
            settings.ChunkDownloadCount = 4;
            settings.FileSizeFormat = FileSizeFormatType.Binary;
            settings.MaxDownloadSpeed = 0;
            settings.RetryCount = 3;
            settings.UseSystemStyle = true;
            settings.ShowMilliseconds = true;
            settings.Language = "en";

            settings.ArchivExtensions.Add(".rar", true);
            settings.ArchivExtensions.Add(".gz", true);
            settings.ArchivExtensions.Add(".gz2", true);
            settings.ArchivExtensions.Add(".tar", true);
            settings.ArchivExtensions.Add(".zip", true);
            settings.ArchivExtensions.Add(".xz", true);
            settings.ArchivExtensions.Add(".bz2", true);
            settings.ArchivExtensions.Add(".z", true);
            settings.ArchivExtensions.Add(".7z", true);
            settings.ArchivExtensions.Add(".a", true);
            settings.ArchivExtensions.Add(".ar", true);
            settings.ArchivExtensions.Add(".iso", true);
            settings.ArchivExtensions.Add(".lbr", true);
            settings.ArchivExtensions.Add(".mar", true);
            settings.ArchivExtensions.Add(".sbx", true);
            settings.ArchivExtensions.Add(".lz", true);
            settings.ArchivExtensions.Add(".lzma", true);
            settings.ArchivExtensions.Add(".lzo", true);
            settings.ArchivExtensions.Add(".rz", true);
            settings.ArchivExtensions.Add(".sz", true);
            settings.ArchivExtensions.Add(".s7z", true);
            settings.ArchivExtensions.Add(".ace", true);
            settings.ArchivExtensions.Add(".afa", true);
            settings.ArchivExtensions.Add(".alz", true);
            settings.ArchivExtensions.Add(".arc", true);
            settings.ArchivExtensions.Add(".ark", true);
            settings.ArchivExtensions.Add(".cab", true);
            settings.ArchivExtensions.Add(".car", true);
            settings.ArchivExtensions.Add(".cfs", true);
            settings.ArchivExtensions.Add(".dmg", true);
            settings.ArchivExtensions.Add(".pak", true);
            settings.ArchivExtensions.Add(".tgz", true);
            settings.ArchivExtensions.Add(".txz", true);
            settings.ArchivExtensions.Add(".tlz", true);
            settings.ArchivExtensions.Add(".tbz2", true);
            settings.ArchivExtensions.Add(".zipx", true);

            settings.ImageExtensions.Add(".jpg", true);
            settings.ImageExtensions.Add(".jpeg", true);
            settings.ImageExtensions.Add(".png", true);
            settings.ImageExtensions.Add(".dds", true);
            settings.ImageExtensions.Add(".svg", true);
            settings.ImageExtensions.Add(".tga", true);
            settings.ImageExtensions.Add(".bmp", true);
            settings.ImageExtensions.Add(".gif", true);

            settings.AudioExtensions.Add(".aac", true);
            settings.AudioExtensions.Add(".au", true);
            settings.AudioExtensions.Add(".flac", true);
            settings.AudioExtensions.Add(".gsm", true);
            settings.AudioExtensions.Add(".m4a", true);
            settings.AudioExtensions.Add(".m4b", true);
            settings.AudioExtensions.Add(".mp3", true);
            settings.AudioExtensions.Add(".mpc", true);
            settings.AudioExtensions.Add(".ogg", true);
            settings.AudioExtensions.Add(".oga", true);
            settings.AudioExtensions.Add(".mogg", true);
            settings.AudioExtensions.Add(".raw", true);
            settings.AudioExtensions.Add(".wav", true);
            settings.AudioExtensions.Add(".wma", true);

            settings.VideoExtensions.Add(".webm", true);
            settings.VideoExtensions.Add(".mkv", true);
            settings.VideoExtensions.Add(".flv", true);
            settings.VideoExtensions.Add(".vob", true);
            settings.VideoExtensions.Add(".ogv", true);
            settings.VideoExtensions.Add(".gifv", true);
            settings.VideoExtensions.Add(".mng", true);
            settings.VideoExtensions.Add(".avi", true);
            settings.VideoExtensions.Add(".mts", true);
            settings.VideoExtensions.Add(".m2ts", true);
            settings.VideoExtensions.Add(".mov", true);
            settings.VideoExtensions.Add(".qt", true);
            settings.VideoExtensions.Add(".wmv", true);
            settings.VideoExtensions.Add(".rm", true);
            settings.VideoExtensions.Add(".rmvb", true);
            settings.VideoExtensions.Add(".asf", true);
            settings.VideoExtensions.Add(".amv", true);
            settings.VideoExtensions.Add(".mp4", true);
            settings.VideoExtensions.Add(".m4p", true);
            settings.VideoExtensions.Add(".m4v", true);
            settings.VideoExtensions.Add(".mpg", true);
            settings.VideoExtensions.Add(".mp2", true);
            settings.VideoExtensions.Add(".mpeg", true);
            settings.VideoExtensions.Add(".mpe", true);
            settings.VideoExtensions.Add(".mpv", true);
            settings.VideoExtensions.Add(".m2v", true);
            settings.VideoExtensions.Add(".3gp", true);
            settings.VideoExtensions.Add(".3g2", true);
            settings.VideoExtensions.Add(".ts", true);
        }

        private void CreateDirectories(string path)
        {
            FileInfo fileInfo = new FileInfo(path);
            DirectoryInfo directoryInfo = fileInfo.Directory;
            Queue<string> queue = new Queue<string>();

            while (directoryInfo != null)
            {
                if (!Directory.Exists(directoryInfo.FullName))
                    queue.Enqueue(directoryInfo.FullName);

                directoryInfo = directoryInfo.Parent;
            }

            if (queue.Count > 0)
            {
                foreach (var dir in queue.Reverse())
                    Directory.CreateDirectory(dir);
            }
        }

        private void LoadContainer(string strPath, out ICollection<DownloadContainer> output)
        {
            if (File.Exists(strPath))
            {
                using (var fs = new FileStream(strPath, FileMode.Open, FileAccess.Read))
                {
                    output = DownloadContainer.LoadFromStream(fs, HosterManager, null);
                }
            }
            else
                output = null;
        }

        private void SaveContainer(string strPath, ICollection<DownloadContainer> entries)
        {
            if (File.Exists(strPath))
                File.Delete(strPath);

            using (var fs = new FileStream(strPath, FileMode.CreateNew, FileAccess.Write))
            {
                DownloadContainer.SaveToStream(fs, entries);
            }
        }

        void IEntryCreator.CreateEntries(IEnumerable<Uri> urls, IDownloadContainerView container, 
            Action<IContainer, DownloadEntry> entryAddedCallback, Action<Uri> scanUrlChangedCallback, CancellationToken? token)
        {
            ContainerCreator2 creator = new ContainerCreator2(container, m_createdContainer, entryAddedCallback);

            HosterManager.CreateDownloadEntries(urls, creator, scanUrlChangedCallback, token);
        }

        public string GetContainerName(string filename)
        {
            if (IsArchiv(filename))
            {
                string[] filenameParts = filename.Split('.');
                int index = filenameParts.Length - 2;

                if (index < 0)
                    return filename;

                if (!filenameParts[index].Contains("part"))
                    return filename;

                string containerName = string.Empty;

                for (int i = 0; i < index; ++i)
                    containerName += filenameParts[i] + ".";

                if (containerName.Length > 1)
                    return containerName.Remove(containerName.Length - 1, 1);

                return filename;
            }

            return filename;
        }
    }
}
