﻿using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.IO;
using System.IO.Compression;

namespace CryDownloader.Logic
{
    public class StartUpdateEventArgs : EventArgs
    {
        public ulong UpdateSize { get; }

        public StartUpdateEventArgs(ulong updateSize)
        {
            UpdateSize = updateSize;
        }
    }

    public class UpdateFinishedEventArgs : EventArgs
    {
        public bool Cancelled { get; }

        public UpdateFinishedEventArgs(bool cancelled)
        {
            Cancelled = cancelled;
        }
    }

    public class UpdateProcessChangedEventArgs : EventArgs
    {
        public ulong UpdateSize { get; }
        public ulong ProcessedBytes { get; }
        public bool Cancel { get; set; }

        public UpdateProcessChangedEventArgs(ulong updateSize, ulong processedBytes)
        {
            UpdateSize = updateSize;
            ProcessedBytes = processedBytes;
        }
    }

    public class FileUnpackEventArgs : EventArgs
    {
        public string Filename { get; }
        public ulong FileSize { get; }

        public FileUnpackEventArgs(string fileName, ulong fileSize)
        {
            Filename = fileName;
            FileSize = fileSize;
        }
    }

    public class FileUnpackFinishedEventArgs : UpdateFinishedEventArgs
    {
        public string Filename { get; }
        public ulong FileSize { get; }

        public FileUnpackFinishedEventArgs(string fileName, ulong fileSize, bool cancelled)
            : base(cancelled)
        {
            Filename = fileName;
            FileSize = fileSize;
        }
    }

    public class UpdatePackage
    {
        public Version Version { get; }
        public string Changelog { get; }
        public UpdateChannel UpdateChannel { get; }

        public event EventHandler<StartUpdateEventArgs> StartUpdate;

        public event EventHandler<UpdateProcessChangedEventArgs> UpdateProcessChanged;

        public event EventHandler<UpdateProcessChangedEventArgs> UnpackProcessChanged;

        public event EventHandler<FileUnpackFinishedEventArgs> FileUnpackFinished;

        public event EventHandler<FileUnpackEventArgs> FileUnpack;

        public event EventHandler<UpdateFinishedEventArgs> UpdateFinished;

        internal UpdatePackage(Version version, string changeLog, UpdateChannel updateChannel)
        {
            Version = version;
            Changelog = changeLog;
            UpdateChannel = updateChannel;
        }

        public virtual void Update(Application app)
        {
            string tempDirectory = app.GetApplicationDirectory() + "temp/";

            if (!Directory.Exists(tempDirectory))
                Directory.CreateDirectory(tempDirectory);

            string localUpdatePackage = tempDirectory + "update.pkg";
            bool bCancel = false;

            using (Stream packageStream = File.Create(localUpdatePackage))
            {
                using (var sftp = new SftpClient(Updater.SftpHost, Updater.SftpUser, Updater.SftpPassword))
                {
                    string remoteFilePath = $"{UpdateChannel.ToString()}/{Version.Major}.{Version.Minor}.{Version.Build}.{Version.Revision}/update.pkg";

                    var file = sftp.Get(remoteFilePath);
                    ulong fileSize = (ulong)file.Length;

                    StartUpdate?.Invoke(this, new StartUpdateEventArgs(fileSize));

                    SftpDownloadAsyncResult asyncResult = null;

                    asyncResult = (SftpDownloadAsyncResult)sftp.BeginDownloadFile(remoteFilePath, packageStream, null, null, 
                        (x) => {
                            var args = new UpdateProcessChangedEventArgs(fileSize, x);
                            UpdateProcessChanged?.Invoke(this, args);

                            if (args.Cancel && asyncResult != null)
                            {
                                asyncResult.IsDownloadCanceled = true;
                                bCancel = true;
                            }
                        });

                    sftp.EndDownloadFile(asyncResult);
                }

                if (!bCancel)
                {
                    packageStream.Seek(0, SeekOrigin.Begin);
                    Unpack(tempDirectory, packageStream, out bCancel);
                }
            }

            if (File.Exists(localUpdatePackage))
                File.Delete(localUpdatePackage);

            UpdateFinished?.Invoke(this, new UpdateFinishedEventArgs(bCancel));
        }

        protected bool Unpack(string tempPath, Stream fs, out bool cancelled)
        {
            cancelled = false;
            tempPath = tempPath + "old/";

            if (!fs.Read(out int fileCount))
                return false;

            for (int i = 0; i < fileCount; ++i)
            {
                if (!fs.Read(out ulong fileSize))
                    return false;

                if (!fs.Read(out long offset))
                    return false;

                if (!fs.Read(out string fileName))
                    return false;

                fs.Seek(offset, SeekOrigin.Begin);

                string tempFilePath = tempPath + fileName;

                if (File.Exists(fileName))
                {
                    if (!Directory.Exists(tempPath))
                        Directory.CreateDirectory(tempPath);

                    File.Move(fileName, tempFilePath);
                }

                FileUnpack?.Invoke(this, new FileUnpackEventArgs(fileName, fileSize));

                ulong unpackedSize = 0;
                using (Stream fs2 = File.Create(fileName))
                {
                    using (GZipStream decompress = new GZipStream(fs, CompressionMode.Decompress))
                    {
                        byte[] buffer = new byte[8192];
                        ulong readedBytes = 0;

                        while (fileSize > readedBytes)
                        {
                            int readSize = Math.Min(buffer.Length, (int)(fileSize - readedBytes));
                            int size = decompress.Read(buffer, 0, readSize);
                            fs2.Write(buffer, 0, size);

                            var args = new UpdateProcessChangedEventArgs(fileSize, readedBytes);
                            UnpackProcessChanged?.Invoke(this, args);

                            if (args.Cancel)
                            {
                                cancelled = true;
                                break;
                            }

                            readedBytes += (ulong)size;
                        }
                    }

                    unpackedSize = (ulong)fs2.Length;
                }

                if (cancelled)
                {
                    if (File.Exists(fileName))
                        File.Delete(fileName);

                    File.Move(tempFilePath, fileName);
                }

                FileUnpackFinished?.Invoke(this, new FileUnpackFinishedEventArgs(fileName, unpackedSize, cancelled));
            }

            return true;
        }

        protected virtual Uri GetDownloadPath()
        {
            return new Uri($"https://update.cryfact.com/CryDownloader/{UpdateChannel.ToString()}/{Version.Major}.{Version.Minor}.{Version.Build}.{Version.Revision}/");
        }
    }
}
