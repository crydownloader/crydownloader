﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace CryDownloader.Logic
{
    public class ScanEngine
    {
        public static Regex PathBadCharacters { get; }
        public static Regex LocalPath { get; }

        private List<string> m_stringStarts = new List<string>();

        static ScanEngine()
        {
            string str = new string(Path.GetInvalidPathChars());
            str += @":/?*" + "\"";
            PathBadCharacters = new Regex("[" + Regex.Escape(str) + "]");
            LocalPath = new Regex(@"\D:\/(?:.*)");
        }

        public ScanEngine()
        {
            m_stringStarts = new List<string>()
            {
                "http://www.",
                "https://www.",
                "www.",
                "http://",
                "https://"
            };
        }

        public List<string> GetUrlsFromText(string text)
        {
            var result = new List<string>();
            var urls = ParseText(text);

            foreach (var path in urls)
            {
                if (string.IsNullOrEmpty(path))
                    continue;

                if (path.StartsWith("http://") || path.StartsWith("https://") || IsValidPath(path))
                    result.Add(path);
                else if (path.StartsWith("www."))
                    result.Add("http://" + path);
                else if (IsValidPath(path))
                    result.Add(path);
            }

            return result;
        }

        public bool IsValidPath(string path)
        {
            if (path.Length > 3)
                return !PathBadCharacters.IsMatch(path.Substring(3, path.Length - 3));

            return false;
        }

        private int GetNextIndex(string text, int index, out int length)
        {
            int startIndex;

            foreach (var starts in m_stringStarts)
            {
                startIndex = text.IndexOf(starts, index);

                if (startIndex >= 0)
                {
                    length = starts.Length;
                    return startIndex;
                }
            }

            startIndex = text.IndexOf("file:///", index);
            length = 8;

            if (startIndex <= 0)
            {
                string path = text.Substring(index);

                if (IsValidPath(path))
                {
                    if (File.Exists(path))
                    {
                        length = path.Length;
                        return 0;
                    }
                }

                return startIndex;
            }

            var match = LocalPath.Match(text);

            startIndex = match.Index;
            length = 3;

            if (match.Success)
                return startIndex;

            match = LocalPath.Match(text);

            startIndex = match.Index;
            length = 3;

            if (match.Success)
                return startIndex;

            return -1;
        }
       
        private IEnumerable<string> ParseText(string text)
        {
            List<string> links = new();

            if (text.Contains("\n") || text.Contains(";"))
            {
                var lines = text.Split('\r', '\n', ';').Where((x) => !string.IsNullOrEmpty(x));

                foreach (var line in lines)
                    ParseLine(line, links);
            }
            else
                ParseLine(text, links);

            links = links.Where((x) => !string.IsNullOrEmpty(x)).ToList();
            return links;
        }

        private void ParseLine(string line, List<string> result)
        {
            int startIndex = 0;
            int startElementLength = 0;

            try
            {
                while ((startIndex = GetNextIndex(line, startIndex + startElementLength, out startElementLength)) >= 0)
                {
                    int endIndex = GetNextIndex(line, startIndex + startElementLength, out var _);

                    if (endIndex < 0)
                        result.Add(line.Substring(startIndex));
                    else
                        result.Add(line.Substring(startIndex, endIndex - startIndex));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"---------------------------- Failed to parse line: {line} --------------------------");
                Debug.WriteLine(ex);
            }
        }
    }
}
