﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace CryDownloader.Logic
{
    static class DlcFormat
    {
        public const string JdServiceUrl = "http://service.jdownloader.org/dlcrypt/service.php";
        public const string RateLimitExceededKey = "2YVhzRFdjR2dDQy9JL25aVXFjQ1RPZ";
        public const int TempKeyLength = 88;

        // API Keys
        public static readonly byte[] AppSecret;
        public static readonly string ApplicationId;
        public static readonly string Revision;

        public static string DecodeDataString(string encodedString)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(encodedString));
        }
    }

    class DlcContainer : DownloadContainer
    {
        public DlcContainer(string filename, IEntryCreator creator)
        {
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                ReadContainer(fs, creator);
            }
        }

        public DlcContainer(Stream stream, IEntryCreator creator)
        {
            ReadContainer(stream, creator);
        }

        private void ReadContainer(Stream stream, IEntryCreator creator)
        {
            byte[] data = new byte[stream.Length];

            stream.Read(data, 0, data.Length);

            string dataStr = Encoding.UTF8.GetString(data);

            string key = dataStr.Substring(dataStr.Length - DlcFormat.TempKeyLength);
            dataStr = dataStr.Remove(dataStr.Length - DlcFormat.TempKeyLength);

            byte[] contentBuffer = Convert.FromBase64String(dataStr.Trim());
            byte[] tempKey = CallJDService(key);
            byte[] decryptionKey = CreateDecryptionKey(tempKey);
            string xml = DecryptContainer(contentBuffer, decryptionKey);
            XDocument document = CreateXmlDocument(xml);
            List<Uri> urls = new List<Uri>();

            var dlc = document.Element("dlc");

            if (dlc != null)
            {
                var content = dlc.Element("content");
                if (content != null)
                {
                    var packages = content.Elements("package");
                    foreach (var p in packages)
                    {
                        if (p == null)
                            continue;

                        var files = p.Elements("file");
                        foreach (var f in files)
                        {
                            if (f == null)
                                continue;

                            var url = f.Element("url");
                            if (url != null)
                                urls.Add(new Uri(url.Value, UriKind.Absolute));
                        }
                    }
                }
            }

        //    creator.CreateEntries(urls, this);
        }

        private static byte[] CallJDService(string data)
        {
            var ub = new UriBuilder(DlcFormat.JdServiceUrl);
            var query = HttpUtility.ParseQueryString(ub.Query);
            query["destType"] = "jdtc6";
            query["b"] = DlcFormat.ApplicationId;
            query["srcType"] = "dlc";
            query["data"] = data;
            query["v"] = DlcFormat.Revision;

            ub.Query = query.ToString();

            var msg = new HttpRequestMessage(HttpMethod.Post, ub.Uri);

            using (var cl = new HttpClient())
            {
                using (var res = cl.SendAsync(msg).Result)
                {
                    if (!res.IsSuccessStatusCode)
                        throw new DlcDecryptionException("Server responded with unsuccessful HTTP status code.");

                    var resContent = res.Content.ReadAsStringAsync().Result ?? string.Empty;

                    var match = Regex.Match(resContent, "<rc>(.*)</rc>");
                    if (!match.Success)
                        throw new DlcDecryptionException($"Server responded with non-XML content:{Environment.NewLine}{resContent}");

                    var tempKey = match.Groups[1]?.Value ?? string.Empty;
                    tempKey = tempKey.Trim();

                    if (tempKey == string.Empty)
                        throw new DlcDecryptionException("Server responded with empty decryption key.");

                    if (tempKey == DlcFormat.RateLimitExceededKey)
                        throw new DlcLimitExceededException();

                    return Convert.FromBase64String(tempKey);
                }
            }
        }

        private static byte[] CreateDecryptionKey(byte[] tempKey)
        {
            using (var aes = new AesManaged())
            {
                aes.Key = DlcFormat.AppSecret;
                aes.Mode = CipherMode.ECB;
                aes.Padding = PaddingMode.None;

                using (var dec = aes.CreateDecryptor())
                {
                    var res = dec.TransformFinalBlock(tempKey, 0, tempKey.Length);
                    var b64Str = Encoding.UTF8.GetString(res);
                    b64Str = b64Str.TrimEnd('\0');
                    return Convert.FromBase64String(b64Str); ;
                }
            }
        }

        private static string DecryptContainer(byte[] containerContent, byte[] key)
        {
            byte[] decContainer;

            try
            {
                decContainer = DecryptWithPadding(key, PaddingMode.PKCS7, containerContent);
            }
            catch (CryptographicException)
            {
                decContainer = DecryptWithPadding(key, PaddingMode.None, containerContent);
            }

            var str = Encoding.UTF8.GetString(decContainer).TrimEnd('\0');

            return DlcFormat.DecodeDataString(str);
        }

        private static byte[] DecryptWithPadding(byte[] ivPlusKey, PaddingMode paddingMode, byte[] data)
        {
            using (var aes = new AesManaged())
            {
                aes.Key = aes.IV = ivPlusKey;
                aes.Mode = CipherMode.CBC;
                aes.Padding = paddingMode;

                using (var dec = aes.CreateDecryptor())
                    return dec.TransformFinalBlock(data, 0, data.Length);
            }
        }

        private static XDocument CreateXmlDocument(string xml)
        {
            var doc = XDocument.Parse(xml);
            DecodeXmlDataForElement(doc.Element("dlc"));
            return doc;
        }

        private static void DecodeXmlDataForElement(XElement element)
        {
            if (element == null)
                return;

            foreach (var attr in element.Attributes())
                attr.Value = DlcFormat.DecodeDataString(attr.Value);

            foreach (var sub in element.Elements())
            {
                if (!sub.HasElements && !sub.IsEmpty)
                    sub.Value = DlcFormat.DecodeDataString(sub.Value);
                DecodeXmlDataForElement(sub);
            }
        }
    }
}
