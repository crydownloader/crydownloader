﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Logic
{
    public class JobTaskManager
    {
        private Queue<IScanJobBase> m_scanJobs;
        private Queue<IScanJobBase> m_highPrioScanJobs;

        private object m_syncObject;
        private List<IScanJobBase> m_executedJobs;
        private ManualResetEvent m_manualReset;
        private Thread[] m_threads;
        private Thread m_sidekickThread;
        private Settings m_settings;

        public bool IsRunning { get; private set; }

        public int JobCount => m_scanJobs.Count + m_highPrioScanJobs.Count;

        public JobTaskManager(Settings settings)
        {
            m_scanJobs = new Queue<IScanJobBase>();
            m_syncObject = new object();
            m_executedJobs = new List<IScanJobBase>();
            m_highPrioScanJobs = new Queue<IScanJobBase>();

            m_manualReset = new ManualResetEvent(false);
            m_settings = settings;
            m_settings.ScanCountChanged += OnScanCountChanged;

            IsRunning = true;

            m_threads = new Thread[m_settings.ScanCount];
            m_sidekickThread = new Thread(ProcessSidekickThread);
            m_sidekickThread.Start();

            for (int i = 0; i < m_threads.Length; ++i)
            {
                m_threads[i] = new Thread(ProcessThread);
                m_threads[i].Start();
            }
        }

        private void OnScanCountChanged(object sender, EventArgs e)
        {
            if (m_threads == null)
                return;

            if (m_settings.ScanCount != m_threads.Length )
            {
                int idx = m_threads.Length;

                if (m_threads == null)
                    m_threads = new Thread[m_settings.ScanCount];
                else
                    Array.Resize(ref m_threads, m_settings.ScanCount);

                for (int i = idx; i < m_threads.Length; ++i)
                {
                    m_threads[i] = new Thread(ProcessThread);
                    m_threads[i].Start();
                }
            }
        }

        public void Stop()
        {
            IsRunning = false;

            lock (m_executedJobs)
            {
                foreach (var job in m_executedJobs)
                    job.Abort();
            }

            m_manualReset.Set();

            foreach (var thread in m_threads)
                thread.Join();
        }

        public void Add(IScanJob job)
        {
            Add(job, false);
        }

        public void Add(IScanJob job, bool highPrio)
        {
            job.State = ScanJobState.Wait;

            lock (m_syncObject)
            {
                if (highPrio)
                    m_highPrioScanJobs.Enqueue(job);
                else
                    m_scanJobs.Enqueue(job);
            }

            if (JobCount > 0)
                m_manualReset.Set();
        }

        public void Add(IChildScanJob job)
        {
            Add(job, false);
        }

        public void Add(IChildScanJob job, bool highPrio)
        {
            lock (m_syncObject)
            {
                if (highPrio)
                    m_highPrioScanJobs.Enqueue(job);
                else
                    m_scanJobs.Enqueue(job);
            }

            if (JobCount > 0)
                m_manualReset.Set();
        }

        private void ProcessThread()
        {
            while (IsRunning)
            {
                m_manualReset.WaitOne();
                m_manualReset.Reset();

                if (m_settings.ScanCount <= 0)
                    return;

                IScanJobBase job = GetNextJob();

                if (job != null)
                {
                    lock (m_executedJobs)
                        m_executedJobs.Add(job);

                    job.Scan(this);

                    lock (m_executedJobs)
                        m_executedJobs.Remove(job);
                }

                if (JobCount > 0)
                    m_manualReset.Set();
            }
        }

        private void ProcessSidekickThread()
        {
            while (IsRunning)
            {
                m_manualReset.WaitOne();
                m_manualReset.Reset();

                if (m_settings.ScanCount <= 0)
                    return;

                IScanJobBase job = GetNextHighPrioJob();

                if (job != null)
                {
                    lock (m_executedJobs)
                        m_executedJobs.Add(job);

                    job.Scan(this);

                    lock (m_executedJobs)
                        m_executedJobs.Remove(job);
                }

                if (m_highPrioScanJobs.Count > 0)
                    m_manualReset.Set();
            }
        }

        private IScanJobBase GetNextHighPrioJob()
        {
            IScanJobBase job = null;

            lock (m_syncObject)
            {
                if (m_highPrioScanJobs.Count > 0)
                    job = m_highPrioScanJobs.Dequeue();
            }

            return job;
        }

        private IScanJobBase GetNextJob()
        {
            IScanJobBase job = null;

            lock (m_syncObject)
            {
                if (JobCount > 0)
                {
                    if (m_highPrioScanJobs.Count > 0)
                        job = m_highPrioScanJobs.Dequeue();
                    else
                        job = m_scanJobs.Dequeue();
                }
            }

            return job;
        }
    }
}
