﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace CryDownloader.Logic
{
    public class PluginManager<T> where T : IPlugin
    {
        class PluginInitState
        {
            public ManualResetEvent InitEvent { get; }
            public bool IsInitialized { get; set; }

            public PluginInitState()
            {
                InitEvent = new ManualResetEvent(false);
            }
        }

        public Dictionary<string, T> Plugins { get; }

        protected IApplication Application { get; }

        private readonly Thread m_pluginInitThread;
        private readonly Queue<T> m_initQueue;
        private readonly ManualResetEvent m_condition;
        private readonly Dictionary<T, PluginInitState> m_pluginInitStates;

        public PluginManager(IApplication application)
        {
            Application = application;
            m_pluginInitStates = new Dictionary<T, PluginInitState>();
            Plugins = new Dictionary<string, T>();
            m_condition = new ManualResetEvent(false);
            m_initQueue = new Queue<T>();
            m_pluginInitThread = new Thread(ProcessPluginInit);
            m_pluginInitThread.Start();
        }

        public bool AddPlugin(string pluginPackage, string pluginDirectory)
        {
            // TODO: create plugin package

            if (LoadPlugin(pluginDirectory, false))
            {
                Application.LogManager.WriteLog(LogLevel.Hard, $"Plugin '{pluginPackage}' loaded.");
                return true;
            }

            Application.LogManager.WriteLog(LogLevel.Normal, $"failed to add '{pluginPackage}'");
            return false;
        }

        public bool RemovePlugin(T host)
        {
            return Plugins.Remove(host.Name);
        }

        public bool RemovePlugin(string plugin)
        {
            return Plugins.Remove(plugin);
        }

        public bool IsInitialized(T plugin)
        {
            lock (m_pluginInitStates)
            {
                if (!m_pluginInitStates.ContainsKey(plugin))
                    return false;

                return m_pluginInitStates[plugin].IsInitialized;
            }
        }

        public void WaitUntilInitialized(T plugin)
        {
            PluginInitState state;

            lock (m_pluginInitStates)
            {
                m_pluginInitStates.TryGetValue(plugin, out state);
            }

            if (state != null)
            {
                if (!state.IsInitialized)
                    state.InitEvent.WaitOne();
            }
        }

        public void Destroy()
        {
            if (m_pluginInitThread.IsAlive)
                m_pluginInitThread.Join();

            foreach (var plugin in Plugins)
                plugin.Value.Destroy();
        }

        private void ProcessPluginInit()
        {
            Queue<T> queue = new Queue<T>();

            m_condition.WaitOne();

            lock (m_initQueue)
            {
                while (m_initQueue.Count > 0)
                    queue.Enqueue(m_initQueue.Dequeue());
            }

            while (queue.Count > 0)
            {
                T data = queue.Dequeue();

                DateTime startTime = DateTime.Now;
                data.Initialize();
                var dif = DateTime.Now - startTime;
                Application.LogManager.WriteLog(LogLevel.Hard, $"Plugin for '{data.Name}' loaded in {dif.TotalMilliseconds} ms.");

                PluginInitState state;

                lock (m_pluginInitStates)
                {
                    if (!m_pluginInitStates.TryGetValue(data, out state))
                    {
                        state = new PluginInitState();
                        m_pluginInitStates.Add(data, state);
                    }
                }

                state.IsInitialized = true;
                state.InitEvent.Set();
            }
        }

        private bool LoadPlugin(string strDirectory, bool asyncInit)
        {
            if (!Directory.Exists(strDirectory))
            {
                Application.LogManager.WriteLog(LogLevel.Normal, $"Directory '{strDirectory}' not found.");
                return false;
            }

            string[] files = Directory.GetFiles(strDirectory, "*.dll", SearchOption.TopDirectoryOnly);
            // AppDomain pluginDomain = CreateAppDomain(strDirectory);
            bool result = false;
            Type pluginType = typeof(T);

            foreach (var file in files)
            {
                try
                {
                    Assembly assembly = Assembly.LoadFrom(Path.GetFullPath(file));

                    var asemblies = AppDomain.CurrentDomain.GetAssemblies();
                    var ty = assembly.GetTypes();
                    var types = (from t in assembly.GetTypes()
                                 where pluginType.IsAssignableFrom(t)
                                 select t).ToList();

                    if (types != null && types.Count > 0)
                    {
                        foreach (var type in types)
                        {
                            T plugin = (T)Activator.CreateInstance(type, Application);

                            if (!Plugins.ContainsKey(plugin.Name))
                            {
                                result = true;
                                Plugins.Add(plugin.Name, plugin);

                                OnLoadPlugin(plugin);

                                if (asyncInit)
                                {
                                    lock (m_initQueue)
                                    {
                                        m_initQueue.Enqueue(plugin);
                                    }
                                }
                                else
                                {
                                    DateTime startTime = DateTime.Now;
                                    plugin.Initialize();
                                    var dif = DateTime.Now - startTime;
                                    Application.LogManager.WriteLog(LogLevel.Hard, $"Plugin for '{plugin.Name}' loaded in {dif.TotalMilliseconds} ms.");

                                    lock (m_pluginInitStates)
                                    {
                                        m_pluginInitStates[plugin] = new PluginInitState();
                                    }
                                }
                            }
                            else
                                Application.LogManager.WriteLog(LogLevel.Normal, $"Plugin for '{plugin.Name}' already loaded.");
                        }
                    }
                    else
                        Application.LogManager.WriteLog(LogLevel.Normal, $"Plugin implementation not found in '{file}'");
                }
                catch (Exception ex)
                {
                    Application.LogManager.WriteException(LogLevel.Low, $"Error by loading '{file}'", ex);
                }
            }

            return result;
        }

        protected void LoadPlugins(string[] directories, bool async = true)
        {
            foreach (var dir in directories)
                LoadPlugin(dir, async);
        }

        protected virtual void OnLoadPlugin(T plugin)
        {

        }

        protected void EnablePluginInitialization()
        {
            m_condition.Set();
        }
    }
}
