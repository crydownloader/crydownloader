﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CryDownloader.Logic
{
    public interface IEntryCreator
    {
        void CreateEntries(IEnumerable<Uri> urls, IDownloadContainerView container,
            Action<IContainer, DownloadEntry> entryAddedCallback, Action<Uri> scanUrlChangedCallback, CancellationToken? token);
    }
}
