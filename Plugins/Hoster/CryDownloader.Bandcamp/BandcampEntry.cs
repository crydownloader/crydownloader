﻿using CryDownloader.Plugin;
using System;
using System.IO;

namespace CryDownloader.Bandcamp
{
    class BandcampEntry : DownloadEntry
    {
        public BandcampEntry(Bandcamp hoster, string fileType, string url, string title)
            : base(hoster)
        {
            string[] type = fileType.Split('-');

            if (type.Length > 1)
            {
                string str = string.Empty;

                for (int i = 1; i < type.Length; ++i)
                    str += type[i];

                FileName = title + "_" + str + "." + type[0];
            }
            else
                FileName = title + "." + type[0];

            Url = new Uri(url);
        }

        public BandcampEntry(Bandcamp hoster, string fileType, string url, string title, string albumTitle)
            : base(hoster)
        {
            string[] type = fileType.Split('-');

            if (type.Length > 1)
            {
                string str = string.Empty;

                for (int i = 1; i < type.Length; ++i)
                    str += type[i];

                FileName = Path.Combine(albumTitle, title + "_" + str + "." + type[0]);
            }
            else
                FileName = Path.Combine(albumTitle, title + "." + type[0]);

            Url = new Uri(url);
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }

        protected override void OnLoad(object data)
        {
            UpdateFileStateAndSize(Url);
        }
    }
}
