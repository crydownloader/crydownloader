﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;

namespace CryDownloader.Bandcamp
{
    public class Bandcamp : Hoster
    {
        #region Json Objects

        class BandcampJsonCurrentData
        {
            public string type { get; set; }
            public string title { get; set; }
        }

        class BandcampJsonTrackData
        {
            public Dictionary<string, string> file { get; set; }
            public string title { get; set; }
        }

        class BandcampJsonData
        {
            public BandcampJsonCurrentData current { get; set; }
            public string artist { get; set; }
            public List<BandcampJsonTrackData> trackinfo { get; set; }
        }

        class BandcampJsonAlbum
        {
            public string page_url { get; set; }
            public string band_name { get; set; }
        }

        #endregion

        public override string Name => "Bandcamp";

        public override string Host => "bandcamp.com";

        public override Bitmap Icon => Properties.Resources.bc_favicon;

        public override System.Windows.FrameworkElement SettingControl { get; }

        public Bandcamp(IApplication application)
            : base(application)
        { }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>();
        }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        { }


        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            CreateEntriesForAlbumOrTrack(args.Url, null, args.ContainerCreator, args.CancellationToken);
        }

        private string DownloadPage(Uri url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd().Replace("&quot;", "\"");
                    }
                }
            }
        }

        private void CreateEntriesForAlbumOrTrack(Uri url, IContainer container, IContainerCreator creator, CancellationToken? cancellationToken)
        {
            if (cancellationToken.HasValue)
            {
                if (cancellationToken.Value.IsCancellationRequested)
                    return;
            }

            string text = DownloadPage(url);

            Regex regex = new Regex(@"var\s?TralbumData\s?=\s?(?s)(.*)");
            var regexMatch = regex.Match(text);

            if (regexMatch.Success)
            {
                var json = regexMatch.Groups[1].Value.Replace("\" + \"", "").Replace("\"+\"", "");

                BandcampJsonData data = Application.LoadJsonObject<BandcampJsonData>(json);
                bool bAddAlbumName = false;
                if (container == null)
                    container = creator.CreateContainer(data.current.title);
                else
                    bAddAlbumName = true;

                foreach (var track in data.trackinfo)
                {
                    foreach (var file in track.file)
                    {
                        if (cancellationToken.HasValue)
                        {
                            if (cancellationToken.Value.IsCancellationRequested)
                                return;
                        }

                        BandcampEntry entry;

                        if (bAddAlbumName)
                            entry = new BandcampEntry(this, file.Key, file.Value, track.title, data.current.title);
                        else
                            entry = new BandcampEntry(this, file.Key, file.Value, track.title);

                        entry.Load(container);
                        container.AddEntry(entry.FileName, entry);
                    }
                }
            }
            else
            {
                regex = new Regex("data-edit-callback=\"/music_reorder\" data-initial-values=\"(?s)(.*)\">");
                regexMatch = regex.Match(text);
                var json = regexMatch.Groups[1].Value.Replace("\" + \"", "").Replace("\"+\"", "");
                List<BandcampJsonAlbum> data = Application.LoadJsonObject<List<BandcampJsonAlbum>>(json);

                foreach (var album in data)
                {
                    container = creator.CreateContainer(album.band_name);

                    CreateEntriesForAlbumOrTrack(new Uri(url, album.page_url), container, creator, cancellationToken);
                }
            }
        }
    }
}
