﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace CryDownloader.Twitter
{
    public class Twitter : Hoster
    {
        #region Json Objects

        class TwitterResponse
        {
            public bool has_more_items { get; set; }
            public string items_html { get; set; }
            public int new_latent_count { get; set; }

            public string min_position { get; set; }

            public string max_position { get; set; }

            public string position => min_position == null ? max_position : min_position;
        }

        class TwitterGuestToken
        {
            public string guest_token { get; set; }
        }

        class TwitterTweetTrackInfo
        {
            public string playbackUrl { get; set; }
            public string playbackType { get; set; }
        }

        class TwitterTweetInfo
        {
            public TwitterTweetTrackInfo track { get; set; }
        }

        #endregion

        public override string Name => "Twitter";

        public override string Host => "twitter.com";

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public override System.Windows.FrameworkElement SettingControl { get; }

        public Twitter(IApplication app)
            : base(app)
        { }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            bool isMediaTimeline = false;
            bool isRepliesTimeline = false;

            if (args.Url.Segments.Length < 1)
                return;

            string name = args.Url.Segments[1].Replace("/", "");

            IContainer container = args.ContainerCreator.CreateContainer(name);

            if (args.Url.Segments.Length > 2)
            {
                isMediaTimeline = args.Url.Segments[2] == "media";
                isRepliesTimeline = args.Url.Segments[2] == "with_replies";
            }

            if (isMediaTimeline)
                GetTweets(name, "media_timeline", container, args.CancellationToken);
            else if (isRepliesTimeline)
                GetTweets(name, "timeline/with_replies", container, args.CancellationToken);
            else
                GetTweets(name, "timeline/tweets", container, args.CancellationToken);
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>();
        }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        { }

        private void GetTweets(string profileName, string timeline, IContainer container, CancellationToken? cancellationToken)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("accept", "application/json, text/javascript, */*; q=0.01");

                GetTweets(profileName, timeline, container, "0", client, cancellationToken);
            }
        }

        private T SerializeResponse<T>(string str) where T : TwitterResponse
        {
            return Application.LoadJsonObject<T>(str);
        }

        private void GetTweets(string profileName, string timeline, IContainer container, string lastTweetID, WebClient webClient, CancellationToken? cancellationToken)
        {
            string str;

            if (lastTweetID == "0" || string.IsNullOrEmpty(lastTweetID))
                str = webClient.DownloadString($"https://twitter.com/i/profiles/show/{profileName}/{timeline}?include_available_features=1&include_entities=1&min_position={lastTweetID}&reset_error_state=false");
            else
                str = webClient.DownloadString($"https://twitter.com/i/profiles/show/{profileName}/{timeline}?include_available_features=1&include_entities=1&max_position={lastTweetID}&reset_error_state=false");

            TwitterResponse response = SerializeResponse<TwitterResponse>(str);
            lastTweetID = response.position;

            IHtmlDocument document = Application.LoadHtmlDocument(System.Uri.UnescapeDataString(response.items_html));

            var nodes = document.DocumentNode.SelectNodes("/li");
            List<TwitterEntry> videos = new List<TwitterEntry>();

            foreach (var node in nodes)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                    {
                        GetVideoUrls(container, videos);
                        return;
                    }
                }

                if (node.Attributes.Contains("data-item-id"))
                {
                    string id = node.Attributes["data-item-id"].Value;
                    var n = node.SelectNodes(".//div[@class='AdaptiveMedia-photoContainer js-adaptive-photo ']");

                    if (n != null && n.Count > 0)
                    {

                        foreach (var pic in n)
                        {
                            if (pic.Attributes.Contains("data-image-url"))
                            {
                                var url = pic.Attributes["data-image-url"].Value;

                                var entry = new TwitterEntry(this, new Uri(url), id);

                                entry.Load(container);
                                container.AddEntry(entry.FileName, entry);
                            }
                        }
                    }
                    else
                        videos.Add(new TwitterEntry(this, id));
                }
            }

            GetVideoUrls(container, videos);

            if (cancellationToken.HasValue)
            {
                if (cancellationToken.Value.IsCancellationRequested)
                    return;
            }

            if (response.has_more_items)
                GetTweets(profileName, timeline, container, lastTweetID, webClient, cancellationToken);
        }

        private void GetVideoUrls(IContainer container, List<TwitterEntry> input)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.twitter.com/1.1/guest/activate.json");
            request.Headers.Add("authorization", "Bearer AAAAAAAAAAAAAAAAAAAAAPYXBAAAAAAACLXUNDekMxqa8h%2F40K4moUkGsoc%3DTYfbDKbT3jJPCEVnMYqilB28NHfOPqkca3qaAxGfsyKCs0wRbw");

            request.Method = "POST";

            string guestToken;


            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        TwitterGuestToken token = Application.LoadJsonObject<TwitterGuestToken>(reader);
                        guestToken = token.guest_token;
                    }
                }
            }

            using (var webClient = new WebClient())
            {
                webClient.Headers.Add("authorization", "Bearer AAAAAAAAAAAAAAAAAAAAAPYXBAAAAAAACLXUNDekMxqa8h%2F40K4moUkGsoc%3DTYfbDKbT3jJPCEVnMYqilB28NHfOPqkca3qaAxGfsyKCs0wRbw");
                webClient.Headers.Add("x-guest-token", guestToken);

                foreach (var entry in input)
                {
                    string str = webClient.DownloadString($"https://api.twitter.com/1.1/videos/tweet/config/{entry.TweetID}.json");

                    TwitterTweetInfo token = Application.LoadJsonObject<TwitterTweetInfo>(str);

                    entry.SetUrl(new Uri(token.track.playbackUrl));
                    entry.IsStream = token.track.playbackType == "application/x-mpegURL";

                    var fileInfo = new FileInfo(entry.Url.AbsolutePath);

                    if (entry.IsStream)
                        entry.FileName = fileInfo.Name.Replace(fileInfo.Extension, ".mp4");
                    else
                        entry.FileName = fileInfo.Name;

                    entry.Load(container);
                    container.AddEntry(entry.FileName, entry);
                }
            }
        }
    }
}
