﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace CryDownloader.Twitter
{
    class TwitterEntry : DownloadEntry
    {
        public string TweetID { get; }

        public bool IsPicture { get; }

        public bool IsStream { get; set; }

        private Twitter m_hoster;

        public TwitterEntry(Twitter hoster, Uri url, string tweetID)
            : base(hoster)
        {
            IsPicture = true;
            m_hoster = hoster;
            TweetID = tweetID;
            Url = url;
            FileName = new FileInfo(Url.AbsolutePath).Name;
        }

        public TwitterEntry(Twitter hoster, string tweetID)
            : base(hoster)
        {
            IsPicture = false;
            m_hoster = hoster;
            TweetID = tweetID;
        }

        protected override void OnLoad(object data)
        {
            if (!IsStream)
                FileSize = GetFileSize(Url);
            else
            {
                using (var webClient = new WebClient())
                {
                    string str = webClient.DownloadString(Url);

                    M3UPlaylist playlist = LoadM3UPlaylist(str);

                    M3UFile bestUrl = null;
                    int bestHeight = 0;
                    List<Tuple<int, M3UFile>> bestHeights = new List<Tuple<int, M3UFile>>();

                    foreach (var url in playlist.Files)
                    {
                        string[] splt = url.MetaData["#EXT-X-STREAM-INF"]["RESOLUTION"].Value.Split('x');

                        if (splt.Length != 2)
                            continue;

                        int width = int.Parse(splt[0]);
                        int height = int.Parse(splt[1]);

                        if (height > bestHeight)
                        {
                            bestHeight = height;
                            bestHeights.Add(new Tuple<int, M3UFile>(width, url));
                        }
                    }

                    int bestWidth = 0;

                    foreach (var url in bestHeights)
                    {
                        if (url.Item1 > bestWidth)
                        {
                            bestWidth = url.Item1;
                            bestUrl = url.Item2;
                        }
                    }

                    if (bestUrl == null)
                        throw new NotSupportedException();

                    str = webClient.DownloadString(new Uri(Url, bestUrl.Path));

                    playlist = LoadM3UPlaylist(str);

                    long fileSize = 0;

                    foreach (var mediaUrl in playlist.Files)
                        fileSize += GetFileSize(new Uri(Url, mediaUrl.Path));

                    FileSize = fileSize;
                }
            }

            if (FileSize != 0)
                FileState = GetFileState(FileStateType.FileInfoAvail);
            else
                FileState = GetFileState(FileStateType.FileNotFound);
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }

        public override bool StartDownload(string path)
        {
            if (!IsStream)
                return DownloadFile(Url, path);
            else
            {
                // TODO: download all parts and muxing them
                return false;
            }
        }

        public void SetUrl(Uri url)
        {
            Url = url;
        }

        public override string ToString()
        {
            return TweetID;
        }
    }
}
