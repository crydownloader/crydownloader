﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.DirectUpload
{
    public class DirectUpload : PictureHoster, IShareHoster
    {
        public override string Name => "Directupload";

        public override string Host => "directupload.net";

        public override Bitmap Icon => Properties.Resources.favicon;

        public DirectUpload(IApplication app)
           : base(app)
        { }

        public override void Initialize()
        { }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1].Replace(".htm", "");
            int idx = filename.LastIndexOf("_");

            if (idx != -1)
            {
                filename = filename.Insert(idx, "/").Remove(idx + 1, 1).Remove(0, 2);
                IContainer container = args.ContainerCreator.CreateContainer(filename);
                CreateEntry(args.Url, container, args.CancellationToken);
            }
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename;

            if (url.Segments[1] == "images/")
            {
                if (url.LocalPath.Contains("temp/"))
                {
                    string fixedPath = url.PathAndQuery.Replace("temp/", "");
                    url = new Uri(url, fixedPath);
                }

                filename = url.Segments[url.Segments.Length - 1];
            }
            else
            {
                filename = url.Segments[url.Segments.Length - 1].Replace(".htm", "");
                int idx = filename.LastIndexOf("_");

                if (idx != -1)
                    filename = filename.Insert(idx, "/").Remove(idx + 1, 1).Remove(0, 2);
            }

            var entry = new DirectUploadEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }
    }
}
