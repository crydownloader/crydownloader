﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.Threading;
using System.Net;
using System.IO;

namespace CryDownloader.Imageshack
{
    public class Imageshack : PictureHoster, IShareHoster
    {
        public override string Name => "Imageshack";

        public override string Host => "imageshack.com";

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public Imageshack(IApplication app)
            : base(app)
        { }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            IContainer container = args.ContainerCreator.CreateContainer(args.Url.Segments[args.Url.Segments.Length - 1]);
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        public bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];
            var entry = new ImageshackEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }
    }
}
