﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Imageshack
{
    class ImageshackEntry : DownloadEntry
    {
        private bool m_exists;

        public ImageshackEntry(Imageshack hoster, Uri url, string fileName)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        protected override void OnLoad(object data)
        {
            try
            {
                string strHtml = Hoster.DownloadPage(Url);

                if (string.IsNullOrEmpty(strHtml))
                    FileState = GetFileState(FileStateType.FileNotFound);
                else
                {
                    IHtmlDocument document = Hoster.Application.LoadHtmlDocument(strHtml);
                    IHtmlNode picNode = document.GetElementbyId("lp-image");

                    if (picNode == null)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else if (picNode.Attributes.Contains("src"))
                    {
                        Url = new Uri(Url, "/" + picNode.Attributes["src"].Value);

                        UpdateFileStateAndSize(Url);
                    }
                    else
                        FileState = GetFileState(FileStateType.FileNotFound);
                }
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }
    }
}
