﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.ImageUpper
{
    class ImageUpperEntry : DownloadEntry
    {
        public ImageUpperEntry(ImageUpper hoster, Uri url, string fileName)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        private void Resolve(Uri url)
        {
            string strHtml = Hoster.DownloadPage(url);

            if (string.IsNullOrEmpty(strHtml))
                FileState = GetFileState(FileStateType.FileNotFound);
            else
            {
                IHtmlDocument document = Hoster.Application.LoadHtmlDocument(strHtml);
                IHtmlNode picNode = document.GetElementbyId("img");

                if (picNode == null)
                    FileState = GetFileState(FileStateType.FileNotFound);
                else if (picNode.Attributes.Contains("src"))
                {
                    Url = new Uri(picNode.Attributes["src"].Value);

                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
                else
                    FileState = GetFileState(FileStateType.FileNotFound);
            }
        }

        protected override void OnLoad(object data)
        {
            try
            {
                if (Url.Segments[1] == "i/")
                    Resolve(Url);
                else
                {
                    UpdateFileStateAndSize(Url);
                }
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }
    }
}
