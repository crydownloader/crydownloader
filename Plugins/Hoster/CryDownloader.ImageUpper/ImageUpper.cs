﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.ImageUpper
{
    public class ImageUpper : PictureHoster, IShareHoster
    {
        public override string Name => "Imageupper";

        public override string Host => "imageupper.com";

        public override Bitmap Icon => Properties.Resources.logo;

        public ImageUpper(IApplication app)
           : base(app)
        { }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return base.IsValidUrl(url);
        }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1].Replace(".html", "");
            IContainer container = args.ContainerCreator.CreateContainer(filename);
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];

            if (url.Segments[1].Contains("_t/"))
            {
                int idx = url.Host.IndexOf(".");

                if (idx != -1)
                {
                    string sub = url.Host.Substring(0, idx);

                    string fixedPath = url.PathAndQuery.Replace(url.Segments[1], url.Segments[1].Replace("_t", ""));
                    url = new Uri($"{url.Scheme}://{Host}/{sub}{fixedPath}");
                }
                else if (NoThumb)
                    return false;
            }

            var entry = new ImageUpperEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }
    }
}
