﻿using CryDownloader.Plugin;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace CryDownloader.Mixcloud
{
    class MixcloudEntry : DownloadEntry
    {
        private readonly UrlFileType m_urlFileType;

        public MixcloudEntry(Hoster hoster, string title, Uri url, UrlFileType urlFileType)
            : base(hoster)
        {
            m_urlFileType = urlFileType;

            Url = url;

            if (urlFileType == UrlFileType.M4A)
                FileName = title + new FileInfo(url.LocalPath).Extension;
            else
                FileName = title + ".m4a";
        }

        public override bool CheckStream(Stream stream)
        {
            return false;
        }

        protected override void OnLoad(object data)
        {
            switch (m_urlFileType)
            {
                case UrlFileType.M3U:
                    LoadInfoFromM3U();
                    break;
                case UrlFileType.MPD:
                    LoadInfoFromMPD();
                    break;
                case UrlFileType.M4A:
                    LoadInfoFromM4A();
                    break;
            }
        }

        private void LoadInfoFromM3U()
        {
            try
            {
                M3UPlaylist playList;

                var request = (HttpWebRequest)WebRequest.Create(Url);

                using (var response = request.GetResponse())
                {
                    using (var s = response.GetResponseStream())
                    {
                        using (var r = new StreamReader(s))
                        {
                            playList = LoadM3UPlaylist(r);
                        }
                    }
                }

                long fileSize = 0;

                foreach (var file in playList.Files)
                {
                    Debug.WriteLine(file.Path);
                    fileSize += GetFileSize(new Uri(file.Path));
                }

                FileSize = fileSize;

                FileSizeType = FileSizeType.Relative;

                if (FileSize == 0)
                    FileState = GetFileState(FileStateType.FileInfoNotAvail);
                else
                    FileState = GetFileState(FileStateType.FileInfoAvail);
            }
            catch (Exception ex)
            {
                Hoster.Application.LogManager.WriteException(LogLevel.Normal, "failed to get file informations", ex);
                FileState = GetFileState(FileStateType.FileNotFound);
            }
        }

        private void LoadInfoFromM4A()
        {
            try
            {
                FileSize = GetFileSize(Url);

                if (FileSize == 0)
                    FileState = GetFileState(FileStateType.FileInfoNotAvail);
                else
                    FileState = GetFileState(FileStateType.FileInfoAvail);
            }
            catch (Exception ex)
            {
                Hoster.Application.LogManager.WriteException(LogLevel.Normal, "failed to get file informations", ex);
                FileState = GetFileState(FileStateType.FileNotFound);
            }
        }

        private void LoadInfoFromMPD()
        {
            FileState = GetFileState(FileStateType.FileInfoNotAvail);
        }

        public override bool StartDownload(string path)
        {
            switch (m_urlFileType)
            {
                case UrlFileType.M3U:
                case UrlFileType.MPD:
                    return Muxing(Url.ToString(), path);
                case UrlFileType.M4A:
                    return DownloadFile(Url, path);
            }

            return false;
        }
    }
}
