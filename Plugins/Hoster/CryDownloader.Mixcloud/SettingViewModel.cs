﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CryDownloader.Mixcloud
{
    class SettingViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Mixcloud Hoster { get; }

        public FileTypeViewModel PreferredType { get; set; }

        public ObservableCollection<FileTypeViewModel> FileTypes { get; }

        public SettingViewModel(Mixcloud hoster)
        {
            Hoster = hoster;
            FileTypes = new ObservableCollection<FileTypeViewModel>();

            Array values = Enum.GetValues(typeof(UrlFileType));

            foreach (UrlFileType val in values)
            {
                FileTypeViewModel viewModel = new FileTypeViewModel(val);

                if (val == hoster.PreferredType)
                    PreferredType = viewModel;

                FileTypes.Add(viewModel);
            }
        }

        public void Apply()
        {
            Hoster.PreferredType = PreferredType.Data;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
