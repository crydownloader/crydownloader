﻿using System.Windows.Controls;

namespace CryDownloader.Mixcloud
{
    /// <summary>
    /// Interaktionslogik für Settings.xaml
    /// </summary>
    internal partial class Settings : UserControl
    {
        public SettingViewModel ViewModel { get; }

        public Settings(SettingViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
        }
    }
}
