﻿namespace CryDownloader.Mixcloud
{
    public enum UrlFileType
    {
        None,
        M3U,
        MPD,
        M4A,
    }
}
