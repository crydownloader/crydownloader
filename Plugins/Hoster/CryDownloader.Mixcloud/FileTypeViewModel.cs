﻿namespace CryDownloader.Mixcloud
{
    class FileTypeViewModel
    {
        public UrlFileType Data { get; }

        public FileTypeViewModel(UrlFileType data)
        {
            Data = data;
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
