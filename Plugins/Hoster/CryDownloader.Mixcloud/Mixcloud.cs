﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace CryDownloader.Mixcloud
{
    public class Mixcloud : Hoster
    {
        #region Json Objects

        class MixcloudJsonStreamInfoResult
        {
            public string hlsUrl { get; set; }
            public string dashUrl { get; set; }
            public string url { get; set; }
        }

        class MixcloudJsonUserUploadEdgeNodeResult
        {
            public MixcloudJsonStreamInfoResult streamInfo { get; set; }
            public string name { get; set; }
        }

        class MixcloudJsonCloudcastEdgeNodeResult
        {
            public MixcloudJsonUserUploadEdgeNodeResult cloudcast { get; set; }
        }

        class MixcloudJsonUserUploadEdgeResult<T>
        {
            public T node { get; set; }
        }

        class MixcloudJsonUserUploadPageInfoResult
        {
            public string endCursor { get; set; }
            public bool hasNextPage { get; set; }
        }

        class MixcloudJsonUserUploadsResult<T>
        {
            public List<MixcloudJsonUserUploadEdgeResult<T>> edges { get; set; }
            public MixcloudJsonUserUploadPageInfoResult pageinfo { get; set; }
        }

        class MixcloudJsonUserLookupResult
        {
            public string id { get; set; }
            public string username { get; set; }
            public MixcloudJsonUserUploadsResult<MixcloudJsonUserUploadEdgeNodeResult> uploads { get; set; }
        }

        class MixcloudJsonPlaylistLookupResult
        {
            public string id { get; set; }
            public string name { get; set; }
            public MixcloudJsonUserUploadsResult<MixcloudJsonCloudcastEdgeNodeResult> playlistItems { get; set; }
        }

        class MixcloudJsonDataResult
        {
            public MixcloudJsonUserLookupResult userLookup { get; set; }
            public MixcloudJsonUserUploadEdgeNodeResult cloudcastLookup { get; set; }
            public MixcloudJsonPlaylistLookupResult playlistLookup { get; set; }
        }

        class MixcloudJsonResult
        {
            public MixcloudJsonDataResult data { get; set; }
        }

        #endregion

        private readonly byte[] UrlDecryptKey = new byte[] { 0x49, 0x46, 0x59, 0x4F, 0x55, 0x57, 0x41, 0x4E, 0x54, 0x54, 0x48, 0x45, 0x41,
            0x52, 0x54, 0x49, 0x53, 0x54, 0x53, 0x54, 0x4F, 0x47, 0x45, 0x54, 0x50, 0x41, 0x49, 0x44, 0x44, 0x4F, 0x4E, 0x4F, 0x54, 0x44,
            0x4F, 0x57, 0x4E, 0x4C, 0x4F, 0x41, 0x44, 0x46, 0x52, 0x4F, 0x4D, 0x4D, 0x49, 0x58, 0x43, 0x4C, 0x4F, 0x55, 0x44 };

        public override string Name => "Mixcloud";

        public override string Host => "mixcloud.com";

        public UrlFileType PreferredType { get; set; }

        public override System.Drawing.Bitmap Icon => Properties.Resources.touch_icon_120;

        public override System.Windows.FrameworkElement SettingControl => m_settingControl;

        private string m_csrftoken;
        private System.Windows.FrameworkElement m_settingControl;

        public Mixcloud(IApplication app)
            : base(app)
        {
            m_csrftoken = string.Empty;
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>()
            {
                { "PreferredType", ((int)PreferredType).ToString() }
            };
        }

        public override void Initialize()
        {
            SettingViewModel viewModel = new SettingViewModel(this);
            Application.Dispatcher.Invoke(() => m_settingControl = new Settings(viewModel));
        }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            if (properties.TryGetValue("PreferredType", out var value))
            {
                if (int.TryParse(value, out var type))
                    PreferredType = (UrlFileType)type;
            }
        }

        protected override void OnCreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            if (string.IsNullOrEmpty(m_csrftoken))
            {
                var mainUrl = new Uri("https://www.mixcloud.com/");
                var request = (HttpWebRequest)WebRequest.Create(mainUrl);

                var response = request.GetResponse();

                var cookieValues = response.Headers.GetValues("Set-Cookie");

                if (cookieValues == null)
                    cookieValues = response.Headers.GetValues("set-cookie");

                foreach (var strCookie in cookieValues)
                {
                    string[] splt = strCookie.Split(';');

                    Dictionary<string, string> dictCookie = new Dictionary<string, string>();

                    foreach (var s in splt)
                    {
                        var splt2 = s.Split('=');

                        if (splt2.Length == 2)
                            dictCookie.Add(splt2[0].Trim(), splt2[1]);
                        else
                            dictCookie.Add(splt2[0].Trim(), "");
                    }

                    if (dictCookie.TryGetValue("csrftoken", out var val))
                        m_csrftoken = val;
                }
            }

            foreach (var url in urls)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        break;
                }

                scanUrlChangedCallback?.Invoke(url);

                if (url.Segments.Length == 4)
                    CreateAndAddEntriesFromPlaylist(creator, url.Segments[1].Replace("/", ""), url.Segments[3].Replace("/", ""), cancellationToken);
                else if (url.Segments.Length == 3)
                    CreateAndAddEntryFromTrack(url.Segments[2].Replace("/", ""), url.Segments[1].Replace("/", ""), creator, cancellationToken);
                else if (url.Segments.Length == 2)
                    CreateAndAddEntriesFromUserUploads(creator, url.Segments[1].Replace("/", ""), cancellationToken);
            }
        }

        private void CreateAndAddEntriesFromUserUploads(IContainerCreator creator, string artistName, CancellationToken? cancellationToken)
        {
            var queryResult = ExecuteQuery($"{{\"id\":\"q25\", \"query\":\"query UserDefault($lookup_0:UserLookup!,$first_1:Int!,$lighten_2:Int!,$first_4:Int!,$first_5:Int!,$alpha_3:Float!,$orderBy_6:CloudcastOrderByEnum!) {{userLookup(lookup:$lookup_0) {{id,...Ft}}}} fragment F0 on Picture {{urlRoot,primaryColor}} fragment F1 on User {{id}} fragment F2 on User {{username,hasProFeatures,hasPremiumFeatures,isStaff,isSelect,id}} fragment F3 on Cloudcast {{isExclusive,isExclusivePreviewOnly,slug,id,owner {{username,id}}}} fragment F4 on CloudcastTag {{tag {{name,slug,isCategory,id}},position}} fragment F5 on Cloudcast {{_tags4ruy33:tags {{...F4}},id}} fragment F6 on Cloudcast {{restrictedReason,owner {{username,isSubscribedTo,isViewer,id}},slug,id,isAwaitingAudio,isDraft,isPlayable,streamInfo {{hlsUrl,dashUrl,url,uuid}},audioLength,currentPosition,proportionListened,repeatPlayAmount,seekRestriction,previewUrl,isExclusivePreviewOnly,isExclusive,picture {{primaryColor,isLight,_primaryColor2pfPSM:primaryColor(lighten:$lighten_2),_primaryColor3Yfcks:primaryColor(alpha:$alpha_3)}}}} fragment F7 on Cloudcast {{id,name,slug,owner {{id,username,displayName,isSelect,...F1,...F2}},isUnlisted,isExclusive,...F3,...F5,...F6}} fragment F8 on Cloudcast {{isDraft,hiddenStats,plays,publishDate,qualityScore,listenerMinutes,id}} fragment F9 on Cloudcast {{id,isFavorited,isPublic,hiddenStats,favorites {{totalCount}},slug,owner {{id,isFollowing,username,displayName,isViewer}}}} fragment Fa on Cloudcast {{id,isReposted,isPublic,hiddenStats,reposts {{totalCount}},owner {{isViewer,id}}}} fragment Fb on Cloudcast {{id,isUnlisted,isPublic}} fragment Fc on Cloudcast {{id,isUnlisted,isPublic,slug,description,picture {{urlRoot}},owner {{displayName,isViewer,username,id}}}} fragment Fd on Cloudcast {{id,isPublic,isHighlighted,owner {{isViewer,id}}}} fragment Fe on Cloudcast {{id,isPublic,isExclusive,owner {{id,username,isViewer,isSubscribedTo}},...F8,...F9,...Fa,...Fb,...Fc,...Fd}} fragment Ff on Cloudcast {{owner {{quantcastTrackingPixel,id}},id}} fragment Fg on Cloudcast {{id,slug,name,isAwaitingAudio,isDraft,isScheduled,restrictedReason,publishDate,waveformUrl,audioLength,owner {{username,id}},picture {{...F0}},...F7,...Fe,...Ff}} fragment Fh on Playlist {{name,slug,owner {{displayName,username,twitterAccount {{username}},id}},picture {{urlRoot}},id}} fragment Fi on Playlist {{id,slug,description,picture {{urlRoot}},owner {{displayName,username,id}}}} fragment Fj on Playlist {{id,name,slug,owner {{isViewer,id}},items {{totalCount}},...Fi}} fragment Fk on Playlist {{id,name,owner {{id,isViewer}},_items4ZPqV:items(first:$first_1) {{totalCount,edges {{node {{cloudcast {{id,...Fg}},id}},cursor}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}},...Fh,...Fj}} fragment Fl on PlaylistView {{playlist {{id,name,...Fk}}}} fragment Fm on Cloudcast {{id,streamInfo {{hlsUrl,dashUrl,url,uuid}},seekRestriction,audioLength,currentPosition,name,owner {{displayName,id}},picture {{urlRoot,primaryColor}}}} fragment Fn on User {{_drafts1ZnOCH:drafts(first:$first_5) {{edges {{node {{id,...Fg}},cursor}},pageInfo {{hasNextPage,hasPreviousPage}}}},id}} fragment Fo on User {{_highlighted3YoLrG:highlighted(first:$first_1) {{edges {{node {{id,...Fg}},cursor}},pageInfo {{hasNextPage,hasPreviousPage}}}},id}} fragment Fp on User {{id,displayName,username,isViewer,_favorites5eonc:favorites(first:$first_1) {{edges {{node {{id,favorites {{totalCount}},...Fg}},cursor}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}}}} fragment Fq on User {{id,isViewer,displayName,username,_listeningHistory4DgkNF:listeningHistory(first:$first_1) {{totalCount,edges {{cursor,node {{id,cloudcast {{id,...Fg}}}}}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}}}} fragment Fr on User {{id,displayName,username,_stream2KKTjD:stream(first:$first_1) {{edges {{cursor,repostedBy,node {{id,...Fg}}}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}}}} fragment Fs on User {{id,displayName,username,uploads:uploads(first:$first_1,orderBy:$orderBy_6) {{edges {{node {{id,...Fg}},cursor}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}}}} fragment Ft on User {{id,displayName,username,profileNavigation {{defaultView {{__typename,...Fl}}}},_highlighted3YoLrG:highlighted(first:$first_1) {{edges {{node {{id,...Fm}},cursor}},pageInfo {{hasNextPage,hasPreviousPage}}}},_drafts3vWlk9:drafts(first:$first_4) {{totalCount}},...Fn,...Fo,...Fp,...Fq,...Fr,...Fs}}\", \"variables\":{{\"lookup_0\":{{\"username\":\"{artistName}\"}},\"first_1\":10,\"lighten_2\":15,\"first_4\":1,\"first_5\":100,\"alpha_3\":0.3,\"orderBy_6\":\"LATEST\"}}}}");

            IContainer container = creator.CreateContainer(queryResult.data.userLookup.username);

            foreach (var edge in queryResult.data.userLookup.uploads.edges)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                AddEdgeToContainer(container, edge.node);
            }

            if (queryResult.data.userLookup.uploads.pageinfo.hasNextPage)
                CreateAndAddEntriesFromUserUploads(container, queryResult.data.userLookup.id, queryResult.data.userLookup.uploads.pageinfo.endCursor, cancellationToken);
        }

        private void CreateAndAddEntriesFromPlaylist(IContainerCreator creator, string artistName, string playlistName, CancellationToken? cancellationToken)
        {
            var queryResult = ExecuteQuery($"{{\"id\":\"q25\", \"query\":\"query Playlist($lookup_0:PlaylistLookup!,$first_1:Int!,$lighten_2:Int!,$alpha_3:Float!) {{playlistLookup(lookup:$lookup_0) {{id,...Fl}}}} fragment F0 on Picture {{urlRoot,primaryColor}} fragment F1 on User {{id}} fragment F2 on User {{username,hasProFeatures,hasPremiumFeatures,isStaff,isSelect,id}} fragment F3 on Cloudcast {{isExclusive,isExclusivePreviewOnly,slug,id,owner {{username,id}}}} fragment F4 on CloudcastTag {{tag {{name,slug,isCategory,id}},position}} fragment F5 on Cloudcast {{_tags4ruy33:tags {{...F4}},id}} fragment F6 on Cloudcast {{restrictedReason,owner {{username,isSubscribedTo,isViewer,id}},slug,id,isAwaitingAudio,isDraft,isPlayable,streamInfo {{hlsUrl,dashUrl,url,uuid}},audioLength,currentPosition,proportionListened,repeatPlayAmount,seekRestriction,previewUrl,isExclusivePreviewOnly,isExclusive,picture {{primaryColor,isLight,_primaryColor2pfPSM:primaryColor(lighten:$lighten_2),_primaryColor3Yfcks:primaryColor(alpha:$alpha_3)}}}} fragment F7 on Cloudcast {{id,name,slug,owner {{id,username,displayName,isSelect,...F1,...F2}},isUnlisted,isExclusive,...F3,...F5,...F6}} fragment F8 on Cloudcast {{isDraft,hiddenStats,plays,publishDate,qualityScore,listenerMinutes,id}} fragment F9 on Cloudcast {{id,isFavorited,isPublic,hiddenStats,favorites {{totalCount}},slug,owner {{id,isFollowing,username,displayName,isViewer}}}} fragment Fa on Cloudcast {{id,isReposted,isPublic,hiddenStats,reposts {{totalCount}},owner {{isViewer,id}}}} fragment Fb on Cloudcast {{id,isUnlisted,isPublic}} fragment Fc on Cloudcast {{id,isUnlisted,isPublic,slug,description,picture {{urlRoot}},owner {{displayName,isViewer,username,id}}}} fragment Fd on Cloudcast {{id,isPublic,isHighlighted,owner {{isViewer,id}}}} fragment Fe on Cloudcast {{id,isPublic,isExclusive,owner {{id,username,isViewer,isSubscribedTo}},...F8,...F9,...Fa,...Fb,...Fc,...Fd}} fragment Ff on Cloudcast {{owner {{quantcastTrackingPixel,id}},id}} fragment Fg on Cloudcast {{id,slug,name,isAwaitingAudio,isDraft,isScheduled,restrictedReason,publishDate,waveformUrl,audioLength,owner {{username,id}},picture {{...F0}},...F7,...Fe,...Ff}} fragment Fh on Playlist {{name,slug,owner {{displayName,username,twitterAccount {{username}},id}},picture {{urlRoot}},id}} fragment Fi on Playlist {{id,slug,description,picture {{urlRoot}},owner {{displayName,username,id}}}} fragment Fj on Playlist {{id,name,slug,owner {{isViewer,id}},items {{totalCount}},...Fi}} fragment Fk on Playlist {{id,name,owner {{id,isViewer}},playlistItems:items(first:$first_1) {{totalCount,edges {{node {{cloudcast {{id,...Fg}},id}},cursor}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}},...Fh,...Fj}} fragment Fl on Playlist {{id,name,owner {{displayName,id}},...Fk}}\", \"variables\":{{\"lookup_0\":{{\"username\":\"{artistName}\",\"slug\":\"{playlistName}\"}},\"first_1\":10,\"lighten_2\":15,\"alpha_3\":0.3}}}}");

            IContainer container = creator.CreateContainer(queryResult.data.playlistLookup.name);

            foreach (var edge in queryResult.data.playlistLookup.playlistItems.edges)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                AddEdgeToContainer(container, edge.node.cloudcast);
            }

            if (queryResult.data.playlistLookup.playlistItems.pageinfo.hasNextPage)
                CreateAndAddEntriesFromPlaylist(container, queryResult.data.playlistLookup.id, queryResult.data.playlistLookup.playlistItems.pageinfo.endCursor, cancellationToken);
        }

        private void CreateAndAddEntriesFromUserUploads(IContainer container, string artistID, string cursor, CancellationToken? cancellationToken)
        {
            var queryResult = ExecuteQuery($"{{\"id\":\"q134\",\"query\":\"query UserUploadsPageQuery($first_0:Int!,$lighten_2: Int!,$orderBy_1: CloudcastOrderByEnum!,$alpha_3: Float!) {{ user: user(id:\\\"{artistID}\\\") {{ id,...Fh}}}} fragment F0 on Picture {{urlRoot,primaryColor}} fragment F1 on User {{id}} fragment F2 on User {{username,hasProFeatures,hasPremiumFeatures,isStaff,isSelect,id}} fragment F3 on Cloudcast {{isExclusive,isExclusivePreviewOnly,slug,id,owner {{username,id}}}} fragment F4 on CloudcastTag {{tag {{name,slug,isCategory,id}},position}} fragment F5 on Cloudcast {{_tags4ruy33:tags {{...F4}},id}} fragment F6 on Cloudcast {{restrictedReason,owner {{username,isSubscribedTo,isViewer,id}},slug,id,isAwaitingAudio,isDraft,isPlayable,streamInfo {{hlsUrl,dashUrl,url,uuid}},audioLength,currentPosition,proportionListened,repeatPlayAmount,seekRestriction,previewUrl,isExclusivePreviewOnly,isExclusive,picture {{primaryColor,isLight,_primaryColor2pfPSM:primaryColor(lighten:$lighten_2),_primaryColor3Yfcks:primaryColor(alpha:$alpha_3)}}}} fragment F7 on Cloudcast {{id,name,slug,owner {{id,username,displayName,isSelect,...F1,...F2}},isUnlisted,isExclusive,...F3,...F5,...F6}} fragment F8 on Cloudcast {{isDraft,hiddenStats,plays,publishDate,qualityScore,listenerMinutes,id}} fragment F9 on Cloudcast {{id,isFavorited,isPublic,hiddenStats,favorites {{totalCount}},slug,owner {{id,isFollowing,username,displayName,isViewer}}}} fragment Fa on Cloudcast {{id,isReposted,isPublic,hiddenStats,reposts {{totalCount}},owner {{isViewer,id}}}} fragment Fb on Cloudcast {{id,isUnlisted,isPublic}} fragment Fc on Cloudcast {{id,isUnlisted,isPublic,slug,description,picture {{urlRoot}},owner {{displayName,isViewer,username,id}}}} fragment Fd on Cloudcast {{id,isPublic,isHighlighted,owner {{isViewer,id}}}} fragment Fe on Cloudcast {{id,isPublic,isExclusive,owner {{id,username,isViewer,isSubscribedTo}},...F8,...F9,...Fa,...Fb,...Fc,...Fd}} fragment Ff on Cloudcast {{owner {{quantcastTrackingPixel,id}},id}} fragment Fg on Cloudcast {{id,slug,name,isAwaitingAudio,isDraft,isScheduled,restrictedReason,publishDate,waveformUrl,audioLength,owner {{username,id}},picture {{...F0}},...F7,...Fe,...Ff}} fragment Fh on User {{id,displayName,username,uploads:uploads(first:$first_0,after:\\\"{cursor}\\\", orderBy:$orderBy_1) {{edges {{node {{id,...Fg}},cursor}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}}}}\", \"variables\":{{ \"first_0\":20,\"lighten_2\":15,\"orderBy_1\":\"LATEST\",\"alpha_3\":0.3}}}}");

            foreach (var edge in queryResult.data.userLookup.uploads.edges)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                AddEdgeToContainer(container, edge.node);
            }

            if (queryResult.data.userLookup.uploads.pageinfo.hasNextPage)
                CreateAndAddEntriesFromUserUploads(container, artistID, queryResult.data.userLookup.uploads.pageinfo.endCursor, cancellationToken);
        }

        private void CreateAndAddEntriesFromPlaylist(IContainer container, string playlistID, string cursor, CancellationToken? cancellationToken)
        {
            var queryResult = ExecuteQuery($"{{\"id\":\"q56\",\"query\":\"query UserPlaylistPageQuery($first_0:Int!,$lighten_1:Int!,$alpha_2:Float!) {{playlistLookup:playlist(id:\\\"{playlistID}\\\") {{id,...Fk}}}} fragment F0 on Picture {{urlRoot,primaryColor}} fragment F1 on User {{id}} fragment F2 on User {{username,hasProFeatures,hasPremiumFeatures,isStaff,isSelect,id}} fragment F3 on Cloudcast {{isExclusive,isExclusivePreviewOnly,slug,id,owner {{username,id}}}} fragment F4 on CloudcastTag {{tag {{name,slug,isCategory,id}},position}} fragment F5 on Cloudcast {{_tags4ruy33:tags {{...F4}},id}} fragment F6 on Cloudcast {{restrictedReason,owner {{username,isSubscribedTo,isViewer,id}},slug,id,isAwaitingAudio,isDraft,isPlayable,streamInfo {{hlsUrl,dashUrl,url,uuid}},audioLength,currentPosition,proportionListened,repeatPlayAmount,seekRestriction,previewUrl,isExclusivePreviewOnly,isExclusive,picture {{primaryColor,isLight,_primaryColor2pfPSM:primaryColor(lighten:$lighten_1),_primaryColor3Yfcks:primaryColor(alpha:$alpha_2)}}}} fragment F7 on Cloudcast {{id,name,slug,owner {{id,username,displayName,isSelect,...F1,...F2}},isUnlisted,isExclusive,...F3,...F5,...F6}} fragment F8 on Cloudcast {{isDraft,hiddenStats,plays,publishDate,qualityScore,listenerMinutes,id}} fragment F9 on Cloudcast {{id,isFavorited,isPublic,hiddenStats,favorites {{totalCount}},slug,owner {{id,isFollowing,username,displayName,isViewer}}}} fragment Fa on Cloudcast {{id,isReposted,isPublic,hiddenStats,reposts {{totalCount}},owner {{isViewer,id}}}} fragment Fb on Cloudcast {{id,isUnlisted,isPublic}} fragment Fc on Cloudcast {{id,isUnlisted,isPublic,slug,description,picture {{urlRoot}},owner {{displayName,isViewer,username,id}}}} fragment Fd on Cloudcast {{id,isPublic,isHighlighted,owner {{isViewer,id}}}} fragment Fe on Cloudcast {{id,isPublic,isExclusive,owner {{id,username,isViewer,isSubscribedTo}},...F8,...F9,...Fa,...Fb,...Fc,...Fd}} fragment Ff on Cloudcast {{owner {{quantcastTrackingPixel,id}},id}} fragment Fg on Cloudcast {{id,slug,name,isAwaitingAudio,isDraft,isScheduled,restrictedReason,publishDate,waveformUrl,audioLength,owner {{username,id}},picture {{...F0}},...F7,...Fe,...Ff}} fragment Fh on Playlist {{name,slug,owner {{displayName,username,twitterAccount {{username}},id}},picture {{urlRoot}},id}} fragment Fi on Playlist {{id,slug,description,picture {{urlRoot}},owner {{displayName,username,id}}}} fragment Fj on Playlist {{id,name,slug,owner {{isViewer,id}},items {{totalCount}},...Fi}} fragment Fk on Playlist {{id,name,owner {{id,isViewer}},playlistItems:items(first:$first_0,after:\\\"{cursor}\\\") {{totalCount,edges {{node {{cloudcast {{id,...Fg}},id}},cursor}},pageInfo {{endCursor,hasNextPage,hasPreviousPage}}}},...Fh,...Fj}}\", \"variables\":{{\"first_0\":20,\"lighten_1\":15,\"alpha_2\":0.3}}}}");

            foreach (var edge in queryResult.data.playlistLookup.playlistItems.edges)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                AddEdgeToContainer(container, edge.node.cloudcast);
            }

            if (queryResult.data.playlistLookup.playlistItems.pageinfo.hasNextPage)
                CreateAndAddEntriesFromPlaylist(container, playlistID, queryResult.data.playlistLookup.playlistItems.pageinfo.endCursor, cancellationToken);
        }

        private void CreateAndAddEntryFromTrack(string title, string artistName, IContainerCreator creator, CancellationToken? cancellationToken)
        {
            if (cancellationToken.HasValue)
            {
                if (cancellationToken.Value.IsCancellationRequested)
                    return;
            }

            var queryResult = ExecuteQuery($"{{\"id\":\"q12\",\"query\":\"query CloudcastHeader($lookup_0:CloudcastLookup!,$lighten_1:Int!,$alpha_2:Float!) {{cloudcastLookup(lookup:$lookup_0) {{id,...Fn}}}} fragment F0 on Picture {{urlRoot,primaryColor}} fragment F1 on Cloudcast {{picture {{urlRoot,primaryColor}},id}} fragment F2 on Cloudcast {{id,name,slug,owner {{username,id}}}} fragment F3 on Cloudcast {{owner {{id,displayName,followers {{totalCount}}}},id}} fragment F4 on Cloudcast {{restrictedReason,owner {{username,isSubscribedTo,isViewer,id}},slug,id,isAwaitingAudio,isDraft,isPlayable,streamInfo {{hlsUrl,dashUrl,url,uuid}},audioLength,currentPosition,proportionListened,seekRestriction,previewUrl,isExclusivePreviewOnly,isExclusive,picture {{primaryColor,isLight,_primaryColor2pfPSM:primaryColor(lighten:$lighten_1),_primaryColor3Yfcks:primaryColor(alpha:$alpha_2)}}}} fragment F5 on Node {{id,__typename}} fragment F6 on Cloudcast {{id,isFavorited,isPublic,hiddenStats,favorites {{totalCount}},slug,owner {{id,isFollowing,username,displayName,isViewer}}}} fragment F7 on Cloudcast {{id,isUnlisted,isPublic}} fragment F8 on Cloudcast {{id,isReposted,isPublic,hiddenStats,reposts {{totalCount}},owner {{isViewer,id}}}} fragment F9 on Cloudcast {{id,isUnlisted,isPublic,slug,description,picture {{urlRoot}},owner {{displayName,isViewer,username,id}}}} fragment Fa on Cloudcast {{id,slug,isSpam,owner {{username,isViewer,id}}}} fragment Fb on Cloudcast {{owner {{isViewer,isSubscribedTo,username,hasProFeatures,isBranded,id}},sections {{__typename,...F5}},id,slug,isExclusive,isUnlisted,isShortLength,...F6,...F7,...F8,...F9,...Fa}} fragment Fc on Cloudcast {{qualityScore,listenerMinutes,id}} fragment Fd on Cloudcast {{slug,plays,publishDate,hiddenStats,owner {{username,id}},id,...Fc}} fragment Fe on User {{id}} fragment Ff on User {{username,hasProFeatures,hasPremiumFeatures,isStaff,isSelect,id}} fragment Fg on User {{id,isFollowed,isFollowing,isViewer,followers {{totalCount}},username,displayName}} fragment Fh on Cloudcast {{isExclusive,isExclusivePreviewOnly,slug,id,owner {{username,id}}}} fragment Fi on Cloudcast {{isExclusive,owner {{id,username,displayName,...Fe,...Ff,...Fg}},id,...Fh}} fragment Fj on Cloudcast {{id,streamInfo {{uuid,url,hlsUrl,dashUrl}},audioLength,seekRestriction,currentPosition}} fragment Fk on Cloudcast {{owner {{displayName,isSelect,username,id}},seekRestriction,id}} fragment Fl on Cloudcast {{id,waveformUrl,previewUrl,audioLength,isPlayable,streamInfo {{hlsUrl,dashUrl,url,uuid}},seekRestriction,currentPosition,...Fk}} fragment Fm on Cloudcast {{__typename,isExclusivePreviewOnly,isExclusive,owner {{isSelect,isSubscribedTo,username,displayName,isViewer,id}},id}} fragment Fn on Cloudcast {{id,name,picture {{isLight,primaryColor,...F0}},owner {{displayName,isViewer,isBranded,selectUpsell {{text}},id}},...F1,...F2,...F3,...F4,...Fb,...Fd,...Fi,...Fj,...Fl,...Fm}}\",\"variables\":{{\"lookup_0\":{{\"username\":\"{artistName}\",\"slug\":\"{title}\"}},\"lighten_1\":15,\"alpha_2\":0.3}}}}");

            IContainer container = creator.CreateContainer(queryResult.data.cloudcastLookup.name);
            AddEdgeToContainer(container, queryResult.data.cloudcastLookup);
        }

        private void AddEdgeToContainer(IContainer container, MixcloudJsonUserUploadEdgeNodeResult edge)
        {
            Uri url = null;
            UrlFileType urlType = UrlFileType.None;

            if (!string.IsNullOrEmpty(edge.streamInfo.dashUrl))
            {
                if (Uri.TryCreate(DecryptUrl(edge.streamInfo.dashUrl, UrlDecryptKey), UriKind.Absolute, out var mpdUrl))
                {
                    urlType = UrlFileType.MPD;
                    url = mpdUrl;

                    if (PreferredType == UrlFileType.MPD)
                    {
                        AddUrlToContainer(container, edge.name, url, urlType);
                        return;
                    }
                }
            }

            if (!string.IsNullOrEmpty(edge.streamInfo.hlsUrl))
            {
                if (Uri.TryCreate(DecryptUrl(edge.streamInfo.hlsUrl, UrlDecryptKey), UriKind.Absolute, out var m3u8Url))
                {
                    urlType = UrlFileType.M3U;
                    url = m3u8Url;

                    if (PreferredType == UrlFileType.M3U)
                    {
                        AddUrlToContainer(container, edge.name, url, urlType);
                        return;
                    }
                }
            }

            if (!string.IsNullOrEmpty(edge.streamInfo.url))
            {
                if (Uri.TryCreate(DecryptUrl(edge.streamInfo.url, UrlDecryptKey), UriKind.Absolute, out var m4aUrl))
                {
                    urlType = UrlFileType.M4A;
                    url = m4aUrl;

                    if (PreferredType == UrlFileType.M4A)
                    {
                        AddUrlToContainer(container, edge.name, url, urlType);
                        return;
                    }
                }
            }

            AddUrlToContainer(container, edge.name, url, urlType);
        }

        private void AddUrlToContainer(IContainer container, string title, Uri url, UrlFileType urlFileType)
        {
            MixcloudEntry entry = new MixcloudEntry(this, title, url, urlFileType);
            entry.Load(container);
            container.AddEntry(entry.FileName, entry);

            if (urlFileType == UrlFileType.None)
            {
                entry.FileState = Application.CreateFileState(FileStateIcon.Error, "failed to decrypt stream url");
                Application.LogManager.WriteLog(LogLevel.Normal, $"failed to decrypt stream url for '{title}'");
            }
        }

        private MixcloudJsonResult ExecuteQuery(string query)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://www.mixcloud.com/graphql");

            request.Method = "POST";
            request.Accept = "application/json";
            request.ContentType = "application/json";
            request.Referer = "https://www.mixcloud.com/";

            request.Headers.Add(HttpRequestHeader.Cookie, $"csrftoken={m_csrftoken}");
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            request.Headers.Add("X-CSRFToken", m_csrftoken);

            using (var s = request.GetRequestStream())
            {
                using (var writer = new StreamWriter(s))
                {
                    writer.Write(query);
                }
            }

            using (var response = request.GetResponse())
            {
                using (var s = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(s))
                    {
                        return Application.LoadJsonObject<MixcloudJsonResult>(reader);
                    }
                }
            }
        }

        private string DecryptUrl(string input, byte[] key)
        {
            byte[] data = Convert.FromBase64String(input);
            byte[] output = new byte[data.Length];

            int currKeyIndex = 0;

            for (int i = 0; i < data.Length; ++i)
            {
                output[i] = (byte)(data[i] ^ key[currKeyIndex++]);

                if (currKeyIndex >= key.Length)
                    currKeyIndex = 0;
            }

            return Encoding.UTF8.GetString(output);
        }
    }
}
