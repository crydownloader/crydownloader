﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace CryDownloader.Instagram
{
    class Instagram : Hoster
    {
        class InstagramEdgeTimelinePageInfo
        {
            public string end_cursor { get; set; }
            public bool has_next_page { get; set; }
        }

        class InstagramEdgeNode
        {
            public string __typename { get; set; }
            public string shortcode { get; set; }
            public string display_url { get; set; }
            public bool is_video { get; set; }

            public InstagramEntryChildren edge_sidecar_to_children { get; set; }
        }

        class InstagramEdge
        {
            public InstagramEdgeNode node { get; set; }
        }

        class InstagramEdgeTimeline
        {
            public InstagramEdgeTimelinePageInfo page_info { get; set; }
            public List<InstagramEdge> edges { get; set; }
        }

        class InstagramJsonUser
        {
            public string full_name { get; set; }
            public string id { get; set; }
            public string username { get; set; }
            public InstagramEdgeTimeline edge_owner_to_timeline_media { get; set; }
        }

        class InstagramJsonGraphQl
        {
            public InstagramJsonUser user { get; set; }
        }

        class InstagramJsonData
        {
            public InstagramJsonUser user { get; set; }
        }

        class InstagramJsonProfilePage
        {
            public InstagramJsonGraphQl graphql { get; set; }
        }

        class InstagramJsonEntryData
        {
            public List<InstagramJsonProfilePage> ProfilePage { get; set; }
        }

        class InstagramJsonResult
        {
            public InstagramJsonEntryData entry_data { get; set; }
        }

        class InstagramJsonResult2
        {
            public InstagramJsonData data { get; set; }
        }

        class InstagramEntryChildren
        {
            public List<InstagramEdge> edges { get; set; }
        }

        class InstagramEntryMedia
        {
            public string video_url { get; set; }

            public InstagramEntryChildren edge_sidecar_to_children { get; set; }
        }

        class InstagramEntryData
        {
            public InstagramEntryMedia shortcode_media { get; set; }
        }

        class InstagramEntryResult
        {
            public InstagramEntryData data { get; set; }
        }

        public override string Name => "Instagram";

        public override string Host => "instagram.com";

        public override System.Drawing.Bitmap Icon => Properties.Resources._36b3ee2d91ed;

        public override System.Windows.FrameworkElement SettingControl { get; }

        public Instagram(IApplication app)
            : base(app)
        { }
        // ds_user_id=50347319880; sessionid=50347319880%3ACFbUWJiRePBCdm%3A17
        protected override void OnCreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            using (var webClient = new WebClient())
            {
                foreach (var url in urls)
                {
                    if (cancellationToken.HasValue)
                    {
                        if (cancellationToken.Value.IsCancellationRequested)
                            break;
                    }

                    scanUrlChangedCallback?.Invoke(url);
                    string str = webClient.DownloadString(url);

                    IHtmlDocument doc = Application.LoadHtmlDocument(str);

                    var node = doc.DocumentNode;
                    var nodes = node.SelectNodes(".//script[@type='text/javascript']");

                    string jsonData = string.Empty;

                    foreach (var n in nodes)
                    {
                        if (n.InnerText.Contains("window._sharedData"))
                        {
                            jsonData = n.InnerText.Replace("window._sharedData = ", "");
                            jsonData = jsonData.Replace("window._sharedData=", "");
                            break;
                        }
                    }

                    InstagramJsonResult profileData;

                    profileData = Application.LoadJsonObject<InstagramJsonResult>(jsonData);

                    GetMediaEntries(url, profileData, webClient, creator, cancellationToken);
                }
            }
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>();
        }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        { }

        private InstagramEntryResult GetEntryInfo(WebClient webClient, string shortCode)
        {
            string str = webClient.DownloadString($"https://www.instagram.com/graphql/query/?query_hash=477b65a610463740ccdb83135b2014db&variables={{\"shortcode\":\"{shortCode}\", \"child_comment_count\":0,\"fetch_comment_count\":0,\"parent_comment_count\":0,\"has_threaded_comments\":false}}");

            return Application.LoadJsonObject<InstagramEntryResult>(str);
        }

        private void GetMediaEntries(Uri url, InstagramJsonUser profileData, string profileId, string userName, WebClient webClient, IContainer container, CancellationToken? cancellationToken)
        {
            foreach (var edge in profileData.edge_owner_to_timeline_media.edges)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        break;
                }

                InstagramEntry entry;

                if (edge.node.__typename == "GraphVideo")
                {
                    var data = GetEntryInfo(webClient, edge.node.shortcode);

                    entry = new InstagramEntry(this, new Uri(data.data.shortcode_media.video_url));

                    entry.Load(container);
                    container.AddEntry(entry.FileName, entry);
                }
                else if (edge.node.__typename == "GraphSidecar")
                {
                    var mediaData = GetEntryInfo(webClient, edge.node.shortcode);

                    foreach (var e in mediaData.data.shortcode_media.edge_sidecar_to_children.edges)
                    {
                        entry = new InstagramEntry(this, new Uri(e.node.display_url));

                        entry.Load(container);
                        container.AddEntry(entry.FileName, entry);
                    }

                    continue;
                }
                else if (edge.node.is_video)
                {
                    Application.LogManager.WriteLog(LogLevel.Normal, "Instagram: Invalid Node: video node but typename is not GraphVideo");
                    continue;
                }
                else
                    entry = new InstagramEntry(this, new Uri(edge.node.display_url));

                entry.Load(container);
                container.AddEntry(entry.FileName, entry);
            }

            if (profileData.edge_owner_to_timeline_media.page_info.has_next_page)
            {
                InstagramJsonResult2 newProfileData;

                try
                {
                    string str = webClient.DownloadString($"https://www.instagram.com/graphql/query/?query_hash=f2405b236d85e8296cf30347c9f08c2a&variables={{\"id\":\"{profileId}\", \"first\":12,\"after\":\"{profileData.edge_owner_to_timeline_media.page_info.end_cursor}\"}}");

                    newProfileData = Application.LoadJsonObject<InstagramJsonResult2>(str);

                    GetMediaEntries(url, newProfileData.data.user, profileId, userName, webClient, container, cancellationToken);
                }
                catch (Exception ex)
                {
                    Application.LogManager.WriteException(LogLevel.Normal, $"error occured by scanning {url}", ex);
                    var entry = new InstagramEntry(this, "error occured by scanning");
                    return;
                }
            }
        }

        private void GetMediaEntries(Uri url, InstagramJsonResult profileData, WebClient webClient, IContainerCreator creator, CancellationToken? cancellationToken)
        {
            foreach (var user in profileData.entry_data.ProfilePage)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        break;
                }

                string name;

                if (string.IsNullOrEmpty(user.graphql.user.full_name))
                    name = user.graphql.user.username;
                else
                    name = user.graphql.user.full_name;

                IContainer container = creator.CreateContainer(name);
                GetMediaEntries(url, user.graphql.user, user.graphql.user.id, user.graphql.user.username, webClient, container, cancellationToken);
            }
        }
    }
}
