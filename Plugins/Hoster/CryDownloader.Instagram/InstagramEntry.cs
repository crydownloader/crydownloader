﻿using CryDownloader.Plugin;
using System;
using System.IO;

namespace CryDownloader.Instagram
{
    class InstagramEntry : DownloadEntry
    {
        public InstagramEntry(Instagram hoster, Uri url)
            : base(hoster)
        {
            FileName = new FileInfo(url.AbsolutePath).Name;
            Url = url;
        }

        public InstagramEntry(Instagram hoster, string message)
             : base(hoster)
        { }

        protected override void OnLoad(object data)
        {
            try
            {
                FileSize = GetFileSize(Url);
                FileState = GetFileState(FileStateType.FileInfoAvail);
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileNotFound);
            }
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }
    }
}
