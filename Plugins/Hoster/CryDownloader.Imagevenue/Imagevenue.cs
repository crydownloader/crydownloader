﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;

namespace CryDownloader.Imagevenue
{
    public class Imagevenue : PictureHoster, IShareHoster
    {
        public override string Name => "Imagevenue";

        public override string Host => "imagevenue.com";

        public override Bitmap Icon => Properties.Resources.favicon;

        public Imagevenue(IApplication application)
            : base(application)
        { }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            UriQuery query = new UriQuery(args.Url);
            string filename;

            if (!query.TryGetElement("image", out filename))
            {
                if (!query.TryGetElement("i", out filename))
                    filename = args.Url.Segments[args.Url.Segments.Length - 1];
            }

            FileInfo fileInfo = new FileInfo(filename);

            IContainer container = args.ContainerCreator.CreateContainer(filename.Replace(fileInfo.Extension, ""));
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            if (cancellationToken.HasValue)
            {
                if (cancellationToken.Value.IsCancellationRequested)
                    return false;
            }

            string strUrl = url.ToString();
            string filename;

            UriQuery query = new UriQuery(url);

            if (!query.TryGetElement("image", out filename))
            {
                if (!query.TryGetElement("i", out filename))
                {
                    if (url.Segments[url.Segments.Length - 1].StartsWith("th_") && url.Segments.Length == 3)
                    {
                        filename = url.Segments[url.Segments.Length - 1].Remove(0, 3);
                        url = new Uri(url, $"/img.php?image={filename}");

                        var test = DownloadPage(url, cancellationToken);

                        if (string.IsNullOrEmpty(test))
                            url = new Uri(url, $"/img.php?i={filename}");

                        IHtmlDocument document = Application.LoadHtmlDocument(test);

                        if (!IsImageExists(document))
                            url = new Uri(url, $"/img.php?i={filename}");
                    }
                    else
                        filename = url.Segments[url.Segments.Length - 1];
                }
            }

            var entry = new ImagevenueEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }

        internal static bool IsImageExists(IHtmlDocument doc)
        {
            var titles = doc.DocumentNode.SelectNodes("//head/title");

            if (titles == null)
                return false;

            foreach (var title in titles)
            {
                if (title.InnerHtml.Contains("not exist on this server") || title.InnerHtml.Contains("not found"))
                    return false;
            }

            return true;
        }


        protected override void OnDownloadPage(HttpWebRequest httpWebRequest, Uri url, object userData)
        {
            httpWebRequest.UserAgent = "Chrome/79.0.3945.94";

            Cookie cookie = new Cookie("__ve1apb", "1", "/", url.Host.Substring(url.Host.IndexOf(".")));
            cookie.Expires = DateTime.Now.AddHours(1);

            httpWebRequest.CookieContainer = new CookieContainer();
            httpWebRequest.CookieContainer.Add(cookie);

            cookie = new Cookie("_inter", "1", "/", url.Host.Substring(url.Host.IndexOf(".")));
            cookie.Expires = DateTime.Now.AddHours(1);
            httpWebRequest.CookieContainer.Add(cookie);
        }
    }
}
