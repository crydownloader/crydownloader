﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Imagevenue
{
    class ImagevenueEntry : DownloadEntry
    {
        public ImagevenueEntry(Imagevenue hoster, Uri url, string fileName)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        private void Resolve(Uri url)
        {
            string strHtml = Hoster.DownloadPage(url);

            if (string.IsNullOrEmpty(strHtml))
                FileState = GetFileState(FileStateType.FileNotFound);
            else
            {
                IHtmlDocument document = Hoster.Application.LoadHtmlDocument(strHtml);

                if (!Imagevenue.IsImageExists(document))
                    FileState = GetFileState(FileStateType.FileNotFound);
                else
                {
                    IHtmlNode picNode = document.GetElementbyId("thepic");

                    if (picNode == null)
                    {
                        var picNodes = document.DocumentNode.SelectNodes($"//img[contains(@src, '{FileName}')]");

                        if (picNodes != null)
                        {
                            picNode = picNodes.FirstOrDefault();

                            if (picNode != null && picNode.Name != "img")
                                picNode = null;
                        }
                    }

                    if (picNode == null)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else if (picNode.Attributes.Contains("src"))
                    {
                        var relativeUri = new Uri(picNode.Attributes["src"].Value);

                        if (!relativeUri.IsAbsoluteUri)
                            Url = new Uri(Url, relativeUri);
                        else
                            Url = relativeUri;

                        FileSize = GetFileSize(Url);

                        if (FileSize == 0)
                            FileState = GetFileState(FileStateType.FileNotFound);
                        else
                            FileState = GetFileState(FileStateType.FileInfoAvail);
                    }
                    else
                        FileState = GetFileState(FileStateType.FileNotFound);
                }
            }
        }

        protected override void OnLoad(object data)
        {
            try
            {
                if (Url.Query.Contains("?image=") || Url.Query.Contains("?i="))
                    Resolve(Url);
                else
                {
                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }
    }
}
