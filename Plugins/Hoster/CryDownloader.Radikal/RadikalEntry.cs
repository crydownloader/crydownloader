﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Radikal
{
    class RadikalEntry : DownloadEntry
    {
        private IHtmlDocument m_htmlDocument;

        public RadikalEntry(Radikal hoster, Uri url, string fileName, IHtmlDocument htmlDocument)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
            m_htmlDocument = htmlDocument;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        private void Resolve(Uri url)
        {
            if (m_htmlDocument == null)
            {
                string strHtml = Hoster.DownloadPage(url);

                if (string.IsNullOrEmpty(strHtml))
                {
                    FileState = GetFileState(FileStateType.FileNotFound);
                    return;
                }
                else
                    m_htmlDocument = Hoster.Application.LoadHtmlDocument(strHtml);
            }

            IHtmlNode picNode = Radikal.GetImageNode(m_htmlDocument);

            if (picNode == null)
                FileState = GetFileState(FileStateType.FileNotFound);
            else if (picNode.Attributes.Contains("src"))
            {
                Url = new Uri(picNode.Attributes["src"].Value);

                FileSize = GetFileSize(Url);

                if (FileSize == 0)
                    FileState = GetFileState(FileStateType.FileNotFound);
                else
                    FileState = GetFileState(FileStateType.FileInfoAvail);
            }
            else
                FileState = GetFileState(FileStateType.FileNotFound);

            m_htmlDocument = null;
        }

        protected override void OnLoad(object data)
        {
            try
            {
                if (Url.PathAndQuery.EndsWith("htm") || !Url.PathAndQuery.Contains("."))
                    Resolve(Url);
                else
                {
                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }
    }
}
