﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.Radikal
{
    public class Radikal : PictureHoster, IShareHoster
    {
        public Radikal(IApplication app)
            : base(app)
        { }

        public override string Name => "Radikal";

        public override string Host => "radikal.ru";

        public override Bitmap Icon => Properties.Resources.favicon;

        public override void Initialize()
        { }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1];
            IContainer container = args.ContainerCreator.CreateContainer(filename);
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];

            int lastIdx = url.PathAndQuery.LastIndexOf(".");

            IHtmlDocument htmlDocument = null;

            if (lastIdx > 0)
            {
                if (url.PathAndQuery[lastIdx - 1] == 't' && url.Scheme == "http")
                {
                    string fixedPath = url.PathAndQuery.Remove(lastIdx - 1, 1);
                    lastIdx = filename.LastIndexOf(".");
                    filename = filename.Remove(lastIdx - 1, 1);
                    url = new Uri($"https://{url.Host}{fixedPath}");
                }
            }
            else
            {
                string strHtml = DownloadPage(url);

                htmlDocument = Application.LoadHtmlDocument(strHtml);

                var picNode = GetImageNode(htmlDocument);

                if (picNode != null)
                {
                    var uri = new Uri(picNode.Attributes["src"].Value);
                    filename = uri.Segments[uri.Segments.Length - 1];
                }

            }

            var entry = new RadikalEntry(this, url, filename, htmlDocument);
            container.AddEntry(filename, entry);
            return true;
        }

        internal static IHtmlNode GetImageNode(IHtmlDocument document)
        {
            return document.DocumentNode.SelectSingleNode("//div[@class='mainBlock']/div/img");
        }
    }
}
