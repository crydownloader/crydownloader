﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Imagebam
{
    public class Imagebam : Hoster, IShareHoster
    {
        public override string Name => "Imagebam";

        public override string Host => "imagebam.com";

        public override Bitmap Icon => Properties.Resources.favicon;

        public override System.Windows.FrameworkElement SettingControl { get; }

        internal bool SettingCrtfToken { get; set; }
        internal string CrtfToken { get; set; }

        private IHtmlDocument m_downloadedPage;

        public Imagebam(IApplication application)
            : base(application)
        { }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>();
        }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        { }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            Uri containerUrl = args.Url;

            bool? isHtml = CheckFileInfo(args.Url);

            if (isHtml != null && isHtml.Value)
            {
                string strHtml = DownloadPage(args.Url);

                if (!string.IsNullOrEmpty(strHtml))
                {
                    m_downloadedPage = Application.LoadHtmlDocument(strHtml);

                    var nodes = m_downloadedPage.DocumentNode.SelectNodes("//*[text()[contains(., 'Back to gallery')]]");

                    if (nodes != null)
                    {
                        IHtmlNode linkNode = nodes.FirstOrDefault();

                        while (linkNode != null)
                        {
                            if (linkNode.Attributes.Contains("href"))
                                break;

                            linkNode = linkNode.ParentNode;
                        }

                        if (linkNode != null)
                            containerUrl = new Uri(linkNode.Attributes["href"].Value);
                    }
                }
            }

            string filename = containerUrl.Segments[args.Url.Segments.Length - 1];

            FileInfo fi = new FileInfo(filename);

            string onlyName;

            if (string.IsNullOrEmpty(fi.Extension))
                onlyName = fi.Name;
            else
                onlyName = fi.Name.Replace(fi.Extension, string.Empty);

            if (onlyName.EndsWith("_t") || onlyName.EndsWith("_o"))
                filename = onlyName.Remove(onlyName.Length - 2, 2);

            IContainer container = args.ContainerCreator.CreateContainer(filename);
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];

            if (filename.Contains("."))
            {
                if (url.Host.StartsWith("thumbnails") || url.Host.StartsWith("thumbs"))
                {
                    FileInfo fi = new FileInfo(filename);
                    string onlyName = fi.Name.Replace(fi.Extension, string.Empty);
                    
                    if (onlyName.EndsWith("_t"))
                    {
                        filename = onlyName.Remove(onlyName.Length - 2, 2);
                        url = new Uri($"{url.Scheme}://{Host}/view/{filename}");
                        filename += fi.Extension;
                    }
                    else
                    {
                        url = new Uri($"{url.Scheme}://{Host}/image/{filename}");
                    }
                }

                var entry = new ImagebamEntry(this, url, filename, m_downloadedPage);
                container.AddEntry(filename, entry);
            }
            else
            {
                var entry = new ImagebamEntry(this, url, filename, m_downloadedPage);

                filename = entry.FileName = entry.GetRealPath();

                container.AddEntry(filename, entry);
            }

            return true;
        }

        protected override void OnDownloadPage(HttpWebRequest httpWebRequest, Uri url, object userData)
        {
            Cookie cookie = new Cookie("ibpuc_sfw", "yes", "/image", "www." + url.Host);
            cookie.Expires = DateTime.Now.AddHours(1);

            httpWebRequest.CookieContainer = new CookieContainer();
            httpWebRequest.CookieContainer.Add(cookie);

            cookie = new Cookie("ibpuc_nsfw", "yes", "/image", "www." + url.Host);
            cookie.Expires = DateTime.Now.AddHours(1);
            httpWebRequest.CookieContainer.Add(cookie);

            cookie = new Cookie("pop_type", "0", "/", "." + url.Host);
            cookie.Expires = DateTime.Now.AddHours(1);
            httpWebRequest.CookieContainer.Add(cookie);

            cookie = new Cookie("nsfw_inter", "eyJpdiI6Ilhucks3QVc2TFdhSFpNbWV3ZTkvbHc9PSIsInZhbHVlIjoiUFlQUSs3R3d4eEZQcXcycXk1Zmx3L3dsYWI0OU92ZHhTaHloQy9KMmVvYTFsbWNrbk43LzBsd0YwNmNKQ1FYSCIsIm1hYyI6IjViOGJmNTc2ODI2Mzg3NzliMTEzMjZiOWY4OTU0ZDAxN2M3MTEyYzI4YzFmZjg2MTI1MGJjNmQ5M2FkYjIwZjcifQ%3D%3D");
            cookie.Domain = url.Host;
            httpWebRequest.CookieContainer.Add(cookie);

            cookie = new Cookie("imagebam_session", "eyJpdiI6Ilhucks3QVc2TFdhSFpNbWV3ZTkvbHc9PSIsInZhbHVlIjoiUFlQUSs3R3d4eEZQcXcycXk1Zmx3L3dsYWI0OU92ZHhTaHloQy9KMmVvYTFsbWNrbk43LzBsd0YwNmNKQ1FYSCIsIm1hYyI6IjViOGJmNTc2ODI2Mzg3NzliMTEzMjZiOWY4OTU0ZDAxN2M3MTEyYzI4YzFmZjg2MTI1MGJjNmQ5M2FkYjIwZjcifQ%3D%3D");
            cookie.Domain = url.Host;
            httpWebRequest.CookieContainer.Add(cookie);

            cookie = new Cookie("XSRF-TOKEN", "eyJpdiI6Ilhucks3QVc2TFdhSFpNbWV3ZTkvbHc9PSIsInZhbHVlIjoiUFlQUSs3R3d4eEZQcXcycXk1Zmx3L3dsYWI0OU92ZHhTaHloQy9KMmVvYTFsbWNrbk43LzBsd0YwNmNKQ1FYSCIsIm1hYyI6IjViOGJmNTc2ODI2Mzg3NzliMTEzMjZiOWY4OTU0ZDAxN2M3MTEyYzI4YzFmZjg2MTI1MGJjNmQ5M2FkYjIwZjcifQ%3D%3D");
            cookie.Domain = url.Host;
            httpWebRequest.CookieContainer.Add(cookie);
        }
    }
}
