﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Imagebam
{
    class ImagebamEntry : DownloadEntry
    {
        private IHtmlDocument m_htmlDocument;

        public ImagebamEntry(Imagebam hoster, Uri url, string fileName, IHtmlDocument htmlDocument)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
            m_htmlDocument = htmlDocument;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        public string GetRealPath()
        {
            ResolveUrl(Url);

            return Url.Segments[Url.Segments.Length - 1];
        }

        private void ResolveUrl(Uri url)
        {
            if (m_htmlDocument == null)
            {
                string strHtml = Hoster.DownloadPage(url);

                if (string.IsNullOrEmpty(strHtml))
                {
                    FileState = GetFileState(FileStateType.FileNotFound);
                    return;
                }
                else
                    m_htmlDocument = Hoster.Application.LoadHtmlDocument(strHtml);
            }

            string id = url.Segments[url.Segments.Length - 1].Substring(6);
            int dotIdx = id.IndexOf(".");

            if (dotIdx >= 0)
            {
                id = id.Substring(0, dotIdx);

                IHtmlNode picNode = m_htmlDocument.GetElementbyId("i" + id);

                if (picNode != null && picNode.Attributes.Contains("src"))
                {
                    Url = new Uri(picNode.Attributes["src"].Value);
                    m_htmlDocument = null;
                    return;
                }
            }

            var nodes = m_htmlDocument.DocumentNode.SelectNodes("//*[text()[contains(., 'Save')]]");

            if (nodes != null)
            {
                IHtmlNode linkNode = nodes.FirstOrDefault();

                while (linkNode != null)
                {
                    if (linkNode.Attributes.Contains("href"))
                        break;

                    linkNode = linkNode.ParentNode;
                }

                if (linkNode != null)
                {
                    Url = new Uri(linkNode.Attributes["href"].Value);
                    m_htmlDocument = null;
                    return;
                }
            }

            nodes = m_htmlDocument.DocumentNode.SelectNodes("//a/img[contains(@class, 'main-image')]");

            if (nodes != null)
            {
                IHtmlNode linkNode = nodes.FirstOrDefault();

                if (linkNode != null)
                {
                    Url = new Uri(linkNode.Attributes["src"].Value);
                    m_htmlDocument = null;
                    return;
                }
            }

            nodes = m_htmlDocument.DocumentNode.SelectNodes("//a/img");

            if (nodes != null)
            {
                IHtmlNode linkNode = null;

                foreach (var node in nodes)
                {
                    if (node.Attributes.Contains("src"))
                    {
                        var u = new Uri(node.Attributes["src"].Value);
                        if (u.Host.StartsWith("images"))
                        {
                            linkNode = node;
                            break;
                        }
                    }
                }

                if (linkNode != null)
                {
                    Url = new Uri(linkNode.Attributes["src"].Value);
                    m_htmlDocument = null;
                    return;
                }
            }

            FileState = GetFileState(FileStateType.FileNotFound);

            m_htmlDocument = null;
        }

        private void Resolve(Uri url)
        {
            ResolveUrl(url);
        }

        protected override void OnLoad(object data)
        {
            try
            {
                Resolve(Url);
                UpdateFileStateAndSize(Url);
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }
    }
}
