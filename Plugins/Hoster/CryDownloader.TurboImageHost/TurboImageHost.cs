﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.TurboImageHost
{
    public class TurboImageHost : PictureHoster, IShareHoster
    {
        public override string Name => "turboimagehost";

        public override string Host => "turboimagehost.com";

        public override Bitmap Icon => Properties.Resources.favicon;

        public TurboImageHost(IApplication app)
           : base(app)
        { }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return base.IsValidUrl(url) || url.Host.Contains("turboimg.net");
        }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1].Replace(".html", "");

            FileInfo fileInfo = new FileInfo(filename);

            IContainer container = args.ContainerCreator.CreateContainer(filename.Replace(fileInfo.Extension, ""));
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            if (cancellationToken.HasValue)
            {
                if (cancellationToken.Value.IsCancellationRequested)
                    return false;
            }

            string filename = url.Segments[url.Segments.Length - 1];

            if (url.Segments[1] == "t/")
            {
                int idx = url.PathAndQuery.IndexOf("_");

                if (idx != -1)
                {
                    string fixedPath = url.PathAndQuery.Insert(idx, "/").Remove(idx + 1, 1).Remove(0, 2);
                    url = new Uri($"{url.Scheme}://{Host}/p{fixedPath}.html");
                    filename = url.Segments[url.Segments.Length - 1].Replace(".html", string.Empty);
                }
                else if (NoThumb)
                    return false;
            }
            else
                filename = filename.Replace(".html", string.Empty);

            var entry = new TurboImageHostEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }
    }
}
