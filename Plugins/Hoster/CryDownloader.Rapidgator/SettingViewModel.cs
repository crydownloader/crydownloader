﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace CryDownloader.Rapidgator
{
    class SettingViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Rapidgator Hoster { get; }

        public string Username
        {
            get => Hoster.Username;
            set
            {
                Hoster.Username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => Hoster.Password;
            set
            {
                Hoster.Password = value;
                OnPropertyChanged();
            }
        }

        public string AccountStatus
        {
            get => m_accountStatus;
            set
            {
                m_accountStatus = value;
                OnPropertyChanged();
            }
        }

        public Brush FontBrush
        {
            get => m_fontBrush;
            set
            {
                m_fontBrush = value;
                OnPropertyChanged();
            }
        }

        public bool ApplyButtonEnabled
        {
            get => m_applyButtonEnabled;
            set
            {
                m_applyButtonEnabled = value;
                OnPropertyChanged();
            }
        }

        private Brush m_fontBrush;
        private string m_accountStatus;
        private bool m_applyButtonEnabled;

        public SettingViewModel(Rapidgator hoster)
        {
            Hoster = hoster;
            SetAccountStatus();
        }

        public void Apply()
        {
            Hoster.Login();
            SetAccountStatus();
        }

        private void SetAccountStatus()
        {
            if (string.IsNullOrEmpty(Hoster.LoginError))
            {
                if (Hoster.IsPremium)
                {
                    FontBrush = Brushes.Green;
                    AccountStatus = "Premium until " + Hoster.PremiumEndTime;
                }
                else if (Hoster.IsRegistered)
                {
                    AccountStatus = "Freemode (Registrated)";
                    FontBrush = Brushes.Yellow;
                }
                else
                {
                    AccountStatus = "Freemode";
                    FontBrush = Brushes.Yellow;
                }
            }
            else
            {
                AccountStatus = Hoster.LoginError;
                FontBrush = Brushes.Red;
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
