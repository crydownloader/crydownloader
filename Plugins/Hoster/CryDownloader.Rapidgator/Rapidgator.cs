﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace CryDownloader.Rapidgator
{
    public class Rapidgator : Hoster
    {
        class RapidgatorUsage
        {
            public string total { get; set; }
            public string left { get; set; }
        }

        class RapidgatorUploadUsage
        {
            public ulong max_file_size { get; set; }
            public int nb_pipes { get; set; }
        }

        class RapidgatorRemoteUploadUsage
        {
            public ulong max_nb_jobs { get; set; }
            public int refresh_time { get; set; }
        }

        class RapidgatorUser
        {
            public string email { get; set; }
            public bool is_premium { get; set; }
            public string premium_end_time { get; set; }
            public int state { get; set; }
            public string state_label { get; set; }
            public RapidgatorUsage traffic { get; set; }
            public RapidgatorUsage storage { get; set; }
            public RapidgatorUploadUsage upload { get; set; }
            public RapidgatorRemoteUploadUsage remote_upload { get; set; }
        }

        class RapidgatorUserLogin
        {
            public string token { get; set; }
            public RapidgatorUser user { get; set; }
        }

        internal class RapidgatorUrlInfo
        {
            public string url { get; set; }
            public string filename { get; set; }
            public long size { get; set; }
            public string status { get; set; }
        }

        internal class RapidgatorResponse<T>
        {
            public T response { get; set; }
            public int status { get; set; }
            public string details { get; set; }
        }

        private const int MaxCheckCount = 25;

        public override string Name => "Rapidgator";

        public override string Host => "rapidgator.net";

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public override System.Windows.FrameworkElement SettingControl => m_settingControl;

        public string Username { get; set; }

        public string Password { get; set; }

        public bool IsPremium { get; set; }

        public string PremiumEndTime { get; set; }

        public string AccessToken { get; private set; }

        public string LoginError { get; private set; }

        public bool IsRegistered { get; private set; }

        private readonly Queue<RapidgatorEntry> m_checkEntries;

        private Settings m_settingControl;

        public Rapidgator(IApplication application)
            : base(application)
        {
            m_checkEntries = new Queue<RapidgatorEntry>();
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>
            {
                { "Username", Username },
                { "Password", Password }
            };
        }

        public override void Initialize()
        {
            SettingViewModel viewModel = new SettingViewModel(this);

            if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
                Login();

            Application.Dispatcher.Invoke(() => m_settingControl = new Settings(viewModel));
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            if (properties.TryGetValue("Username", out var value))
                Username = value;

            if (properties.TryGetValue("Password", out value))
                Password = value;
        }

        internal void AddToCheckQueue(RapidgatorEntry entry)
        {
            m_checkEntries.Enqueue(entry);
        }

        protected override void OnCreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            List<Uri> checkUrls = new List<Uri>();

            if (string.IsNullOrEmpty(AccessToken))
            {
                using (var webClient = new WebClient())
                {
                    foreach (var url in urls)
                    {
                        if (cancellationToken.HasValue)
                        {
                            if (cancellationToken.Value.IsCancellationRequested)
                                return;
                        }

                        scanUrlChangedCallback(url);
                        CheckUrl(url, webClient, creator, cancellationToken);
                    }
                }
            }
            else
            {
                foreach (var url in urls)
                {
                    if (cancellationToken.HasValue)
                    {
                        if (cancellationToken.Value.IsCancellationRequested)
                            return;
                    }

                    checkUrls.Add(url);

                    if (checkUrls.Count == MaxCheckCount)
                    {
                        CheckUrls(checkUrls, creator, scanUrlChangedCallback, cancellationToken);
                        checkUrls.Clear();
                    }
                }

                CheckUrls(checkUrls, creator, scanUrlChangedCallback, cancellationToken);
            }
        }

        internal void Login()
        {
            var response = SendRequest<RapidgatorUserLogin>($"https://rapidgator.net/api/v2/user/login?login={Username}&password={Password}");

            if (response != null)
            {
                if (response.response != null)
                {
                    AccessToken = response.response.token;
                    IsPremium = response.response.user.is_premium;
                    IsRegistered = response.response.user.state == 1;

                    if (IsPremium)
                        PremiumEndTime = response.response.user.premium_end_time;
                }
                else
                {
                    LogManager.WriteLog(LogLevel.Normal, response.details);
                    LoginError = response.details;
                }
            }
        }

        internal RapidgatorResponse<T> SendRequest<T>(string url)
        {
            int count = 0;

            while (count != 3)
            {
                var request = (HttpWebRequest)WebRequest.Create(url);

                try
                {
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        using (var responseStream = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(responseStream))
                            {
                                return Application.LoadJsonObject<RapidgatorResponse<T>>(reader);
                            }
                        }
                    }
                }
                catch (WebException)
                { }
            }

            return null;
        }

        private List<RapidgatorUrlInfo> CheckUrls(IEnumerable<Uri> urls)
        {
            StringBuilder urlBuilder = new StringBuilder();

            foreach (var url in urls)
            {
                urlBuilder.Append(url);
                urlBuilder.Append(",");
            }

            if (urlBuilder.Length == 0)
                return null;

            var createdUrl = $"https://rapidgator.net/api/v2/file/check_link?url={urlBuilder.Remove(urlBuilder.Length - 1, 1).ToString()}&token={AccessToken}";

            var result = SendRequest<List<RapidgatorUrlInfo>>(createdUrl);

            if (result.response == null)
            {
                LogManager.WriteLog(LogLevel.Normal, result.details);
                return null;
            }

            return result.response;
        }

        private void CheckUrls(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            var checkedUrls = CheckUrls(urls);

            foreach (var urlInfo in checkedUrls)
            {
                scanUrlChangedCallback(new Uri(urlInfo.url));

                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                string containerName = Application.GetContainerName(urlInfo.filename);

                var container = creator.CreateContainer(containerName);

                var entry = new RapidgatorEntry(this, urlInfo.filename, urlInfo.url, urlInfo.size, urlInfo.status);
                entry.Load(container);

                container.AddEntry(entry.FileName, entry);
            }
        }

        private void GetFileInfo(IHtmlNodeCollection htmlNodes, ref string filename, ref string size)
        {
            foreach (var node in htmlNodes)
            {
                if (node.Name == "strong" && string.IsNullOrEmpty(size))
                {
                    size = node.InnerText.Trim();

                    string[] splt = size.Split(' ');

                    if (splt.Length != 2)
                        size = string.Empty;
                }
                else if (node.Name == "a" && string.IsNullOrEmpty(filename))
                    filename = node.InnerText.Trim();
                else
                    GetFileInfo(node.ChildNodes, ref filename, ref size);
            }
        }

        private void GetFileInfo(IHtmlNodeCollection htmlNodes, out string filename, out long size)
        {
            filename = string.Empty;
            size = 0;

            string strSize = string.Empty;

            GetFileInfo(htmlNodes, ref filename, ref strSize);

            if (!string.IsNullOrEmpty(strSize) && size == 0)
            {
                string[] splt = strSize.Split(' ');

                if (splt.Length == 2)
                {
                    splt[1] = splt[1].Trim();

                    if (double.TryParse(splt[0].Trim().Replace('.', ','), out var dsize))
                    {
                        if (splt[1] == "KB")
                            size = (long)(dsize * 1000L);
                        else if (splt[1] == "MB")
                            size = (long)(dsize * 1000L * 1000L);
                        else if (splt[1] == "GB")
                            size = (long)(dsize * 1000L * 1000L * 1000L);
                        else if (splt[1] == "TB")
                            size = (long)(dsize * 1000L * 1000L * 1000L * 1000L);
                    }
                }
            }
        }

        private void CheckUrl(Uri url, WebClient webClient, IContainerCreator creator, CancellationToken? cancellationToken)
        {
            string strHtml = webClient.DownloadString(url);
            IHtmlDocument document = Application.LoadHtmlDocument(strHtml);

            var nodes = document.DocumentNode.SelectNodes("//div[@class = 'text-block file-descr']/div[@class='btm']");
            RapidgatorEntry entry;

            if (nodes == null)
            {
                nodes = document.DocumentNode.SelectNodes("//div[contains(@class,'main-block')]");

                if (nodes != null)
                    entry = new RapidgatorEntry(this, url.Segments[url.Segments.Length - 1], url.ToString(), 0, "NO ACCESS");
                else
                {
                    LogManager.WriteLog(LogLevel.Normal, "node with file informations not found");
                    return;
                }
            }
            else
            {
                GetFileInfo(nodes, out var fileName, out var fileSize);

                if (string.IsNullOrEmpty(fileName))
                {
                    LogManager.WriteLog(LogLevel.Normal, "filename not found");
                    return;
                }

                if (fileSize == 0)
                {
                    LogManager.WriteLog(LogLevel.Normal, "file size not found");
                    return;
                }

                entry = new RapidgatorEntry(this, fileName, url.ToString(), fileSize, "ACCESS");
            }

            IContainer container = creator.CreateContainer(Application.GetContainerName(entry.FileName));

            entry.Load(container);

            container.AddEntry(entry.FileName, entry);
        }
    }
}
