﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Rapidgator
{
    class RapidgatorEntry : DownloadEntry
    {
        class RapidgatorDownload
        {
            public string download_url { get; set; }
            public int delay { get; set; }
        }

        public string Status { get; private set; }

        private string m_fileID;

        public RapidgatorEntry(Rapidgator hoster, string filename, string url, long size, string status)
            : base(hoster)
        {
            FileSize = size;
            FileName = filename;
            Url = new Uri(url);
            Status = status;
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }

        public override bool StartDownload(string path)
        {
            Rapidgator hoster = (Rapidgator)Hoster;

            if (string.IsNullOrEmpty(hoster.AccessToken))
            {
                var browser = Hoster.Application.CreateBrowser();
                browser.Navigate(Url);

                IHtmlNode headerNode = browser.GetElementById("table_header");

                if (headerNode == null)
                {
                    Hoster.LogManager.WriteLog(LogLevel.Normal, "Header not found.");
                    return false;
                }

                if (headerNode.InnerHtml.Contains("style=\"color: red\""))
                {
                    if (hoster.IsPremium)
                        FileState = Hoster.Application.GetFileState(FileDownloadState.Waiting, FileStateIcon.Error, "Limit reached");
                    else
                        FileState = Hoster.Application.GetFileState(FileDownloadState.Waiting, FileStateIcon.Error, "Limit reached - Buy Premium for no limits");

                    return true;
                }

                IHtmlNode documentNode = browser.GetDocument().DocumentNode;

                var freeBtnNodes = documentNode.SelectNodes("//a[contains(@class, 'btn-free')]");

                if (freeBtnNodes == null)
                {
                    Hoster.LogManager.WriteLog(LogLevel.Normal, "No free download button found.");
                    return false;
                }

                foreach (var node in freeBtnNodes)
                {
                    browser.SendClickEvent(node);
                    break;
                }

                var downloadTimerNodes = documentNode.SelectNodes("//span[contains(@class, 'download-timer-seconds')]"); // download-timer-seconds

                browser.Show(System.Windows.Visibility.Visible);

                while (downloadTimerNodes != null)
                {
                    string strWaitTime = string.Empty;
                    foreach (var node in downloadTimerNodes)
                    {
                        string str = node.InnerText.Trim();

                        if (!string.IsNullOrEmpty(str))
                            strWaitTime = str;

                        break;
                    }

                    documentNode = browser.GetDocument().DocumentNode;
                    downloadTimerNodes = documentNode.SelectNodes("//span[contains(@class, 'download-timer-seconds')]"); // download-timer-seconds

                    if (!string.IsNullOrEmpty(strWaitTime))
                    {
                        FileState = Hoster.Application.GetFileState(FileDownloadState.Waiting, FileStateIcon.None, strWaitTime);
                        Debug.WriteLine(FileState.Description);
                    }
                }

                FileState = Hoster.Application.GetFileState(FileDownloadState.Resolving, FileStateIcon.None, "Resolving captcha...");

                IHtmlNode captchaResponseNode = null;

                while (captchaResponseNode == null)
                {
                    var document = browser.GetDocument();

                    if (document != null)
                        captchaResponseNode = document.DocumentNode.SelectSingleNode("//div[@class='g-recaptcha']");
                }

                var captchaNode = captchaResponseNode.ParentNode;

                var captchaFormNode = browser.GetElementById("captchaform");

                if (captchaFormNode == null)
                {
                    Hoster.LogManager.WriteLog(LogLevel.Normal, "captchaform element not found.");
                    return false;
                }

                var btnNode = captchaFormNode.SelectSingleNode(".//a[@class='btn']");

                if (btnNode == null)
                {
                    Hoster.LogManager.WriteLog(LogLevel.Normal, "Button element not found.");
                    return false;
                }

                List<IHtmlNode> hiddenNodes = new List<IHtmlNode> { btnNode };

                Hoster.Application.ResolveCaptcha(browser, captchaResponseNode, hiddenNodes, null, () =>
                {
                    browser.SendClickEvent(btnNode);
                    Uri url;
                    IHtmlNode downloadBtnNode = null;

                    do
                    {
                        IHtmlDocument doc = browser.GetDocument();

                        if (doc != null)
                            downloadBtnNode = doc.DocumentNode.SelectSingleNode("//a[contains(@class, 'btn-download')]");

                    } while (downloadBtnNode == null);

                    browser.SendClickEvent(downloadBtnNode);

                    do
                    {
                        url = browser.GetDownloadUrl();
                    } while (url == null);

                    DownloadFile(url, path);
                });

                return true;
            }
            else
            {
                string createdUrl = $"https://rapidgator.net/api/v2/file/download?file_id={m_fileID}&token={hoster.AccessToken}";

                Rapidgator.RapidgatorResponse<RapidgatorDownload> downloadResponse = hoster.SendRequest<RapidgatorDownload>(createdUrl);

                if (downloadResponse == null)
                    return false;

                if (downloadResponse.response == null)
                {
                    Hoster.LogManager.WriteLog(LogLevel.Normal, downloadResponse.details);
                    return false;
                }

                return DownloadFile(new Uri(downloadResponse.response.download_url), path);
            }
        }

        protected override void OnLoad(object data)
        {
            if (data != null)
            {
                Rapidgator.RapidgatorUrlInfo urlInfo = (Rapidgator.RapidgatorUrlInfo)data;

                FileSize = urlInfo.size;
                Status = urlInfo.status;
                FileName = urlInfo.filename;
            }

            if (Status == "ACCESS")
                FileState = GetFileState(FileStateType.FileInfoAvail);
            else
                FileState = GetFileState(FileStateType.FileNotFound);
        }
    }
}
