﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Pixhost
{
    class PixhostEntry : DownloadEntry
    {
        public PixhostEntry(Pixhost hoster, Uri url, string fileName)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        private void Resolve(Uri url)
        {
            string strHtml = Hoster.DownloadPage(url);

            if (string.IsNullOrEmpty(strHtml))
                FileState = GetFileState(FileStateType.FileNotFound);
            else
            {
                IHtmlDocument document = Hoster.Application.LoadHtmlDocument(strHtml);
                IHtmlNode picNode = document.GetElementbyId("image");

                if (picNode == null)
                    FileState = GetFileState(FileStateType.FileNotFound);
                else if (picNode.Attributes.Contains("src"))
                {
                    Url = new Uri(picNode.Attributes["src"].Value);

                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else if (FileSize < 0)
                        FileState = GetFileState(FileStateType.FileInfoNotAvail);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
                else
                    FileState = GetFileState(FileStateType.FileNotFound);
            }
        }

        protected override void OnLoad(object data)
        {
            try
            {
                if (!Url.PathAndQuery.Contains("show"))
                {
                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else if (FileSize < 0)
                    {
                        FileSize = 0;
                        FileState = GetFileState(FileStateType.FileInfoNotAvail);
                    }
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
                else
                    Resolve(Url);
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }

        protected override void OnWebRequestCreated(HttpWebRequest request, Uri url)
        {
            request.UserAgent = "Chrome/79.0.3945.94";

            Cookie cookie = new Cookie("content_type", "0", "/", "." + Hoster.Host);
            cookie.Expires = DateTime.Now.AddHours(1);

            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(cookie);

            cookie = new Cookie("view_type", "list", "/", "." + Hoster.Host);
            cookie.Expires = DateTime.Now.AddHours(1);
            request.CookieContainer.Add(cookie);
        }
    }
}
