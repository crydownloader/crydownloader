﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.Pixhost
{
    public class Pixhost : PictureHoster, IShareHoster
    {
        public override string Name => "Pixhost";

        public override string Host => "pixhost.to";

        public override Bitmap Icon => Properties.Resources.favicon;

        public Pixhost(IApplication app)
            : base(app)
        { }

        public override void Initialize()
        { }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1];
            IContainer container = args.ContainerCreator.CreateContainer(filename);
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];

            if (url.PathAndQuery.Contains("/thumbs/"))
                url = new Uri(url.Scheme + "://" + Host + url.PathAndQuery.Replace("thumbs", "show"));

            var entry = new PixhostEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }

        protected override void OnDownloadPage(HttpWebRequest httpWebRequest, Uri url, object userData)
        {
            httpWebRequest.UserAgent = "Chrome/79.0.3945.94";

            Cookie cookie = new Cookie("content_type", "0", "/", "." + Host);
            cookie.Expires = DateTime.Now.AddHours(1);

            httpWebRequest.CookieContainer = new CookieContainer();
            httpWebRequest.CookieContainer.Add(cookie);

            cookie = new Cookie("view_type", "list", "/", "." + Host);
            cookie.Expires = DateTime.Now.AddHours(1);
            httpWebRequest.CookieContainer.Add(cookie);
        }
    }
}
