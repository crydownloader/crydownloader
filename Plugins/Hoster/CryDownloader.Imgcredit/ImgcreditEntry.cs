﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Imgcredit
{
    class ImgcreditEntry : DownloadEntry
    {
        public ImgcreditEntry(Imgcredit hoster, Uri url, string fileName)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        protected override void OnLoad(object data)
        {
            try
            {
                if (Url.PathAndQuery.LastIndexOf(".") == -1)
                {
                    string strHtml = Hoster.DownloadPage(Url);

                    if (string.IsNullOrEmpty(strHtml))
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                    {
                        IHtmlDocument document = Hoster.Application.LoadHtmlDocument(strHtml);
                        IHtmlNode picNode = document.GetElementbyId("image-viewer-container");

                        if (picNode == null)
                            FileState = GetFileState(FileStateType.FileNotFound);
                        else
                        {
                            picNode = picNode.SelectSingleNode("./img");

                            if (picNode == null)
                                FileState = GetFileState(FileStateType.FileNotFound);
                            else if (picNode.Attributes.Contains("src"))
                            {
                                Url = new Uri(picNode.Attributes["src"].Value);

                                FileSize = GetFileSize(Url);

                                if (FileSize == 0)
                                    FileState = GetFileState(FileStateType.FileNotFound);
                                else
                                    FileState = GetFileState(FileStateType.FileInfoAvail);
                            }
                            else
                                FileState = GetFileState(FileStateType.FileNotFound);
                        }
                    }
                }
                else
                {
                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }
    }
}
