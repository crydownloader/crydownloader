﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.Imgcredit
{
    public class Imgcredit : Hoster, IShareHoster
    {
        public override string Name => "Imgcredit";

        public override string Host => "imgcredit.xyz";

        public override Bitmap Icon => Properties.Resources.favicon;

        public override FrameworkElement SettingControl => null;

        public Imgcredit(IApplication app)
            : base(app)
        { }

        public override void Initialize()
        { }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1];
            IContainer container = args.ContainerCreator.CreateContainer(filename);

            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];

            int lastIdx = url.PathAndQuery.LastIndexOf(".");

            if (lastIdx != -1)
            {
                int secondlastidx = url.PathAndQuery.LastIndexOf(".", lastIdx - 1);

                if (secondlastidx != -1)
                    filename = url.PathAndQuery.Substring(0, secondlastidx) + url.PathAndQuery.Substring(lastIdx);
            }

            var entry = new ImgcreditEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }

        protected override void OnDownloadPage(HttpWebRequest httpWebRequest, Uri url, object userData)
        {
            httpWebRequest.UserAgent = "Chrome/79.0.3945.94";
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>();
        }
    }
}
