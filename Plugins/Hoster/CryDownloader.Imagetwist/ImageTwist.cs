﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.Imagetwist
{
    public class ImageTwist : PictureHoster, IShareHoster
    {
        public override string Name => "Imagetwist";

        public override string Host => "imagetwist.com";

        public override Bitmap Icon => Properties.Resources.favicon;

        public ImageTwist(IApplication app)
           : base(app)
        { }

        public override void Initialize()
        { }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1].Replace(".htm", "");
            int idx = filename.LastIndexOf("_");

            if (idx != -1)
            {
                filename = filename.Insert(idx, "/").Remove(idx + 1, 1).Remove(0, 2);
                IContainer container = args.ContainerCreator.CreateContainer(filename);
                CreateEntry(args.Url, container, args.CancellationToken);
            }
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];

            if (url.Segments[1] == "th/")
                url = new Uri($"{url.Scheme}://{Host}/{filename}");

            var entry = new ImageTwistEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }
    }
}
