﻿// Code based on https://github.com/flagbug/YoutubeExtractor and https://github.com/cmd64/YoutubeExtractor

using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace CryDownloader.Youtube
{
    class Youtube : Hoster
    {
        class YoutubeStreamingFormat
        {
            public string url { get; set; }
            public string cipher { get; set; }
        }

        class YoutubeStreamingData
        {
            public List<YoutubeStreamingFormat> formats { get; set; }
            public List<YoutubeStreamingFormat> adaptiveFormats { get; set; }
        }

        class YoutubeVideoDetails
        {
            public string title { get; set; }
        }

        class YoutubeAssets
        {
            public string js { get; set; }
        }

        class YoutubePlayerResponse
        {
            public YoutubeVideoDetails videoDetails { get; set; }
            public YoutubeStreamingData streamingData { get; set; }
        }

        class YoutubeArgs
        {
            public string title { get; set; }
            public string player_response { get; set; }
            public string adaptive_fmts { get; set; }
            public string url_encoded_fmt_stream_map { get; set; }
        }

        class YoutubeJson
        {
            public YoutubeAssets assets { get; set; }
            public YoutubeArgs args { get; set; }
        }

        private const string RateBypassFlag = "ratebypass";

        public override string Name => "Youtube";

        public override string Host => "youtube.com";

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public override System.Windows.FrameworkElement SettingControl => m_settingView;

        public VideoFormatType PreferredType { get; set; }
        public bool AutoChoose { get; set; }

        public bool VideoOnly { get; set; }

        public bool AudioOnly { get; set; }

        private Settings m_settingView;

        public Youtube(IApplication app)
            : base(app)
        {
            AutoChoose = true;
        }

        public Uri Normalize(Uri url, out string videoID)
        {
            string strUrl = url.ToString();

            strUrl = strUrl.Trim();

            strUrl = strUrl.Replace("youtu.be/", "youtube.com/watch?v=");
            strUrl = strUrl.Replace("www.youtube", "youtube");
            strUrl = strUrl.Replace("youtube.com/embed/", "youtube.com/watch?v=");

            if (strUrl.Contains("/v/"))
            {
                strUrl = "http://youtube.com" + new Uri(strUrl).AbsolutePath.Replace("/v/", "/watch?v=");
            }

            strUrl = strUrl.Replace("/watch#", "/watch?");

            IDictionary<string, string> query = Utils.ParseQueryString(strUrl);

            videoID = string.Empty;

            if (!query.TryGetValue("v", out var v))
                return null;

            videoID = v;
            return new Uri("http://youtube.com/watch?v=" + v);
        }

        protected override void OnCreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            using (WebClient webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;

                foreach (var uri in urls)
                {
                    Uri url = uri;

                    if (cancellationToken.HasValue)
                    {
                        if (cancellationToken.Value.IsCancellationRequested)
                            return;
                    }

                    scanUrlChangedCallback?.Invoke(url);

                    Uri normalizedUrl = Normalize(url, out var videoID);

                    if (normalizedUrl == null)
                        continue;

                    var json = LoadJson(normalizedUrl, webClient);

                    if (json != null)
                    {
                        YoutubePlayerResponse playerResponse = Application.LoadJsonObject<YoutubePlayerResponse>(json.args.player_response);

                        string videoTitle = GetVideoTitle(json, playerResponse);

                        IContainer container = creator.CreateContainer(videoTitle);

                        ICollection<Tuple<bool, Tuple<string, Uri>>> downloadUrls = ExtractDownloadUrls(json.args, playerResponse);

                        if (downloadUrls.Count != 0)
                        {
                            var infos = GetVideoInfos(downloadUrls).ToList();
                            string htmlPlayerVersion = GetHtml5PlayerVersion(json);

                            Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>> video = null;
                            Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>> audio = null;

                            if (url.Query.Contains("vtag=") || url.Query.Contains("atag="))
                            {
                                var query = Utils.ParseQueryString(url.Query);
                                int audioTag = 0;
                                int videoTag = 0;

                                string str = url.ToString();

                                if (query.ContainsKey("vtag"))
                                {
                                    str = str.Replace($"&vtag={query["vtag"]}", string.Empty);
                                    int.TryParse(query["vtag"], out videoTag);
                                }

                                if (query.ContainsKey("atag"))
                                {
                                    str = str.Replace($"&atag={query["atag"]}", string.Empty);
                                    int.TryParse(query["atag"], out audioTag);
                                }

                                url = new Uri(str);

                                foreach (var info in infos)
                                {
                                    if (info.Item1.FormatCode == videoTag)
                                    {
                                        if (info.Item1.VideoInfo != null)
                                            video = info;
                                    }

                                    if (info.Item1.FormatCode == audioTag)
                                    {
                                        if (info.Item1.AudioInfo != null)
                                            audio = info;
                                    }
                                }

                                if (!VideoOnly && !AudioOnly)
                                {
                                    if (video == null)
                                        video = GetBestVideo(infos);

                                    if (audio == null)
                                        audio = GetBestAudio(infos);

                                    AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion,
                                        new StreamInfo(video.Item1, video.Item2.Item2.Item2, video.Item2.Item1, video.Item2.Item2.Item1),
                                        new StreamInfo(audio.Item1, audio.Item2.Item2.Item2, audio.Item2.Item1, audio.Item2.Item2.Item1)));
                                }
                                else
                                {
                                    if (VideoOnly)
                                    {
                                        AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion,
                                            new StreamInfo(video.Item1, video.Item2.Item2.Item2, video.Item2.Item1, video.Item2.Item2.Item1),
                                            null));
                                    }

                                    if (AudioOnly)
                                    {
                                        AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion, null,
                                            new StreamInfo(audio.Item1, audio.Item2.Item2.Item2, audio.Item2.Item1, audio.Item2.Item2.Item1)));
                                    }
                                }
                            }
                            else if (AutoChoose)
                            {
                                video = GetBestVideo(infos);
                                audio = GetBestAudio(infos);

                                if (!VideoOnly && !AudioOnly)
                                {
                                    AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion,
                                        new StreamInfo(video.Item1, video.Item2.Item2.Item2, video.Item2.Item1, video.Item2.Item2.Item1),
                                        new StreamInfo(audio.Item1, audio.Item2.Item2.Item2, audio.Item2.Item1, audio.Item2.Item2.Item1)));
                                }
                                else
                                {
                                    if (VideoOnly)
                                    {
                                        AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion,
                                            new StreamInfo(video.Item1, video.Item2.Item2.Item2, video.Item2.Item1, video.Item2.Item2.Item1),
                                            null));
                                    }

                                    if (AudioOnly)
                                    {
                                        AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion, null,
                                            new StreamInfo(audio.Item1, audio.Item2.Item2.Item2, audio.Item2.Item1, audio.Item2.Item2.Item1)));
                                    }
                                }
                            }
                            else if (PreferredType != null)
                            {
                                foreach (var info in infos)
                                {
                                    if (info.Item1.FormatCode == PreferredType.FormatCode)
                                    {
                                        if (info.Item1.AudioInfo != null)
                                            audio = info;

                                        if (info.Item1.VideoInfo != null)
                                            video = info;

                                        break;
                                    }
                                }

                                if (!VideoOnly && !AudioOnly)
                                {
                                    if (video == null)
                                        video = GetBestVideo(infos);

                                    if (audio == null)
                                        audio = GetBestAudio(infos);

                                    AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion,
                                        new StreamInfo(video.Item1, video.Item2.Item2.Item2, video.Item2.Item1, video.Item2.Item2.Item1),
                                        new StreamInfo(audio.Item1, audio.Item2.Item2.Item2, audio.Item2.Item1, audio.Item2.Item2.Item1)));
                                }
                                else if (video != null)
                                {
                                    AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion,
                                        new StreamInfo(video.Item1, video.Item2.Item2.Item2, video.Item2.Item1, video.Item2.Item2.Item1),
                                        null));
                                }
                                else if (audio != null)
                                {
                                    AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion, null,
                                        new StreamInfo(audio.Item1, audio.Item2.Item2.Item2, audio.Item2.Item1, audio.Item2.Item2.Item1)));
                                }
                            }
                            else
                            {
                                foreach (var info in infos)
                                {
                                    if (info.Item1.VideoInfo != null)
                                    {
                                        AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion,
                                            new StreamInfo(info.Item1, info.Item2.Item2.Item2, info.Item2.Item1, info.Item2.Item2.Item1),
                                            null));
                                    }

                                    if (info.Item1.AudioInfo != null)
                                    {
                                        AddEntry(container, webClient, new EntryCreateInfo(url, videoTitle, htmlPlayerVersion, null,
                                            new StreamInfo(info.Item1, info.Item2.Item2.Item2, info.Item2.Item1, info.Item2.Item2.Item1)));
                                    }
                                }
                            }
                        }
                        else
                        {
                            var entry = new YoutubeEntry(this, videoID, url, videoTitle, Application.GetFileState(FileStateType.FileNotFound));
                            entry.Load(webClient);

                            container.AddEntry(url.ToString() + videoTitle, entry);
                        }
                    }
                    else
                    {
                        var entry = new YoutubeEntry(this, videoID, url, Application.GetFileState(FileStateType.FileNotFound));
                        IContainer container = creator.CreateContainer(videoID);

                        entry.Load(webClient);
                        container.AddEntry(url.ToString() + videoID, entry);
                    }
                }
            }
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>()
            {
                {"PreferredType", PreferredType == null ? "" : PreferredType.FormatCode.ToString() },
                {"AutoChoose", AutoChoose ? "1" : "0" },
                {"VideoOnly", VideoOnly ? "1" : "0" },
                {"AudioOnly", AudioOnly ? "1" : "0" }
            };
        }

        public override void Initialize()
        {
            SettingViewModel viewModel = new SettingViewModel(this);

            Application.Dispatcher.Invoke(() => m_settingView = new Settings(viewModel));
        }

        public override bool IsValidUrl(Uri url)
        {
            string strUrl = url.ToString();

            return strUrl.Contains("youtu.be") || strUrl.Contains("youtube") || strUrl.Contains("googlevideo.com");
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            if (properties.TryGetValue("AutoChoose", out var str))
                AutoChoose = str == "1";

            if (properties.TryGetValue("VideoOnly", out str))
                VideoOnly = str == "1";

            if (properties.TryGetValue("AudioOnly", out str))
                AudioOnly = str == "1";

            if (properties.TryGetValue("PreferredType", out var preferredType))
            {
                if (string.IsNullOrEmpty(preferredType))
                    return;

                if (int.TryParse(preferredType, out var type))
                {
                    if (VideoFormatType.DefaultVideoTypes.TryGetValue(type, out var value))
                        PreferredType = value;
                    else
                        Application.LogManager.WriteLog(LogLevel.Normal, "Video type not found");
                }
                else
                    Application.LogManager.WriteLog(LogLevel.Normal, "Invalid video type");
            }
        }

        private void AddEntry(IContainer container, WebClient webClient, EntryCreateInfo createInfo)
        {
            YoutubeEntry entry = new YoutubeEntry(this, createInfo);

            entry.Load(webClient);

            container.AddEntry(entry.FileName, entry);
        }

        private YoutubeJson LoadJson(Uri url, WebClient client)
        {
            string pageSource = client.DownloadString(url);

            if (IsVideoUnavailable(pageSource))
                return null;

            var dataRegex = new Regex(@"ytplayer\.config\s*=\s*(\{.+?\});", RegexOptions.Multiline);

            string extractedJson = dataRegex.Match(pageSource).Result("$1");
            return Application.LoadJsonObject<YoutubeJson>(extractedJson);
        }

        private static bool IsVideoUnavailable(string pageSource)
        {
            const string unavailableContainer = "<div id=\"watch-player-unavailable\">";

            return pageSource.Contains(unavailableContainer);
        }

        private static string GetVideoTitle(YoutubeJson json, YoutubePlayerResponse playerResponse)
        {
            string title = json.args.title;

            if (title == null)
                title = playerResponse.videoDetails.title;

            return title == null ? string.Empty : title.ToString();
        }

        private static ICollection<Tuple<bool, Tuple<string, Uri>>> ExtractDownloadUrls(YoutubeArgs json, YoutubePlayerResponse playerResponse)
        {
            string strStreamMap = GetStreamMap(json);
            List<Tuple<bool, Tuple<string, Uri>>> result = new List<Tuple<bool, Tuple<string, Uri>>>();

            string[] splitByUrls;
            string[] adaptiveFmtSplitByUrls;

            if (string.IsNullOrEmpty(strStreamMap))
            {
                YoutubeStreamingData streamData = playerResponse.streamingData;

                if (streamData == null)
                    return result;

                List<string> tmp = new List<string>();
                foreach (var format in streamData.formats)
                {
                    if (format.cipher != null)
                        tmp.Add(format.cipher);
                    else
                    {
                        result.Add(new Tuple<bool, Tuple<string, Uri>>(false, new Tuple<string, Uri>(
                            string.Empty, new Uri(format.url))));
                    }
                }

                splitByUrls = tmp.ToArray();
                tmp.Clear();

                foreach (var format in streamData.adaptiveFormats)
                {
                    if (format.cipher != null)
                        tmp.Add(format.cipher);
                    else
                    {
                        result.Add(new Tuple<bool, Tuple<string, Uri>>(false, new Tuple<string, Uri>(
                            string.Empty, new Uri(format.url))));
                    }
                }

                adaptiveFmtSplitByUrls = tmp.ToArray();
            }
            else
            {
                splitByUrls = strStreamMap.Split(',');
                adaptiveFmtSplitByUrls = GetAdaptiveStreamMap(json).Split(',');
            }

            splitByUrls = splitByUrls.Concat(adaptiveFmtSplitByUrls).ToArray();

            foreach (string s in splitByUrls)
            {
                IDictionary<string, string> queries = Utils.ParseQueryString(s);
                string url;

                bool requiresDecryption = false;

                if (queries.ContainsKey("s") || queries.ContainsKey("sig"))
                {
                    requiresDecryption = queries.ContainsKey("s");
                    string signature = queries.ContainsKey("s") ? queries["s"] : queries["sig"];

                    url = string.Format("{0}&{1}={2}", queries["url"], YoutubeEntry.SignatureQuery, signature);

                    string fallbackHost = queries.ContainsKey("fallback_host") ? "&fallback_host=" + queries["fallback_host"] : String.Empty;

                    url += fallbackHost;
                }
                else
                {
                    url = queries["url"];
                }

                string sigTag = "signature";

                if (queries.ContainsKey("sp"))
                    sigTag = queries["sp"];


                url = System.Web.HttpUtility.UrlDecode(url);
                url = System.Web.HttpUtility.UrlDecode(url);

                IDictionary<string, string> parameters = Utils.ParseQueryString(url);

                if (!parameters.ContainsKey(RateBypassFlag))
                    url += string.Format("&{0}={1}", RateBypassFlag, "yes");

                result.Add(new Tuple<bool, Tuple<string, Uri>>(requiresDecryption, new Tuple<string, Uri>(sigTag, new Uri(url))));
            }

            return result;
        }

        private static string GetAdaptiveStreamMap(YoutubeArgs args)
        {
            string streamMap = args.adaptive_fmts;

            // bugfix: adaptive_fmts is missing in some videos, use url_encoded_fmt_stream_map instead
            if (streamMap == null)
                streamMap = args.url_encoded_fmt_stream_map;

            return streamMap;
        }

        private static string GetStreamMap(YoutubeArgs args)
        {
            string streamMapString = args.url_encoded_fmt_stream_map;

            if (streamMapString == null || streamMapString.Contains("been+removed"))
                return string.Empty;

            return streamMapString;
        }

        private static IEnumerable<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>> GetVideoInfos(IEnumerable<Tuple<bool, Tuple<string, Uri>>> extractionInfos)
        {
            var downLoadInfos = new List<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>>();

            foreach (var extractionInfo in extractionInfos)
            {
                string itag = Utils.ParseQueryString(extractionInfo.Item2.Item2.Query)["itag"];

                int formatCode = int.Parse(itag);

                if (VideoFormatType.DefaultVideoTypes.TryGetValue(formatCode, out var info))
                {
                    var tuple = new Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>(info,
                        new Tuple<bool, Tuple<string, string>>(extractionInfo.Item1,
                        new Tuple<string, string>(extractionInfo.Item2.Item1, extractionInfo.Item2.Item2.ToString())));

                    downLoadInfos.Add(tuple);
                }
            }

            return downLoadInfos;
        }

        private static string GetHtml5PlayerVersion(YoutubeJson json)
        {
            var regex = new Regex(@"player(.+?).js");

            return regex.Match(json.assets.js).Result("$1");
        }

        private static Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>> GetBestAudio(IEnumerable<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>> formats)
        {
            int bestAudio = 0;
            Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>> result = null;

            foreach (var info in formats)
            {
                if (info.Item1.AudioInfo != null)
                {
                    if (info.Item1.AudioInfo.Bitrate > bestAudio)
                        bestAudio = info.Item1.AudioInfo.Bitrate;
                }
            }

            foreach (var info in formats)
            {
                if (info.Item1.AudioInfo != null)
                {
                    if (info.Item1.AudioInfo.Bitrate == bestAudio)
                        result = info;
                }
            }

            return result;
        }

        private static Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>> GetBestVideo(IEnumerable<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>> formats)
        {
            int bestHeight = 0;
            int bestWidth = 0;
            int bestFps = 0;
            List<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>> bestList1 = new List<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>>();
            List<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>> bestList2 = new List<Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>>>();
            Tuple<VideoFormatType, Tuple<bool, Tuple<string, string>>> result = null;

            foreach (var info in formats)
            {
                if (info.Item1.VideoInfo != null)
                {
                    if (info.Item1.VideoInfo.Height > bestHeight)
                        bestHeight = info.Item1.VideoInfo.Height;
                }
            }

            foreach (var info in formats)
            {
                if (info.Item1.VideoInfo != null)
                {
                    if (info.Item1.VideoInfo.Height == bestHeight)
                        bestList1.Add(info);
                }
            }

            foreach (var info in bestList1)
            {
                if (info.Item1.VideoInfo != null)
                {
                    if (info.Item1.VideoInfo.Width > bestWidth)
                        bestWidth = info.Item1.VideoInfo.Width;
                }
            }

            foreach (var info in bestList1)
            {
                if (info.Item1.VideoInfo != null)
                {
                    if (info.Item1.VideoInfo.Width == bestWidth)
                        bestList2.Add(info);
                }
                else
                    bestList2.Add(info);
            }

            foreach (var info in bestList2)
            {
                if (info.Item1.VideoInfo != null)
                {
                    if (info.Item1.VideoInfo.Fps > bestFps)
                        bestFps = info.Item1.VideoInfo.Fps;
                }
            }

            foreach (var info in bestList2)
            {
                if (info.Item1.VideoInfo != null)
                {
                    if (info.Item1.VideoInfo.Fps == bestFps)
                        result = info;
                }
            }

            return result;
        }
    }
}
