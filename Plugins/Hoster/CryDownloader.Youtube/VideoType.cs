﻿// Code based on https://github.com/flagbug/YoutubeExtractor and https://github.com/cmd64/YoutubeExtractor

using System;

namespace CryDownloader.Youtube
{
    enum VideoType
    {
        H263,
        H264,
        H265,
        MP4V,
        VP8,
        VP9
    }

    [Flags]
    enum VideoFormat
    {
        None,
        ThirdDimension = 1 << 0,
        Hls = 1 << 2,
        Dash = 1 << 3
    }
}
