﻿// Code based on https://github.com/flagbug/YoutubeExtractor and https://github.com/cmd64/YoutubeExtractor

using System;

namespace CryDownloader.Youtube
{
    [Flags]
    enum AdaptiveType
    {
        None,
        Audio = 1 << 0,
        Video = 1 << 1,
        Hdr = 1 << 2,
        ThirdDimension = 1 << 3
    }
}
