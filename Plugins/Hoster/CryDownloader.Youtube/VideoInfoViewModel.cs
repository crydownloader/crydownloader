﻿namespace CryDownloader.Youtube
{
    class VideoInfoViewModel
    {
        public VideoFormatType Data { get; }

        public VideoInfoViewModel(VideoFormatType data)
        {
            Data = data;
        }

        public override string ToString()
        {
            if (Data == null)
                return "All";

            if (Data.VideoInfo != null && Data.AudioInfo != null)
                return Data.VideoInfo.ToString() + " " + Data.AudioInfo.ToString();

            if (Data.VideoInfo != null)
                return Data.VideoInfo.ToString();

            if (Data.AudioInfo != null)
                return Data.AudioInfo.ToString();

            return "Unknown";
        }
    }
}
