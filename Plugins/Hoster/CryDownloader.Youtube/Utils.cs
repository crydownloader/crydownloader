﻿// Code based on https://github.com/flagbug/YoutubeExtractor and https://github.com/cmd64/YoutubeExtractor

using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CryDownloader.Youtube
{
    static class Utils
    {
        public static IDictionary<string, string> ParseQueryString(string s)
        {
            // remove anything other than query string from url
            if (s.Contains("?"))
            {
                s = s.Substring(s.IndexOf('?') + 1);
            }

            var dictionary = new Dictionary<string, string>();

            foreach (string vp in Regex.Split(s, "&"))
            {
                int idx = vp.IndexOf("=");

                if (idx >= 0)
                {
                    string itm1 = vp.Substring(0, idx);
                    string itm2 = vp.Substring(idx + 1);

                    dictionary.Add(itm1, System.Web.HttpUtility.UrlDecode(itm2));
                }
            }

            return dictionary;
        }
    }
}
