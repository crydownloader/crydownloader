﻿// Code based on https://github.com/flagbug/YoutubeExtractor and https://github.com/cmd64/YoutubeExtractor

using System;

namespace CryDownloader.Youtube
{
    enum AudioType
    {
        AAC,
        MP3,
        DTSE,
        EC3,
        Vorbis,
        Opus,
    }

    [Flags]
    enum AudioFormat
    {
        None,
        Dash = 1 << 0,
    }
}
