﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace CryDownloader.Youtube
{
    class SettingViewModel : INotifyPropertyChanged
    {
        private VideoInfoViewModel m_preferredType;

        public VideoInfoViewModel PreferredType
        {
            get => m_preferredType;
            set
            {
                m_preferredType = value;
                m_hoster.PreferredType = m_preferredType.Data;
                OnPropertyChanged();
            }
        }

        public bool AutoChoose
        {
            get => m_hoster.AutoChoose;
            set
            {
                m_hoster.AutoChoose = value;
                OnPropertyChanged();
                OnPropertyChanged("ComboBoxEnabled");
            }
        }

        public bool AudioOnly
        {
            get => m_hoster.AudioOnly;
            set
            {
                m_hoster.AudioOnly = value;
                OnPropertyChanged();
            }
        }

        public bool VideoOnly
        {
            get => m_hoster.VideoOnly;
            set
            {
                m_hoster.VideoOnly = value;
                OnPropertyChanged();
            }
        }

        public bool ComboBoxEnabled => !AutoChoose;

        public ObservableCollection<VideoInfoViewModel> VideoTypes { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly Youtube m_hoster;

        public SettingViewModel(Youtube hoster)
        {
            m_hoster = hoster;
            VideoTypes = new ObservableCollection<VideoInfoViewModel>();

            VideoTypes.Add(new VideoInfoViewModel(null));

            foreach (var info in VideoFormatType.DefaultVideoTypes)
            {
                if (info.Value.VideoInfo == null)
                    continue;

                if (info.Value.AudioInfo == null)
                    continue;

                if (info.Value.VideoInfo.Height != 0)
                {
                    var viewModel = new VideoInfoViewModel(info.Value);

                    if (info.Value == hoster.PreferredType)
                        PreferredType = viewModel;

                    VideoTypes.Add(viewModel);
                }
            }

            if (PreferredType == null)
                PreferredType = VideoTypes.First();

            AutoChoose = hoster.AutoChoose;
        }

        public void Apply()
        {
            m_hoster.AutoChoose = AutoChoose;

            if (!AutoChoose)
                m_hoster.PreferredType = PreferredType.Data;
            else
                m_hoster.PreferredType = null;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
