﻿// All formata from: https://github.com/ytdl-org/youtube-dl

using System.Collections.Generic;

namespace CryDownloader.Youtube
{
    class VideoInfo
    {
        public int Width { get; }
        public int Height { get; }
        public int Fps { get; }
        public VideoType Type { get; }
        public VideoFormat Format { get; }
        public string Container { get; }

        public VideoInfo(int width, int height, VideoType videoType)
        {
            Width = width;
            Height = height;
            Type = videoType;
        }

        public VideoInfo(VideoType videoType)
        {
            Width = 0;
            Height = 0;
            Type = videoType;
            Format = VideoFormat.None;
        }

        public VideoInfo(int width, int height, VideoType videoType, VideoFormat format)
        {
            Width = width;
            Height = height;
            Type = videoType;
            Format = format;
        }

        public VideoInfo(int height, VideoType videoType, VideoFormat format)
        {
            Width = 0;
            Height = height;
            Type = videoType;
            Format = format;
        }

        public VideoInfo(VideoType videoType, VideoFormat format)
        {
            Width = 0;
            Height = 0;
            Type = videoType;
            Format = format;
        }

        public VideoInfo(int width, int height, VideoType videoType, string container, VideoFormat format)
        {
            Width = width;
            Height = height;
            Type = videoType;
            Format = format;
            Container = container;
        }

        public VideoInfo(int height, VideoType videoType, int fps, string container, VideoFormat format)
        {
            Width = 0;
            Height = height;
            Type = videoType;
            Format = format;
            Container = container;
            Fps = fps;
        }

        public VideoInfo(int height, VideoType videoType, string container, VideoFormat format)
        {
            Width = 0;
            Height = height;
            Type = videoType;
            Format = format;
            Container = container;
        }

        public override string ToString()
        {
            return ToString(false);
        }

        public string ToString(bool bNoSpaces)
        {
            string strResult = "";

            if (Height != 0)
            {
                if (Width != 0)
                {
                    if (Fps != 0)
                        strResult += $"{Width}x{Height}@{Fps}fps ";
                    else
                        strResult += $"{Width}x{Height} ";
                }
                else if (Fps != 0)
                    strResult += $"{Height}p@{Fps}fps ";
                else
                    strResult += $"{Height}p ";
            }

            strResult += $"{Type} ";

            if ((Format & VideoFormat.ThirdDimension) != 0)
                strResult += $"3D ";

            if ((Format & VideoFormat.Dash) != 0)
                strResult += $"DASH ";

            if ((Format & VideoFormat.Hls) != 0)
                strResult += $"HLS ";

            if (!string.IsNullOrEmpty(Container))
                strResult += $"{Container} ";

            strResult = strResult.Remove(strResult.Length - 1, 1);

            if (bNoSpaces)
                return strResult.Replace(" ", "_");

            return strResult;
        }
    }

    class AudioInfo
    {
        public int Bitrate { get; }
        public AudioType Type { get; }
        public AudioFormat Format { get; }
        public string Container { get; }

        public AudioInfo(AudioType audioType, int bitrate)
        {
            Bitrate = bitrate;
            Type = audioType;
        }

        public AudioInfo(AudioType audioType)
        {
            Bitrate = 0;
            Type = audioType;
        }

        public AudioInfo(AudioType audioType, int bitrate, AudioFormat format)
        {
            Bitrate = bitrate;
            Type = audioType;
            Format = format;
        }

        public AudioInfo(AudioType audioType, int bitrate, string container, AudioFormat format)
        {
            Bitrate = bitrate;
            Type = audioType;
            Format = format;
            Container = container;
        }

        public AudioInfo(AudioType audioType, string container, AudioFormat format)
        {
            Bitrate = 0;
            Type = audioType;
            Format = format;
            Container = container;
        }

        public override string ToString()
        {
            return ToString(false);
        }

        public string ToString(bool bNoSpaces)
        {
            string strResult = string.Empty;

            if (Bitrate != 0)
            {
                if (bNoSpaces)
                    strResult += $"{Bitrate}kbit ";
                else
                    strResult += $"{Bitrate} kbit ";
            }

            strResult += $"{Type} ";

            if ((Format & AudioFormat.Dash) != 0)
                strResult += "DASH ";

            if (!string.IsNullOrEmpty(Container))
                strResult += $"{Container} ";

            strResult = strResult.Remove(strResult.Length - 1, 1);

            if (bNoSpaces)
                return strResult.Replace(" ", "_");

            return strResult;
        }
    }

    class VideoFormatType
    {
        public static readonly Dictionary<int, VideoFormatType> DefaultVideoTypes = new Dictionary<int, VideoFormatType>()
        {
            { 5, new VideoFormatType(5, ".flv", new VideoInfo(400, 240, VideoType.H263), new AudioInfo(AudioType.MP3, 64)) },
            { 6, new VideoFormatType(6, ".flv", new VideoInfo(450, 270, VideoType.H263), new AudioInfo(AudioType.MP3, 64)) },
            { 13, new VideoFormatType(13, ".3gp", new VideoInfo(VideoType.MP4V), new AudioInfo(AudioType.AAC)) },
            { 17, new VideoFormatType(17, ".3gp", new VideoInfo(176, 144, VideoType.MP4V), new AudioInfo(AudioType.AAC, 24)) },
            { 18, new VideoFormatType(18, ".mp4", new VideoInfo(640, 360, VideoType.H264), new AudioInfo(AudioType.AAC, 96)) },
            { 22, new VideoFormatType(22, ".mp4", new VideoInfo(1280, 720, VideoType.H264), new AudioInfo(AudioType.AAC, 192)) },
            { 34, new VideoFormatType(34, ".flv", new VideoInfo(640, 360, VideoType.H264), new AudioInfo(AudioType.AAC, 128)) },
            { 35, new VideoFormatType(35, ".flv", new VideoInfo(854, 480, VideoType.H264), new AudioInfo(AudioType.AAC, 128)) },
            { 36, new VideoFormatType(36, ".3gp", new VideoInfo(320, 0, VideoType.MP4V), new AudioInfo(AudioType.AAC)) },
            { 37, new VideoFormatType(37, ".mp4", new VideoInfo(1920, 1080, VideoType.H264), new AudioInfo(AudioType.AAC, 192)) },
            { 38, new VideoFormatType(38, ".mp4", new VideoInfo(4096, 3072, VideoType.H264), new AudioInfo(AudioType.AAC, 192)) },
            { 43, new VideoFormatType(43, ".webm", new VideoInfo(640, 360, VideoType.VP8), new AudioInfo(AudioType.Vorbis, 128)) },
            { 44, new VideoFormatType(44, ".webm", new VideoInfo(854, 480, VideoType.VP8), new AudioInfo(AudioType.Vorbis, 128)) },
            { 45, new VideoFormatType(45, ".webm", new VideoInfo(1280, 720, VideoType.VP8), new AudioInfo(AudioType.Vorbis, 192)) },
            { 46, new VideoFormatType(46, ".webm", new VideoInfo(1920, 1080, VideoType.VP8), new AudioInfo(AudioType.Vorbis, 192)) },
             /* MP4 3D */
            { 82, new VideoFormatType(82, ".mp4", new VideoInfo(360, VideoType.H264, VideoFormat.ThirdDimension), new AudioInfo(AudioType.AAC, 128)) },
            { 83, new VideoFormatType(83, ".mp4", new VideoInfo(480, VideoType.H264, VideoFormat.ThirdDimension), new AudioInfo(AudioType.AAC, 192)) },
            { 84, new VideoFormatType(84, ".mp4", new VideoInfo(720, VideoType.H264, VideoFormat.ThirdDimension), new AudioInfo(AudioType.AAC, 192)) },
            { 85, new VideoFormatType(85, ".mp4", new VideoInfo(1080, VideoType.H264, VideoFormat.ThirdDimension), new AudioInfo(AudioType.AAC, 192)) },
             /* WEBM 3D */
            { 100, new VideoFormatType(100, ".webm", new VideoInfo(360, VideoType.VP8, VideoFormat.ThirdDimension), new AudioInfo(AudioType.Vorbis, 128)) },
            { 101, new VideoFormatType(101, ".webm", new VideoInfo(480, VideoType.VP8, VideoFormat.ThirdDimension), new AudioInfo(AudioType.Vorbis, 192)) },
            { 102, new VideoFormatType(102, ".webm", new VideoInfo(720, VideoType.VP8, VideoFormat.ThirdDimension), new AudioInfo(AudioType.Vorbis, 192)) },
             /* Apple HTTP Live Streaming */
            { 91, new VideoFormatType(91, ".mp4", new VideoInfo(144, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 48)) },
            { 92, new VideoFormatType(101, ".mp4", new VideoInfo(240, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 48)) },
            { 93, new VideoFormatType(102, ".mp4", new VideoInfo(360, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 128)) },
            { 94, new VideoFormatType(102, ".mp4", new VideoInfo(480, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 128)) },
            { 95, new VideoFormatType(102, ".mp4", new VideoInfo(720, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 256)) },
            { 96, new VideoFormatType(102, ".mp4", new VideoInfo(1080, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 256)) },
            { 132, new VideoFormatType(102, ".mp4", new VideoInfo(240, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 48)) },
            { 151, new VideoFormatType(102, ".mp4", new VideoInfo(72, VideoType.H264, VideoFormat.Hls), new AudioInfo(AudioType.AAC, 24)) },
             /* DASH MP4 Video */
            { 133, new VideoFormatType(133, ".mp4", new VideoInfo(240, VideoType.H264, VideoFormat.Dash)) },
            { 134, new VideoFormatType(134, ".mp4", new VideoInfo(360, VideoType.H264, VideoFormat.Dash)) },
            { 135, new VideoFormatType(135, ".mp4", new VideoInfo(480, VideoType.H264, VideoFormat.Dash)) },
            { 136, new VideoFormatType(136, ".mp4", new VideoInfo(720, VideoType.H264, VideoFormat.Dash)) },
            { 137, new VideoFormatType(137, ".mp4", new VideoInfo(1080, VideoType.H264, VideoFormat.Dash)) },
            { 138, new VideoFormatType(138, ".mp4", new VideoInfo(VideoType.H264, VideoFormat.Dash)) },
            { 160, new VideoFormatType(160, ".mp4", new VideoInfo(144, VideoType.H264, VideoFormat.Dash)) },
            { 212, new VideoFormatType(212, ".mp4", new VideoInfo(480, VideoType.H264, VideoFormat.Dash)) },
            { 264, new VideoFormatType(264, ".mp4", new VideoInfo(1440, VideoType.H264, VideoFormat.Dash)) },
            { 298, new VideoFormatType(298, ".mp4", new VideoInfo(720, 60, VideoType.H264, VideoFormat.Dash)) },
            { 299, new VideoFormatType(299, ".mp4", new VideoInfo(1080, 60, VideoType.H264, VideoFormat.Dash)) },
            { 266, new VideoFormatType(266, ".mp4", new VideoInfo(2160, VideoType.H264, VideoFormat.Dash)) },
             /* DASH Audio */
            { 139, new VideoFormatType(139, ".m4a", new AudioInfo(AudioType.AAC, 48, "m4a_dash", AudioFormat.Dash)) },
            { 140, new VideoFormatType(140, ".m4a", new AudioInfo(AudioType.AAC, 128, "m4a_dash", AudioFormat.Dash)) },
            { 141, new VideoFormatType(141, ".m4a", new AudioInfo(AudioType.AAC, 256, "m4a_dash", AudioFormat.Dash)) },
            { 256, new VideoFormatType(256, ".m4a", new AudioInfo(AudioType.AAC, "m4a_dash", AudioFormat.Dash)) },
            { 258, new VideoFormatType(258, ".m4a", new AudioInfo(AudioType.AAC, "m4a_dash", AudioFormat.Dash)) },
            { 325, new VideoFormatType(325, ".m4a", new AudioInfo(AudioType.DTSE, "m4a_dash", AudioFormat.Dash)) },
            { 328, new VideoFormatType(328, ".m4a", new AudioInfo(AudioType.EC3, "m4a_dash", AudioFormat.Dash)) },
             /* DASH WEBM Video */
            { 167, new VideoFormatType(167, ".webm", new VideoInfo(640, 360, VideoType.VP8, "webm", VideoFormat.Dash)) },
            { 168, new VideoFormatType(168, ".webm", new VideoInfo(854, 480, VideoType.VP8, "webm", VideoFormat.Dash)) },
            { 169, new VideoFormatType(169, ".webm", new VideoInfo(1280, 720, VideoType.VP8, "webm", VideoFormat.Dash)) },
            { 170, new VideoFormatType(170, ".webm", new VideoInfo(1920, 1080, VideoType.VP8, "webm", VideoFormat.Dash)) },
            { 218, new VideoFormatType(218, ".webm", new VideoInfo(854, 480, VideoType.VP8, "webm", VideoFormat.Dash)) },
            { 219, new VideoFormatType(219, ".webm", new VideoInfo(854, 480, VideoType.VP8, "webm", VideoFormat.Dash)) },
            { 278, new VideoFormatType(278, ".webm", new VideoInfo(144, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 242, new VideoFormatType(242, ".webm", new VideoInfo(240, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 243, new VideoFormatType(243, ".webm", new VideoInfo(360, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 244, new VideoFormatType(244, ".webm", new VideoInfo(480, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 245, new VideoFormatType(245, ".webm", new VideoInfo(480, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 246, new VideoFormatType(246, ".webm", new VideoInfo(480, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 247, new VideoFormatType(247, ".webm", new VideoInfo(720, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 248, new VideoFormatType(248, ".webm", new VideoInfo(1080, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 271, new VideoFormatType(271, ".webm", new VideoInfo(1440, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 272, new VideoFormatType(272, ".webm", new VideoInfo(2160, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 302, new VideoFormatType(302, ".webm", new VideoInfo(720, VideoType.VP9, 60, "webm", VideoFormat.Dash)) },
            { 303, new VideoFormatType(303, ".webm", new VideoInfo(1080, VideoType.VP9, 60, "webm", VideoFormat.Dash)) },
            { 308, new VideoFormatType(308, ".webm", new VideoInfo(1440, VideoType.VP9, 60, "webm", VideoFormat.Dash)) },
            { 313, new VideoFormatType(313, ".webm", new VideoInfo(2160, VideoType.VP9, "webm", VideoFormat.Dash)) },
            { 315, new VideoFormatType(315, ".webm", new VideoInfo(2160, VideoType.VP9, 60, "webm", VideoFormat.Dash)) },
             /* DASH WEBM Audio */
            { 171, new VideoFormatType(171, ".webm", new AudioInfo(AudioType.Vorbis, 128, AudioFormat.Dash)) },
            { 172, new VideoFormatType(172, ".webm", new AudioInfo(AudioType.Vorbis, 256, AudioFormat.Dash)) },
             /* DASH WEBM Audio with opus inside */
            { 249, new VideoFormatType(249, ".webm", new AudioInfo(AudioType.Opus, 50, AudioFormat.Dash)) },
            { 250, new VideoFormatType(250, ".webm", new AudioInfo(AudioType.Opus, 70, AudioFormat.Dash)) },
            { 251, new VideoFormatType(251, ".webm", new AudioInfo(AudioType.Opus, 160, AudioFormat.Dash)) },
        };

        public int FormatCode { get; }
        public string Extension { get; }
        public VideoInfo VideoInfo { get; }
        public AudioInfo AudioInfo { get; }

        public VideoFormatType(int formatCode, string extension, VideoInfo videoInfo, AudioInfo audioInfo)
        {
            FormatCode = formatCode;
            Extension = extension;
            VideoInfo = videoInfo;
            AudioInfo = audioInfo;
        }

        public VideoFormatType(int formatCode, string extension, VideoInfo videoInfo)
        {
            FormatCode = formatCode;
            Extension = extension;
            VideoInfo = videoInfo;
            AudioInfo = null;
        }

        public VideoFormatType(int formatCode, string extension, AudioInfo audioInfo)
        {
            FormatCode = formatCode;
            Extension = extension;
            VideoInfo = null;
            AudioInfo = audioInfo;
        }

        public override string ToString()
        {
            string result = string.Empty;

            if (VideoInfo != null)
                result += VideoInfo.ToString() + " ";

            if (AudioInfo != null)
                result += AudioInfo.ToString() + " ";

            return result.Remove(result.Length - 1, 1);
        }

        public static bool operator ==(VideoFormatType left, VideoFormatType right)
        {
            if ((object)left == null && (object)right == null)
                return true;
            else if ((object)left == null)
                return false;
            else if ((object)right == null)
                return false;

            return left.FormatCode == right.FormatCode;
        }

        public static bool operator !=(VideoFormatType left, VideoFormatType right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            if (obj is VideoFormatType videoFormatType)
                return this == videoFormatType;

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
