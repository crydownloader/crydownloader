﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Youtube
{
    class StreamInfo
    {
        public VideoFormatType FormatType { get; }
        public string DownloadUrl { get; }
        public bool RequireDecryption { get; }
        public string SigTag { get; }

        public StreamInfo(VideoFormatType formatType, string downloadUrl, 
            bool requireDecryption, string sigTag)
        {
            FormatType = formatType;
            DownloadUrl = downloadUrl;
            RequireDecryption = requireDecryption;
            SigTag = sigTag;
        }
    }

    class EntryCreateInfo
    {
        public Uri Url { get; }
        public string Title { get; }
        public string HtmlPlayerVersion { get; }
        public StreamInfo VideoStreamInfo { get; }
        public StreamInfo AudioStreamInfo { get; }

        public EntryCreateInfo(Uri url, string title, string htmlPlayerVersion, StreamInfo videoStream, StreamInfo audioStream)
        {
            Url = url;
            Title = title;
            HtmlPlayerVersion = htmlPlayerVersion;
            VideoStreamInfo = videoStream;
            AudioStreamInfo = audioStream;
        }
    }
}
