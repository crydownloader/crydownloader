﻿// Code based on https://github.com/flagbug/YoutubeExtractor and https://github.com/cmd64/YoutubeExtractor

using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace CryDownloader.Youtube
{
    class YoutubeEntry : DownloadEntry
    {
        public const string SignatureQuery = "signature";

        public VideoFormatType VideoInfo { get; }

        public StreamInfo VideoStreamInfo { get; }
        public StreamInfo AudioStreamInfo { get; }

        public string Title { get; }

        private readonly string m_htmlPlayerVersion;

        public YoutubeEntry(Hoster hoster, string videoID, Uri url, string videoTitle, IFileState fileState)
            : base(hoster)
        {
            Url = url;
            FileState = fileState;
            Title = videoTitle;

            if (!string.IsNullOrEmpty(videoTitle))
                FileName = RemoveIllegalPathCharacters(videoTitle);
            else
                FileName = videoID;
        }

        public YoutubeEntry(Hoster hoster, string videoID, Uri url, IFileState fileState)
            : this(hoster, videoID, url, string.Empty, fileState)
        { }

        public YoutubeEntry(Hoster hoster, EntryCreateInfo entryCreateInfo)
            : base(hoster)
        {
            if (entryCreateInfo.VideoStreamInfo != null && entryCreateInfo.AudioStreamInfo != null)
            {
                Url = new Uri(entryCreateInfo.Url.ToString() + $"&vtag={entryCreateInfo.VideoStreamInfo.FormatType.FormatCode}&atag={entryCreateInfo.AudioStreamInfo.FormatType.FormatCode}");
                FileName = GenerateFilename(entryCreateInfo.VideoStreamInfo.FormatType, entryCreateInfo.AudioStreamInfo.FormatType, entryCreateInfo.Title);
                FileSizeType = FileSizeType.Relative;
            }
            else if (entryCreateInfo.VideoStreamInfo != null)
            {
                Url = new Uri(entryCreateInfo.Url.ToString() + $"&vtag={entryCreateInfo.VideoStreamInfo.FormatType.FormatCode}");
                FileName = GenerateFilename(entryCreateInfo.VideoStreamInfo.FormatType, null, entryCreateInfo.Title);
            }
            else if (entryCreateInfo.AudioStreamInfo != null)
            {
                Url = new Uri(entryCreateInfo.Url.ToString() + $"&atag={entryCreateInfo.AudioStreamInfo.FormatType.FormatCode}");
                FileName = GenerateFilename(entryCreateInfo.AudioStreamInfo.FormatType, null, entryCreateInfo.Title);
            }

            VideoStreamInfo = entryCreateInfo.VideoStreamInfo;
            AudioStreamInfo = entryCreateInfo.AudioStreamInfo;
            m_htmlPlayerVersion = entryCreateInfo.HtmlPlayerVersion;
            Title = RemoveIllegalPathCharacters(entryCreateInfo.Title);

            if (string.IsNullOrEmpty(FileName))
                FileName = Title;
        }

        protected override void OnLoad(object data)
        {
            bool created = false;

            if (data == null)
            {
                data = new WebClient();
                created = true;
            }

            FileSize = 0;

            if (VideoStreamInfo != null)
            {
                Uri videoUrl = DecryptDownloadUrl(VideoStreamInfo, m_htmlPlayerVersion, (WebClient)data);

                try
                {
                    FileSize += GetFileSizeFromUrl(videoUrl);

                    if (Url == null)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
                catch
                {
                    FileState = GetFileState(FileStateType.FileNotFound);
                }
            }

            if (AudioStreamInfo != null)
            {
                Uri audioUrl = DecryptDownloadUrl(AudioStreamInfo, m_htmlPlayerVersion, (WebClient)data);

                try
                {
                    FileSize += GetFileSizeFromUrl(audioUrl);

                    if (Url == null)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
                catch
                {
                    FileState = GetFileState(FileStateType.FileNotFound);
                }
            }

            if (created)
                ((WebClient)data).Dispose();
        }

        public override bool StartDownload(string path)
        {
            using (WebClient webClient = new WebClient())
            {
                if (VideoStreamInfo != null && AudioStreamInfo != null)
                {
                    return Muxing(DecryptDownloadUrl(VideoStreamInfo, m_htmlPlayerVersion, webClient),
                                      DecryptDownloadUrl(AudioStreamInfo, m_htmlPlayerVersion, webClient), path);
                }
                else if (VideoStreamInfo != null)
                    return DownloadFile(DecryptDownloadUrl(VideoStreamInfo, m_htmlPlayerVersion, webClient), path);
                else if (AudioStreamInfo != null)
                    return DownloadFile(DecryptDownloadUrl(AudioStreamInfo, m_htmlPlayerVersion, webClient), path);
            }

            return false;
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }

        public override string ToString()
        {
            return Title;
        }

        private string GenerateFilename(VideoFormatType videoInfo, VideoFormatType audioInfo, string videoTitle)
        {
            if (audioInfo != null && audioInfo.AudioInfo != null && videoInfo.VideoInfo != null)
                return videoTitle + "_" + videoInfo.VideoInfo.ToString(true) + "_" + audioInfo.AudioInfo.ToString(true) + videoInfo.Extension;

            if (videoInfo.VideoInfo != null && videoInfo.AudioInfo != null)
                return videoTitle + "_" + videoInfo.VideoInfo.ToString(true) + "_" + videoInfo.AudioInfo.ToString(true) + videoInfo.Extension;

            if (videoInfo.VideoInfo != null)
                return videoTitle + "_" + videoInfo.VideoInfo.ToString(true) + videoInfo.Extension;

            if (videoInfo.AudioInfo != null)
                return videoTitle + "_" + videoInfo.AudioInfo.ToString(true) + videoInfo.Extension;

            return videoTitle;
        }

        private long GetFileSizeFromUrl(Uri url)
        {
            string strSize = GetQueryStringParameter(url.ToString(), "clen");

            if (long.TryParse(strSize, out var result))
                return result;

            return 0;
        }

        private Uri DecryptDownloadUrl(StreamInfo streamInfo, string htmlPlayerVersion, WebClient client)
        {
            if (streamInfo.RequireDecryption)
            {
                IDictionary<string, string> queries = Utils.ParseQueryString(streamInfo.DownloadUrl);

                if (queries.ContainsKey(SignatureQuery))
                {
                    string encryptedSignature = queries[SignatureQuery];

                    string decrypted;

                    try
                    {
                        decrypted = DecipherWithVersion(encryptedSignature, htmlPlayerVersion, client);
                    }
                    catch (Exception ex)
                    {
                        Hoster.Application.LogManager.WriteException(LogLevel.Normal, "failed to decrypt youtube url", ex);
                        return null;
                    }

                    string str = streamInfo.DownloadUrl.Replace($"&{SignatureQuery}=", $"&{streamInfo.SigTag}=");
                    return new Uri(ReplaceQueryStringParameter(str, streamInfo.SigTag, decrypted));
                }
            }
            else
                return new Uri(streamInfo.DownloadUrl);

            return null;
        }

        private string DecipherWithVersion(string cipher, string cipherVersion, WebClient client)
        {
            string jsUrl = string.Format("http://s.ytimg.com/yts/jsbin/player{0}.js", cipherVersion);
            string baseJs = client.DownloadString(jsUrl);

            // Find the name of the function that handles deciphering
            var fnname = Regex.Match(baseJs, @"(\w+)=function\(\w+\){(\w+)=\2\.split\(\x22{2}\);.*?return\s+\2\.join\(\x22{2}\)}").Groups[1].Value;
            var _argnamefnbodyresult = Regex.Match(baseJs, @"(?!h\.)" + @fnname + @"=function\(\w+\)\{(.*?)\}").Groups[1].Value;
            var helpername = Regex.Match(_argnamefnbodyresult, @";(.+?)\..+?\(").Groups[1].Value;
            var helperresult = Regex.Match(baseJs, "var " + helpername + "={[\\s\\S]+?};");
            var result = helperresult.Groups[0].Value;

            MatchCollection matches = Regex.Matches(result, @"[A-Za-z0-9]+:function\s*([A-z0-9]+)?\s*\((?:[^)(]+|\((?:[^)(]+|\([^)(]*\))*\))*\)\s*\{(?:[^}{]+|\{(?:[^}{]+|\{[^}{]*\})*\})*\}");

            var funcs = _argnamefnbodyresult.Split(';');

            var sign = cipher.ToCharArray();

            foreach (string func in funcs)
            {
                foreach (Match group in matches)
                {
                    if (group.Value.Contains("reverse"))
                    {
                        var test = Regex.Match(group.Value, "^(.*?):").Groups[1].Value;

                        if (func.Contains(test))
                        {
                            sign = ReverseFunction(sign);
                        }
                    }
                    else if (group.Value.Contains("splice"))
                    {
                        var test = Regex.Match(group.Value, "^(.*?):").Groups[1].Value;

                        if (func.Contains(test))
                        {
                            sign = SpliceFunction(sign, GetOpIndex(func));
                        }
                    }
                    else
                    {
                        var test = Regex.Match(group.Value, "^(.*?):").Groups[1].Value;

                        if (func.Contains(test))
                        {
                            sign = SwapFunction(sign, GetOpIndex(func));
                        }
                    }
                }
            }

            return new string(sign);
        }

        private static char[] SpliceFunction(char[] a, int b)
        {
            return Splice(a, b);
        }

        public static T[] Splice<T>(T[] source, int start)
        {
            var listItems = source.ToList<T>();
            var items = listItems.Skip(start).ToList<T>();
            return items.ToArray<T>();
        }

        private static char[] SwapFunction(char[] a, int b)
        {
            var c = a[0];
            a[0] = a[b % a.Length];
            a[b % a.Length] = c;
            return a;
        }

        private static char[] ReverseFunction(char[] a)
        {
            Array.Reverse(a);
            return a;
        }

        private static int GetOpIndex(string op)
        {
            string parsed = new Regex(@".(\d+)").Match(op).Result("$1");
            int index = int.Parse(parsed);

            return index;
        }

        private static string RemoveIllegalPathCharacters(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(path, "");
        }

        private static string ReplaceQueryStringParameter(string currentPageUrl, string paramToReplace, string newValue)
        {
            var query = Utils.ParseQueryString(currentPageUrl);

            query[paramToReplace] = newValue;

            var resultQuery = new StringBuilder();
            bool isFirst = true;

            foreach (KeyValuePair<string, string> pair in query)
            {
                if (!isFirst)
                {
                    resultQuery.Append("&");
                }

                if (!string.IsNullOrEmpty(pair.Value))
                {
                    resultQuery.Append(pair.Key);
                    resultQuery.Append("=");
                    resultQuery.Append(pair.Value);
                }

                isFirst = false;
            }

            var uriBuilder = new UriBuilder(currentPageUrl)
            {
                Query = resultQuery.ToString()
            };

            return uriBuilder.ToString();
        }

        private static string GetQueryStringParameter(string currentPageUrl, string param)
        {
            var query = Utils.ParseQueryString(currentPageUrl);

            return query[param];
        }
    }
}
