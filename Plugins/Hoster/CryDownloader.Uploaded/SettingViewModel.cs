﻿using CryDownloader.Plugin;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CryDownloader.Uploaded
{
    class SettingViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Uploaded Hoster { get; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string AccountStatus
        {
            get => m_accountStatus;
            set
            {
                m_accountStatus = value;
                OnPropertyChanged();
            }
        }

        public Brush FontBrush
        {
            get => m_fontBrush;
            set
            {
                m_fontBrush = value;
                OnPropertyChanged();
            }
        }

        public bool ApplyButtonEnabled
        {
            get => m_applyButtonEnabled;
            set
            {
                m_applyButtonEnabled = value;
                OnPropertyChanged();
            }
        }

        private string m_accountStatus;
        private Brush m_fontBrush;
        private Task m_checkTask;
        private bool m_applyButtonEnabled;

        public SettingViewModel(Uploaded hoster)
        {
            Hoster = hoster;
            Username = hoster.Username;
            Password = hoster.Password;
            ApplyButtonEnabled = true;

            CheckAccount();
        }

        public void Wait()
        {
            if (m_checkTask == null)
                return;

            if (m_checkTask.Status == TaskStatus.Running)
                m_checkTask.Wait();
        }

        public void Apply()
        {
            ApplyButtonEnabled = false;

            Hoster.Username = Username;
            Hoster.Password = Password;
            Hoster.Application.SaveSettings();

            CheckAccount();

            ApplyButtonEnabled = true;
        }

        private void CheckAccount()
        {
            if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
            {
                AccountStatus = "Prüfe Account...";
                FontBrush = Brushes.Yellow;

                IWebBrowser browser = Hoster.Application.CreateBrowser();

                m_checkTask = new Task(() =>
                {
                    try
                    {
                        if (Hoster.CheckAccount(browser, out var state))
                            FontBrush = Brushes.Green;
                        else
                            FontBrush = Brushes.Red;

                        AccountStatus = state;
                    }
                    catch (Exception ex)
                    {
                        Hoster.Application.LogManager.WriteException(LogLevel.Low, $"failed to check account for {Hoster.Name}", ex);
                    }
                });

                m_checkTask.Start();
            }
            else
            {
                AccountStatus = "Freemode";
                FontBrush = Brushes.Red;
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
