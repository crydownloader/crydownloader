﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;

namespace CryDownloader.Uploaded
{
    class UploadedEntry : DownloadEntry
    {
        private Uploaded m_myHoster;

        public UploadedEntry(Uploaded hoster, Uri url)
            : base(hoster)
        {
            m_myHoster = hoster;
            Url = url;
            FileName = url.Segments[1];
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }

        protected override void OnLoad(object data)
        {
            var webClient = (WebClient)data;

            bool created = false;

            if (data == null)
            {
                webClient = new WebClient();
                created = true;
            }

            var strPage = webClient.DownloadString(Url);

            if (created)
                webClient.Dispose();

            IHtmlDocument doc = LoadHtmlDocument(strPage);

            var nodeElement = doc.GetElementbyId("filename");

            if (nodeElement == null)
                FileState = GetFileState(FileStateType.FileNotFound);
            else
            {
                var sizeNode = nodeElement.ParentNode.SelectSingleNode("./small");

                if (sizeNode == null)
                    FileState = GetFileState(FileStateType.FileExists);
                else
                {
                    FileState = GetFileState(FileStateType.FileInfoAvail);

                    string strSize = sizeNode.InnerText;

                    double size = double.Parse(strSize);

                    size *= (1000 * 1000);
                    FileSize = (long)size;
                }
            }
        }

        public override bool StartDownload(string path)
        {
            IWebBrowser browser = m_myHoster.Application.CreateBrowser();
            bool? bLoginResult = null;

            browser.Navigate(Url);

            if (!string.IsNullOrEmpty(m_myHoster.Username) && !string.IsNullOrEmpty(m_myHoster.Username))
                bLoginResult = m_myHoster.Login(browser);

            bool bResult;

            if (m_myHoster.HasPremium)
                bResult = DownloadPremium(browser, path);
            else
                bResult = DownloadFree(browser, path);

            if (bLoginResult.HasValue && bLoginResult.Value)
                m_myHoster.Logout(browser);

            return bResult;
        }

        private bool DownloadPremium(IWebBrowser browser, string path)
        {
            return false;
        }

        private bool DownloadFree(IWebBrowser browser, string path)
        {
            var captchaElement = browser.GetElementById("captcha");

            if (captchaElement == null)
            {
                Hoster.Application.LogManager.WriteLog(LogLevel.Normal, "captcha element not found");
                return false;
            }

            var btn = captchaElement.SelectSingleNode(".//button");

            if (btn == null)
            {
                Hoster.Application.LogManager.WriteLog(LogLevel.Normal, "Button inside the captcha element not found");
                return false;
            }

            var waitTime = btn.SelectSingleNode(".//span/span");

            if (waitTime == null)
            {
                Hoster.Application.LogManager.WriteLog(LogLevel.Normal, "Wait time inside the captcha element not found");
                return false;
            }

            browser.SendClickEvent(btn);

            IHtmlNode captchaResponseNode = null;

            while (captchaResponseNode == null)
            {
                captchaResponseNode = browser.GetElementById("g-recaptcha-response");
                captchaElement = browser.GetElementById("captcha");

                if (captchaElement == null)
                    break;

                waitTime = btn.SelectSingleNode(".//span/span");

                if (waitTime == null)
                    break;

                FileState = Hoster.Application.CreateFileState(FileStateIcon.None, $"{waitTime.InnerText}");
                Debug.WriteLine(FileState.Description);

                Thread.Sleep(1);
            }

            if (captchaElement == null)
            {
                Hoster.Application.LogManager.WriteLog(LogLevel.Normal, "captcha element not found");
                return false;
            }

            var captchaElementForm = captchaElement.SelectSingleNode(".//form");

            List<IHtmlNode> hiddenElements = new List<IHtmlNode>();

            Hoster.Application.ResolveCaptcha(browser, captchaElementForm, hiddenElements, null, () =>
            {
                Uri url = browser.GetDownloadUrl();
                DownloadFile(url, path);
            });

            return false;
        }
    }
}
