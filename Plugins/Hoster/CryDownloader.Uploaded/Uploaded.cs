﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace CryDownloader.Uploaded
{
    public class Uploaded : Hoster
    {
        public override string Name => "Uploaded";
        public override string Host => "uploaded.to";
        public bool HasPremium { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public override System.Windows.FrameworkElement SettingControl => m_settingControl;

        private System.Windows.FrameworkElement m_settingControl;

        public Uploaded(IApplication app)
            : base(app)
        { }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>()
            {
                { "Username", Username },
                { "Password", Password }
            };
        }

        public override void Initialize()
        {
            SettingViewModel viewModel = new SettingViewModel(this);

            Application.Dispatcher.Invoke(() => m_settingControl = new Settings(viewModel));

            viewModel.Wait();
        }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            if (properties.TryGetValue("Username", out var value))
                Username = value;

            if (properties.TryGetValue("Password", out value))
                Password = value;
        }

        public bool CheckAccount(IWebBrowser browser, out string status)
        {
            status = "Freemode";

            var loginResult = Login(browser);

            if (loginResult == null)
            {
                status = "Failed to parse data";
                return false;
            }

            if (loginResult.Value)
            {
                var loginResponseNode = browser.GetElementById("account");

                if (loginResponseNode == null)
                {
                    status = "Account Status: Failed to parse data";
                    Application.LogManager.WriteLog(LogLevel.Normal, "Element with 'account' not found");
                }
                else
                {
                    var nodes = loginResponseNode.SelectNodes(".//tr");
                    bool hasStatus = false;

                    foreach (var node in nodes)
                    {
                        bool isStatus = false;
                        int index = 0;

                        foreach (var n in node.ChildNodes)
                        {
                            if (n.Name == "td")
                            {
                                if (n.InnerText.Contains("Status"))
                                {
                                    isStatus = true;
                                    break;
                                }
                            }

                            ++index;
                        }

                        if (isStatus)
                        {
                            var statusNode = node.ChildNodes[index + 1];
                            status = "Account Status: " + statusNode.InnerText;
                            hasStatus = true;

                            HasPremium = !statusNode.InnerText.ToLower().Contains("free");
                        }
                    }

                    if (!hasStatus)
                        status = "Account Status: Unknown";
                }

                Logout(browser);
                return true;
            }
            else
                status = "Invalid username or password - using freemode";

            return false;
        }

        internal bool Logout(IWebBrowser browser)
        {
            browser.Navigate(new Uri("http://uploaded.net/logout"));
            return true;
        }

        internal bool? Login(IWebBrowser browser)
        {
            IHtmlNode node = browser.GetElementById("nlogin");

            if (node == null)
            {
                Application.LogManager.WriteLog(LogLevel.Normal, "element with 'nlogin' not found");
                return null;
            }

            browser.SendClickEvent(node);

            node = null;

            while (node == null)
                node = browser.GetElementById("login");

            var idNode = node.SelectSingleNode(".//input[@name='id']");
            var pwNode = node.SelectSingleNode(".//input[@name='pw']");
            var loginBtn = node.SelectSingleNode(".//button[@type='submit']");

            if (idNode == null)
            {
                Application.LogManager.WriteLog(LogLevel.Normal, "element with './/input[@name='id']' not found");
                return null;
            }

            if (pwNode == null)
            {
                Application.LogManager.WriteLog(LogLevel.Normal, "element with './/input[@name='pw']' not found");
                return null;
            }

            if (loginBtn == null)
            {
                Application.LogManager.WriteLog(LogLevel.Normal, "element with './/button[@type='submit']' not found");
                return null;
            }

            browser.SetValue(idNode, Username);
            browser.SetValue(pwNode, Password);

            browser.SendClickEvent(loginBtn);

            IHtmlNode loginResponseNode = null;

            DateTime startTime = DateTime.Now;

            while (loginResponseNode == null)
            {
                if ((DateTime.Now - startTime).TotalSeconds >= 10)
                    break;

                loginResponseNode = browser.GetElementById("login");
                loginResponseNode = loginResponseNode.SelectSingleNode(".//div[@class='error']");

                if (loginResponseNode != null)
                    return false;

                loginResponseNode = browser.GetElementById("account");
            }

            if (loginResponseNode == null)
            {
                Application.LogManager.WriteLog(LogLevel.Normal, "element with './/div[@class='error']' or element with 'account' not found");
                return null;
            }

            var tableNode = loginResponseNode.SelectSingleNode(".//table[@class='data']");

            if (tableNode == null)
            {
                Application.LogManager.WriteLog(LogLevel.Normal, "element with './/table[@class='data']' not found");
                return null;
            }

            return true;
        }

        protected override void OnCreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            Queue<UploadedEntry> queue = new Queue<UploadedEntry>();
            Dictionary<Uri, bool> dict = new Dictionary<Uri, bool>();

            foreach (var url in urls)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        break;
                }

                if (dict.ContainsKey(url))
                    continue;

                scanUrlChangedCallback?.Invoke(url);
                queue.Enqueue(new UploadedEntry(this, url));
                dict.Add(url, true);
            }

            using (var client = new WebClient())
            {
                ProcessGetCheckLinkStates(queue, client, creator);
            }
        }

        private void ProcessGetCheckLinkStates(Queue<UploadedEntry> requests, WebClient client, IContainerCreator creator)
        {
            while (requests.Count > 0)
            {
                var entry = requests.Dequeue();

                var strPage = client.DownloadString(entry.Url);

                IHtmlDocument doc = Application.LoadHtmlDocument(strPage);

                var nodeElement = doc.GetElementbyId("filename");

                if (nodeElement == null)
                    entry.FileState = Application.GetFileState(FileStateType.FileNotFound);
                else
                    entry.FileName = nodeElement.InnerText;

                string name = CreateName(entry.FileName);
                var container = creator.CreateContainer(name);

                entry.Load(client);
                container.AddEntry(entry.FileName, entry);
            }
        }

        private string CreateName(string fileName)
        {
            string[] fileNameParts = fileName.Split('.');

            if (fileNameParts.Length < 3)
                return fileName;

            string result = string.Empty;

            for (int i = 0; i < fileNameParts.Length - 2; ++i)
                result += fileNameParts[i];

            return result;
        }
    }
}
