﻿namespace CryDownloader.Twitch
{
    enum ClipRange
    {
        LastDay,
        LastWeek,
        LastMonth,
        AllTime
    }
}
