﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CryDownloader.Twitch
{
    class SettingViewModel : INotifyPropertyChanged
    {
        private EnumViewModel<VideoQuality> m_preferredType;
        private EnumViewModel<ClipRange> m_clipRange;
        private bool m_autoChoose;

        public EnumViewModel<VideoQuality> PreferredType
        {
            get => m_preferredType;
            set
            {
                m_preferredType = value;
                OnPropertyChanged();
            }
        }

        public EnumViewModel<ClipRange> ClipRange
        {
            get => m_clipRange;
            set
            {
                m_clipRange = value;
                OnPropertyChanged();
            }
        }

        public bool AutoChoose
        {
            get => m_autoChoose;
            set
            {
                m_autoChoose = value;
                OnPropertyChanged();
                OnPropertyChanged("ComboBoxEnabled");
            }
        }

        public bool CalculateFileSize { get; set; }

        public bool ComboBoxEnabled => !AutoChoose;

        public ObservableCollection<EnumViewModel<VideoQuality>> VideoTypes { get; }
        public ObservableCollection<EnumViewModel<ClipRange>> ClipRanges { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly Twitch m_hoster;

        public SettingViewModel(Twitch hoster)
        {
            VideoTypes = new ObservableCollection<EnumViewModel<VideoQuality>>();
            ClipRanges = new ObservableCollection<EnumViewModel<ClipRange>>();

            Array values = Enum.GetValues(typeof(VideoQuality));

            foreach (VideoQuality quality in values)
            {
                var viewModel = new EnumViewModel<VideoQuality>(quality);

                if (quality == hoster.ChoosenQuality)
                    PreferredType = viewModel;

                VideoTypes.Add(viewModel);
            }

            values = Enum.GetValues(typeof(ClipRange));

            foreach (ClipRange clipRange in values)
            {
                var viewModel = new EnumViewModel<ClipRange>(clipRange);

                if (clipRange == hoster.ClipRange)
                    ClipRange = viewModel;

                ClipRanges.Add(viewModel);
            }

            AutoChoose = hoster.ChoosenQuality == VideoQuality.None;

            m_hoster = hoster;
        }

        public void Apply()
        {
            m_hoster.ClipRange = ClipRange.Data;

            if (!AutoChoose)
                m_hoster.ChoosenQuality = PreferredType.Data;
            else
                m_hoster.ChoosenQuality = VideoQuality.None;

        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
