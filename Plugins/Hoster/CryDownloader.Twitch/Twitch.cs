﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace CryDownloader.Twitch
{
    class Twitch : Hoster
    {
        #region Json Objects

        class TwitchPageInfoResult
        {
            public bool hasNextPage { get; set; }
        }

        class TwitchEdgeNode
        {
            public string id { get; set; }
            public string title { get; set; }
        }

        class TwitchClipEdgeNode : TwitchEdgeNode
        {
            public string slug { get; set; }
            public TwitchBroadcaster broadcaster { get; set; }
        }

        class TwitchEdgeResult<T> where T : TwitchEdgeNode
        {
            public T node { get; set; }
        }

        class TwitchVideoResult<T> where T : TwitchEdgeNode
        {
            public List<TwitchEdgeResult<T>> edges { get; set; }
            public TwitchPageInfoResult pageInfo { get; set; }
        }

        class TwitchUserResult
        { }

        class TwitchVideoUserResult : TwitchUserResult
        {
            public TwitchVideoResult<TwitchEdgeNode> videos { get; set; }
        }

        class TwitchClipUserResult : TwitchUserResult
        {
            public TwitchVideoResult<TwitchClipEdgeNode> clips { get; set; }
        }

        class TwitchDataResult<T> where T : TwitchUserResult
        {
            public T user { get; set; }
        }

        class TwitchClipResult<T> where T : TwitchUserResult
        {
            public TwitchDataResult<T> data { get; set; }
        }

        class TwitchBroadcasterSettings
        {

        }

        class TwitchBroadcaster
        {
            public string displayName { get; set; }
        }

        class TwitchClipNode
        {
            public TwitchClipEdgeNode clip { get; set; }
        }

        class TwitchClipResult2
        {
            public TwitchClipNode data { get; set; }
        }

        class TwitchVideoQuality
        {
            public int frameRate { get; set; }
            public string quality { get; set; }
            public string sourceURL { get; set; }
        }

        class TwitchClipInfo
        {
            public List<TwitchVideoQuality> videoQualities { get; set; }
        }

        class TwitchClipInfoNode
        {
            public TwitchClipInfo clip { get; set; }
        }

        class TwitchClipDataInfo
        {
            public TwitchClipInfoNode data { get; set; }
        }

        class TwitchAccessToken
        {
            public string token { get; set; }
            public string sig { get; set; }
        }

        class TwitchError
        {
            public string error_code { get; set; }
        }

        class TwitchChannelInfo
        {
            public string name { get; set; }
        }

        class TwitchVideoInfo
        {
            public string title { get; set; }

            public TwitchChannelInfo channel { get; set; }
        }

        #endregion

        public override string Name => "Twitch";

        public override string Host => "twitch.tv";

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public override System.Windows.FrameworkElement SettingControl => m_settingView;

        public ClipRange ClipRange { get; set; }

        public VideoQuality ChoosenQuality { get; set; }

        public bool CalculateFileSize { get; set; }

        private Settings m_settingView;

        public Twitch(IApplication app)
            : base(app)
        {
            CalculateFileSize = true;
        }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string streamerName = args.Url.Segments[1].Replace("/", "");
            bool directVideo = args.Url.Segments[1].Contains("videos");
            bool directClip = false;
            bool onlyClip = false;
            bool onlyVideos = false;

            if (args.Url.Segments.Length > 2)
            {
                directClip = args.Url.Segments[2].Contains("clip");
                onlyClip = args.Url.Segments[2] == "clips";
                onlyVideos = args.Url.Segments[2] == "videos";
            }

            if (onlyClip)
            {
                Dictionary<string, TwitchClipEdgeNode> allClips = new Dictionary<string, TwitchClipEdgeNode>();

                GetAllClips(streamerName, allClips, args.CancellationToken);

                IContainer clipContainer = args.ContainerCreator.CreateContainer(streamerName);

                foreach (var entry in allClips)
                    AddClipUrlsToContainer(entry.Value.slug, false, clipContainer, null, entry.Value.title);
            }
            else if (onlyVideos)
            {
                Dictionary<string, TwitchEdgeNode> allVideos = new Dictionary<string, TwitchEdgeNode>();

                GetAllVideos(streamerName, allVideos, args.CancellationToken);

                IContainer vidContainer = args.ContainerCreator.CreateContainer(streamerName);

                foreach (var entry in allVideos)
                    AddVideoPlaylistsToContainer(entry.Value.id, false, vidContainer, null, entry.Value.title);
            }
            else if (directVideo)
            {
                if (args.Url.Segments.Length < 3)
                    return;

                AddVideoPlaylistsToContainer(args.Url.Segments[2].Replace("/", ""), true, null, args.ContainerCreator);
            }
            else if (directClip)
            {
                if (args.Url.Segments.Length < 4)
                    return;

                AddClipUrlsToContainer(args.Url.Segments[3].Replace("/", ""), true, null, args.ContainerCreator);
            }
            else
            {
                Dictionary<string, TwitchClipEdgeNode> allClips = new Dictionary<string, TwitchClipEdgeNode>();
                Dictionary<string, TwitchEdgeNode> allVideos = new Dictionary<string, TwitchEdgeNode>();

                GetAllClipsAndVideos(streamerName, allClips, allVideos, args.CancellationToken);

                IContainer clipContainer = args.ContainerCreator.CreateContainer($"{streamerName} Clips");

                foreach (var entry in allClips)
                    AddClipUrlsToContainer(entry.Value.slug, false, clipContainer, null, entry.Value.title);

                IContainer vidContainer = args.ContainerCreator.CreateContainer($"{streamerName} Videos");

                foreach (var entry in allVideos)
                    AddVideoPlaylistsToContainer(entry.Value.id, false, vidContainer, null, entry.Value.title);
            }
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>()
            {
                { "ClipRange", ((int)ClipRange).ToString() },
                { "ChoosenQuality", ((int)ChoosenQuality).ToString() },
                { "CalculateFileSize", CalculateFileSize ? "1" : "0" },
            };
        }

        public override void Initialize()
        {
            SettingViewModel viewModel = new SettingViewModel(this);

            Application.Dispatcher.Invoke(() => m_settingView = new Settings(viewModel));
        }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            if (properties.TryGetValue("ClipRange", out var val))
            {
                if (int.TryParse(val, out var value))
                    ClipRange = (ClipRange)value;
            }

            if (properties.TryGetValue("ChoosenQuality", out val))
            {
                if (int.TryParse(val, out var value))
                    ChoosenQuality = (VideoQuality)value;
            }

            if (properties.TryGetValue("CalculateFileSize", out val))
                CalculateFileSize = val == "1";
        }

        private bool TryToAddEntry(IContainer container, TwitchEntry entry)
        {
            if (ChoosenQuality == VideoQuality.None)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            if ((ChoosenQuality & VideoQuality.Pick160p) != 0)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            if ((ChoosenQuality & VideoQuality.Pick240p) != 0)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            if ((ChoosenQuality & VideoQuality.Pick360p) != 0)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            if ((ChoosenQuality & VideoQuality.Pick480p) != 0)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            if ((ChoosenQuality & VideoQuality.Pick720p) != 0)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            if ((ChoosenQuality & VideoQuality.Pick1080p) != 0)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            if ((ChoosenQuality & VideoQuality.PickSource) != 0)
            {
                container.AddEntry(entry.FileName, entry);
                return true;
            }

            return false;
        }

        private void AddClipUrlsToContainer(string videoSlug, bool needStreamerName, IContainer container, IContainerCreator creator, string title = null)
        {
            string streamerName = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://gql.twitch.tv/gql");
            request.Headers.Add("Client-Id", "jzkbprff40iqj646a697cyrvl0zt2m6");
            request.ContentType = "text/plain;charset=UTF-8";
            request.Method = "POST";

            if (needStreamerName)
            {
                using (var stream = request.GetRequestStream())
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write($"{{\"operationName\": \"ChannelRoot_Clip\",\"variables\":{{\"currentChannelLogin\": \"\",\"includeChannel\": false,\"videoID\": \"\",\"includeVideo\": false,\"collectionID\": \"\",\"includeCollection\": false,\"slugID\": \"{videoSlug}\",\"includeClip\": true,\"includeChanlets\": false}},\"extensions\": {{\"persistedQuery\": {{\"version\": 1,\"sha256Hash\": \"64c54d0909122ca1016771076066309afa8e747f40fb70ca45537689e9ede78a\"}}}}}}");
                    }

                    stream.Flush();

                    using (var response = request.GetResponse())
                    {
                        using (var responseStream = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(responseStream))
                            {
                                TwitchClipResult2 clipInfo = Application.LoadJsonObject<TwitchClipResult2>(reader);

                                if (title == null)
                                    title = clipInfo.data.clip.title;

                                streamerName = clipInfo.data.clip.broadcaster.displayName;

                                if (container == null)
                                    container = creator.CreateContainer(streamerName);
                            }
                        }
                    }
                }

                request = (HttpWebRequest)WebRequest.Create("https://gql.twitch.tv/gql");
                request.Headers.Add("Client-Id", "jzkbprff40iqj646a697cyrvl0zt2m6");
                request.ContentType = "text/plain;charset=UTF-8";
                request.Method = "POST";
            }

            using (var stream = request.GetRequestStream())
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write($"{{\"query\":\"\\n query getClipStatus($slug: ID!) {{\\n clip(slug: $slug) {{\\n creationState\\n videoQualities {{\\n frameRate\\n quality\\n sourceURL\\n            }}\\n          }}\\n    }}\\n\",\"variables\":{{ \"slug\":\"{videoSlug}\"}}}}");
                }

                stream.Flush();

                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            TwitchClipDataInfo clipInfo = Application.LoadJsonObject<TwitchClipDataInfo>(reader);

                            foreach (var quali in clipInfo.data.clip.videoQualities)
                            {
                                var entry = new TwitchClip(this, title, quali.frameRate, quali.quality, quali.sourceURL);

                                entry.Load(container);
                                TryToAddEntry(container, entry);
                            }
                        }
                    }
                }
            }
        }

        private void AddVideoPlaylistsToContainer(string videoID, bool needStreamerName, IContainer container, IContainerCreator creator, string title = null)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Client-ID", "jzkbprff40iqj646a697cyrvl0zt2m6");
                client.Encoding = Encoding.UTF8;
                client.Headers.Add("Accept", "application/x-mpegURL, application/vnd.apple.mpegurl, application/json, text/plain");
                string str;

                if (needStreamerName)
                {
                    str = client.DownloadString($"https://api.twitch.tv/kraken/videos/v{videoID}");

                    TwitchVideoInfo videoInfo = Application.LoadJsonObject<TwitchVideoInfo>(str);

                    if (title == null)
                        title = videoInfo.title;

                    string streamerName = videoInfo.channel.name;

                    if (container == null)
                        container = creator.CreateContainer(streamerName);
                }

                client.Encoding = Encoding.UTF8;
                str = client.DownloadString($"https://api.twitch.tv/api/vods/{videoID}/access_token?need_https=true&oauth_token=&platform=_&player_backend=mediaplayer&player_type=site");

                string playlistLink = string.Empty;

                TwitchAccessToken token = Application.LoadJsonObject<TwitchAccessToken>(str);

                playlistLink = $"https://usher.ttvnw.net/vod/{videoID}.m3u8?allow_source=true&nauth={token.token}&nauthsig={token.sig}&playlist_include_framerate=true";

                List<TwitchVodPlaylist> result = new List<TwitchVodPlaylist>();

                try
                {
                    client.Encoding = Encoding.UTF8;
                    client.Headers.Add("Accept", "application/x-mpegURL, application/vnd.apple.mpegurl, application/json, text/plain");
                    str = client.DownloadString(playlistLink);
                }
                catch (WebException ex)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            List<TwitchError> err = Application.LoadJsonObject<List<TwitchError>>(reader);

                            foreach (var e in err)
                            {
                                if (e.error_code == "vod_manifest_restricted")
                                {
                                    container.AddEntry(videoID, new TwitchVodPlaylist(this, videoID, "Login and/or subscription required"));
                                    return;
                                }
                            }
                        }
                    }
                }

                M3UPlaylist playlist = LoadPlaylist(str);

                foreach (var file in playlist.Files)
                {
                    var metaData = file.MetaData["#EXT-X-STREAM-INF"];
                    string codecs = metaData["CODECS"].Value;
                    string resolution = metaData["RESOLUTION"].Value;
                    double framerate = double.Parse(metaData["FRAME-RATE"].Value);
                    bool isSource = metaData["VIDEO"].Value == "chunked";

                    var entry = new TwitchVodPlaylist(this, title, framerate, resolution, file.Path, isSource);
                    entry.Load(client);
                    TryToAddEntry(container, entry);
                }
            }
        }

        private void GetAllClips(string streamerName, Dictionary<string, TwitchClipEdgeNode> allClips, CancellationToken? cancellationToken)
        {
            int currClipIndex = 0;
            bool hasMoreClips = true;
            bool switchClipRange = true;
            ClipRange range = ClipRange;

            while (hasMoreClips)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                var lst = GetClips(streamerName, currClipIndex, range);

                foreach (var l in lst)
                {
                    if (l.data.user.clips.edges.Count == 0 && switchClipRange)
                    {
                        switch (range)
                        {
                            case ClipRange.LastDay:
                                range = ClipRange.LastWeek;
                                break;
                            case ClipRange.LastWeek:
                                range = ClipRange.LastMonth;
                                break;
                            case ClipRange.LastMonth:
                                range = ClipRange.AllTime;
                                break;
                            default:
                                break;
                        }
                    }
                    else if (switchClipRange && l.data.user.clips.edges.Count > 0)
                        switchClipRange = false;

                    foreach (var clip in l.data.user.clips.edges)
                    {
                        if (!allClips.ContainsKey(clip.node.id))
                            allClips.Add(clip.node.id, clip.node);
                    }

                    if (!switchClipRange)
                    {
                        hasMoreClips = l.data.user.clips.pageInfo.hasNextPage;
                        currClipIndex += l.data.user.clips.edges.Count;
                    }
                }
            }
        }

        private void GetAllVideos(string streamername, Dictionary<string, TwitchEdgeNode> allVideos, CancellationToken? cancellationToken)
        {
            int currVideoIndex = 0;
            bool hasMoreVideos = true;

            while (hasMoreVideos)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                var lst = GetVideos(streamername, currVideoIndex);

                foreach (var l in lst)
                {
                    foreach (var vid in l.data.user.videos.edges)
                    {
                        if (!allVideos.ContainsKey(vid.node.id))
                            allVideos.Add(vid.node.id, vid.node);
                    }

                    hasMoreVideos = l.data.user.videos.pageInfo.hasNextPage;
                    currVideoIndex += l.data.user.videos.edges.Count;
                }
            }
        }

        private void GetAllClipsAndVideos(string streamerName, Dictionary<string, TwitchClipEdgeNode> allClips, Dictionary<string, TwitchEdgeNode> allVideos, CancellationToken? cancellationToken)
        {
            int currClipIndex = 0;
            int currVideoIndex = 0;
            bool hasMoreClips = true;
            bool hasMoreVideos = true;
            bool switchClipRange = true;
            ClipRange range = ClipRange;

            while (hasMoreClips || hasMoreVideos)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                if (hasMoreClips)
                {
                    var lst = GetClips(streamerName, currClipIndex, range);

                    foreach (var l in lst)
                    {
                        if (l.data.user.clips.edges.Count == 0 && switchClipRange)
                        {
                            switch (range)
                            {
                                case ClipRange.LastDay:
                                    range = ClipRange.LastWeek;
                                    break;
                                case ClipRange.LastWeek:
                                    range = ClipRange.LastMonth;
                                    break;
                                case ClipRange.LastMonth:
                                    range = ClipRange.AllTime;
                                    break;
                                default:
                                    break;
                            }
                        }
                        else if (switchClipRange && l.data.user.clips.edges.Count > 0)
                            switchClipRange = false;

                        foreach (var clip in l.data.user.clips.edges)
                        {
                            if (!allClips.ContainsKey(clip.node.id))
                                allClips.Add(clip.node.id, clip.node);
                        }

                        if (!switchClipRange)
                        {
                            hasMoreClips = l.data.user.clips.pageInfo.hasNextPage;
                            currClipIndex += l.data.user.clips.edges.Count;
                        }
                    }
                }

                if (hasMoreVideos)
                {
                    var lst = GetVideos(streamerName, currVideoIndex);

                    foreach (var l in lst)
                    {
                        foreach (var vid in l.data.user.videos.edges)
                        {
                            if (!allVideos.ContainsKey(vid.node.id))
                                allVideos.Add(vid.node.id, vid.node);
                        }

                        hasMoreVideos = l.data.user.videos.pageInfo.hasNextPage;
                        currVideoIndex += l.data.user.videos.edges.Count;
                    }
                }
            }
        }

        private List<TwitchClipResult<TwitchClipUserResult>> GetClips(string streamerName, int startPosition, ClipRange range)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(startPosition.ToString());
            string lastItem = Convert.ToBase64String(bytes);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://gql.twitch.tv/gql");
            request.Headers.Add("Client-Id", "kimne78kx3ncx6brgo4mv6wki5h1ko");
            request.ContentType = "text/plain;charset=UTF-8";
            request.Method = "POST";

            using (var stream = request.GetRequestStream())
            {
                using (var writer = new StreamWriter(stream))
                {
                    switch (range)
                    {
                        case ClipRange.LastDay:
                        default:
                            writer.Write($"[{{\"operationName\":\"ClipsCards__User\", \"variables\":{{\"login\":\"{streamerName}\", \"limit\":20,\"criteria\":{{\"filter\":\"LAST_DAY\"}},\"cursor\":\"{lastItem}\"}},\"extensions\":{{\"persistedQuery\":{{\"version\":1,\"sha256Hash\":\"1d7fff46b86c89226480be341b7d23c82934bf803cf054653256f539e9f9e90b\"}}}}}}]");
                            break;
                        case ClipRange.LastWeek:
                            writer.Write($"[{{\"operationName\":\"ClipsCards__User\", \"variables\":{{\"login\":\"{streamerName}\", \"limit\":20,\"criteria\":{{\"filter\":\"LAST_WEEK\"}},\"cursor\":\"{lastItem}\"}},\"extensions\":{{\"persistedQuery\":{{\"version\":1,\"sha256Hash\":\"1d7fff46b86c89226480be341b7d23c82934bf803cf054653256f539e9f9e90b\"}}}}}}]");
                            break;
                        case ClipRange.LastMonth:
                            writer.Write($"[{{\"operationName\":\"ClipsCards__User\", \"variables\":{{\"login\":\"{streamerName}\", \"limit\":20,\"criteria\":{{\"filter\":\"LAST_MONTH\"}},\"cursor\":\"{lastItem}\"}},\"extensions\":{{\"persistedQuery\":{{\"version\":1,\"sha256Hash\":\"1d7fff46b86c89226480be341b7d23c82934bf803cf054653256f539e9f9e90b\"}}}}}}]");
                            break;
                        case ClipRange.AllTime:
                            writer.Write($"[{{\"operationName\":\"ClipsCards__User\", \"variables\":{{\"login\":\"{streamerName}\", \"limit\":20,\"criteria\":{{\"filter\":\"ALL_TIME\"}},\"cursor\":\"{lastItem}\"}},\"extensions\":{{\"persistedQuery\":{{\"version\":1,\"sha256Hash\":\"1d7fff46b86c89226480be341b7d23c82934bf803cf054653256f539e9f9e90b\"}}}}}}]");
                            break;
                    }
                }

                stream.Flush();

                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            return Application.LoadJsonObject<List<TwitchClipResult<TwitchClipUserResult>>>(reader);
                        }
                    }
                }
            }
        }

        private List<TwitchClipResult<TwitchVideoUserResult>> GetVideos(string streamerName, int startPosition)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(startPosition.ToString());
            string lastItem = Convert.ToBase64String(bytes);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://gql.twitch.tv/gql");
            request.Headers.Add("Client-Id", "kimne78kx3ncx6brgo4mv6wki5h1ko");
            request.ContentType = "text/plain;charset=UTF-8";
            request.Method = "POST";

            using (var stream = request.GetRequestStream())
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write($"[{{\"operationName\":\"FilterableVideoTower_Videos\",\"variables\":{{\"limit\":30,\"channelOwnerLogin\":\"{streamerName}\",\"broadcastType\":\"ARCHIVE\",\"videoSort\":\"TIME\",\"cursor\":\"{lastItem}\"}},\"extensions\":{{\"persistedQuery\":{{\"version\":1,\"sha256Hash\":\"3cba0497ef8f5c5953787e5a8284b7277d089f3f00e4bbe89368fd41e2e035ad\"}}}}}}]");
                }


                stream.Flush();

                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            return Application.LoadJsonObject<List<TwitchClipResult<TwitchVideoUserResult>>>(reader);
                        }
                    }
                }
            }
        }
    }
}
