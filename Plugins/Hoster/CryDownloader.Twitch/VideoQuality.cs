﻿using System;

namespace CryDownloader.Twitch
{
    [Flags]
    enum VideoQuality
    {
        None,
        Pick160p = 1 << 0,
        Pick240p = 1 << 1,
        Pick360p = 1 << 2,
        Pick480p = 1 << 3,
        Pick720p = 1 << 4,
        Pick1080p = 1 << 5,
        PickSource = 1 << 6,
    }
}
