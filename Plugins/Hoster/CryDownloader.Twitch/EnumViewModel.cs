﻿namespace CryDownloader.Twitch
{
    class EnumViewModel<T>
    {
        public T Data { get; }

        public EnumViewModel(T quality)
        {
            Data = quality;
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
