﻿using CryDownloader.Plugin;
using System;
using System.IO;

namespace CryDownloader.Twitch
{
    class TwitchEntry : DownloadEntry
    {
        public string Tag { get; set; }

        public string Quality { get; }
        public string Title { get; }
        public double Framerate { get; }

        public Twitch MyHoster { get; }

        public TwitchEntry(Twitch hoster, string quali, string url, string title, double frameRate, string filename)
            : base(hoster)
        {
            MyHoster = hoster;
            Quality = quali;
            Title = title;
            Framerate = frameRate;
            FileName = filename;

            if (!string.IsNullOrEmpty(url))
                Url = new Uri(url);

            if (string.IsNullOrEmpty(filename) && Url != null)
                FileName = $"{title.Trim()}_{quali}p_{frameRate}fps{new FileInfo(Url.Segments[1]).Extension}";

            if (string.IsNullOrEmpty(url))
            {
                if (string.IsNullOrEmpty(filename))
                    FileName = title;
            }
        }

        protected override void OnLoad(object data)
        {
            if (Url != null)
                FileState = GetFileState(FileStateType.FileInfoAvail);
            else
                FileState = GetFileState(FileStateType.FileNotFound);
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }

        public override bool StartDownload(string path)
        {
            return DownloadFile(Url, path);
        }

        public override string ToString()
        {
            return FileName;
        }
    }
}
