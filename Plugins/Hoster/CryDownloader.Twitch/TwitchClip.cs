﻿using System.Net;

namespace CryDownloader.Twitch
{
    class TwitchClip : TwitchEntry
    {
        public TwitchClip(Twitch hoster, string title, int frameRate, string quali, string url)
            : base(hoster, quali, url, title, frameRate, string.Empty)
        { }

        protected override void OnLoad(object data)
        {
            if (Url != null)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);

                request.Method = "HEAD";

                using (var response = request.GetResponse())
                {
                    FileSize = response.ContentLength;
                }
            }

            base.OnLoad(data);
        }
    }
}
