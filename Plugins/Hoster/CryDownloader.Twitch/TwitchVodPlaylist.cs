﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace CryDownloader.Twitch
{
    class TwitchVodPlaylist : TwitchEntry
    {
        public bool IsSource { get; }

        public TwitchVodPlaylist(Twitch hoster, string title, double frameRate, string quali, string url, bool isSource)
            : base(hoster, quali, url, title, frameRate,
                  isSource ? $"{title.Trim()}_{quali}p_{frameRate}fps_Source.mp4" : $"{title.Trim()}_{quali}p_{frameRate}fps.mp4")
        {
            IsSource = isSource;
        }

        public TwitchVodPlaylist(Twitch hoster, string videoID, string errorMsg)
            : base(hoster, string.Empty, null, $"{videoID} - {errorMsg}", 0, $"{videoID} - {errorMsg}")
        { }

        protected override void OnLoad(object data)
        {
            WebClient webClient = (WebClient)data;

            if (Url != null)
            {
                string str = webClient.DownloadString(Url);

                List<string> lst = GetFiles(str, Url.Scheme + "://" + Url.Host + GetPath(Url));

                if (MyHoster.CalculateFileSize)
                {
                    long size = GetDownloadSize(lst.First());
                    FileSizeType = FileSizeType.Relative;
                    FileSize = lst.Count * size;
                }
                else
                {
                    foreach (var chunk in lst)
                        FileSize += GetDownloadSize(chunk);
                }
            }

            base.OnLoad(webClient);
        }

        public override bool StartDownload(string path)
        {
            return Muxing(Url.ToString(), path);
        }

        private long GetDownloadSize(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "HEAD";

            using (var response = request.GetResponse())
            {
                return response.ContentLength;
            }
        }

        private string GetPath(Uri url)
        {
            string result = string.Empty;

            for (int i = 0; i < url.Segments.Length - 1; ++i)
                result += url.Segments[i];

            return result;
        }

        private List<string> GetFiles(string str, string strDirectory)
        {
            string[] lines = str.Split('\n');
            List<string> result = new List<string>();

            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line))
                    continue;

                if (!line.StartsWith("#"))
                    result.Add(Path.Combine(strDirectory, line));

            }

            return result;
        }
    }
}
