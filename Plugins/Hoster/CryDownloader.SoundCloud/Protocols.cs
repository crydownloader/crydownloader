﻿namespace CryDownloader.Soundcloud
{
    static class Protocols
    {
        public const string HLS = "hls";
        public const string Progressive = "progressive";

        public static string GetName(string protocolType)
        {
            if (protocolType == HLS)
                return "HLS";
            else if (protocolType == Progressive)
                return "Progressive";

            return protocolType;
        }
    }
}
