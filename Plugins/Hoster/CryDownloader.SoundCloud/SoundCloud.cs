﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace CryDownloader.SoundCloud
{
    public class SoundCloud : Hoster
    {
        #region JsonObjects

        internal class SoundCloudJsonResultMediaDataTranscodingFormat
        {
            public string protocol { get; set; }
            public string mime_type { get; set; }
        }

        internal class SoundCloudJsonResultMediaDataTranscoding
        {
            public string url { get; set; }
            public string quality { get; set; }
            public long duration { get; set; }
            public bool snipped { get; set; }
            public SoundCloudJsonResultMediaDataTranscodingFormat format { get; set; }
        }

        class SoundCloudJsonResultMediaData
        {
            public List<SoundCloudJsonResultMediaDataTranscoding> transcodings { get; set; }
        }

        class SoundCloudJsonResultData
        {
            public string title { get; set; }
            public SoundCloudJsonResultMediaData media { get; set; }

            public List<SoundCloudJsonResultData> tracks { get; set; }

            public string uri { get; set; }
            public long id { get; set; }
            public string username { get; set; }
        }

        class SoundCloudJsonResult
        {
            public int id { get; set; }
            public List<SoundCloudJsonResultData> data { get; set; }
        }

        class SoundCloudTrackJsonResult
        {
            public List<SoundCloudJsonResultData> collection { get; set; }
            public string next_href { get; set; }
        }

        #endregion

        internal const string ClientID = "geYn8eUFaQInDycVClt3oHtBO2U6uEYm";

        public string Protocol { get; set; }
        public string MimeType { get; set; }

        public override string Name => "Soundcloud";

        public override string Host => "soundcloud.com";

        public override System.Drawing.Bitmap Icon => Properties.Resources.ios_a62dfc8f;

        public override System.Windows.FrameworkElement SettingControl { get; }

        public SoundCloud(IApplication app)
            : base(app)
        { }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>()
            {
                {"Protocol", Protocol },
                {"MimeType", MimeType },
            };
        }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            if (properties.TryGetValue("Protocol", out var value))
                Protocol = value;

            if (properties.TryGetValue("MimeType", out value))
                MimeType = value;
        }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            try
            {
                CreateEntriesForUrl(args.Url, args.ContainerCreator, args.CancellationToken);
            }
            catch (WebException ex)
            {
                var response = (HttpWebResponse)ex.Response;

                if (response.StatusCode == HttpStatusCode.BadRequest)
                    Application.LogManager.WriteException(LogLevel.Low, "failed to send GET request to Soundcloud.", ex);
            }
        }

        private void CreateEntriesForUrl(Uri url, IContainerCreator creator, CancellationToken? cancellationToken)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0";

            string str = "";

            using (var response = request.GetResponse())
            {
                using (var s = response.GetResponseStream())
                {
                    using (var r = new StreamReader(s))
                        str = r.ReadToEnd();
                }
            }

            IHtmlDocument htmlDocument = Application.LoadHtmlDocument(str);

            var nodes = htmlDocument.DocumentNode.SelectNodes(".//script");

            foreach (var n in nodes)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;
                }

                if (n.InnerText.StartsWith("webpackJsonp"))
                {
                    var regex = new Regex(@"}\s?var\s\D\s?=\s?(.*),\s?\D\s?=\s?D");
                    var regexMatch = regex.Match(n.InnerText);
                    var json = regexMatch.Groups[1].Value;
                    var soundCloudResult = Application.LoadJsonObject<List<SoundCloudJsonResult>>(json);

                    GetMediaAndUri(soundCloudResult, out var artist, out var albums, out var tracks);

                    if (tracks.Count != 0)
                    {
                        foreach (var data in tracks)
                        {
                            IContainer container = creator.CreateContainer(data.Item1);
                            CreateAndAddEntry(container, data);
                        }
                    }
                    else if (albums.Count != 0)
                    {
                        foreach (var album in albums)
                        {
                            IContainer container = creator.CreateContainer(album.Item1);

                            List<long> trackIds = new List<long>();

                            foreach (var track in album.Item2)
                            {
                                if (track.media == null)
                                    trackIds.Add(track.id);
                                else
                                {
                                    CreateAndAddEntry(container,
                                        new Tuple<string, SoundCloudJsonResultMediaData>(track.title, track.media));
                                }
                            }

                            GetTrackInfo(trackIds, out var trackList);

                            foreach (var track in trackList)
                            {
                                CreateAndAddEntry(container,
                                        new Tuple<string, SoundCloudJsonResultMediaData>(track.Item1.Item2, track.Item2));
                            }
                        }
                    }
                    else
                    {
                        GetTrackLists(artist, out var trackList);

                        foreach (var data in trackList)
                        {
                            IContainer container = creator.CreateContainer(data.Item1);

                            foreach (var track in data.Item2)
                                CreateAndAddEntry(container, track);
                        }
                    }
                }
            }
        }

        private void CreateAndAddEntry(IContainer container, Tuple<string, SoundCloudJsonResultMediaData> track)
        {
            SoundCloudEntry entry;

            if (string.IsNullOrEmpty(Protocol) && string.IsNullOrEmpty(MimeType))
                entry = new SoundCloudEntry(this, track.Item1, track.Item2.transcodings.First());
            else
            {
                List<SoundCloudJsonResultMediaDataTranscoding> protocols = new List<SoundCloudJsonResultMediaDataTranscoding>();
                SoundCloudJsonResultMediaDataTranscoding prefTrans = null;

                if (!string.IsNullOrEmpty(Protocol))
                {
                    foreach (var trans in track.Item2.transcodings)
                    {
                        if (trans.format.protocol == Protocol)
                            protocols.Add(trans);
                    }
                }

                if (!string.IsNullOrEmpty(MimeType))
                {
                    if (protocols.Count == 0)
                    {
                        foreach (var trans in track.Item2.transcodings)
                        {
                            if (trans.format.mime_type == MimeType)
                            {
                                prefTrans = trans;
                                break;
                            }
                        }

                        if (prefTrans == null)
                            prefTrans = track.Item2.transcodings.First();
                    }
                    else
                    {
                        foreach (var proto in protocols)
                        {
                            if (proto.format.mime_type == MimeType)
                            {
                                prefTrans = proto;
                                break;
                            }
                        }

                        if (prefTrans == null)
                            prefTrans = protocols.First();
                    }
                }
                else if (protocols.Count == 0)
                    prefTrans = track.Item2.transcodings.First();
                else
                    prefTrans = protocols.First();

                entry = new SoundCloudEntry(this, track.Item1, prefTrans);
            }

            entry.Load(container);
            container.AddEntry(entry.FileName, entry);
        }

        private void GetTrackLists(List<Tuple<long, string>> userDatas, out List<Tuple<string, List<Tuple<string, SoundCloudJsonResultMediaData>>>> tracks)
        {
            tracks = new List<Tuple<string, List<Tuple<string, SoundCloudJsonResultMediaData>>>>();

            foreach (var uri in userDatas)
            {
                var userTracks = new List<Tuple<string, SoundCloudJsonResultMediaData>>();

                GetTrackList(uri.Item1, userTracks);

                tracks.Add(new Tuple<string, List<Tuple<string, SoundCloudJsonResultMediaData>>>(uri.Item2, userTracks));
            }
        }

        private void GetTrackList(long userID, List<Tuple<string, SoundCloudJsonResultMediaData>> tracks)
        {
            GetTrackList($"https://api-v2.soundcloud.com/users/{userID}/tracks?representation=&limit=20&offset=0&linked_partitioning=1", tracks);
        }

        private void GetTrackList(string url, List<Tuple<string, SoundCloudJsonResultMediaData>> tracks)
        {
            var request = (HttpWebRequest)WebRequest.Create(url + $"&client_id={ClientID}");
            SoundCloudTrackJsonResult jsonData;

            using (var response = request.GetResponse())
            {
                using (var s = response.GetResponseStream())
                {
                    using (var r = new StreamReader(s))
                    {
                        jsonData = Application.LoadJsonObject<SoundCloudTrackJsonResult>(r);
                    }
                }
            }

            foreach (var track in jsonData.collection)
                tracks.Add(new Tuple<string, SoundCloudJsonResultMediaData>(track.title, track.media));

            if (jsonData.next_href != null)
                GetTrackList(jsonData.next_href, tracks);
        }

        private void GetTrackInfo(List<long> trackIds, out List<Tuple<Tuple<long, string>, SoundCloudJsonResultMediaData>> tracks)
        {
            tracks = new List<Tuple<Tuple<long, string>, SoundCloudJsonResultMediaData>>();
            var sb = new StringBuilder();

            foreach (var trackId in trackIds)
            {
                sb.Append(trackId);
                sb.Append(",");
            }

            sb = sb.Remove(sb.Length - 1, 1);

            var request = (HttpWebRequest)WebRequest.Create($"https://api-v2.soundcloud.com/tracks?ids={sb.ToString()}&client_id=geYn8eUFaQInDycVClt3oHtBO2U6uEYm&%5Bobject%20Object%5D=");
            List<SoundCloudJsonResultData> jsonData;

            using (var response = request.GetResponse())
            {
                using (var s = response.GetResponseStream())
                {
                    using (var r = new StreamReader(s))
                    {
                        jsonData = Application.LoadJsonObject<List<SoundCloudJsonResultData>>(r);
                    }
                }
            }

            foreach (var track in jsonData)
            {
                if (track.media != null)
                    tracks.Add(new Tuple<Tuple<long, string>, SoundCloudJsonResultMediaData>(new Tuple<long, string>(track.id, track.title), track.media));
            }
        }

        private void GetMediaAndUri(List<SoundCloudJsonResult> soundCloudResult,
            out List<Tuple<long, string>> uris,
            out List<Tuple<string, List<SoundCloudJsonResultData>>> albums,
            out List<Tuple<string, SoundCloudJsonResultMediaData>> tracks)
        {
            uris = new List<Tuple<long, string>>();
            tracks = new List<Tuple<string, SoundCloudJsonResultMediaData>>();
            albums = new List<Tuple<string, List<SoundCloudJsonResultData>>>();

            foreach (var r in soundCloudResult)
            {
                if (r.data != null)
                {
                    foreach (var rr in r.data)
                    {
                        if (rr.media != null && rr.title != null)
                            tracks.Add(new Tuple<string, SoundCloudJsonResultMediaData>(rr.title, rr.media));

                        if (rr.tracks != null)
                            albums.Add(new Tuple<string, List<SoundCloudJsonResultData>>(rr.title, rr.tracks));

                        if (rr.uri != null && rr.username != null)
                            uris.Add(new Tuple<long, string>(rr.id, rr.username));
                    }
                }
            }
        }
    }
}
