﻿namespace CryDownloader.Soundcloud
{
    static class MimeTypes
    {
        public const string MPEG = "audio/mpeg";
        public const string OPUS = "audio/ogg; codecs=\"opus\"";

        public static string GetName(string mimeType)
        {
            if (mimeType == MPEG)
                return "MPEG";
            else if (mimeType == OPUS)
                return "OPUS";

            return mimeType;
        }
    }
}
