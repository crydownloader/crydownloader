﻿using CryDownloader.Plugin;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace CryDownloader.SoundCloud
{
    class SoundCloudEntry : DownloadEntry
    {
        class JsonResult
        {
            public string url { get; set; }
        }

        private SoundCloud.SoundCloudJsonResultMediaDataTranscoding m_transcoding;

        public SoundCloudEntry(Hoster hoster, string title,
            SoundCloud.SoundCloudJsonResultMediaDataTranscoding transcoding)
            : base(hoster)
        {
            Url = new Uri(transcoding.url + $"?client_id={SoundCloud.ClientID}");
            FileName = title + (transcoding.format.mime_type.Contains("ogg") ? ".ogg" : ".mp3");
            m_transcoding = transcoding;
            FileSizeType = FileSizeType.Relative;
        }

        public override bool CheckStream(Stream stream)
        {
            return true;
        }

        protected override void OnLoad(object data)
        {
            try
            {
                var playList = LoadM3UPlaylist(GetPlaylist());

                long fileSize = 0;

                foreach (var file in playList.Files)
                {
                    Debug.WriteLine(file.Path);
                    fileSize += GetFileSize(new Uri(file.Path));
                }

                FileSize = fileSize;

                if (FileSize == 0)
                    FileState = GetFileState(FileStateType.FileInfoNotAvail);
                else
                    FileState = GetFileState(FileStateType.FileInfoAvail);
            }
            catch (Exception ex)
            {
                Hoster.Application.LogManager.WriteException(LogLevel.Normal, "failed to get file informations", ex);
                FileState = GetFileState(FileStateType.FileNotFound);
            }
        }

        public override bool StartDownload(string path)
        {
            return Muxing(GetPlaylistUrl(), path);
        }

        private string GetPlaylistUrl()
        {
            var request = (HttpWebRequest)WebRequest.Create(Url);
            JsonResult playlistResult;

            using (var response = request.GetResponse())
            {
                using (var s = response.GetResponseStream())
                {
                    using (var r = new StreamReader(s))
                    {
                        playlistResult = LoadJsonObject<JsonResult>(r);
                    }
                }
            }

            return playlistResult.url;
        }

        private string GetPlaylist()
        {
            var request = (HttpWebRequest)WebRequest.Create(GetPlaylistUrl());

            using (var response = request.GetResponse())
            {
                using (var s = response.GetResponseStream())
                {
                    using (var r = new StreamReader(s))
                    {
                        return r.ReadToEnd();
                    }
                }
            }
        }
    }
}
