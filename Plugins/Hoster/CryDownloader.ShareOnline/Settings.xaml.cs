﻿using System.Windows;
using System.Windows.Controls;

namespace CryDownloader.ShareOnline
{
    /// <summary>
    /// Interaktionslogik für Settings.xaml
    /// </summary>
    internal partial class Settings : UserControl
    {
        public SettingViewModel ViewModel { get; }

        public Settings(SettingViewModel viewModel)
        {
            ViewModel = viewModel;

            InitializeComponent();

            m_password.Password = viewModel.Password;
        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.Apply();
        }

        private void PasswordChanged(object sender, RoutedEventArgs e)
        {
            ViewModel.Password = m_password.Password;
        }
    }
}
