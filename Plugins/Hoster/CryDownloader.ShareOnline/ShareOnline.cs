﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CryDownloader.ShareOnline
{
    public class ShareOnline : Hoster
    {
        internal const string ShareOnlineApiUrl = "http://api.share-online.biz/linkcheck.php?md5=1";
        internal const int MaxLinkEntries = 200;

        public bool Secured { get; set; }
        public bool HasPremium { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public override string Name => "Share Online";

        public override string Host => "share-online.biz";

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public override FrameworkElement SettingControl => m_settingControl;

        private Settings m_settingControl;

        public ShareOnline(IApplication app)
            : base(app)
        { }

        public override void Initialize()
        {
            SettingViewModel viewModel = new SettingViewModel(this);

            Application.Dispatcher.Invoke(() => m_settingControl = new Settings(viewModel));

            viewModel.Wait();
        }

        protected override void OnCreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            Queue<ShareOnlineEntry> queue = new Queue<ShareOnlineEntry>();
            Dictionary<Uri, bool> dict = new Dictionary<Uri, bool>();

            foreach (var url in urls)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        break;
                }

                if (dict.ContainsKey(url))
                    continue;

                scanUrlChangedCallback?.Invoke(url);

                queue.Enqueue(new ShareOnlineEntry(url, this));
                dict.Add(url, true);
            }

            using (var client = new HttpClient())
            {
                ProcessGetCheckLinkStates(queue, client, creator, ShareOnlineApiUrl, MaxLinkEntries);
            }
        }

        public override void Destroy()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        public bool CheckAccount(IWebBrowser browser, out string status)
        {
            Uri siteLine = new Uri("http://share-online.biz");
            // account_details
            browser.Navigate(siteLine);
            System.Threading.Thread.Sleep(500);
            status = "Invalid account data";

            if (Login(browser))
            {
                status = "Failed to parse data";

                var element = browser.GetElementById("account_details");
                DateTime dateTime = DateTime.Now;

                while (element == null)
                {
                    element = browser.GetElementById("account_details");

                    TimeSpan timeSpan = DateTime.Now - dateTime;

                    if (timeSpan.TotalSeconds > 10)
                        break;
                }

                if (element != null)
                {
                    var elements = element.SelectNodes("//p[@class='p_r']");

                    if (elements != null && elements.Count == 10)
                    {
                        IHtmlNode accountState = elements[0];

                        status = "Freemode";

                        if (accountState.InnerText.ToLower().Contains("premium"))
                        {
                            IHtmlNode accountLimit = elements[2];
                            status = "Premium until " + accountLimit.InnerText.Trim();

                            Logout(browser);
                            HasPremium = true;
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        internal bool Logout(IWebBrowser browser)
        {
            browser.Navigate(new Uri("https://www.share-online.biz/user/logout"));
            System.Threading.Thread.Sleep(500);
            return true;
        }

        internal bool Login(IWebBrowser browser)
        {
            var loginBar = browser.GetElementById("login-link");

            if (loginBar == null)
                return true;

            var login = browser.GetElementById("login");

            if (login == null)
                return false;

            var user = browser.GetElementById("l_user");

            if (user == null)
                return false;

            var pass = browser.GetElementById("l_pass");

            if (pass == null)
                return false;

            var loginElements = login.SelectNodes("//input[@class='button ui-button ui-widget ui-state-default ui-corner-all']");

            if (loginElements == null)
                return false;

            IHtmlNode loginNode = null;
            IHtmlAttribute attr;

            browser.SetValue(user, Username);
            browser.SetValue(pass, Password);

            if (loginElements.Count != 0)
            {
                foreach (var element in loginElements)
                {
                    attr = element.Attributes["value"];

                    if (attr == null)
                        continue;

                    if (attr.Value.Contains("Einloggen") || attr.Value.Contains("Log in"))
                    {
                        loginNode = element;
                        break;
                    }
                }
            }

            if (loginNode == null)
                return false;

            browser.SendClickEvent(loginNode);
            return true;
        }

        private async void ProcessGetCheckLinkStates(Queue<ShareOnlineEntry> requests, HttpClient client, IContainerCreator creator, string url, int maxLinkEntries)
        {
            Dictionary<string, List<ShareOnlineEntry>> requestsList = new Dictionary<string, List<ShareOnlineEntry>>();
            IContainer container = null;

            while (requests.Count > 0)
            {
                StringBuilder sb = CreateRequest(requests, requestsList, maxLinkEntries);
                string[] linkResponses = await GetLinkStates(client, url, sb);
                container = ProcessResponse(linkResponses, requestsList, creator, container);
            }
        }

        private IContainer ProcessResponse(string[] linkResponses, Dictionary<string, List<ShareOnlineEntry>> requestsList, IContainerCreator creator, IContainer container)
        {
            IContainer result = container;

            foreach (var resp in linkResponses)
            {
                string[] lnkResponse = resp.Split(';');

                if (lnkResponse.Length < 3)
                    continue;

                foreach (var req in requestsList)
                {
                    if (req.Key.Contains(lnkResponse[0]))
                    {
                        foreach (var entry in req.Value)
                        {
                            entry.Load(lnkResponse);

                            string name = CreateName(entry.FileName);

                            result = creator.CreateContainer(name);

                            result.AddEntry(entry.FileName, entry);
                        }
                    }
                }
            }

            return result;
        }

        private string CreateName(string fileName)
        {
            string[] fileNameParts = fileName.Split('.');

            if (fileNameParts.Length < 3)
                return fileName;

            string result = string.Empty;

            for (int i = 0; i < fileNameParts.Length - 2; ++i)
                result += fileNameParts[i];

            return result;
        }

        private StringBuilder CreateRequest(Queue<ShareOnlineEntry> requests, Dictionary<string, List<ShareOnlineEntry>> requestsList, int maxLinkEntries)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < Math.Min(requests.Count, maxLinkEntries); ++i)
            {
                ShareOnlineEntry req = requests.Dequeue();
                string entryUrl = req.Url.ToString();

                if (requestsList.ContainsKey(entryUrl))
                {
                    requestsList[entryUrl].Add(req);
                    continue;
                }

                sb.Append($"{entryUrl}\n");
                requestsList.Add(entryUrl, new List<ShareOnlineEntry>() { req });
            }

            return sb.Remove(sb.Length - 1, 1);
        }

        private async Task<string[]> GetLinkStates(HttpClient client, string url, StringBuilder builder)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>() {
                    { "links", builder.ToString() }
                };

            HttpContent content = new FormUrlEncodedContent(dict);
            HttpResponseMessage msg = client.PostAsync(url, content).Result;
            string responseMessage = await msg.Content.ReadAsStringAsync();
            return responseMessage.Split('\n');
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            if (properties.TryGetValue("Secured", out var value))
                Secured = value == "1";

            if (properties.TryGetValue("Username", out value))
                Username = value;

            if (properties.TryGetValue("Password", out value))
                Password = value;
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>()
            {
                { "Secured", Secured ? "1" : "0" },
                { "Username", Username },
                { "Password", Password }
            };
        }
    }
}
