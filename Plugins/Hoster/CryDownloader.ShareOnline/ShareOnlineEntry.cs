﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.ShareOnline
{
    class ShareOnlineEntry : DownloadEntry
    {
        public string Hash { get; private set; }

        private ShareOnline m_hoster;
        private bool m_isLoaded;
        private object m_normalDownloadSync;

        public ShareOnlineEntry(Uri url, ShareOnline hoster)
            : base(hoster)
        {
            Url = url;
            m_hoster = hoster;
            m_isLoaded = false;
            m_normalDownloadSync = new object();
        }

        protected override void OnLoad(object data)
        {
            string[] apiResponse = (string[])data;

            if (apiResponse[1].ToLower() == "ok")
                FileState = GetFileState(FileStateType.FileInfoAvail);
            else if (apiResponse[1].ToLower() == "deleted")
                FileState = GetFileState(FileStateType.FileDeleted);
            else if (apiResponse[1].ToLower() == "not found" || apiResponse[1].ToLower() == "notfound")
                FileState = GetFileState(FileStateType.FileNotFound);

            if (!m_isLoaded)
            {
                if (apiResponse.Length > 3)
                {
                    FileName = apiResponse[2];

                    if (long.TryParse(apiResponse[3], out var size))
                    {
                        if (FileSize != size)
                            FileSize = size;
                    }

                    if (apiResponse.Length == 5)
                        Hash = apiResponse[4];
                }
                else
                    FileName = apiResponse[0];

                m_isLoaded = true;
            }
            else if (long.TryParse(apiResponse[3], out var size))
            {
                if (FileSize != size)
                    FileSize = size;
            }
        }

        public override bool StartDownload(string path)
        {
            var browser = Hoster.Application.CreateBrowser();

            browser.Navigate(Url);

            if (m_hoster.HasPremium)
            {
                return StartPremiumDownload(browser, path);
            }
            else
                return StartNormalDownload(browser, path);
        }

        public override bool CheckStream(Stream stream)
        {
            if (string.IsNullOrEmpty(Hash))
                return true;

            var md5 = new MD5CryptoServiceProvider();

            byte[] hash = md5.ComputeHash(stream);

            var sb = new StringBuilder();

            for (int i = 0; i < hash.Length; ++i)
                sb.Append(hash[i].ToString("x2"));

            return sb.ToString() == Hash;
        }

        public override string ToString()
        {
            return FileName;
        }

        private IHtmlNode GetDownloadNode(IWebBrowser browser)
        {
            IHtmlNode download = null;

            while (download == null)
            {
                download = browser.GetElementById("dll");
                var min = browser.GetElementById("dl_min");
                var sec = browser.GetElementById("dl_sec");

                if (min != null && sec != null)
                {
                    var minContent = min.SelectNodes("p");
                    var secContent = sec.SelectNodes("p");

                    if (minContent.Count == 1 && secContent.Count == 1)
                    {
                        FileState = Hoster.Application.CreateFileState(FileStateIcon.None, $"{minContent[0].InnerText}:{secContent[0].InnerText}");
                        Debug.WriteLine(FileState.Description);
                    }
                }

                Thread.Sleep(1);
            }

            return download;
        }

        private bool GetDownloadNodes(IHtmlNode downloadNode, out IHtmlNode normalNode, out IHtmlNode securedNode)
        {
            normalNode = null;
            securedNode = null;

            var downloadContent = downloadNode.SelectNodes("p/a");

            if (downloadContent == null)
                return false;

            if (downloadContent.Count != 2)
                return false;

            foreach (var n in downloadContent)
            {
                if (n.Id == "dlssl")
                    normalNode = n;
                else
                    securedNode = n;
            }

            return true;
        }

        private bool StartPremiumDownload(IWebBrowser browser, string path)
        {
            if (!m_hoster.Login(browser))
                return false;

            var download = GetDownloadNode(browser);

            if (download == null)
                return false;

            if (!GetDownloadNodes(download, out var normalNode, out var securedNode))
                return false;

            if (m_hoster.Secured)
                download = securedNode;
            else
            {
                browser.SendClickEvent(normalNode);
                download = GetDownloadNode(browser);

                if (download == null)
                    return false;

                if (!GetDownloadNodes(download, out _, out normalNode))
                    return false;

                download = normalNode;
            }

            if (download == null)
                return false;

            var attr = download.Attributes["href"];

            if (attr == null)
                return false;

            DownloadFile(new Uri(attr.Value), path);

            return true;
        }

        private bool ClickDownloadLink(IWebBrowser browser, IHtmlNode downloadNode)
        {
            var downloadContent = downloadNode.SelectNodes("p/a");

            if (downloadContent.Count != 1)
                return false;

            downloadNode = downloadContent[0];
            browser.SendClickEvent(downloadNode);
            return true;
        }

        private bool StartNormalDownload(IWebBrowser browser, string path)
        {
            string vidID = string.Empty;

            lock (m_normalDownloadSync)
            {
                IRequest request = browser.CreateRequest();
                request.Method = "POST";
                request.Url = Url.ToString() + "/free";

                NameValueCollection headers = new NameValueCollection
                {
                    { "Content-Type", "application/x-www-form-urlencoded" }
                };

                request.Headers = headers;

                using (var stream = request.GetRequestStream())
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write("dl_free=1&choice=free");
                    }
                }

                browser.LoadRequest(request);

                List<IHtmlNode> hideNodes = new List<IHtmlNode>
                {
                    browser.GetElementById("dl_wait"),
                    browser.GetElementById("dl_ticket")
                };

                IHtmlNode download = null;

                Task timer = Task.Run(() => { download = GetDownloadNode(browser); });
                bool result = false;

                Hoster.Application.ResolveCaptcha(browser, browser.GetElementById("dl_captcha_c"), hideNodes, () =>
                {

                    if (download != null)
                    {
                        if (ClickDownloadLink(browser, download))
                        {
                            download = GetDownloadNode(browser);
                            return !download.InnerText.Contains("Captcha");
                        }
                    }

                    return false;

                }, () =>
                {
                    timer.Wait();

                    if (download != null)
                    {
                        if (ClickDownloadLink(browser, download))
                        {
                            result = true;
                            Uri url = browser.GetDownloadUrl();

                            DownloadFile(url, path);
                        }
                    }
                });

                return result;
            }
        }
    }
}
