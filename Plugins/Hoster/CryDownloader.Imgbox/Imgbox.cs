﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace CryDownloader.Imgbox
{
    public class Imgbox : PictureHoster, IShareHoster
    {
        public override string Name => "Imgbox";

        public override string Host => "imgbox.com";

        public override System.Drawing.Bitmap Icon => Properties.Resources.favicon;

        public Imgbox(IApplication app)
            : base(app)
        { }

        public override void Initialize()
        { }

        public override bool IsValidUrl(Uri url)
        {
            return url.Host.Contains(Host);
        }

        protected override void OnCreateEntry(CreateEntryArgs args)
        {
            string filename = args.Url.Segments[args.Url.Segments.Length - 1];

            FileInfo fi = new FileInfo(filename);


            if (!string.IsNullOrEmpty(fi.Extension))
                filename = fi.Name.Replace(fi.Extension, string.Empty);

            if (filename.EndsWith("_t") || filename.EndsWith("_o"))
                filename = filename.Remove(filename.Length - 2, 2);

            IContainer container = args.ContainerCreator.CreateContainer(filename);
            CreateEntry(args.Url, container, args.CancellationToken);
        }

        bool IShareHoster.CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            return CreateEntry(url, container, cancellationToken);
        }

        private bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken)
        {
            string filename = url.Segments[url.Segments.Length - 1];

            if (url.Host.StartsWith("thumbs"))
            {
                FileInfo fi = new FileInfo(filename);
                filename = filename.Substring(0, filename.IndexOf(".") - 2);
                url = new Uri($"{url.Scheme}://{Host}/{filename.Replace(fi.Extension, string.Empty)}");
            }
            else if (url.Host.StartsWith("t."))
                url = new Uri($"{url.Scheme}://i.{Host}/{filename}");

            if (filename.IndexOf(".") < 0)
            {
                string strHtml = DownloadPage(url);

                IHtmlDocument document = Application.LoadHtmlDocument(strHtml);
                IHtmlNode picNode = document.GetElementbyId("img");

                if (picNode != null)
                {
                    var relativeUri = new Uri(picNode.Attributes["src"].Value);
                    filename = relativeUri.Segments[relativeUri.Segments.Length - 1];
                }

            }

            var entry = new ImgboxEntry(this, url, filename);
            container.AddEntry(filename, entry);
            return true;
        }
    }
}
