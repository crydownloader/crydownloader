﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Imgbox
{
    class ImgboxEntry : DownloadEntry
    {
        public ImgboxEntry(Imgbox hoster, Uri url, string fileName)
            : base(hoster)
        {
            Url = url;
            FileName = fileName;
        }

        public override bool CheckStream(Stream stream)
        {
            return stream.Length == FileSize;
        }

        private void Resolve(Uri url)
        {
            string strHtml = Hoster.DownloadPage(url);

            if (string.IsNullOrEmpty(strHtml))
                FileState = GetFileState(FileStateType.FileNotFound);
            else
            {
                IHtmlDocument document = Hoster.Application.LoadHtmlDocument(strHtml);
                IHtmlNode picNode = document.GetElementbyId("img");

                if (picNode == null)
                    FileState = GetFileState(FileStateType.FileNotFound);
                else if (picNode.Attributes.Contains("src"))
                {
                    var relativeUri = new Uri(picNode.Attributes["src"].Value);

                    if (!relativeUri.IsAbsoluteUri)
                        Url = new Uri(Url, relativeUri);
                    else
                        Url = relativeUri;

                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
                else
                    FileState = GetFileState(FileStateType.FileNotFound);
            }
        }

        protected override void OnLoad(object data)
        {
            try
            {
                if (Url.Host.StartsWith("t.") || Url.Host.StartsWith("thumbs"))
                {
                    Uri newUrl = new Uri(Url, $"/{FileName}");
                    Resolve(newUrl);
                }
                else if (Url.Host.StartsWith("i.") || Url.Host.StartsWith("images"))
                {
                    FileSize = GetFileSize(Url);

                    if (FileSize == 0)
                        FileState = GetFileState(FileStateType.FileNotFound);
                    else
                        FileState = GetFileState(FileStateType.FileInfoAvail);
                }
                else
                    Resolve(Url);
            }
            catch
            {
                FileState = GetFileState(FileStateType.FileInfoNotAvail);
            }
        }
    }
}
