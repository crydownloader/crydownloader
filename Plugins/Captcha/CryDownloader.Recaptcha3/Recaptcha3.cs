﻿using CryDownloader.Plugin;
using System.Collections.Generic;

namespace CryDownloader.Recaptcha3
{
    public class Recaptcha3 : CaptchaType
    {
        public override string Name => "Recaptcha v3";

        public override bool NeedAction => false;

        public Recaptcha3(IApplication app)
            : base(app)
        { }

        public override bool CanResolveCaptcha(IHtmlNode htmlNode)
        {
            if (htmlNode.Attributes.Contains("data-sitekey"))
                return true;

            return htmlNode.InnerHtml.Contains("https://www.google.com/recaptcha/api2");
        }

        public override IEnumerable<IHtmlNode> GetCaptchaNodes(IHtmlDocument htmlDocument)
        {
            List<IHtmlNode> result = new List<IHtmlNode>();

            var nodes = htmlDocument.DocumentNode.SelectNodes("//div[@class='g-recaptcha-bubble-arrow']");

            if (nodes != null)
            {
                foreach (var n in nodes)
                    result.Add(n);
            }

            nodes = htmlDocument.DocumentNode.SelectNodes("//*[@data-sitekey]");

            if (nodes != null)
            {
                foreach (var n in nodes)
                    result.Add(n);
            }

            return result;
        }

        public override void Initialize()
        { }

        protected override bool OnResolveCaptcha(IHtmlNode htmlNode, IWebBrowser webBrowser)
        {
            return false; // force manual
        }

        protected override void OnStartCheckCaptchaTask(IWebBrowser webBrowser)
        {
            StartCheckCaptchaTask(() => {

                string responseValue = webBrowser.GetElementValue("g-recaptcha-response");
                return !string.IsNullOrEmpty(responseValue);
            });
        }
    }
}
