﻿using MahApps.Metro.Controls.Dialogs;
using System;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    internal partial class MainWindow
    {
        public MainViewModel ViewModel { get; }

        public MainWindow()
        {
            ViewModel = new MainViewModel(App.Downloader, DialogCoordinator.Instance);

            InitializeComponent();

            Width = App.Downloader.Settings.LastWidth;
            Height = App.Downloader.Settings.LastHeight;
            WindowState = App.Downloader.Settings.LastWindowState;
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ViewModel.Exit((int)ActualHeight, (int)ActualWidth, WindowState);
            App.Downloader.Exit();
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            ViewModel.Initialize(this, m_MonitorClipboard, m_OptionPage, m_AddLinkPage,
                m_DownloadPage, m_downloadView, m_addLinksView, m_optionView, m_entryProperties, m_MonitorClipboardImage, m_ScanJobs);
        }
    }
}
