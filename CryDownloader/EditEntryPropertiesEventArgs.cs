﻿using System;

namespace CryDownloader
{
    class EditEntryPropertiesEventArgs : EventArgs
    {
        public EntryViewModel Entry { get; }

        public EditEntryPropertiesEventArgs(EntryViewModel entry)
        {
            Entry = entry;
        }
    }
}
