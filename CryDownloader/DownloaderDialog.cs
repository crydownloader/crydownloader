﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Markup;

namespace CryDownloader
{
    [DefaultProperty("Content")]
    [ContentProperty("Content")]
    abstract class DownloaderDialog : BaseMetroDialog
    {
        public abstract Task WaitForResult();
        public abstract void Cancel();
    }

    class DownloaderDialog<T> : DownloaderDialog
    {
        private TaskCompletionSource<T> m_taskCompletion;

        public T Result { get; set; }

        public DownloaderDialog()
        {
            m_taskCompletion = new TaskCompletionSource<T>();
        }

        public override Task WaitForResult()
        {
            return m_taskCompletion.Task;
        }

        public override void Cancel()
        {
            m_taskCompletion.SetResult(default);
        }

        public void Close()
        {
            m_taskCompletion.SetResult(Result);
        }
    }
}
