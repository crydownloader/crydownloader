﻿using System;
using System.Windows.Threading;
//using Windows.Data.Xml.Dom;
//using Windows.UI.Notifications;

namespace CryDownloader
{
    enum NotificationDismissalReason
    {
        UserCanceled = 0,
        ApplicationHidden = 1,
        TimedOut = 2
    }

    class NotificationFailedEventArgs : EventArgs
    {
        public Exception ErrorCode { get; }

        public NotificationFailedEventArgs(Exception ex)
        {
            ErrorCode = ex;
        }
    }

    class NotificationDismissedEventArgs : EventArgs
    {
        public NotificationDismissalReason Reason { get; }

        public NotificationDismissedEventArgs(NotificationDismissalReason reason)
        {
            Reason = reason;
        }
    }

    abstract class MyNotification
    {
        public event EventHandler Activated;
        public event EventHandler<NotificationDismissedEventArgs> Dismissed;
        public event EventHandler<NotificationFailedEventArgs> Failed;

        public MyNotification()
        { }

        public abstract void Show(Dispatcher dispatcher);

        protected void RaiseActivatedEvent()
        {
            Activated?.Invoke(this, new EventArgs());
        }

        protected void RaiseDismissedEvent(NotificationDismissedEventArgs args)
        {
            Dismissed?.Invoke(this, args);
        }
        protected void RaiseFailedEvent(NotificationFailedEventArgs args)
        {
            Failed?.Invoke(this, args);
        }
    }
    // TODO:
    /*class MyToastNotification : MyNotification
    {
        public ToastNotification ToastNotification { get; }
        
        public MyToastNotification(XmlDocument xmlDocument)
        {
            ToastNotification = new ToastNotification(xmlDocument);
            ToastNotification.Activated += ToastActivated;
            ToastNotification.Dismissed += ToastDismissed;
            ToastNotification.Failed += ToastFailed;
        }
        
        private void ToastActivated(ToastNotification sender, object e)
        {
            Dispatcher.CurrentDispatcher.Invoke(() => RaiseActivatedEvent());
        }
        
        private void ToastDismissed(ToastNotification sender, ToastDismissedEventArgs e)
        {
            NotificationDismissalReason reason = NotificationDismissalReason.UserCanceled;
        
            switch (e.Reason)
            {
                case ToastDismissalReason.ApplicationHidden:
                    reason = NotificationDismissalReason.ApplicationHidden;
                    break;
                case ToastDismissalReason.UserCanceled:
                    reason = NotificationDismissalReason.UserCanceled;
                    break;
                case ToastDismissalReason.TimedOut:
                    reason = NotificationDismissalReason.TimedOut;
                    break;
            }
        
            Dispatcher.CurrentDispatcher.Invoke(() => RaiseDismissedEvent(new NotificationDismissedEventArgs(reason)));
        }
        
        private void ToastFailed(ToastNotification sender, ToastFailedEventArgs e)
        {
            Dispatcher.CurrentDispatcher.Invoke(() => RaiseFailedEvent(new NotificationFailedEventArgs(e.ErrorCode)));
        }
        
        public override void Show(Dispatcher dispatcher)
        {
            if (!dispatcher.CheckAccess())
                dispatcher.Invoke(() => ToastNotificationManager.CreateToastNotifier(App.APP_ID).Show(ToastNotification));
            else
                ToastNotificationManager.CreateToastNotifier(App.APP_ID).Show(ToastNotification);
        }
    }*/
}
