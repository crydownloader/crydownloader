﻿using CryDownloader.Logic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Windows.Controls;

namespace CryDownloader
{
    class ScanViewModel : ViewModelBase
    {
        public ObservableCollection<EntryViewModel> SelectedScanEntries { get; }
        public ObservableCollection<ScannedLinkContainerViewModel> ScannedLinks { get; }

        DockPanel m_scanJobs;

        public ScanViewModel()
        {
            ScannedLinks = new ObservableCollection<ScannedLinkContainerViewModel>();
            SelectedScanEntries = new ObservableCollection<EntryViewModel>();
        }

        public void Destroy()
        { }

        public void Initialize(DockPanel scanJobs)
        {
            m_scanJobs = scanJobs;
        }

        public bool ScanText(string text)
        {
            return Scan(text, false, ScanJobState.Scan);
        }

        public bool Scan(string text, bool bIsClipboard, ScanJobState scanJobState = ScanJobState.Scan)
        {
            UserControl scanJob;
            ScanJobViewModel viewModel;

            var urls = App.Downloader.GetUrlsFromText(text);
    
            foreach (var url in urls)
            {
                if (scanJobState == ScanJobState.Loading)
                {
                    var ctrl = new LoadingJobView(url, this, bIsClipboard, scanJobState);
                    scanJob = ctrl;
                    viewModel = ctrl.ViewModel;
                }
                else
                {
                    var ctrl = new ScanJobView(url, this, bIsClipboard, scanJobState);
                    scanJob = ctrl;
                    viewModel = ctrl.ViewModel;
                }

                m_scanJobs.Children.Add(scanJob);

                viewModel.Scan();
            }

            return true;
        }

        public void RemoveScanJob(UserControl scanJob)
        {
            if (!m_scanJobs.Dispatcher.CheckAccess())
                m_scanJobs.Dispatcher.Invoke(() => m_scanJobs.Children.Remove(scanJob));
            else
                m_scanJobs.Children.Remove(scanJob);
        }
    }
}
