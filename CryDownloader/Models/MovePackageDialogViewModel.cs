﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace CryDownloader
{
    class MovePackageDialogViewModel : ViewModelBase
    {
        private string m_name;
        private string m_savePath;
        private bool m_pathIsModified;

        public string Name { get => m_name; set { m_name = value; OnNameChanged(); OnPropertyChanged(); } }

        public string SavePath { get => m_savePath; set { m_savePath = value; m_pathIsModified = true; OnPropertyChanged(); } }

        public MovePackageDialogViewModel(EntryViewModel entryViewModel)
        {
            m_name = entryViewModel.Name;
            m_savePath = entryViewModel.DownloadPath;
        }

        public void ChooseSavePath()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = SavePath;

            if (dialog.ShowDialog() != DialogResult.Cancel)
                SavePath = dialog.SelectedPath;
        }

        private void OnNameChanged()
        {
            if (!m_pathIsModified)
            {
                DirectoryInfo info = new DirectoryInfo(SavePath);

                m_savePath = Path.Combine(info.Parent.FullName, Name);
                OnPropertyChanged(nameof(SavePath));
            }
        }
    }
}
