﻿using CryDownloader.Logic;
using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Threading;

namespace CryDownloader
{
    class DownloadContainerViewModel : EntryViewModel
    {
        private bool m_bIsDefaultName;

        public Logic.DownloadContainer Data { get; }

        public override ImageSource Icon { get => Images.ContainerIcon; protected set => throw new NotSupportedException(); }

        public override string Name
        {
            get => Data.Name;
            set
            {
                string oldName = Data.Name;

                Data.Name = value;

                if (m_bIsDefaultName)
                    DownloadPath = DownloadPath.Replace(oldName, Data.Name);
                else
                {
                    DirectoryInfo info = new DirectoryInfo(DownloadPath);
                    m_bIsDefaultName = info.Name == Name;
                }

                OnPropertyChanged();
            }
        }

        public override string DownloadPath
        {
            get => Data.Directory;
            set
            {
                DirectoryInfo info = new DirectoryInfo(value);

                if (info.Name == Name)
                {
                    Data.Directory = value;
                    m_bIsDefaultName = true;
                }
                else
                {
                    m_bIsDefaultName = false;
                    Data.Directory = value;
                }

                foreach (var entry in Entries)
                {
                    if (entry.IsDefault)
                        entry.DownloadPath = Path.Combine(DownloadPath, entry.Name);
                }

                OnPropertyChanged();
            }
        }

        public override string DownloadUrl => Data.Url.ToString();

        public override AsyncObservableCollection<StatusViewModel> Status { get; }

        public override AsyncObservableCollection<HosterViewModel> Hoster { get; }

        public override string FileSize { get; protected set; }

        public AsyncObservableCollection<DownloadEntryViewModel> Entries { get; }

        public double Percentage
        {
            get => m_downloadPercentage;
            private set
            {
                if (double.IsInfinity(value) || double.IsNaN(value))
                    return;

                m_downloadPercentage = value;
                OnPropertyChanged();
            }
        }

        public string CurrentSpeed
        {
            get => m_currentSpeed;
            private set
            {
                m_currentSpeed = value;
                OnPropertyChanged();
            }
        }

        public string RemainTimeString
        {
            get => m_remainTimeString;
            private set
            {
                m_remainTimeString = value;
                OnPropertyChanged();
            }
        }

        public ulong ReceivedSize { get; private set; }

        public TimeSpan Duration { get; private set; }

        public MainViewModel Parent { get; }

        public ulong CurrentSpeedValue { get; private set; }

        public ulong BytesToReceive { get; private set; }

        public bool IsDownloading
        {
            get => m_isDownloading;
            set
            {
                m_isDownloading = value;
                OnPropertyChanged();
            }
        }

        public Brush ProgressBarBrush
        {
            get => m_progressBarBrush;
            set
            {
                m_progressBarBrush = value;
                OnPropertyChanged();
            }
        }

        public FileDownloadState DownloadState
        { 
            get => m_downloadState; 
            set
            {
                m_downloadState = value;

                if (m_downloadState != FileDownloadState.Downloading)
                {
                    CurrentSpeed = string.Empty;
                    CurrentSpeedValue = 0;
                }

                    OnPropertyChanged();
            }
        }

        public Dispatcher Dispatcher { get; }

        private double m_downloadPercentage;
        private string m_currentSpeed;
        private string m_remainTimeString;
        private bool m_isDownloading;
        private Brush m_progressBarBrush;
        private FileDownloadState m_downloadState;

        public DownloadContainerViewModel(ScannedLinkContainerViewModel data, MainViewModel parent)
        {
            Parent = parent;
            Data = data.Data;
            Hoster = data.Hoster;
            Status = data.Status;
            FileSize = data.FileSize;
            Dispatcher = data.Dispatcher;

            m_bIsDefaultName = true;
            Entries = new AsyncObservableCollection<DownloadEntryViewModel>();

            ulong fileSize = 0;

            foreach (var dl in data.Entries)
            {
                var entry = new DownloadEntryViewModel(dl, this);
                fileSize += entry.BytesToReceive;

                Entries.Add(entry);
            }

            BytesToReceive = fileSize;
        }

        public void RecalculateFileSize()
        {
            ulong fileSize = 0;
            bool relative = false;

            try
            {
                foreach (var entry in Entries)
                {
                    if (entry.IsFileSizeRelative)
                        relative = true;

                    fileSize += (ulong)entry.Data.FileSize;
                }
            }
            catch
            { }

            string strSize = string.Empty;

            if (relative)
                strSize = "~";

            strSize += Logic.FileSize.GetSizeString(fileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;
        }

        public void UpdateStates()
        {
            Dictionary<string, StatusViewModel> states = new Dictionary<string, StatusViewModel>();
            Dictionary<string, Hoster> hoster = new Dictionary<string, Hoster>();
            Dictionary<FileDownloadState, FileDownloadState> downloadStates = new Dictionary<FileDownloadState, FileDownloadState>();
            ulong fileSize = 0;
            bool relative = false;
            int onlineCount = 0;
            int completeCount = 0;
            bool isCancelled = true;
            bool isFinished = true;
            bool isPaused = true;
            bool isFailed = true;

            foreach (var entry in Entries)
            {
                if (entry.Data.FileState == FileState.FileExists)
                    continue;

                fileSize += (ulong)entry.Data.FileSize;

                if (entry.Data.FileState == FileState.FileAvailable)
                    onlineCount += 1;

                if (entry.Data.FileState == FileState.FileDownloadFinished)
                {
                    completeCount += 1;

                    if (completeCount == Entries.Count)
                    {
                        states.Clear();
                        states.Add(entry.Data.FileState.Description, entry.Status.First());
                    }

                    isCancelled = false;
                    isFailed = false;
                    isPaused = false;
                    continue;
                }

                try
                {
                    if (!states.ContainsKey(entry.Data.FileState.Description))
                        states.Add(entry.Data.FileState.Description, entry.Status.First());
                }
                catch
                { }

                if (!hoster.ContainsKey(entry.Data.Hoster.Name))
                    hoster.Add(entry.Data.Hoster.Name, entry.Data.Hoster);

                if (entry.IsFileSizeRelative)
                    relative = true;

                if (!downloadStates.ContainsKey(entry.DownloadState))
                    downloadStates.Add(entry.DownloadState, entry.DownloadState);

                if (entry.DownloadState != FileDownloadState.Cancelled)
                    isCancelled = false;

                if (entry.DownloadState != FileDownloadState.Failed)
                    isFailed = false;

                if (entry.DownloadState != FileDownloadState.Paused)
                    isPaused = false;

                if (entry.DownloadState != FileDownloadState.Finished)
                    isFinished = false;
            }

            string strSize = string.Empty;

            if (relative)
                strSize = "~";

            strSize += Logic.FileSize.GetSizeString(fileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;

            Hoster.Clear();
            Status.Clear();

            foreach (var entry in hoster)
                Hoster.Add(new HosterViewModel(entry.Value, Images.GetHosterIcon(entry.Value)));

            foreach (var state in states)
                Status.Add(state.Value);

            if (isCancelled)
                DownloadState = FileDownloadState.Cancelled;
            else if (isFailed)
                DownloadState = FileDownloadState.Failed;
            else if (isPaused)
                DownloadState = FileDownloadState.Paused;
            else if (isFinished)
                DownloadState = FileDownloadState.Finished;
            else if (downloadStates.ContainsKey(FileDownloadState.Downloading))
                DownloadState = FileDownloadState.Downloading;
            else if (downloadStates.ContainsKey(FileDownloadState.Starting))
                DownloadState = FileDownloadState.Starting;
            else if (downloadStates.ContainsKey(FileDownloadState.Resolving))
                DownloadState = FileDownloadState.Resolving;
            else if (downloadStates.ContainsKey(FileDownloadState.Unpack))
                DownloadState = FileDownloadState.Unpack;

            IsDownloading = DownloadState != FileDownloadState.Cancelled && DownloadState != FileDownloadState.Failed && 
                DownloadState != FileDownloadState.Finished && DownloadState != FileDownloadState.Paused;

            if (!IsDownloading)
            {
                CurrentSpeedValue = 0;
                CurrentSpeed = Logic.FileSize.GetSizeString(CurrentSpeedValue, App.Downloader.Settings.FileSizeFormat) + "/s";
            }

            OnPropertyChanged(nameof(StatusText));
            OnPropertyChanged(nameof(HosterText));
        }

        public void UpdateDownloadState()
        {
            CurrentSpeed = string.Empty;
            RemainTimeString = string.Empty;

            Parent.ClearDownloadProperties();
        }

        public void UpdateDownloadProperties()
        {
            ReceivedSize = 0;
            CurrentSpeedValue = 0;
            BytesToReceive = 0;
            Duration = TimeSpan.Zero;

            TimeSpan addTime = TimeSpan.Zero;
            bool isDownloading = false;

            foreach (var entry in Entries)
            {
                if (entry.Data.FileState == FileState.FileExists)
                    continue;

                BytesToReceive += entry.BytesToReceive;
                ReceivedSize += entry.ReceivedBytes;

                if (entry.IsDownloading)
                {
                    CurrentSpeedValue += entry.DownloadSpeed;
                    isDownloading = true;
                }
                else if (!isDownloading)
                    isDownloading = false;
            }

            IsDownloading = isDownloading;
            string timeString;

            if (CurrentSpeedValue > 0)
            {
                double d = ((double)BytesToReceive - ReceivedSize) / CurrentSpeedValue;
                int timeRemaining = (int)d;
                Duration = new TimeSpan(0, 0, 0, timeRemaining, (int)Math.Round((d - timeRemaining) * 1000));
                timeString = TimeSpanConverter.GetString(Duration, App.Downloader.Settings.ShowMilliseconds);
            }
            else
                timeString = "-";

            var localReceivedSize = ReceivedSize;
            var localBytesToReceive = BytesToReceive;
            var localSpeedValue = CurrentSpeedValue;

            App.Current.Dispatcher.Invoke(() =>
            {
                Percentage = Math.Round((double)localReceivedSize / localBytesToReceive * 100, 2, MidpointRounding.AwayFromZero);

               // Debug.WriteLine($"{Percentage} || {localReceivedSize} / {localBytesToReceive} * 100 == {(double)localReceivedSize / localBytesToReceive * 100}");

                if (localSpeedValue != 0)
                    CurrentSpeed = Logic.FileSize.GetSizeString(localSpeedValue, App.Downloader.Settings.FileSizeFormat) + "/s";

                if (!string.IsNullOrEmpty(timeString))
                    RemainTimeString = timeString;
            });

            Parent.UpdateDownloadProperties();
        }

        public void DownloadFailed()
        {
            Parent.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Error;
        }

        public void DownloadPause()
        {
            Parent.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Paused;

            foreach (var entry in Entries)
                entry.DownloadPause();
        }

        public void DownloadStart()
        {
            Parent.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Normal;
        }
    }
}
