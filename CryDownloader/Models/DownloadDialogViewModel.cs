﻿using CryDownloader.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader
{
    class DownloadDialogViewModel : ViewModelBase
    {
        private ulong m_updateSize;
        private ulong m_fileCount;
        private ulong m_currentFileSize;
        private ulong m_currentFileProcess;
        private string m_currentFileName;
        private bool m_isUpdated;
        private bool m_isUnpacking;

        public ulong UpdateSize
        {
            get => m_updateSize;
            set
            {
                m_updateSize = value;
                OnPropertyChanged();
            }
        }

        public ulong FileCount
        {
            get => m_fileCount;
            set
            {
                m_fileCount = value;
                OnPropertyChanged();
            }
        }

        public ulong CurrentFileSize
        {
            get => m_currentFileSize;
            set
            {
                m_currentFileSize = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ProgressPercentage));
            }
        }

        public ulong CurrentFileProcess
        {
            get => m_currentFileProcess;
            set
            {
                m_currentFileProcess = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ProgressPercentage));
            }
        }

        public double ProgressPercentage => Math.Round((double)CurrentFileProcess / CurrentFileSize * 100, 2, MidpointRounding.AwayFromZero);

        public string CurrentFileName
        {
            get => m_currentFileName;
            set
            {
                m_currentFileName = value;
                OnPropertyChanged();
            }
        }

        public bool IsUpdated
        {
            get => m_isUpdated;
            set
            {
                m_isUpdated = value;
                OnPropertyChanged();
            }
        }

        public bool IsUnpacking 
        { 
            get => m_isUnpacking; 
            set
            {
                m_isUnpacking = value;
                OnPropertyChanged();
            } 
        }

        public bool CancelUpdate { get; set; }

        public TaskCompletionSource<bool> Updated { get; }



        public DownloadDialogViewModel(UpdatePackage updatePackage)
        {
            Updated = new TaskCompletionSource<bool>();

            updatePackage.StartUpdate += UpdatePackage_StartUpdate;
            updatePackage.UpdateFinished += UpdatePackage_UpdateFinished;
            updatePackage.UpdateProcessChanged += UpdatePackage_UpdateProcessChanged;
            updatePackage.FileUnpack += UpdatePackage_FileUnpack;
            updatePackage.UnpackProcessChanged += UpdatePackage_UnpackProcessChanged;
        }


        private void UpdatePackage_UnpackProcessChanged(object sender, UpdateProcessChangedEventArgs e)
        {
            CurrentFileProcess = e.ProcessedBytes;
            e.Cancel = CancelUpdate;
        }

        private void UpdatePackage_FileUnpack(object sender, FileUnpackEventArgs e)
        {
            IsUnpacking = true;
            CurrentFileName = e.Filename;
            CurrentFileSize = e.FileSize;
        }

        private void UpdatePackage_UpdateProcessChanged(object sender, UpdateProcessChangedEventArgs e)
        {
            CurrentFileProcess = e.ProcessedBytes;
            e.Cancel = CancelUpdate;
        }

        private void UpdatePackage_UpdateFinished(object sender, UpdateFinishedEventArgs e)
        {
            IsUnpacking = false;
            IsUpdated = !e.Cancelled;
            Updated.SetResult(!e.Cancelled);
        }

        private void UpdatePackage_StartUpdate(object sender, StartUpdateEventArgs e)
        {
            CurrentFileName = "update package";
            UpdateSize = CurrentFileSize = e.UpdateSize;
        }
    }
}
