﻿using CryDownloader.Plugin;
using System.Windows.Media;

namespace CryDownloader
{
    class StatusViewModel
    {
        public Plugin.IFileState FileState { get; }

        public ImageSource Image { get; }

        public string Description { get; }

        public StatusViewModel(ImageSource icon, IFileState fileState)
        {
            FileState = fileState;
            Image = icon;
            Description = fileState.Description;
        }
    }
}
