﻿using CryDownloader.Logic;
using CryDownloader.Plugin;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CryDownloader
{
    class SettingViewModel : ViewModelBase
    {
        public ObservableCollection<AccentViewModel> Accents { get; }
        public ObservableCollection<StyleViewModel> Styles { get; }
        public ObservableCollection<HosterPluginViewModel> Hosters { get; }
        public ObservableCollection<PluginViewModel<CaptchaType>> Captchas { get; }
        public ObservableCollection<LanguageViewModel> Languages { get; }
        public ObservableCollection<FileSizeFormatType> FileSizeFormatTypes { get; }

        public LanguageViewModel CurrentLanguage
        {
            get => m_currentLanguage;
            set
            {
                m_currentLanguage = value;
                OnPropertyChanged();
            }
        }

        public HosterPluginViewModel SelectedHoster
        {
            get => m_selectedHoster;
            set
            {
                m_selectedHoster = value;
                OnPropertyChanged();
            }
        }

        public PluginViewModel<CaptchaType> SelectedCaptcha
        {
            get => m_selectedCaptcha;
            set
            {
                m_selectedCaptcha = value;
                OnPropertyChanged();
            }
        }

        public AccentViewModel CurrentAccent
        {
            get => m_currentAccent;
            set
            {
                m_currentAccent = value;
                OnPropertyChanged();
            }
        }

        public StyleViewModel CurrentStyle
        {
            get => m_currentStyle;
            set
            {
                m_currentStyle = value;
                OnPropertyChanged();
            }
        }

        public bool UseSystemSetting
        {
            get => m_useSystemSetting;
            set
            {
                m_useSystemSetting = value;
                OnPropertyChanged();
            }
        }

        public int ScanCount 
        { 
            get => m_scanCount; 
            set
            {
                m_scanCount = value;
                OnPropertyChanged();
            }
        }

        public int RetryCount
        {
            get => m_retryCount;
            set
            {
                m_retryCount = value;
                OnPropertyChanged();
            }
        }

        public ulong MaxDownloadSpeed
        {
            get => m_maxDownloadSpeed;
            set
            {
                m_maxDownloadSpeed = value;
                OnPropertyChanged();
            }
        }

        public string DownloadDirectory
        {
            get => m_downloadDirectory;
            set
            {
                m_downloadDirectory = value;
                OnPropertyChanged();
            }
        }

        public int DownloadCount
        {
            get => m_downloadCount;
            set
            {
                m_downloadCount = value;
                OnPropertyChanged();
            }
        }

        public int DownloadCountPerHost
        {
            get => m_downloadCountPerHost;
            set
            {
                m_downloadCountPerHost = value;
                OnPropertyChanged();
            }
        }

        public bool ShowMiliseconds
        {
            get => m_showMiliseconds;
            set
            {
                m_showMiliseconds = value;
                OnPropertyChanged();
            }
        }

        public bool UseSystemIcons
        {
            get => m_useSystemIcons;
            set
            {
                m_useSystemIcons = value;
                OnPropertyChanged();
            }
        }

        public FileSizeFormatType CurrentFileSizeFormat
        {
            get => m_currentFileSizeFormat;
            set
            {
                m_currentFileSizeFormat = value;
                OnPropertyChanged();
            }
        }

        public bool ShowNotifications
        {
            get => m_showNotifications;
            set
            {
                m_showNotifications = value;
                OnPropertyChanged();
            }
        }

        public bool LoadingEntry
        {
            get => m_loadingEntry;
            set
            {
                m_loadingEntry = value;
                OnPropertyChanged();
            }
        }

        private AccentViewModel m_currentAccent;
        private StyleViewModel m_currentStyle;
        private bool m_useSystemSetting;
        private LanguageViewModel m_currentLanguage;
        private HosterPluginViewModel m_selectedHoster;
        private PluginViewModel<CaptchaType> m_selectedCaptcha;
        private int m_retryCount;
        private ulong m_maxDownloadSpeed;
        private string m_downloadDirectory;
        private int m_downloadCount;
        private int m_downloadCountPerHost;
        private bool m_showMiliseconds;
        private bool m_useSystemIcons;
        private FileSizeFormatType m_currentFileSizeFormat;
        private bool m_showNotifications;
        private readonly Logic.Application m_application;
        private bool m_loadingEntry;
        private int m_scanCount;

        public SettingViewModel(Logic.Application application)
        {
            m_application = application;
            Accents = new ObservableCollection<AccentViewModel>();
            Styles = new ObservableCollection<StyleViewModel>();
            Hosters = new ObservableCollection<HosterPluginViewModel>();
            Languages = new ObservableCollection<LanguageViewModel>();
            Captchas = new ObservableCollection<PluginViewModel<CaptchaType>>();

            FileSizeFormatTypes = new ObservableCollection<FileSizeFormatType>()
            {
                FileSizeFormatType.Binary,
                FileSizeFormatType.Decimal
            };

            foreach (var accent in ControlzEx.Theming.ThemeManager.Current.ColorSchemes)
                Accents.Add(new AccentViewModel(accent));

            foreach (var theme in ControlzEx.Theming.ThemeManager.Current.BaseColors)
            {
                if (theme != App.SystemThemeKey)
                    Styles.Add(new StyleViewModel(theme));
            }

            foreach (var plugin in application.HosterManager.Plugins)
                Hosters.Add(new HosterPluginViewModel(plugin.Value));

            foreach (var plugin in application.CaptchaManager.Plugins)
                Captchas.Add(new PluginViewModel<CaptchaType>(plugin.Value));

            LoadLanguages();
            ResetSettings();
        }

        public void SaveSettings()
        {
            App.ChangeTheme(UseSystemSetting, CurrentStyle.Data, CurrentAccent.Data);

            if (!UseSystemSetting)
            {
                App.Downloader.Settings.Accent = CurrentAccent.Data;
                App.Downloader.Settings.Style = CurrentStyle.Data;
            }

            if (CurrentLanguage != null)
                m_application.Settings.Language = CurrentLanguage.Data.Name;

            m_application.Settings.UseSystemStyle = UseSystemSetting;
            m_application.Settings.ShowMilliseconds = ShowMiliseconds;
            m_application.Settings.FileSizeFormat = CurrentFileSizeFormat;
            m_application.Settings.RetryCount = RetryCount;
            m_application.Settings.MaxDownloadSpeed = MaxDownloadSpeed;
            m_application.Settings.DownloadDirectory = DownloadDirectory;
            m_application.Settings.DownloadCount = DownloadCount;
            m_application.Settings.DownloadCountPerHost = DownloadCountPerHost;
            m_application.Settings.PreferApplicationFileIcons = !UseSystemIcons;
            m_application.Settings.ShowNotifications = ShowNotifications;
            m_application.Settings.LoadingEntry = LoadingEntry;
            m_application.Settings.ScanCount = ScanCount;

            CultureInfo.CurrentCulture = CurrentLanguage.Data;
            CultureInfo.CurrentUICulture = CurrentLanguage.Data;
            CultureInfo.DefaultThreadCurrentCulture = CurrentLanguage.Data;
            CultureInfo.DefaultThreadCurrentUICulture = CurrentLanguage.Data;

            m_application.SaveSettings();
        }

        public void ResetSettings()
        {
            foreach (var lang in Languages)
            {
                if (lang.Data.Name == m_application.Settings.Language)
                    CurrentLanguage = lang;
            }

            UseSystemSetting = App.Downloader.Settings.UseSystemStyle;
            RetryCount = m_application.Settings.RetryCount;
            MaxDownloadSpeed = m_application.Settings.MaxDownloadSpeed;
            DownloadDirectory = m_application.Settings.DownloadDirectory;
            DownloadCount = m_application.Settings.DownloadCount;
            DownloadCountPerHost = m_application.Settings.DownloadCountPerHost;
            ShowMiliseconds = m_application.Settings.ShowMilliseconds;
            UseSystemIcons = !m_application.Settings.PreferApplicationFileIcons;
            CurrentFileSizeFormat = m_application.Settings.FileSizeFormat;
            ShowNotifications = m_application.Settings.ShowNotifications;
            LoadingEntry = m_application.Settings.LoadingEntry;
            ScanCount = m_application.Settings.ScanCount;

            CultureInfo.CurrentCulture = CurrentLanguage.Data;
            CultureInfo.CurrentUICulture = CurrentLanguage.Data;
            CultureInfo.DefaultThreadCurrentCulture = CurrentLanguage.Data;
            CultureInfo.DefaultThreadCurrentUICulture = CurrentLanguage.Data;

            if (UseSystemSetting)
                return;

            var appStyle = ControlzEx.Theming.ThemeManager.Current.DetectTheme();

            if (appStyle == null)
                return;

            foreach (var accent in Accents)
            {
                if (accent.Data == appStyle.ColorScheme)
                    CurrentAccent = accent;
            }

            foreach (var style in Styles)
            {
                if (style.Data == appStyle.BaseColorScheme)
                    CurrentStyle = style;
            }
        }

        public void ShowPluginControl(Panel element)
        {
            element.Children.Clear();

            if (SelectedHoster.Data.SettingControl != null)
            {
                SelectedHoster.Data.SettingControl.MaxHeight += 25;
                SelectedHoster.Data.SettingControl.MinHeight += 25;

                element.Children.Add(SelectedHoster.Data.SettingControl);
            }

            element.UpdateLayout();
        }

        private void LoadLanguages()
        {
            Languages.Clear();

            var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);

            foreach (var culture in cultures)
            {
                try
                {
                    ResourceSet rs = Language.ResourceManager.GetResourceSet(culture, true, false);

                    if (rs != null)
                    {
                        if (culture.Name.Length != 0)
                            Languages.Add(new LanguageViewModel(culture));
                    }
                }
                catch (CultureNotFoundException)
                {
                    Debug.WriteLine(culture + " is not available on the machine or is an invalid culture identifier.");
                }
            }
        }
    }
}
