﻿

namespace CryDownloader
{
    class StyleViewModel
    {
        public string Data { get; }

        public StyleViewModel(string data)
        {
            Data = data;
        }

        public override string ToString()
        {
            return Data;
        }
    }
}
