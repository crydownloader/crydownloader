﻿using CryDownloader.Plugin;
using System.Diagnostics;

namespace CryDownloader
{
    class PluginViewModel<T> : ViewModelBase where T : IPlugin
    {
        public T Data { get; }

        public string Name => Data.Name;

        public string Company { get; }

        public string Version { get; }

        public string Description { get; }

        public PluginViewModel(T data)
        {
            Data = data;

            var versionInfo = FileVersionInfo.GetVersionInfo(data.GetType().Assembly.Location);

            Company = versionInfo.CompanyName;
            Version = versionInfo.FileVersion;
            Description = versionInfo.Comments;
        }
    }
}
