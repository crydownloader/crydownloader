﻿using CryDownloader.Logic;
using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;

namespace CryDownloader
{
    class ScannedLinkContainerViewModel : EntryViewModel, IDownloadContainerView
    {
        private bool m_bIsDefaultName;

        public Logic.DownloadContainer Data { get; }

        public override ImageSource Icon { get => Images.ContainerIcon; protected set => throw new NotSupportedException(); }

        public override string Name
        {
            get => Data.Name;
            set
            {
                string oldName = Data.Name;

                Data.Name = value;

                if (m_bIsDefaultName)
                    DownloadPath = DownloadPath.Replace(oldName, Data.Name);
                else
                {
                    DirectoryInfo info = new DirectoryInfo(DownloadPath);
                    m_bIsDefaultName = info.Name == Name;
                }

                OnPropertyChanged();
            }
        }

        public override string DownloadUrl => Data.Url?.ToString();

        public override string DownloadPath
        {
            get => Data.Directory;
            set
            {
                DirectoryInfo info = new DirectoryInfo(value);

                if (info.Name == Name)
                {
                    Data.Directory = value;
                    m_bIsDefaultName = true;
                }
                else
                {
                    m_bIsDefaultName = false;
                    Data.Directory = value;
                }

                foreach (var entry in Entries)
                {
                    if (entry.IsDefault)
                        entry.DownloadPath = Path.Combine(DownloadPath, entry.Name);
                }

                OnPropertyChanged();
            }
        }

        public override AsyncObservableCollection<StatusViewModel> Status { get; }

        public override AsyncObservableCollection<HosterViewModel> Hoster { get; }

        public override string FileSize
        {
            get => m_fileSizeString;
            protected set
            {
                m_fileSizeString = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<ScannedLinkEntryViewModel> Entries { get; }

        public int EntryCount => Entries.Count;

        public int OnlineCount
        {
            get => m_onlineCount;
            private set
            {
                m_onlineCount = value;

                OnPropertyChanged();
                OnPropertyChanged(nameof(EntryCountState));
            }
        }

        public string EntryCountState
        {
            get
            {
                if (OnlineCount == EntryCount)
                    return $"[{EntryCount}]";

                return $"[{OnlineCount}/{EntryCount}]";
            }
        }

        public bool IsScanning
        {
            get => m_isScanning;
            set
            {
                m_isScanning = value;
                OnPropertyChanged();
            }
        }

        public Dispatcher Dispatcher { get; }

        private ulong m_fileSize;
        private string m_fileSizeString;
        private int m_onlineCount;
        private bool m_isScanning;

        public ScannedLinkContainerViewModel(DownloadContainer data, Dispatcher dispatcher)
        {
            Dispatcher = dispatcher;
            Data = data;
            Entries = new AsyncObservableCollection<ScannedLinkEntryViewModel>();
            Hoster = new AsyncObservableCollection<HosterViewModel>();
            Status = new AsyncObservableCollection<StatusViewModel>();
            IsScanning = true;
            m_fileSize = 0;
        }

        public void Load()
        {
            bool relative = false;

            foreach (var entry in Data)
            {
                m_fileSize += (ulong)entry.FileSize;

                var entryViewModel = new ScannedLinkEntryViewModel(entry, this);

                if (entryViewModel.IsFileSizeRelative)
                    relative = true;

                lock (Entries)
                    Entries.Add(entryViewModel);
            }

            string strSize = string.Empty;

            if (relative)
                strSize = "~";

            strSize += Logic.FileSize.GetSizeString(m_fileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;

            UpdateStates();

            m_bIsDefaultName = true;
        }

        public void RecalculateFileSize()
        {
            m_fileSize = 0;
            bool relative = false;

            try
            {
                lock (Entries)
                {
                    foreach (var entry in Entries)
                    {
                        if (entry.IsFileSizeRelative)
                            relative = true;

                        m_fileSize += (ulong)entry.Data.FileSize;
                    }
                }
            }
            catch
            { }

            string strSize = string.Empty;

            if (relative)
                strSize = "~";

            strSize += Logic.FileSize.GetSizeString(m_fileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;
        }

        public void ChoosePath()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog
            {
                SelectedPath = App.Downloader.Settings.DownloadDirectory,
                ShowNewFolderButton = true,
                Description = "Select the folder in which the files from this container should be saved."
            };

            if (dialog.ShowDialog() == DialogResult.OK)
                DownloadPath = dialog.SelectedPath;
        }

        public bool AddEntry(string filename, DownloadEntry entry, bool loadingEntry = true)
        {
            Logic.Application app = (Logic.Application)entry.Hoster.Application;

            if (!app.IsValidPath(entry.FileName))
                entry.FileName = app.RemoveIllegalCharacters(entry.FileName);

            if (TryGetEntry(entry.Url, out var existsEntry))
            {
                if (existsEntry.Url == entry.Url)
                    return false;

                FileInfo fileInfo = new FileInfo(filename);

                if (entry.IsDouble)
                {
                    int idx = filename.LastIndexOf("_");

                    string str = filename.Substring(idx + 1);

                    int number = int.Parse(str) + 1;

                    filename = fileInfo.Name.Replace(fileInfo.Extension, $"_{number}") + fileInfo.Extension;
                }
                else
                    filename = fileInfo.Name.Replace(fileInfo.Extension, "_2") + fileInfo.Extension;

                entry.FileName = filename;
                entry.IsDouble = true;

                return AddEntry(filename, entry, loadingEntry);
            }

            entry.DownloadPath = Path.Combine(Data.Directory, entry.FileName);

            Data.Add(entry);

            Dispatcher.Invoke(() =>
            {
                lock (Entries)
                    Entries.Add(new ScannedLinkEntryViewModel(entry, this));
            });

            Update();
            return true;
        }

        public void Remove(ScannedLinkEntryViewModel entry)
        {
            Dispatcher.Invoke(() => {
                lock (Entries)
                    Entries.Remove(entry);
            });
            Update();
        }

        public void DetachEntry()
        {
            lock (Entries)
            {
                foreach (var entry in Entries)
                    entry.DetachDownloadEntry();
            }
        }

        public void UpdateStates()
        {
            Dictionary<string, StatusViewModel> states = new Dictionary<string, StatusViewModel>();
            Dictionary<string, Hoster> hoster = new Dictionary<string, Hoster>();
            m_fileSize = 0;
            bool relative = false;
            int onlineCount = 0;

            lock (Entries)
            {
                foreach (var entry in Entries)
                {
                    m_fileSize += (ulong)entry.Data.FileSize;

                    if (entry.Data.FileState == FileState.FileAvailable)
                        onlineCount += 1;

                    if (!states.ContainsKey(entry.Data.FileState.Description) && entry.Status.Count > 0)
                    {
                        try
                        {
                            states.Add(entry.Data.FileState.Description, entry.Status.First());
                        }
                        catch(Exception)
                        { }
                    }

                    if (!hoster.ContainsKey(entry.Data.Hoster.Name))
                        hoster.Add(entry.Data.Hoster.Name, entry.Data.Hoster);

                    if (entry.IsFileSizeRelative)
                        relative = true;
                }
            }

            string strSize = string.Empty;

            if (relative)
                strSize = "~";

            strSize += Logic.FileSize.GetSizeString(m_fileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;
            OnlineCount = onlineCount;

            Hoster.Clear();
            Status.Clear();

            foreach (var entry in hoster)
                Hoster.Add(new HosterViewModel(entry.Value, Images.GetHosterIcon(entry.Value)));

            foreach (var state in states)
                Status.Add(state.Value);

            OnPropertyChanged(nameof(StatusText));
            OnPropertyChanged(nameof(HosterText));
        }

        private void SetNameTextBlockSize(TextBlock containerCount, TextBlock containerName)
        {
            if (containerCount.Visibility != System.Windows.Visibility.Visible)
                return;

            var formattedText = new FormattedText(EntryCountState, CultureInfo.CurrentCulture, containerCount.FlowDirection,
                new Typeface(containerCount.FontFamily, containerCount.FontStyle, containerCount.FontWeight, containerCount.FontStretch),
                containerCount.FontSize, containerCount.Foreground, new NumberSubstitution(), 1);

            double containerCountWidth = formattedText.Width + containerCount.Margin.Left + containerCount.Margin.Right;
            double containerNameWidth = containerName.ActualWidth + containerName.Margin.Left + containerName.Margin.Right;

            if (containerName.Parent is DockPanel dockPanel)
            {
                var containerNamePos = containerName.TransformToAncestor(dockPanel).Transform(new System.Windows.Point(0, 0));
                double maxWidth = dockPanel.ActualWidth;

                if (containerNamePos.X + containerNameWidth + containerCountWidth > maxWidth)
                {
                    double newWidth = maxWidth - (containerNamePos.X + containerCountWidth);

                    if (newWidth < 0)
                        containerName.Width = 0.0;
                    else
                        containerName.Width = newWidth;
                }
            }
        }

        private void Update()
        {
            UpdateStates();
            OnPropertyChanged("EntryCount");
        }

        private bool TryGetEntry(Uri fileName, out DownloadEntry entry)
        {
            DownloadEntry result = null;

            lock (Entries)
            {
                Parallel.ForEach(Entries, new ParallelOptions(), (local) =>
                {
                    if (local.Data.Url == fileName)
                        result = local.Data;
                });
            }

            entry = result;
            return result != null;
        }
    }
}
