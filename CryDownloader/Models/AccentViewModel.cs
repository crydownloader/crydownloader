﻿
namespace CryDownloader
{
    class AccentViewModel
    {
        public string Data { get; }

        public AccentViewModel(string data)
        {
            Data = data;
        }

        public override string ToString()
        {
            return Data;
        }
    }
}
