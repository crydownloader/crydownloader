﻿using CryDownloader.Plugin;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Media;

namespace CryDownloader
{
    class ScannedLinkEntryViewModel : EntryViewModel
    {
        private string m_fileSize;
        private ImageSource m_icon;

        public DownloadEntry Data { get; }

        public override ImageSource Icon
        {
            get => m_icon;
            protected set => m_icon = value;
        }

        public override AsyncObservableCollection<StatusViewModel> Status { get; }

        public override AsyncObservableCollection<HosterViewModel> Hoster { get; }

        public override string Name
        {
            get => Data.FileName;
            set
            {
                string oldName = Data.FileName;

                Data.FileName = value;

                if (IsDefault)
                    DownloadPath = DownloadPath.Replace(oldName, Data.FileName);
                else
                {
                    DirectoryInfo info = new DirectoryInfo(DownloadPath);
                    IsDefault = info.Name == Name;
                }

                OnPropertyChanged();
            }
        }

        public override string DownloadPath
        {
            get => Data.DownloadPath;
            set
            {
                DirectoryInfo info = new DirectoryInfo(value);

                if (info.Name == Name)
                {
                    Data.DownloadPath = value;
                    IsDefault = true;
                }
                else
                {
                    IsDefault = false;
                    Data.DownloadPath = value;
                }

                OnPropertyChanged();
            }
        }

        public override string FileSize
        {
            get => m_fileSize;
            protected set
            {
                m_fileSize = value;
                OnPropertyChanged();
            }
        }

        public int EntryCountState { get; }

        public int EntryCount { get; }

        public int OnlineCount { get; }

        public bool IsScanning { get; }

        public bool IsDefault { get; private set; }

        public bool IsFileSizeRelative { get; private set; }

        public override string DownloadUrl => Data.Url.ToString();

        public ScannedLinkContainerViewModel Parent { get; }

        public ScannedLinkEntryViewModel(DownloadEntry data, ScannedLinkContainerViewModel parent)
        {
            Parent = parent;
            Data = data;

            string strSize = string.Empty;

            if (Data.FileSizeType == FileSizeType.Relative)
            {
                IsFileSizeRelative = true;
                strSize = "~";
            }

            strSize += Logic.FileSize.GetSizeString((ulong)Data.FileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;

            AsyncObservableCollection<HosterViewModel> hoster = null;
            AsyncObservableCollection<StatusViewModel> status = null;

            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                hoster = new AsyncObservableCollection<HosterViewModel>() { new HosterViewModel(data.Hoster, Images.GetHosterIcon(data.Hoster)) };
                status = new AsyncObservableCollection<StatusViewModel>();
            });

            Hoster = hoster;
            Status = status;

            data.FileSizeChanged += Data_FileSizeChanged;
            data.FileStateChanged += Data_StateChanged;

            Icon = Images.GetFileIcon(Data.FileName);

            Status.Add(new StatusViewModel(Images.GetFileStateIcon(data.FileState.Icon), Data.FileState));

            IsDefault = true;
        }

        public void DetachDownloadEntry()
        {
            Data.FileSizeChanged -= Data_FileSizeChanged;
            Data.FileStateChanged -= Data_StateChanged;
        }

        private void Data_StateChanged(object sender, FileStateChangedEventArgs e)
        {
            var current = Status.First();

            Status.Clear();

            ImageSource icon;

            if (Data.FileState.Icon == FileStateIcon.Current)
                icon = current.Image;
            else
                icon = Images.GetFileStateIcon(Data.FileState.Icon);

            Status.Add(new StatusViewModel(icon, Data.FileState));

            Parent.UpdateStates();

            OnPropertyChanged(nameof(StatusText));
        }

        private void Data_FileSizeChanged(object sender, System.EventArgs e)
        {
            string strSize = string.Empty;

            if (Data.FileSizeType == FileSizeType.Relative)
            {
                IsFileSizeRelative = true;
                strSize = "~";
            }

            strSize += Logic.FileSize.GetSizeString((ulong)Data.FileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;
            Parent.RecalculateFileSize();
        }

        private void SetNameTextBlockSize(TextBlock containerCount, TextBlock containerName)
        {
            // dummy
        }

        public void ChoosePath()
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                InitialDirectory = App.Downloader.Settings.DownloadDirectory,
                Title = Language.ChooseFile,
                Filter = "*.*|" + Language.AllFiles,
                FilterIndex = 0,
                FileName = new FileInfo(DownloadPath).Name
            };

            if (dialog.ShowDialog() == DialogResult.OK)
                DownloadPath = dialog.FileName;
        }
    }
}
