﻿using System.Globalization;

namespace CryDownloader
{
    class LanguageViewModel : ViewModelBase
    {
        private CultureInfo m_data;

        public CultureInfo Data
        {
            get => m_data;
            set
            {
                m_data = value;
                OnPropertyChanged("Name");
            }
        }
        public string Name => Data.DisplayName;
        public LanguageViewModel(CultureInfo data)
        {
            Data = data;
        }
    }
}
