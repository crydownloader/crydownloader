﻿using CryDownloader.Logic;
using CryDownloader.Plugin;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
// TODO:
//using Windows.Data.Xml.Dom;
//using Windows.UI.Notifications;

namespace CryDownloader
{
    class ScanJobViewModel : ViewModelBase, IScanJob
    {
        class LoadJob : IChildScanJob
        {
            public ScanJobViewModel Parent { get; }
            public IContainer Container { get; }
            public DownloadEntry Entry { get; }

            IScanJob IChildScanJob.Parent => Parent;

            public LoadJob(ScanJobViewModel parent, IContainer container, DownloadEntry entry)
            {
                Parent = parent;
                Container = container;
                Entry = entry;
            }

            public void Scan(JobTaskManager taskManager)
            {
                try
                {
                    Entry.Load(Container);
                }
                catch (Exception ex)
                { }

                Parent.RemoveChildJob(this);
            }

            public void Abort()
            { }
        }

        public string Text { get; }
        public UserControl View { get; }
        public int LinkCount
        {
            get
            {
                if (m_links == null || m_loadingState == ScanJobState.Loading)
                    return m_linkCount;

                return m_links.Count;
            }
        }

        public string ContainerName 
        { 
            get => m_containerName; 
            set
            {
                m_containerName = value;
                OnPropertyChanged();
            }
        }

        public string CurrentLink
        {
            get => m_currentLink;
            set
            {
                m_currentLink = value;
                OnPropertyChanged();
            }
        }

        public int FoundLinkCount
        {
            get => m_foundLinkCount;
            set
            {
                m_foundLinkCount = value;
                OnPropertyChanged();
            }
        }

        public bool IsScanning
        {
            get => m_isScanning;
            set
            {
                m_isScanning = value;
                OnPropertyChanged();
            }
        }

        public string Duration
        {
            get => m_duration;
            set
            {
                m_duration = value;
                OnPropertyChanged();
            }
        }

        public ScanJobState State
        {
            get => m_state;
            set
            {
                m_state = value;
                OnPropertyChanged();
                OnPropertyChanged("JobState");

                if ((m_state == ScanJobState.Wait || m_state == ScanJobState.Scan || m_state == ScanJobState.Loading) && m_timerTask == null)
                {
                    m_startTime = DateTime.Now;

                    m_timerTask = Task.Run(async () =>
                    {
                        while (IsScanning)
                        {
                            TimeSpan timeSpan = DateTime.Now - m_startTime;

                            string str = string.Empty;

                            if (timeSpan.Hours > 0)
                                str += timeSpan.Hours + " h ";

                            if (timeSpan.Minutes > 0)
                                str += timeSpan.Minutes + " m ";

                            str += timeSpan.Seconds + " s";

                            Duration = str;
                            await Task.Delay(1000);
                        }
                    });
                }
            }
        }

        public ImageSource Icon { get; }

        public object ToolTipContent { get; }

        public string JobState
        {
            get
            {
                switch (State)
                {
                    case ScanJobState.Scan:
                        return Language.CollectingUrls;
                    case ScanJobState.Wait:
                        return Language.WaitCollecting;
                    case ScanJobState.Loading:
                        return Language.ContainerLoading;
                    default:
                        return Language.UnknownJobState;
                }
            }
        }

        public List<IChildScanJob> m_childJobs;
        private List<string> m_links;
        private int m_linkCount;
        private Task m_timerTask;
        private bool m_isScanning;
        private bool m_showSuccessMessage;
        private string m_currentLink;
        private int m_foundLinkCount;
        private string m_duration;
        private bool m_busy;

        private readonly CancellationTokenSource m_cancellationTokenSource;
        private readonly ScanViewModel m_scanViewModel;

        private static List<ScanJobViewModel> m_allCurrentScanJobs = new List<ScanJobViewModel>();
        private static object m_allCurrentScanJobLockObj = new object();
        private DateTime m_startTime;
        private ScanJobState m_state;
        private ScanJobState m_loadingState;

        private bool m_bIsClipboard;
        private string m_containerName;

        public event EventHandler ScanFinished;

        public ScanJobViewModel(string text, ScanViewModel scanViewModel, UserControl view, bool isClipboard, ScanJobState scanJobState)
        {
            m_childJobs = new List<IChildScanJob>();
            Text = text;
            View = view;
            m_loadingState = scanJobState;
            m_bIsClipboard = isClipboard;
            m_scanViewModel = scanViewModel;
            m_cancellationTokenSource = new CancellationTokenSource();
            m_links = App.Downloader.GetUrlsFromText(Text);
            CurrentLink = m_links.First();
            Icon = Images.JobScanIcon;
        }

        public void Scan()
        {
            IsScanning = true;
            App.Downloader.JobTaskManagaer.Add(this);
        }

        void IScanJobBase.Scan(JobTaskManager taskManager)
        {
            if (State != ScanJobState.Wait)
                return;

            lock (m_allCurrentScanJobLockObj)
            {
                m_allCurrentScanJobs.Add(this);
            }

            m_showSuccessMessage = true;
            State = m_loadingState;
            ProcessJob(taskManager);
        }

        void IScanJobBase.Abort()
        {
            InternalAbortLinkScan(true);
        }

        public void AbortLinkScan()
        {
            InternalAbortLinkScan(true);
        }

        private void InternalAbortLinkScan(bool removeFromList)
        {
            if (removeFromList)
            {
                lock (m_allCurrentScanJobLockObj)
                {
                    m_allCurrentScanJobs.Remove(this);
                }
            }

            m_showSuccessMessage = false;
            m_cancellationTokenSource.Cancel();
            State = ScanJobState.None;
        }

        public void AbortAllLinkScans()
        {
            lock (m_allCurrentScanJobLockObj)
            {
                foreach (var job in m_allCurrentScanJobs)
                    job.InternalAbortLinkScan(false);

                m_allCurrentScanJobs.Clear();
            }
        }
        // TODO:
        /*private MyNotification SetupToastNotification()
        {
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastImageAndText04);

            XmlNodeList stringElements = toastXml.GetElementsByTagName("text");

            if (stringElements.Count > 1)
            {
                if (m_bIsClipboard)
                    stringElements[0].AppendChild(toastXml.CreateTextNode(Language.ClipboardResearch));
                else if (m_loadingState == ScanJobState.Loading)
                    stringElements[0].AppendChild(toastXml.CreateTextNode(Language.ContainerLoaded));
                else
                    stringElements[0].AppendChild(toastXml.CreateTextNode(Language.LinkResearch));
            }

            if (stringElements.Count >= 3)
            {
                stringElements[1].AppendChild(toastXml.CreateTextNode($"{Language.ScannedUrls}: {LinkCount}"));
                stringElements[2].AppendChild(toastXml.CreateTextNode($"{Language.UrlFound}: {FoundLinkCount}"));
            }
            else if (stringElements.Count >= 2)
                stringElements[1].AppendChild(toastXml.CreateTextNode($"{Language.UrlFound}: {FoundLinkCount}"));

            string imagePath;

            if (m_loadingState == ScanJobState.Loading)
                imagePath = Path.GetFullPath("toastNotificationImageLoading.png");
            else
                imagePath = Path.GetFullPath("toastNotificationImageScan.png");

            if (!File.Exists(imagePath))
            {
                StreamResourceInfo sri;

                if (m_loadingState == ScanJobState.Loading)
                    sri = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Resources/OpeningContainer.png", UriKind.Absolute));
                else
                    sri = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Resources/MonitorClipboardOn.png", UriKind.Absolute));

                Stream resFilestream = sri.Stream;

                if (resFilestream != null)
                {
                    BinaryReader br = new BinaryReader(resFilestream);
                    FileStream fs = new FileStream(imagePath, FileMode.Create);
                    BinaryWriter bw = new BinaryWriter(fs);
                    byte[] ba = new byte[resFilestream.Length];
                    resFilestream.Read(ba, 0, ba.Length);
                    bw.Write(ba);
                    br.Close();
                    bw.Close();
                    resFilestream.Close();
                }
            }

            if (File.Exists(imagePath))
            {
                XmlNodeList imageElements = toastXml.GetElementsByTagName("image");
                imageElements[0].Attributes.GetNamedItem("src").NodeValue = "file:///" + imagePath;
            }

            return new MyToastNotification(toastXml);
        }

        private MyNotification SetupNotification()
        {
            return null;
        }

        private void ShowMessage()
        {
            MyNotification notification;

            if (App.IsWin10())
                notification = SetupToastNotification();
            else
                notification = SetupNotification();

            notification?.Show(View.Dispatcher);
        }*/

        public void RemoveChildJob(IChildScanJob job)
        {
            lock (m_childJobs)
            {
                m_childJobs.Remove(job);
            }

            if (m_childJobs.Count == 0 && !m_busy)
                Finished();
        }

        public void Finished()
        {
            IsScanning = false;

            lock (m_allCurrentScanJobLockObj)
            {
                if (m_timerTask != null)
                {
                    m_timerTask.Wait();
                    m_timerTask.Dispose();
                    m_timerTask = null;
                }
            }

            m_scanViewModel.RemoveScanJob(View);

            ScanFinished?.Invoke(this, new EventArgs());
        }

        private void ProcessJob(JobTaskManager taskManager)
        {
            if (m_links.Count != 0)
            {
                try
                {
                    m_busy = true;
                    App.Downloader.CreateDownloadEntries(m_links,
                        m_cancellationTokenSource.Token,
                        View.Dispatcher,
                        m_scanViewModel.ScannedLinks,
                        (IContainer container, DownloadEntry entry) => {

                            var j = new LoadJob(this, container, entry);

                            lock (m_childJobs)
                                m_childJobs.Add(j);

                            taskManager.Add(j, true);
                            FoundLinkCount += 1;
                        },
                        (Uri url) => CurrentLink = url.ToString(),
                        (string name, int urlCount) =>
                        {

                            ContainerName = name;
                            m_linkCount = urlCount;
                        });

                    //foreach (var entry in m_scanViewModel.ScannedLinks)
                    //{
                    //    foreach (var link in entry.Entries)
                    //    {
                    //       m_links.RemoveAll((x) => x == link.DownloadUrl.ToString());
                    //    }

                    //}

                    //foreach (var link in m_links)
                    //{
                    //    App.Downloader.LogManager.WriteLog(LogLevel.Hard, $"Url {link} was not added.");
                    //}
                }
                catch (Exception)
                {

                }

                m_busy = false;
            }

            Thread.Sleep(100);

            if (m_childJobs.Count == 0)
            {
                Finished();
            }
        }
    }
}
