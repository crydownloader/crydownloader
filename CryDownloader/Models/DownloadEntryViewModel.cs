﻿using CryDownloader.Plugin;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Media;

namespace CryDownloader
{
    class DownloadEntryViewModel : EntryViewModel
    {
        public DownloadEntry Data { get; }

        public override ImageSource Icon
        {
            get => m_icon;
            protected set => m_icon = value;
        }

        public override AsyncObservableCollection<StatusViewModel> Status { get; }

        public override AsyncObservableCollection<HosterViewModel> Hoster { get; }

        public override string Name
        {
            get => Data.FileName;
            set
            {
                string oldName = Data.FileName;

                Data.FileName = value;

                if (IsDefault)
                    DownloadPath = DownloadPath.Replace(oldName, Data.FileName);
                else
                {
                    DirectoryInfo info = new DirectoryInfo(DownloadPath);
                    IsDefault = info.Name == Name;
                }

                OnPropertyChanged();
            }
        }

        public override string DownloadPath
        {
            get => Data.DownloadPath;
            set
            {
                DirectoryInfo info = new DirectoryInfo(value);

                if (info.Name == Name)
                {
                    Data.DownloadPath = value;
                    IsDefault = true;
                }
                else
                {
                    IsDefault = false;
                    Data.DownloadPath = value;
                }

                OnPropertyChanged();
            }
        }

        public override string DownloadUrl => Data.Url.ToString();

        public override string FileSize { get; protected set; }

        public FileDownloadState DownloadState => Data.FileState.DownloadState;

        public bool IsDefault { get; private set; }

        public double Percentage
        {
            get => m_downloadPercentage;
            private set
            {
                if (double.IsInfinity(value) || double.IsNaN(value))
                    return;

                m_downloadPercentage = value;
                OnPropertyChanged();
            }
        }

        public string CurrentSpeed
        {
            get => m_currentSpeed;
            private set
            {
                m_currentSpeed = value;
                OnPropertyChanged();
            }
        }

        public string RemainTimeString
        {
            get => m_remainTimeString;
            private set
            {
                m_remainTimeString = value;
                OnPropertyChanged();
            }
        }

        public ulong ReceivedBytes { get; private set; }

        public ulong BytesToReceive { get; private set; }

        public ulong DownloadSpeed { get; private set; }

        public TimeSpan Duration { get; private set; }

        public bool IsDownloading { get; set; }

        public bool IsQueued { get; set; }

        public Brush ProgressBarBrush
        {
            get => m_progressBarBrush;
            set
            {
                m_progressBarBrush = value;
                OnPropertyChanged();
            }
        }

        private double m_downloadPercentage;
        private string m_currentSpeed;
        private string m_remainTimeString;
        private ImageSource m_icon;
        private Brush m_progressBarBrush;
        private IApplication m_application;

        public DownloadContainerViewModel Parent { get; }
        public bool IsFileSizeRelative { get; private set; }

        public DownloadEntryViewModel(ScannedLinkEntryViewModel data, DownloadContainerViewModel parent)
        {
            m_application = data.Data.Hoster.Application;
            Parent = parent;
            Data = data.Data;
            Hoster = data.Hoster;
            Status = data.Status;
            FileSize = data.FileSize;
            IsFileSizeRelative = data.IsFileSizeRelative;
            IsDefault = true;
            BytesToReceive = (ulong)data.Data.FileSize;
            Icon = Images.GetFileIcon(Data.FileName);

            data.Data.BeginDownload += (x, y) =>
            {
                Data.FileState = m_application.GetFileState(FileDownloadState.Downloading);
                IsDownloading = true;
            };

            data.Data.CompleteDownload += (x, y) =>
            {
                if (y.Error != null)
                {
                    data.Data.Load(parent.Data);
                    y.Retry = false;
                }
                else if (y.IsCancelled)
                    Data.FileState = m_application.GetFileState(FileDownloadState.Cancelled);
                else
                    Data.FileState = m_application.GetFileState(FileDownloadState.Finished);

                IsDownloading = false;
                RemainTimeString = string.Empty;
                CurrentSpeed = string.Empty;

                Parent.UpdateDownloadState();
            };

            data.Data.DownloadProgressChanged += (x, y) =>
            {
                App.Current.Dispatcher.Invoke(() =>
                {
                    ReceivedBytes = (ulong)y.BytesReceived;
                    Percentage = y.PercentComplete;
                    DownloadSpeed = (ulong)y.CurrentSpeed;
                    BytesToReceive = (ulong)y.TotalBytesToReceive;

                    if (y.CurrentSpeed == 0)
                        CurrentSpeed = string.Empty;
                    else
                        CurrentSpeed = Logic.FileSize.GetSizeString(DownloadSpeed, App.Downloader.Settings.FileSizeFormat) + "/s";

                    if (y.CurrentSpeed > 0)
                    {
                        if (y.Duration == null || !y.Duration.HasValue)
                        {
                            double d = ((double)y.TotalBytesToReceive - y.BytesReceived) / y.CurrentSpeed;
                            int timeRemaining = (int)d;

                            Duration = new TimeSpan(0, 0, 0, timeRemaining, (int)Math.Round((d - timeRemaining) * 1000));
                            RemainTimeString = TimeSpanConverter.GetString(Duration, App.Downloader.Settings.ShowMilliseconds);
                        }
                        else
                        {
                            Duration = y.Duration.Value;
                            RemainTimeString = TimeSpanConverter.GetString(y.Duration.Value, App.Downloader.Settings.ShowMilliseconds);
                        }
                    }

                    Parent.UpdateDownloadProperties();
                });
            };

            data.Data.FileSizeChanged += Data_FileSizeChanged;
            data.Data.FileStateChanged += Data_StateChanged;
        }

        public void DownloadPause()
        {

        }

        private void Data_StateChanged(object sender, FileStateChangedEventArgs e)
        {
            Debug.WriteLine($"State for {Data} changed to {Data.FileState.DownloadState}");

            Parent.Dispatcher.Invoke(() =>
            {
                var current = Status.First();

                Status.Clear();

                ImageSource icon;

                if (Data.FileState.Icon == FileStateIcon.Current)
                    icon = current.Image;
                else
                    icon = Images.GetFileStateIcon(Data.FileState.Icon);

                Status.Add(new StatusViewModel(icon, Data.FileState));

                Parent.UpdateStates();
            });

            OnPropertyChanged(nameof(StatusText));
            OnPropertyChanged(nameof(DownloadState));
        }

        private void Data_FileSizeChanged(object sender, System.EventArgs e)
        {
            string strSize = string.Empty;

            if (Data.FileSizeType == FileSizeType.Relative)
            {
                IsFileSizeRelative = true;
                strSize = "~";
            }

            strSize += Logic.FileSize.GetSizeString((ulong)Data.FileSize, App.Downloader.Settings.FileSizeFormat);
            FileSize = strSize;
            Parent.RecalculateFileSize();
        }
    }
}
