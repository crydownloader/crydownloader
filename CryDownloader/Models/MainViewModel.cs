﻿using CryDownloader.Logic;
using CryDownloader.Plugin;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CryDownloader
{
    class MainViewModel : ViewModelBase
    {
        class ThreadData
        {
            public Thread Thread;
            public bool IsDownloading;
            public int Index;
        }

        public ObservableCollection<DownloadContainerViewModel> Downloads { get; }

        public ObservableCollection<EntryViewModel> SelectedDownloadEntries { get; }

        public SettingViewModel SettingViewModel { get; }

        public ScanViewModel ScanViewModel { get; }

        public double DownloadPercentage
        {
            get => m_downloadPercentage;
            private set
            {
                if (double.IsInfinity(value) || double.IsNaN(value))
                    return;

                m_downloadPercentage = value;
                OnPropertyChanged();
            }
        }

        public double DownloadStatePercentage
        {
            get => m_downloadStatePercentage;
            private set
            {
                if (double.IsInfinity(value) || double.IsNaN(value))
                    return;

                m_downloadStatePercentage = value;
                OnPropertyChanged();
            }
        }

        public string DownloadCurrentSpeed
        {
            get => m_downloadCurrentSpeed;
            private set
            {
                m_downloadCurrentSpeed = value;
                OnPropertyChanged();
            }
        }

        public string DownloadSize
        {
            get => m_downloadSize;
            private set
            {
                m_downloadSize = value;
                OnPropertyChanged();
            }
        }

        public string DownloadReceivedSize
        {
            get => m_downloadReceivedSize;
            private set
            {
                m_downloadReceivedSize = value;
                OnPropertyChanged();
            }
        }

        public string DownloadRemainTime
        {
            get => m_downloadRemainTime;
            private set
            {
                m_downloadRemainTime = value;
                OnPropertyChanged();
            }
        }

        public bool IsDownloading
        {
            get
            {
                bool result = false;

                foreach (var threadData in m_downloadThread)
                    result |= threadData.IsDownloading;

                return result;
            }
        }

        public ImageSource PackageIcon => Images.ContainerIcon;

        public System.Windows.Shell.TaskbarItemProgressState ProgressState
        {
            get => m_progressState;
            set
            {
                m_progressState = value;
                OnPropertyChanged();
            }
        }

        private readonly ThreadData[] m_downloadThread;
        private readonly Queue<DownloadEntryViewModel> m_downloadQueue;
        private readonly ManualResetEvent m_downloadCondition;
        private bool m_bIsRunning;
        private double m_downloadPercentage;
        private double m_downloadStatePercentage;
        private string m_downloadCurrentSpeed;
        private string m_downloadRemainTime;
        private string m_downloadSize;
        private string m_downloadReceivedSize;
        private System.Windows.Shell.TaskbarItemProgressState m_progressState;
        private ClipboardManager m_clipboardManager;
        private MenuItem m_optionPage;
        private MenuItem m_addLinkPage;
        private MenuItem m_downloadPage;

        private Image m_clipboardImage;

        private DownloadView m_downloadView;
        private OptionView m_optionView;
        private AddLinkView m_addLinkView;
        private Flyout m_propertyFlyout;
        private Window m_mainWindow;
        private Logic.Application m_application;
        private IDialogCoordinator m_dialogCoordinator;
        private DownloaderDialog m_currDialog;
        private DateTime m_dialogMouseDownTime;

        public MainViewModel(Logic.Application downloader, IDialogCoordinator instance)
        {
            m_dialogCoordinator = instance;
            m_application = downloader;
            Downloads = new ObservableCollection<DownloadContainerViewModel>();
            SelectedDownloadEntries = new ObservableCollection<EntryViewModel>();
            SettingViewModel = new SettingViewModel(downloader);
            ScanViewModel = new ScanViewModel();

            m_downloadCondition = new ManualResetEvent(false);
            m_downloadQueue = new Queue<DownloadEntryViewModel>();
            m_bIsRunning = true;

            if (SettingViewModel.DownloadCount < 1)
                SettingViewModel.DownloadCount = 1;

            m_downloadThread = new ThreadData[SettingViewModel.DownloadCount];
           
            for (int i = 0; i < m_downloadThread.Length; ++i)
            {
                m_downloadThread[i] = new ThreadData();
                m_downloadThread[i].Index = i;
                m_downloadThread[i].Thread = new Thread(ProcessDownloadThread);
                m_downloadThread[i].Thread.Start(m_downloadThread[i]);
            }

            downloader.OpeningBrowser += Downloader_ResolvingCaptcha;

            Language.Culture = SettingViewModel.CurrentLanguage.Data;
            CultureInfo.CurrentCulture = SettingViewModel.CurrentLanguage.Data;
            CultureInfo.CurrentUICulture = SettingViewModel.CurrentLanguage.Data;
        }

        public bool? SaveContainer(IEnumerable<ScannedLinkContainerViewModel> viewModel)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = ".crydlc";
            dialog.Filter = GetDialogFilter();
            dialog.ValidateNames = true;

            bool? result = dialog.ShowDialog();

            if (result != null && result.Value)
                return m_application.SaveContainer(dialog.FileName, viewModel.Select((x) => x.Data));

            return null;
        }

        public async void LoadContainer()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".crydlc";
            dialog.Filter = GetDialogFilter();
            dialog.ValidateNames = true;
            dialog.CheckFileExists = true;

            bool? result = dialog.ShowDialog();

            if (result != null && result.Value)
            {
                if (result.Value)
                {
                    foreach (var file in dialog.FileNames)
                        ScanViewModel.Scan(file, false, ScanJobState.Loading);
                }
                else
                {
                    MetroWindow metroWindow = (System.Windows.Application.Current.MainWindow as MetroWindow);
                    await metroWindow.ShowMessageAsync(Language.ContainerLoadedError, Language.FailedToLoadContainer);
                }
            }
        }

        public void MoveSelectedEntriesToStart()
        {
            foreach (var entry in SelectedDownloadEntries)
            {
                if (entry is DownloadContainerViewModel container)
                {
                    Downloads.Remove(container);
                    Downloads.Insert(0, container);
                }
                else if (entry is DownloadEntryViewModel downloadEntry)
                {
                    downloadEntry.Parent.Entries.Remove(downloadEntry);
                    downloadEntry.Parent.Entries.Insert(0, downloadEntry);
                }
            }
        }

        public void MoveSelectedEntriesToEnd()
        {
            foreach (var entry in SelectedDownloadEntries)
            {
                if (entry is DownloadContainerViewModel container)
                {
                    Downloads.Remove(container);
                    Downloads.Add(container);
                }
                else if (entry is DownloadEntryViewModel downloadEntry)
                {
                    downloadEntry.Parent.Entries.Remove(downloadEntry);
                    downloadEntry.Parent.Entries.Add(downloadEntry);
                }
            }
        }

        public void MoveSelectedEntriesHigher()
        {
            foreach (var entry in SelectedDownloadEntries)
            {
                if (entry is DownloadContainerViewModel container)
                {
                    int index = Downloads.IndexOf(container);

                    if (index > 0)
                    {
                        Downloads.RemoveAt(index);
                        Downloads.Insert(index - 1, container);
                    }
                }
                else if (entry is DownloadEntryViewModel downloadEntry)
                {
                    int index = downloadEntry.Parent.Entries.IndexOf(downloadEntry);

                    if (index > 0)
                    {
                        downloadEntry.Parent.Entries.RemoveAt(index);
                        downloadEntry.Parent.Entries.Insert(index - 1, downloadEntry);
                    }
                }
            }
        }

        public void MoveSelectedEntriesLower()
        {
            foreach (var entry in SelectedDownloadEntries)
            {
                if (entry is DownloadContainerViewModel container)
                {
                    int index = Downloads.IndexOf(container);

                    if (index < Downloads.Count - 1)
                    {
                        Downloads.RemoveAt(index);
                        Downloads.Insert(index + 1, container);
                    }
                }
                else if (entry is DownloadEntryViewModel downloadEntry)
                {
                    int index = downloadEntry.Parent.Entries.IndexOf(downloadEntry);

                    if (index > Downloads.Count - 1)
                    {
                        downloadEntry.Parent.Entries.RemoveAt(index);
                        downloadEntry.Parent.Entries.Insert(index + 1, downloadEntry);
                    }
                }
            }
        }

        public Task ShowDialog(DownloaderDialog dialog, MetroDialogSettings settings = null)
        {
            m_currDialog = dialog;

            return Task.Run(async () =>
            {
                m_mainWindow.PreviewMouseDown += OnDialogMouseDown;
                m_mainWindow.PreviewMouseUp += OnDialogMouseUp;
                await m_dialogCoordinator.ShowMetroDialogAsync(this, dialog, settings);
                await dialog.WaitForResult();
                await m_dialogCoordinator.HideMetroDialogAsync(this, dialog, settings);
                m_mainWindow.PreviewMouseDown -= OnDialogMouseDown;
                m_mainWindow.PreviewMouseUp -= OnDialogMouseUp;
                m_currDialog = null;
            });
        }

        private void OnDialogMouseUp(object sender, MouseButtonEventArgs e)
        {
            var element = m_mainWindow.InputHitTest(e.GetPosition(m_mainWindow));

            if (element is FrameworkElement frameworkElement)
            {
                if (frameworkElement.Name == "PART_OverlayBox")
                {
                    if ((DateTime.Now - m_dialogMouseDownTime).TotalMilliseconds <= 200)
                        m_currDialog.Cancel();
                }
            }
        }

        private void OnDialogMouseDown(object sender, MouseButtonEventArgs e)
        {
            var element = m_mainWindow.InputHitTest(e.GetPosition(m_mainWindow));

            if (element is FrameworkElement frameworkElement)
            {
                if (frameworkElement.Name == "PART_OverlayBox")
                    m_dialogMouseDownTime = DateTime.Now;
            }
        }

        public void Exit(int actualHeight, int actualWidth, WindowState windowState)
        {
            App.Downloader.Settings.LastHeight = actualHeight;
            App.Downloader.Settings.LastWidth = actualWidth;
            App.Downloader.Settings.LastWindowState = windowState;
            App.Downloader.SaveSettings();

            m_bIsRunning = false;
            m_downloadCondition.Set();

            List<DownloadContainer> scanEntries = new List<DownloadContainer>();
            List<DownloadContainer> downloadEntries = new List<DownloadContainer>();

            foreach (var container in ScanViewModel.ScannedLinks)
                scanEntries.Add(container.Data);

            foreach (var container in Downloads)
                downloadEntries.Add(container.Data);

            App.Current.Dispatcher.InvokeShutdown();

            App.Downloader.SaveSessionContainer(scanEntries, downloadEntries);
            App.Downloader.Exit();

            foreach (var thread in m_downloadThread)
            {
                if (!thread.Thread.Join(TimeSpan.FromSeconds(5)))
                    thread.Thread.Abort();
            }

            ScanViewModel.Destroy();
        }

        public void Initialize(Window window, MenuItem monitorClipboard, MenuItem optionPage,
            MenuItem addLinkPage, MenuItem downloadPage, DownloadView downloadView,
            AddLinkView addLinkView, OptionView optionView, Flyout propertyWindow, Image clipboardImage, DockPanel scanJobs)
        {
            m_clipboardManager = new ClipboardManager(window);
            m_clipboardManager.ClipboardChanged += ClipboardChanged;

            m_clipboardManager.IsActive = monitorClipboard.IsChecked;

            m_mainWindow = window;

            m_clipboardImage = clipboardImage;

            m_optionPage = optionPage;
            m_addLinkPage = addLinkPage;
            m_downloadPage = downloadPage;

            m_downloadView = downloadView;
            m_addLinkView = addLinkView;
            m_optionView = optionView;
            m_propertyFlyout = propertyWindow;

            m_propertyFlyout.IsOpenChanged += propertyFlyout_IsOpenChanged;

            ScanViewModel.Initialize(scanJobs);

            monitorClipboard.Checked += MonitorClipboard_Checked;
            monitorClipboard.Unchecked += MonitorClipboard_Checked;
            addLinkView.EditEntryProperties += EditEntryProperties;
            downloadView.EditEntryProperties += EditEntryProperties;

            App.Downloader.LoadSessionContainer(out var scanEntries, out var downloadEntries);

            if (scanEntries != null)
            {
                foreach (var container in scanEntries)
                    ScanViewModel.ScannedLinks.Add(new ScannedLinkContainerViewModel(container, window.Dispatcher));
            }

            if (downloadEntries != null)
            {
                foreach (var container in downloadEntries)
                    Downloads.Add(new DownloadContainerViewModel(new ScannedLinkContainerViewModel(container, window.Dispatcher), this));
            }

            DialogParticipation.SetRegister(m_mainWindow, this);
        }

        public async void ShowAboutWindow()
        {
            MetroWindow metroWindow = (m_mainWindow as MetroWindow);
            AboutDialog aboutDialog = new AboutDialog();

            await metroWindow.ShowMetroDialogAsync(aboutDialog);
            await aboutDialog.Closed.Task;
            await metroWindow.HideMetroDialogAsync(aboutDialog);
        }

        public async void SearchForUpdates()
        {
            var updater = new Updater();

            MetroWindow metroWindow = (m_mainWindow as MetroWindow);

            UpdatePackage updatePackage = await updater.FindUpdateAsync();

            if (updatePackage != null)
            {
                DownloadFoundDialog dialog = new DownloadFoundDialog(updatePackage);
                await metroWindow.ShowMetroDialogAsync(dialog);
                bool doUpdate = await dialog.Update.Task;
                await metroWindow.HideMetroDialogAsync(dialog);

                if (doUpdate)
                {
                    DownloadDialog dialog2 = new DownloadDialog(updatePackage);

                    await metroWindow.ShowMetroDialogAsync(dialog2);

                    updatePackage.Update(m_application);

                    await dialog2.ViewModel.Updated.Task;
                    await metroWindow.HideMetroDialogAsync(dialog2);
                }
            }
        }

        public void MoveSelectedToDownloadList()
        {
            Dictionary<string, DownloadContainerViewModel> dictDownload = new Dictionary<string, DownloadContainerViewModel>();
            List<ScannedLinkContainerViewModel> removeItems = new List<ScannedLinkContainerViewModel>();

            foreach (var entry in ScanViewModel.SelectedScanEntries)
            {
                ScannedLinkContainerViewModel container;

                if (entry is ScannedLinkEntryViewModel linkEntry)
                    container = linkEntry.Parent;
                else
                    container = entry as ScannedLinkContainerViewModel;


                if (container != null)
                {
                    if (!dictDownload.ContainsKey(container.Name))
                    {
                        var downloadContainer = new DownloadContainerViewModel(container, this);
                        container.DetachEntry();

                        removeItems.Add(container);
                        App.Downloader.RemoveContainer(container);
                        

                        dictDownload.Add(downloadContainer.Name, downloadContainer);
                    }
                }
            }

            foreach (var container in removeItems)
                ScanViewModel.ScannedLinks.Remove(container);

            foreach (var container in dictDownload)
                Downloads.Add(container.Value);


            if (IsDownloading)
                QueueEntries();
            else
                ProgressState = System.Windows.Shell.TaskbarItemProgressState.None;
        }

        public async void MoveToNewPackage()
        {
            if (ScanViewModel.SelectedScanEntries.Count > 0)
            {
                var dialog = new MovePackageDialogView(ScanViewModel.SelectedScanEntries.First());
                await ShowDialog(dialog);

                if (dialog.Result == null)
                    return;

                DownloadContainer newDownloadContainer = new DownloadContainer();
                newDownloadContainer.Name = dialog.Result.Name;
                newDownloadContainer.Directory = dialog.Result.SavePath;

                ScannedLinkContainerViewModel newContainer = new ScannedLinkContainerViewModel(newDownloadContainer, m_mainWindow.Dispatcher);

                List<ScannedLinkContainerViewModel> removeList = new List<ScannedLinkContainerViewModel>();

                foreach (var entry in ScanViewModel.SelectedScanEntries)
                {
                    if (entry is ScannedLinkContainerViewModel container)
                    {
                        foreach (var lnk in container.Entries)
                        {
                            if (lnk is ScannedLinkEntryViewModel scanEntry)
                            {
                                newContainer.AddEntry(scanEntry.DownloadPath, scanEntry.Data);
                            }
                        }

                        removeList.Add(container);
                    }
                }

                foreach (var remove in removeList)
                {
                    ScanViewModel.ScannedLinks.Remove(remove);
                    ScanViewModel.SelectedScanEntries.Remove(remove);
                }

                ScanViewModel.ScannedLinks.Add(newContainer);
            }
        }

        public void RemoveSelectedEntry()
        {
            var lst = ScanViewModel.SelectedScanEntries.ToList();
            foreach (var entry in lst)
            {
                if (entry is ScannedLinkEntryViewModel linkEntry)
                    linkEntry.Parent.Remove(linkEntry);
                else
                {
                    var container = entry as ScannedLinkContainerViewModel;
                    ScanViewModel.ScannedLinks.Remove(container);
                    App.Downloader.RemoveContainer(container);
                }
            }

            ScanViewModel.SelectedScanEntries.Clear();
        }

        public void PauseDownload()
        {

        }

        public void StopDownload()
        {

        }

        public void StartDownload()
        {
            ProgressState = System.Windows.Shell.TaskbarItemProgressState.Indeterminate;

            QueueEntries();

            m_downloadCondition.Set();
        }

        public void ClearDownloadProperties()
        {
            DownloadPercentage = 0.0;
            DownloadCurrentSpeed = string.Empty;
            DownloadRemainTime = string.Empty;
        }

        public void UpdateDownloadProperties()
        {
            ulong completeSize = 0;
            ulong receivedSize = 0;
            ulong currentSpeed = 0;
            TimeSpan duration = TimeSpan.Zero;

            foreach (var entry in Downloads)
            {
                completeSize += entry.BytesToReceive;
                receivedSize += entry.ReceivedSize;

                if (entry.IsDownloading)
                    currentSpeed += entry.CurrentSpeedValue;

                if (entry.DownloadState == FileDownloadState.Failed)
                    ProgressState = System.Windows.Shell.TaskbarItemProgressState.Error;
            }

            string timeString;

            if (currentSpeed > 0)
            {
                double d = ((double)completeSize - receivedSize) / currentSpeed;
                int timeRemaining = (int)d;
                duration = new TimeSpan(0, 0, 0, timeRemaining, (int)Math.Round((d - timeRemaining) * 1000));
                timeString = TimeSpanConverter.GetString(duration, App.Downloader.Settings.ShowMilliseconds);
            }
            else
                timeString = "-";

            App.Current.Dispatcher.Invoke(() =>
            {
                DownloadSize = Logic.FileSize.GetSizeString(completeSize, App.Downloader.Settings.FileSizeFormat);
                DownloadReceivedSize = Logic.FileSize.GetSizeString(receivedSize, App.Downloader.Settings.FileSizeFormat);
                DownloadPercentage = Math.Round((double)receivedSize / completeSize * 100, 2, MidpointRounding.AwayFromZero);
                DownloadStatePercentage = DownloadPercentage / 100;

                if (currentSpeed != 0)
                    DownloadCurrentSpeed = Logic.FileSize.GetSizeString(currentSpeed, App.Downloader.Settings.FileSizeFormat) + "/s";

                if (!string.IsNullOrEmpty(timeString))
                    DownloadRemainTime = timeString;
            });
        }

        public void OpenAddLinkPage()
        {
            HideDownloadPage();
            HideOptionPage();
            ShowAddLinkPage();
        }

        public void OpenDownloadPage()
        {
            HideAddLinkPage();
            HideOptionPage();
            ShowDownloadPage();
        }

        public void OpenOptionPage()
        {

            HideDownloadPage();
            HideAddLinkPage();
            ShowOptionPage();
            m_propertyFlyout.IsOpen = false;
        }

        private void HideDownloadPage()
        {
            if (m_downloadPage != null)
                m_downloadPage.IsChecked = false;

            if (m_downloadView != null)
            {
                if (m_downloadView.Visibility == Visibility.Visible)
                    m_downloadView.SelectedEntry = m_propertyFlyout.DataContext;

                m_downloadView.Visibility = Visibility.Collapsed;
            }
        }

        private void QueueEntries()
        {
            foreach (var container in Downloads)
            {
                foreach (var entry in container.Entries)
                {
                    if (!entry.IsQueued)
                    {
                        lock (m_downloadQueue)
                        {
                            entry.IsQueued = true;
                            m_downloadQueue.Enqueue(entry);
                        }
                    }
                }
            }
        }

        private void ShowDownloadPage()
        {
            if (m_downloadPage != null)
                m_downloadPage.IsChecked = true;

            if (m_downloadView != null)
            {
                m_downloadView.Visibility = Visibility.Visible;

                ShowPropertyFlyout(m_downloadView.IsPropertyWindowOpen, m_downloadView.SelectedEntry);
            }
        }

        private void HideOptionPage()
        {
            if (m_optionPage != null)
                m_optionPage.IsChecked = false;

            if (m_optionView != null)
            {
                SettingViewModel.ResetSettings();
                m_optionView.Visibility = Visibility.Collapsed;
            }
        }

        private void ShowOptionPage()
        {
            if (m_optionPage != null)
                m_optionPage.IsChecked = true;

            if (m_optionView != null)
            {
                m_optionView.Reset();
                m_optionView.Visibility = Visibility.Visible;
            }
        }

        private void HideAddLinkPage()
        {
            if (m_addLinkPage != null)
                m_addLinkPage.IsChecked = false;

            if (m_addLinkView != null)
            {
                if (m_addLinkView.Visibility == Visibility.Visible)
                    m_addLinkView.SelectedEntry = m_propertyFlyout.DataContext;

                m_addLinkView.Visibility = Visibility.Collapsed;
            }
        }

        private void ShowAddLinkPage()
        {
            if (m_addLinkPage != null)
                m_addLinkPage.IsChecked = true;

            if (m_addLinkView != null)
            {
                m_addLinkView.Visibility = Visibility.Visible;

                ShowPropertyFlyout(m_addLinkView.IsPropertyWindowOpen, m_addLinkView.SelectedEntry);
            }
        }

        private void ShowPropertyFlyout(bool bShow, object dataContext)
        {
            if (bShow)
            {
                m_propertyFlyout.DataContext = dataContext;
                m_propertyFlyout.IsOpen = true;
            }
            else
                m_propertyFlyout.IsOpen = false;
        }

        private void ProcessDownloadThread(object threadData)
        {
            ThreadData data = (ThreadData)threadData;

            while (m_bIsRunning)
            {
                m_downloadCondition.WaitOne();
                data.IsDownloading = true;
                OnPropertyChanged(nameof(IsDownloading));
                ProcessDownload(data);

                lock (m_downloadQueue)
                {
                    if (m_downloadQueue.Count == 0 && data.IsDownloading)
                    {
                        ProgressState = System.Windows.Shell.TaskbarItemProgressState.Normal;
                        data.IsDownloading = false;
                        OnPropertyChanged(nameof(IsDownloading));
                    }
                }
            }

            data.IsDownloading = false;
        }

        private void ProcessDownload(ThreadData threadData)
        {
            DownloadEntryViewModel viewModel = null;

            lock (m_downloadQueue)
            {
                if (m_downloadQueue.Count == 0)
                    m_downloadCondition.Reset();
                else
                    viewModel = m_downloadQueue.Dequeue();
            }

            if (viewModel != null)
                App.Downloader.DownloadEntry(viewModel.Data);
        }

        private string GetDialogFilter()
        {
            string result = string.Empty;

            foreach (var frmt in App.Downloader.SupportedFormats)
                result += frmt + "|";

            return result.Remove(result.Length - 1, 1);
        }

        private void ClipboardChanged(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                try
                {
                    string clipboard = Clipboard.GetText();
                    m_addLinkPage.IsChecked = ScanViewModel.Scan(clipboard, true);
                }
                catch (Exception)
                { }
            }
        }

        private void MonitorClipboard_Checked(object sender, RoutedEventArgs e)
        {
            if (m_clipboardManager != null)
            {
                if (m_clipboardManager.IsActive)
                {
                    if (m_clipboardImage != null)
                        m_clipboardImage.Source = Images.MonitorClipboardImageOff;


                    m_clipboardManager.IsActive = false;
                }
                else
                {
                    if (m_clipboardImage != null)
                        m_clipboardImage.Source = Images.MonitorClipboardImageOn;

                    m_clipboardManager.IsActive = true;
                }
            }
        }

        private void EditEntryProperties(object sender, EditEntryPropertiesEventArgs e)
        {
            m_addLinkView.IsPropertyWindowOpen = m_addLinkView.Visibility == Visibility.Visible;
            m_downloadView.IsPropertyWindowOpen = m_downloadView.Visibility == Visibility.Visible;
            m_propertyFlyout.DataContext = e.Entry;
            m_propertyFlyout.IsOpen = true;
        }

        private void Downloader_ResolvingCaptcha(object sender, OpenBrowserEventArgs e)
        {
            Browser browser = null;

            if (e.CaptchaType == null)
                m_mainWindow.Dispatcher.Invoke(() => browser = new Browser(e.Browser.Control, true) { Owner = m_mainWindow });
            else
                m_mainWindow.Dispatcher.Invoke(() => browser = new Browser(e.Browser.Control, e.CaptchaType.NeedAction) { Owner = m_mainWindow });

            e.Result = browser;
        }

        private void propertyFlyout_IsOpenChanged(object sender, RoutedEventArgs e)
        {
            if (m_addLinkView.IsPropertyWindowOpen)
                m_addLinkView.IsPropertyWindowOpen = m_propertyFlyout.IsOpen;

            if (m_downloadView.IsPropertyWindowOpen)
                m_downloadView.IsPropertyWindowOpen = m_propertyFlyout.IsOpen;
        }
    }
}
