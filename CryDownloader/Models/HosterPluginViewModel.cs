﻿using CryDownloader.Plugin;
using System.Windows.Media;

namespace CryDownloader
{
    class HosterPluginViewModel : PluginViewModel<Hoster>
    {
        public ImageSource Image => Images.GetHosterIcon(Data);

        public HosterPluginViewModel(Hoster data)
            : base(data)
        { }
    }
}
