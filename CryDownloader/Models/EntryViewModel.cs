﻿using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Media;

namespace CryDownloader
{
    abstract class EntryViewModel : ViewModelBase
    {
        private bool m_isSelected;

        public abstract ImageSource Icon { get; protected set; }

        public abstract AsyncObservableCollection<StatusViewModel> Status { get; }

        public abstract AsyncObservableCollection<HosterViewModel> Hoster { get; }

        public abstract string Name { get; set; }

        public abstract string DownloadPath { get; set; }

        public abstract string FileSize { get; protected set; }

        public abstract string DownloadUrl { get; }

        public virtual string StatusText
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                Status.Lock();

                foreach (var entry in Status)
                {
                    if (!string.IsNullOrEmpty(entry.Description))
                        sb.Append($"{entry.Description}, ");
                }

                Status.Unlock();

                if (sb.Length > 2)
                    return sb.Remove(sb.Length - 2, 2).ToString();

                return string.Empty;
            }
        }

        public string HosterText
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (Hoster.Count > 1)
                    return string.Empty;

                Hoster.Lock();

                foreach (var entry in Hoster)
                    sb.Append($"{entry.Description}, ");

                Hoster.Unlock();

                if (sb.Length > 2)
                    return sb.Remove(sb.Length - 2, 2).ToString();

                return string.Empty;
            }
        }

        public bool IsSelected
        {
            get => m_isSelected;
            set
            {
                m_isSelected = value;
                OnPropertyChanged();
            }
        }
    }
}
