﻿using CryDownloader.Plugin;
using System.Windows.Media;

namespace CryDownloader
{
    class HosterViewModel
    {
        public Hoster Data { get; }

        public ImageSource Image { get; }

        public string Description { get; }

        public HosterViewModel(Hoster data, ImageSource icon)
        {
            Data = data;
            Image = icon;
            Description = data.Name;
        }

        public override string ToString()
        {
            return Data.Name;
        }
    }
}
