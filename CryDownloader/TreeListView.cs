﻿using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace CryDownloader
{
    public class TreeListView : TreeView
    {
        private bool m_bInCalculateNewWidths;

        public static readonly DependencyProperty ItemContextMenuProperty =
            DependencyProperty.Register("ItemContextMenu", typeof(ContextMenu), typeof(TreeListView),
            new PropertyMetadata(null));

        public static readonly DependencyProperty NoItemContextMenuProperty =
            DependencyProperty.Register("NoItemContextMenu", typeof(ContextMenu), typeof(TreeListView),
            new PropertyMetadata(null));

        public ContextMenu ItemContextMenu
        {
            get => (ContextMenu)GetValue(ItemContextMenuProperty);
            set => SetValue(ItemContextMenuProperty, value);
        }

        public ContextMenu NoItemContextMenu
        {
            get => (ContextMenu)GetValue(NoItemContextMenuProperty);
            set => SetValue(NoItemContextMenuProperty, value);
        }

        public TreeListView()
        {
            SizeChanged += TreeListView_SizeChanged;
        }

        private void UpdateColumnWidths()
        {
            if (m_bInCalculateNewWidths)
                return;

            m_bInCalculateNewWidths = true;
            double width = ActualWidth;

            if (Resources["gvcc"] is GridViewColumnCollection columns)
            {
                foreach (var column in columns)
                {
                    // Dirty way to detect column resize
                    ((INotifyPropertyChanged)column).PropertyChanged -= ColumnPropertyChanged;
                    ((INotifyPropertyChanged)column).PropertyChanged += ColumnPropertyChanged;

                    if (!double.IsNaN(column.ActualWidth))
                        width -= column.ActualWidth;
                }

                GridViewColumn lastColumn = columns.Last();

                width += lastColumn.ActualWidth;

                if (width > 25)
                    lastColumn.Width = width - 25;

                m_bInCalculateNewWidths = false;
            }
        }

        private void TreeListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateColumnWidths();
        }

        private void ColumnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Dirty way to detect column resize
            if (e.PropertyName == "ActualWidth")
                UpdateColumnWidths();
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }

        private bool IsSelected(object obj, IList lst)
        {
            foreach (var o in lst)
            {
                if (o == obj)
                    return true;
            }

            return false;
        }

        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    TreeViewMultipleSelectionBehavior.DeSelectAllItems(this);
                    break;
            }
            base.OnItemsChanged(e);
        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            if (ItemContextMenu != null)
            {
                var pos = Mouse.GetPosition(this);
                var hitResult = VisualTreeHelper.HitTest(this, pos);

                if (hitResult != null && hitResult.VisualHit is FrameworkElement element)
                {
                    if (element.DataContext is EntryViewModel viewModel)
                    {
                        SetSelection(this, viewModel);

                        ContextMenu = ItemContextMenu;
                        ContextMenu.DataContext = viewModel;
                        ContextMenu.IsOpen = true;

                        return;
                    }
                }
            }

            ContextMenu = NoItemContextMenu;
            base.OnContextMenuOpening(e);
        }

        private bool SetSelection(ItemsControl parent, object child)
        {
            if (parent == null || child == null)
                return false;

            if (parent.ItemContainerGenerator.ContainerFromItem(child) is TreeListViewItem childNode)
            {
                childNode.Focus();
                TreeViewMultipleSelectionBehavior.SetIsItemSelected(childNode, true);
                return childNode.IsSelected = true;
            }

            if (parent.Items.Count > 0)
            {
                foreach (object childItem in parent.Items)
                {
                    var childControl = parent.ItemContainerGenerator.ContainerFromItem(childItem) as ItemsControl;

                    if (parent.ItemContainerGenerator.ContainerFromItem(childControl) is TreeListViewItem treeListViewItem)
                        TreeViewMultipleSelectionBehavior.SetIsItemSelected(treeListViewItem, false);

                    if (SetSelection(childControl, child))
                        return true;
                }
            }

            return false;
        }
    }

    public class TreeListViewItem : TreeViewItem
    {
        public TreeListViewItem()
        {

        }
        public int Level
        {
            get
            {
                if (m_level == -1)
                    m_level = (ItemsControlFromItemContainer(this) is TreeListViewItem parent) ? parent.Level + 1 : 0;

                return m_level;
            }
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }

        private int m_level = -1;
    }
}
