﻿using System;
using System.Windows;
using System.Windows.Markup;

namespace CryDownloader
{
    public class SystemTypeExtension : MarkupExtension
    {
        private object parameter;

        public int Int { set { parameter = value; } }
        public double Double { set { parameter = value; } }
        public float Float { set { parameter = value; } }
        public bool Bool { set { parameter = value; } }
        public Visibility Visibility { set { parameter = value; } }
        // add more as needed here

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return parameter;
        }
    }
}
