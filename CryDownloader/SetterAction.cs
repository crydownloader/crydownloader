﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using Microsoft.Xaml.Behaviors;

namespace CryDownloader
{
    public class SetterAction : TargetedTriggerAction<FrameworkElement>
    {
        #region Properties

        #region PropertyName

        /// <summary>
        /// Property that is being set by this setter.
        /// </summary>
        public string PropertyName
        {
            get => (string)GetValue(PropertyNameProperty);
            set => SetValue(PropertyNameProperty, value);
        }

        public static readonly DependencyProperty PropertyNameProperty =
            DependencyProperty.Register("PropertyName", typeof(string), typeof(SetterAction),
            new PropertyMetadata(string.Empty));

        #endregion

        #region Value

        /// <summary>
        /// Property value that is being set by this setter.
        /// </summary>
        public object Value
        {
            get => GetValue(PropertyValue);
            set => SetValue(PropertyValue, value);
        }

        public object ValueObject
        {
            get => GetValue(PropertyValueObject);
            set => SetValue(PropertyValueObject, value);
        }

        public static readonly DependencyProperty PropertyValue =
            DependencyProperty.Register("Value", typeof(object), typeof(SetterAction),
            new PropertyMetadata(null));

        public static readonly DependencyProperty PropertyValueObject =
            DependencyProperty.Register("ValueObject", typeof(object), typeof(SetterAction),
                new PropertyMetadata(string.Empty));

        #endregion

        #endregion

        #region Overrides

        protected override void Invoke(object parameter)
        {
            if (ValueObject is string a)
            {
                if (string.IsNullOrEmpty(a))
                    ValueObject = parameter;
            }

            var target = TargetObject ?? AssociatedObject;

            var targetType = target.GetType();
            var valueType = Value.GetType();

            var targetProperty = targetType.GetProperty(PropertyName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

            if (targetProperty == null)
                throw new ArgumentException($"Property not found: {PropertyName}");

            if (!targetProperty.CanWrite)
                throw new ArgumentException($"Property is not settable: {PropertyName}");

            PropertyInfo valueProperty = null;

            if (Value is string p)
            {
                var parameterType = ValueObject.GetType();
                valueProperty = parameterType.GetProperty(p, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

                if (valueProperty != null)
                {
                    if (!valueProperty.CanRead)
                        throw new ArgumentException($"Property is not readable: {p}");
                }
            }

            object convertedValue;

            if (valueProperty != null)
            {
                Value = valueProperty.GetValue(ValueObject);
                valueType = valueProperty.PropertyType;
            }

            if (Value == null)
                convertedValue = null;
            else
            {
                var propertyType = targetProperty.PropertyType;

                if (valueProperty != null)
                    valueType = valueProperty.PropertyType;

                if (valueType == propertyType)
                    convertedValue = Value;
                else
                {
                    var propertyConverter = TypeDescriptor.GetConverter(propertyType);

                    if (propertyConverter.CanConvertFrom(valueType))
                        convertedValue = propertyConverter.ConvertFrom(Value);
                    else if (valueType.IsSubclassOf(propertyType))
                        convertedValue = Value;

                    else
                        convertedValue = Value;
                }
            }

            targetProperty.SetValue(target, convertedValue);
        }

        #endregion
    }
}
