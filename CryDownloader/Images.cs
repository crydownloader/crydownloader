﻿using CryDownloader.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Cache;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CryDownloader
{
    public static class Images
    {
        private static readonly Dictionary<string, ImageSource> ConvertedHosterIcons = new Dictionary<string, ImageSource>();
        private static readonly Dictionary<string, ImageSource> ConvertedFileExtensionIcons = new Dictionary<string, ImageSource>();

        public static readonly ImageSource ContainerIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/DownloadGroup.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource CheckMark = new BitmapImage(new Uri("pack://application:,,,/Resources/CheckMark.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource ErrorIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/ErrorIcon.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource QuestionMarkIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/QuestionMarkIcon.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource DownloadEntryIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/DownloadEntry.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource ArchivIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/ArchivIcon.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource MovieIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/MovieIcon.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource MusicIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/MusicIcon.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource PictureIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/PictureIcon.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource WarningIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/Warning.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource ExclamationMark = new BitmapImage(new Uri("pack://application:,,,/Resources/ExclamationMark.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource MonitorClipboardImageOff = new BitmapImage(new Uri("pack://application:,,,/Resources/MonitorClipboardOff.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource MonitorClipboardImageOn = new BitmapImage(new Uri("pack://application:,,,/Resources/MonitorClipboardOn.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource JobLoadingIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/OpeningContainer.png", UriKind.Absolute), new RequestCachePolicy(RequestCacheLevel.BypassCache));

        public static readonly ImageSource JobScanIcon = MonitorClipboardImageOff;

        public static readonly ImageSource ApplicationIcon = Logic.ImageSourceConverter.ImageSourceFromIcon(
            System.Drawing.Icon.ExtractAssociatedIcon(System.Reflection.Assembly.GetEntryAssembly().Location));

        public static ImageSource GetHosterIcon(Hoster hoster)
        {
            if (!ConvertedHosterIcons.TryGetValue(hoster.Name, out ImageSource img))
            {
                img = Logic.ImageSourceConverter.ImageSourceFromBitmap(hoster.Icon);
                ConvertedHosterIcons.Add(hoster.Name, img);
            }

            if (img.CanFreeze)
                img.Freeze();

            return img;
        }

        public static ImageSource GetFileIcon(string path)
        {
            FileInfo fileInfo = new FileInfo(path);

            if (!ConvertedFileExtensionIcons.TryGetValue(fileInfo.Extension, out var result))
            {
                var icon = Logic.Application.GetFileIcon(path);
                var app = App.Downloader;

                if (icon == null || app.Settings.PreferApplicationFileIcons)
                {
                    if (app.IsArchiv(path))
                        return ArchivIcon;
                    else if (app.IsImage(path))
                        return PictureIcon;
                    else if (app.IsAudio(path))
                        return MusicIcon;
                    else if (app.IsVideo(path))
                        return MovieIcon;
                    else
                        return DownloadEntryIcon;
                }

                result = Logic.ImageSourceConverter.ImageSourceFromIcon(icon);

                icon.Dispose();

                ConvertedFileExtensionIcons.Add(fileInfo.Extension, result);
            }

            if (result.CanFreeze)
                result.Freeze();

            return result;
        }

        public static ImageSource GetFileStateIcon(FileStateIcon fileStateIcon)
        {
            switch (fileStateIcon)
            {
                case FileStateIcon.None:
                case FileStateIcon.Current:
                default:
                    return null;
                case FileStateIcon.CheckMark:
                    return CheckMark;
                case FileStateIcon.Error:
                    return ErrorIcon;
                case FileStateIcon.ExclamationMark:
                    return ExclamationMark;
                case FileStateIcon.QuestionMark:
                    return QuestionMarkIcon;
                case FileStateIcon.Unpack:
                    return ArchivIcon;
                case FileStateIcon.Warning:
                    return WarningIcon;
            }
        }
    }
}
