﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace CryDownloader
{
    public class AsyncObservableCollection<T> : ObservableCollection<T>
    {
        private readonly object m_syncLock;

        public AsyncObservableCollection()
        {
            m_syncLock = new object();
            Application.Current.Dispatcher.Invoke(() => BindingOperations.EnableCollectionSynchronization(this, m_syncLock));
        }

        public AsyncObservableCollection(IEnumerable<T> list)
            : base(list)
        {
        }

        public void Lock()
        {
            Monitor.Enter(m_syncLock);
        }

        public void Unlock()
        {
            Monitor.Exit(m_syncLock);
        }

        protected override void InsertItem(int index, T item)
        {
            lock (m_syncLock)
                base.InsertItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            lock (m_syncLock)
                base.RemoveItem(index);
        }

        protected override void SetItem(int index, T item)
        {
            lock (m_syncLock)
                base.SetItem(index, item);
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            lock (m_syncLock)
                base.MoveItem(oldIndex, newIndex);
        }

        protected override void ClearItems()
        {
            lock (m_syncLock)
                base.ClearItems();
        }
    }
}
