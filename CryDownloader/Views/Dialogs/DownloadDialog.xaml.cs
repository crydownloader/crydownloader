﻿using CryDownloader.Logic;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für DownloadDialog.xaml
    /// </summary>
    partial class DownloadDialog
    {
        public DownloadDialogViewModel ViewModel { get; }

        public DownloadDialog(UpdatePackage updatePackage)
        {
            ViewModel = new DownloadDialogViewModel(updatePackage);
            InitializeComponent();
        }
    }
}
