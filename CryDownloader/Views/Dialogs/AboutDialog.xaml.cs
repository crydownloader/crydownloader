﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für AboutDialog.xaml
    /// </summary>
    partial class AboutDialog
    {
        public TaskCompletionSource<bool> Closed { get; private set; }

        public AboutDialog()
        {
            Closed = new TaskCompletionSource<bool>();
            InitializeComponent();
        }

        public void Close(bool result)
        {
            Closed.SetResult(result);
        }
    }
}
