﻿using CryDownloader.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für DownloadFoundDialog.xaml
    /// </summary>
    partial class DownloadFoundDialog
    {
        public TaskCompletionSource<bool> Update { get; private set; }

        public string Version { get; }

        public string Changelog { get; }

        public bool IsNotAvailable { get; }

        public bool IsActual { get; }

        public bool HasUpdate => !(IsNotAvailable || IsActual);

        public DownloadFoundDialog(UpdatePackage updatePackage)
        {
            Update = new TaskCompletionSource<bool>();

            if (updatePackage.UpdateChannel == UpdateChannel.None)
            {
                if (updatePackage.Version == null)
                    IsNotAvailable = true;
                else
                    IsActual = true;
            }
            else
            {
                Version = CryDownloader.Language.Version + ": " + updatePackage.Version.ToString();
                Changelog = updatePackage.Changelog;
            }

            InitializeComponent();
        }

        public void Close(bool update)
        {
            Update.SetResult(update && HasUpdate);
        }
    }
}
