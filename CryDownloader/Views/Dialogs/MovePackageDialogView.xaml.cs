﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für MovePackageDialog.xaml
    /// </summary>
    internal partial class MovePackageDialogView
    {
        public MovePackageDialogViewModel ViewModel { get; }

        public MovePackageDialogView(EntryViewModel entryViewModel)
        {
            ViewModel = new MovePackageDialogViewModel(entryViewModel);
            InitializeComponent();
        }

        private void OnOkClicked(object sender, RoutedEventArgs e)
        {
            Result = ViewModel;
            Close();
        }

        private void OnCancelClicked(object sender, RoutedEventArgs e)
        {
            Result = null;
            Close();
        }
    }
}
