﻿using CryDownloader.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für ScanJobView.xaml
    /// </summary>
    partial class ScanJobView
    {
        public ScanJobViewModel ViewModel { get; }

        public ScanJobView(string text, ScanViewModel scanViewModel, bool bIsClipboard, ScanJobState scanJobState)
        {
            ViewModel = new ScanJobViewModel(text, scanViewModel, this, bIsClipboard, scanJobState);
            InitializeComponent();
        }
    }
}
