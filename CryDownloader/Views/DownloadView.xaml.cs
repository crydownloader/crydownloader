﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für DownloadView.xaml
    /// </summary>
    internal partial class DownloadView : UserControl
    {
        public event EventHandler<EditEntryPropertiesEventArgs> EditEntryProperties;

        public bool IsPropertyWindowOpen { get; set; }

        public object SelectedEntry { get; set; }

        public DownloadView()
        {
            InitializeComponent();
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            if (sizeInfo.WidthChanged)
            {
                GridViewColumnCollection columnCollection = FindResource("gvcc") as GridViewColumnCollection;

                double fullWidth = 0.0;
                int idx = 0;

                foreach (var column in columnCollection)
                {
                    ++idx;

                    if (idx == 1)
                        continue;

                    fullWidth += column.Width;
                }

                double dif = (sizeInfo.NewSize.Width - fullWidth);
                columnCollection[0].Width = dif - 30;
            }

            base.OnRenderSizeChanged(sizeInfo);
        }

        private void TreeListView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (IsPropertyWindowOpen)
            {
                if (m_treeListView.SelectedItem != null)
                    EditEntryProperties?.Invoke(m_treeListView.SelectedItem, new EditEntryPropertiesEventArgs((EntryViewModel)m_treeListView.SelectedItem));
            }
        }

        private void TreeListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (m_treeListView.SelectedItem != null)
                EditEntryProperties?.Invoke(m_treeListView.SelectedItem, new EditEntryPropertiesEventArgs((EntryViewModel)m_treeListView.SelectedItem));
        }
    }
}
