﻿using CryDownloader.Logic;
using CryDownloader.Plugin;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using Application = System.Windows.Application;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für AddLinkView.xaml
    /// </summary>
    internal partial class AddLinkView : UserControl
    {
        public event EventHandler<EditEntryPropertiesEventArgs> EditEntryProperties;

        public bool IsPropertyWindowOpen { get; set; }

        public object SelectedEntry { get; set; }

        public AddLinkView()
        {
            InitializeComponent();
            ControlzEx.Theming.ThemeManager.Current.ThemeChanged += (x, y) => m_waterMark.Foreground = (Brush)App.Current.Resources["TextBrush"];
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            if (sizeInfo.WidthChanged)
            {
                GridViewColumnCollection columnCollection = FindResource("gvcc") as GridViewColumnCollection;

                double fullWidth = 0.0;
                int idx = 0;

                foreach (var column in columnCollection)
                {
                    ++idx;

                    if (idx == 1)
                        continue;

                    fullWidth += column.Width;
                }

                double dif = (sizeInfo.NewSize.Width - fullWidth);
                columnCollection[0].Width = dif - 30;
            }

            base.OnRenderSizeChanged(sizeInfo);
        }

        private void TreeListView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (IsPropertyWindowOpen)
            {
                if (m_treeListView.SelectedItem != null)
                    EditEntryProperties?.Invoke(m_treeListView.SelectedItem, new EditEntryPropertiesEventArgs((EntryViewModel)m_treeListView.SelectedItem));
            }
        }

        private void TreeListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.OriginalSource is FrameworkElement ctrl)
            {
                if (ctrl.DataContext is EntryViewModel)
                {
                    if (m_treeListView.SelectedItem != null)
                        EditEntryProperties?.Invoke(m_treeListView.SelectedItem, new EditEntryPropertiesEventArgs((EntryViewModel)m_treeListView.SelectedItem));
                }
            }
        }

        private void Properties_Click(object sender, RoutedEventArgs e)
        {
            if (m_treeListView.ContextMenu.DataContext != null)
                EditEntryProperties?.Invoke(m_treeListView.ContextMenu.DataContext, new EditEntryPropertiesEventArgs((EntryViewModel)m_treeListView.ContextMenu.DataContext));
        }

        //private void StartDownload_Click(object sender, RoutedEventArgs e)
        //{
        //    MainViewModel viewModel = DataContext as MainViewModel;
        //    viewModel.MoveSelectedToDownloadList();
        //    viewModel.StartDownload();
        //}

        private async void SaveAs_Click(object sender, RoutedEventArgs e)
        {
            MainViewModel mainViewModel = DataContext as MainViewModel;
            List<ScannedLinkContainerViewModel> container = new List<ScannedLinkContainerViewModel>();

            foreach (var entry in TreeViewMultipleSelectionBehavior.GetSelectedItems(m_treeListView))
            {
                if (entry is ScannedLinkContainerViewModel con)
                    container.Add(con);
            }

            MetroWindow metroWindow = (Application.Current.MainWindow as MetroWindow);

            if (container.Count == 0)
            {
                await metroWindow.ShowMessageAsync(CryDownloader.Language.ContainerSavedError, CryDownloader.Language.InvalidItem);
                return;
            }

            bool? result = mainViewModel.SaveContainer(container);

            if (result != null && result.HasValue)
            {
                if (result.Value)
                    await metroWindow.ShowMessageAsync(CryDownloader.Language.ContainerSaved, CryDownloader.Language.ContainerSuccessfullySaved);
                else
                    await metroWindow.ShowMessageAsync(CryDownloader.Language.ContainerSavedError, CryDownloader.Language.FailedToSaveContainer);
            }
        }

        private void m_treeListView_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (m_treeListView.SelectedItem != null)
            {
                if (m_treeListView.ContextMenu == null)
                    return;

                ScannedLinkContainerViewModel viewModel = m_treeListView.SelectedItem as ScannedLinkContainerViewModel;

                if (viewModel != null)
                {
                    if (!m_treeListView.ContextMenu.Items.Contains(m_saveContextMenuItem))
                        m_treeListView.ContextMenu.Items.Add(m_saveContextMenuItem);
                }
                else
                    m_treeListView.ContextMenu.Items.Remove(m_saveContextMenuItem);
            }
            else
                e.Handled = true;
        }
    }
}
