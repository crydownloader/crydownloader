﻿using System.Windows;
using System.Windows.Controls;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für OptionView.xaml
    /// </summary>
    public partial class OptionView : UserControl
    {
        public OptionView()
        {
            InitializeComponent();
            m_generalImage.Source = Images.ApplicationIcon;
            m_hosterInfoControl.Visibility = Visibility.Hidden;
            m_captchaInfoControl.Visibility = Visibility.Hidden;
        }

        public void Reset()
        {
            m_designWarning.Visibility = Visibility.Collapsed;
            m_generalWarning.Visibility = Visibility.Collapsed;
        }
    }
}
