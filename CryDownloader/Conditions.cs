﻿using Microsoft.Xaml.Behaviors;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows;
using System.Windows.Markup;

namespace CryDownloader
{
    enum ConditionType
    {
        If,
        ElseIf,
    }

    enum CompareType
    {
        Or,
        And
    }

    class ConditionValue : DependencyObject
    {
        public static readonly DependencyProperty TargetObjectProperty
        = DependencyProperty.Register("TargetObject", typeof(object),
        typeof(ConditionValue));

        public static readonly DependencyProperty PropertyNameProperty
        = DependencyProperty.Register("PropertyName", typeof(string),
        typeof(ConditionValue));

        public static readonly DependencyProperty ValueProperty
        = DependencyProperty.Register("Value", typeof(object),
        typeof(ConditionValue));

        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set { SetValue(PropertyNameProperty, value); }
        }

        public object TargetObject
        {
            get { return GetValue(TargetObjectProperty); }
            set { SetValue(TargetObjectProperty, value); }
        }

        public object Value
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public object GetObject()
        {
            if (Value != null)
                return Value;

            PropertyInfo propertyInfo = TargetObject.GetType().GetProperty(PropertyName, BindingFlags.Instance | 
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.InvokeMethod);

            return propertyInfo.GetValue(TargetObject);
        }
    }

    abstract class ConditionComparer : DependencyObject
    {
        public static readonly DependencyProperty Value1Property
         = DependencyProperty.Register("Value1", typeof(ConditionValue),
         typeof(ConditionComparer), null);

         public static readonly DependencyProperty Value2Property
         = DependencyProperty.Register("Value2", typeof(ConditionValue),
         typeof(ConditionComparer), null);

        public ConditionValue Value1
        {
            get { return (ConditionValue)GetValue(Value1Property); }
            set { SetValue(Value1Property, value); }
        }

        public ConditionValue Value2
        {
            get { return (ConditionValue)GetValue(Value2Property); }
            set { SetValue(Value2Property, value); }
        }

        public abstract bool Compare();
    }

    class Equal : ConditionComparer
    {
        public override bool Compare()
        {
            object value1 = Value1.GetObject();
            object value2 = Value2.GetObject();

            bool b = value1.Equals(value2);
            return b;
        }
    }

    [ContentProperty("Comparers")]
    class ConditionComparers : DependencyObject
    {
        private static readonly DependencyProperty ComparersProperty
        = DependencyProperty.RegisterAttached("Comparers", typeof(Collection<ConditionComparer>),
        typeof(ConditionComparers));

        private static readonly DependencyProperty CompareTypeProperty
        = DependencyProperty.RegisterAttached("CompareType", typeof(CompareType),
        typeof(ConditionComparers));

        public Collection<ConditionComparer> Comparers
        {
            get { return (Collection<ConditionComparer>)GetValue(ComparersProperty); }
            set { SetValue(ComparersProperty, value); }
        }

        public CompareType CompareType
        {
            get { return (CompareType)GetValue(CompareTypeProperty); }
            set { SetValue(CompareTypeProperty, value); }
        }

        public ConditionComparers()
        {
            Comparers = new Collection<ConditionComparer>();
            CompareType = CompareType.And;
        }

        public bool Compare()
        {
            bool result;

            switch (CompareType)
            {
                default:
                case CompareType.And:
                    result = true;
                    break;
                case CompareType.Or:
                    result = false;
                    break;
            }

            foreach (var comparer in Comparers)
            {
                switch (CompareType)
                {
                    case CompareType.And:
                        result = result && comparer.Compare();
                        break;
                    case CompareType.Or:
                        result = result || comparer.Compare();
                        break;
                }
            }

            return result;
        }
    }

    [ContentProperty("Actions")]
    class Condition : DependencyObject
    {
        private static readonly DependencyProperty ActionsProperty
        = DependencyProperty.RegisterAttached("Actions", typeof(Collection<TriggerAction<FrameworkElement>>),
        typeof(Condition));

        private static readonly DependencyProperty ComparersProperty
        = DependencyProperty.RegisterAttached("Comparers", typeof(ConditionComparers),
        typeof(Condition));

        private static readonly Type TriggerActionType;
        private static readonly MethodInfo TriggerActionInvokeMethod;

        public Collection<TriggerAction<FrameworkElement>> Actions
        {
            get { return (Collection<TriggerAction<FrameworkElement>>)GetValue(ActionsProperty); }
            set { SetValue(ActionsProperty, value); }
        }

        public ConditionComparers Comparers
        {
            get { return (ConditionComparers)GetValue(ComparersProperty); }
            set { SetValue(ComparersProperty, value); }
        }

        static Condition()
        {
            TriggerActionType = typeof(TriggerAction<FrameworkElement>);
            TriggerActionInvokeMethod = TriggerActionType.GetMethod("CallInvoke", BindingFlags.Instance | BindingFlags.NonPublic |
                    BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Static);
        }

        public Condition()
        {
            Actions = new Collection<TriggerAction<FrameworkElement>>();
        }

        public void Invoke(object parameter)
        {
            foreach (var action in Actions)
                TriggerActionInvokeMethod.Invoke(action, new object[] { parameter });
        }
    }

    [ContentProperty("Condition")]
    class Conditions : TriggerAction<FrameworkElement>
    {
        public static readonly DependencyProperty ConditionTypeProperty
            = DependencyProperty.Register("ConditionType", typeof(ConditionType),
            typeof(Conditions), null);

        private static readonly DependencyProperty ConditionProperty
              = DependencyProperty.RegisterAttached("Condition", typeof(Collection<Condition>),
                  typeof(Conditions));

        public ConditionType ConditionType
        {
            get { return (ConditionType)GetValue(ConditionTypeProperty); }
            set { SetValue(ConditionTypeProperty, value); }
        }

        public Collection<Condition> Condition
        {
            get { return (Collection<Condition>)GetValue(ConditionProperty); }
            set { SetValue(ConditionProperty, value); }
        }

        public Conditions()
        {
            ConditionType = ConditionType.If;
            Condition = new Collection<Condition>();
        }

        protected override void Invoke(object parameter)
        {
            switch (ConditionType)
            {
                case ConditionType.If:
                    {
                        foreach (var condition in Condition)
                        {
                            if (condition.Comparers.Compare())
                                condition.Invoke(parameter);
                        }
                        break;
                    }
                case ConditionType.ElseIf:
                    {
                        foreach (var condition in Condition)
                        {
                            if (condition.Comparers.Compare())
                            {
                                condition.Invoke(parameter);
                                break;
                            }
                        }
                        break;
                    }
            }
        }
    }
}
