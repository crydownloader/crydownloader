﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using Microsoft.Xaml.Behaviors;
using System.Windows.Markup;

namespace CryDownloader
{
    class Parameter : DependencyObject
    {
        public static readonly DependencyProperty ValueProperty
         = DependencyProperty.Register("Value", typeof(object),
         typeof(Parameter), null, OnValueChanged);

        public object Value
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private static bool OnValueChanged(object value)
        {
            return true;
        }
    }

    [ContentProperty("Parameters")]
    class CallMethodAction : TriggerAction<FrameworkElement>
    {
        public static readonly DependencyProperty MethodNameProperty
              = DependencyProperty.Register("MethodName", typeof(string),
              typeof(CallMethodAction));

        public static readonly DependencyProperty TargetObjectProperty
              = DependencyProperty.Register("TargetObject", typeof(object),
              typeof(CallMethodAction));

        private static readonly DependencyProperty ParametersProperty
              = DependencyProperty.RegisterAttached("Parameters", typeof(Collection<Parameter>),
                  typeof(CallMethodAction));

        public string MethodName
        {
            get { return (string)GetValue(MethodNameProperty); }
            set { SetValue(MethodNameProperty, value); }
        }

        public object TargetObject
        {
            get { return GetValue(TargetObjectProperty); }
            set { SetValue(TargetObjectProperty, value); }
        }

        public Collection<Parameter> Parameters
        {
            get { return (Collection<Parameter>)GetValue(ParametersProperty); }
            set { SetValue(ParametersProperty, value); }
        }

        public CallMethodAction()
        {
            Parameters = new Collection<Parameter>();
        }

        protected override void Invoke(object parameter)
        {
            object target = TargetObject ?? AssociatedObject;

            if (target is FrameworkElement element && TargetObject == null)
            {
                if (element.DataContext != null)
                    target = element.DataContext;
            }

            MethodInfo methodInfo = target.GetType().GetMethod(
                MethodName,
                BindingFlags.Instance | BindingFlags.Public
                | BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Static);

            var methodParams = Parameters;

            foreach (var param in methodParams)
            {
                if (param.Value == null)
                    param.Value = AssociatedObject;
            }

            if (methodInfo == null)
                return;

            if (methodInfo.IsStatic)
                methodInfo.Invoke(null, methodParams.Select((x) => x.Value).ToArray());
            else
                methodInfo.Invoke(target, methodParams.Select((x) => x.Value).ToArray());
        }
    }
}
