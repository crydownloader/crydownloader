﻿using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using MS.WindowsAPICodePack.Internal;
using ShellHelpers;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : System.Windows.Application
    {
        public const string APP_ID = "Cryfact.CryDownloader";
        public const string SystemThemeKey = "SystemTheme";

        public static Logic.Application Downloader { get; private set; }

        public static bool IsWin10()
        {
            return Environment.OSVersion.Version.Major >= 10;
        }

        private static SolidColorBrush GetSolidColorBrush(Color color, double opacity = 1d)
        {
            var brush = new SolidColorBrush(color) { Opacity = opacity };
            brush.Freeze();
            return brush;
        }

        private static Color CreateNewColor(Color baseColor, int percent)
        {
            if (percent >= 100)
                return baseColor;

            Color colorLeft = baseColor;
            Color colorRight = baseColor;

            double step = 1.0;
            double percentRight = percent / step;
            double percentLeft = 1.0 - percentRight;

            return Color.FromArgb((byte)(colorLeft.A * percentLeft + colorRight.A * percentRight),
                (byte)(colorLeft.R * percentLeft + colorRight.R * percentRight),
                (byte)(colorLeft.G * percentLeft + colorRight.G * percentRight),
                (byte)(colorLeft.B * percentLeft + colorRight.B * percentRight));
        }

        public static ResourceDictionary CreateStyle(Color accentColor)
        {
            var resourceDictionary = new ResourceDictionary();

            // Matadata
            SolidColorBrush accentBaseBrush = GetSolidColorBrush(accentColor);

            resourceDictionary.Add("Theme.Name", App.SystemThemeKey);
            resourceDictionary.Add("Theme.DisplayName", "SystemTheme");
            resourceDictionary.Add("Theme.BaseColorScheme", "SystemTheme");
            resourceDictionary.Add("Theme.ColorScheme", "SystemTheme");
            resourceDictionary.Add("Theme.ShowcaseBrush", accentBaseBrush);
            resourceDictionary.Add("Theme.PrimaryAccentColor", accentColor);

            // Colors

            resourceDictionary.Add("MahApps.Colors.Highlight", accentColor);
            resourceDictionary.Add("MahApps.Colors.AccentBase", accentColor);
            // 80%
            resourceDictionary.Add("MahApps.Colors.Accent", CreateNewColor(accentColor, 80));
            // 60%
            resourceDictionary.Add("MahApps.Colors.Accent2", CreateNewColor(accentColor, 60));
            // 40%
            resourceDictionary.Add("MahApps.Colors.Accent3", CreateNewColor(accentColor, 40));
            // 20%
            resourceDictionary.Add("MahApps.Colors.Accent4", CreateNewColor(accentColor, 20));

            var fileName = Path.GetTempFileName() + ".xaml";

            using (var writer = System.Xml.XmlWriter.Create(fileName, new System.Xml.XmlWriterSettings { Indent = true }))
            {
                System.Windows.Markup.XamlWriter.Save(resourceDictionary, writer);
                writer.Close();
            }

            resourceDictionary = new ResourceDictionary() { Source = new Uri(fileName, UriKind.Absolute) };

            File.Delete(fileName);
            return resourceDictionary;
        }

        private void InstallShortcut(string shortcutPath)
        {
            string exePath = Process.GetCurrentProcess().MainModule.FileName;
            IShellLinkW newShortcut = (IShellLinkW)new CShellLink();

            ErrorHelper.VerifySucceeded(newShortcut.SetPath(exePath));
            ErrorHelper.VerifySucceeded(newShortcut.SetArguments(""));

            IPropertyStore newShortcutProperties = (IPropertyStore)newShortcut;

            using (PropVariant appId = new PropVariant(APP_ID))
            {
                ErrorHelper.VerifySucceeded(newShortcutProperties.SetValue(SystemProperties.System.AppUserModel.ID, appId));
                ErrorHelper.VerifySucceeded(newShortcutProperties.Commit());
            }

            IPersistFile newShortcutSave = (IPersistFile)newShortcut;

            ErrorHelper.VerifySucceeded(newShortcutSave.Save(shortcutPath, true));
        }

        public static void AddBaseTheme(ResourceDictionary resourceDictionary, bool isLightMode)
        {
            if (isLightMode)
            {
                resourceDictionary.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri($"/BaseThemes/Light.xaml", UriKind.Relative)
                });
            }
            else
            {
                resourceDictionary.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri($"/BaseThemes/Dark.xaml", UriKind.Relative)
                });
            }
        }

        public static void FixBorderColors(ResourceDictionary resourceDictionary)
        {
            resourceDictionary["MahApps.Brushes.TextBox.Border"] = resourceDictionary["MahApps.Brushes.Gray8"];
            resourceDictionary["MahApps.Brushes.Controls.Border"] = resourceDictionary["MahApps.Brushes.Gray8"];
            resourceDictionary["MahApps.Brushes.CheckBox"] = resourceDictionary["MahApps.Brushes.Gray8"];
            resourceDictionary["MahApps.Brushes.TextBox.MouseOverInnerBorder"] = resourceDictionary["HighlightBorderBrush"];
            resourceDictionary["MahApps.Brushes.TextBox.FocusBorder"] = resourceDictionary["HighlightBorderBrush"];
            resourceDictionary["MahApps.Brushes.TextBox.MouseOverBorder"] = resourceDictionary["HighlightBorderBrush"];
            resourceDictionary["MahApps.Brushes.Button.MouseOverBorder"] = resourceDictionary["HighlightBorderBrush"];
            resourceDictionary["MahApps.Brushes.Button.MouseOverInnerBorder"] = resourceDictionary["HighlightBorderBrush"];
            resourceDictionary["MahApps.Brushes.ComboBox.MouseOverBorder"] = resourceDictionary["HighlightBorderBrush"];
            resourceDictionary["MahApps.Brushes.ComboBox.MouseOverInnerBorder"] = resourceDictionary["HighlightBorderBrush"];
            resourceDictionary["MahApps.Brushes.ComboBox.PopupBorder"] = resourceDictionary["MahApps.Brushes.White"];
        }

        private static void CreateStyle(Color accentColor, bool isLightMode)
        {
            var resourceDictionary = new ResourceDictionary();

            AddBaseTheme(resourceDictionary, isLightMode);

            resourceDictionary.MergedDictionaries.Add(App.CreateStyle(accentColor));

            if (Current.Resources.FindName("HighlightBrush2") == null)
                resourceDictionary.Add("HighlightBrush2", GetSolidColorBrush((Color)resourceDictionary["MahApps.Colors.Highlight"], 0.3));

            if (Current.Resources.FindName("HighlightBorderBrush") == null)
                resourceDictionary.Add("HighlightBorderBrush", GetSolidColorBrush((Color)resourceDictionary["MahApps.Colors.Highlight"], 0.8));


            FixBorderColors(resourceDictionary);

            ControlzEx.Theming.ThemeManager.Current.AddLibraryTheme(new ControlzEx.Theming.LibraryTheme(resourceDictionary, ControlzEx.Theming.ThemeManager.Current.LibraryThemeProviders.First()));
        }

        public static void ChangeTheme(bool useSystemSetting, string style, string accent)
        {
            if (useSystemSetting)
            {
                bool isLightMode = true;

                if (App.IsWin10())
                {
                    try
                    {
                        var v = Microsoft.Win32.Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize", "AppsUseLightTheme", "1");
                        if (v != null && v.ToString() == "0")
                            isLightMode = false;
                    }
                    catch { }
                }

                var color = SystemParameters.WindowGlassColor;
                CreateStyle(color, isLightMode);

                ControlzEx.Theming.ThemeManager.Current.ChangeTheme(App.Current, App.SystemThemeKey);
                ControlzEx.Theming.ThemeManager.Current.ChangeTheme(App.Current, App.SystemThemeKey);
            }
            else
            {
                ControlzEx.Theming.ThemeManager.Current.ChangeTheme(App.Current, style, accent);
                ControlzEx.Theming.ThemeManager.Current.ChangeTheme(App.Current, style, accent);

                Current.Resources.Clear();

                if (Current.Resources.FindName("HighlightBrush2") == null)
                    Current.Resources.Add("HighlightBrush2", GetSolidColorBrush((Color)Current.Resources["MahApps.Colors.Highlight"], 0.3));

                if (Current.Resources.FindName("HighlightBorderBrush") == null)
                    Current.Resources.Add("HighlightBorderBrush", GetSolidColorBrush((Color)Current.Resources["MahApps.Colors.Highlight"], 0.8));
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            Downloader = new Logic.Application(Current.Dispatcher);

            base.OnStartup(e);

            string shortcutPath = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) + "\\Programs\\CryDownloader\\CryDownloader.lnk";
            FileInfo fi = new FileInfo(shortcutPath);
            if (!fi.Exists)
            {
                if (!Directory.Exists(fi.DirectoryName))
                    Directory.CreateDirectory(fi.DirectoryName);

                InstallShortcut(shortcutPath);
            }

            if (Downloader.Settings.UseSystemStyle)
                ChangeTheme(true, Downloader.Settings.Style, Downloader.Settings.Accent);
            else
            {
                if (string.IsNullOrEmpty(Downloader.Settings.Style))
                    Downloader.Settings.Style = "Light";

                if (string.IsNullOrEmpty(Downloader.Settings.Accent))
                    Downloader.Settings.Accent = "Blue";

                ChangeTheme(false, Downloader.Settings.Style, Downloader.Settings.Accent);
            }       
        }
    }
}
