﻿using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;
using System.Windows.Media;

namespace CryDownloader
{
    /// <summary>
    /// A behavior that extends a <see cref="TreeView"/> with multiple selection capabilities.
    /// </summary>


    /// <remarks>
    /// Largely based on http://chrigas.blogspot.com/2014/08/wpf-treeview-with-multiple-selection.html
    /// </remarks>
    public class TreeViewMultipleSelectionBehavior : Behavior<TreeListView>
    {
        public static readonly DependencyProperty IsMultipleSelectionProperty =
                DependencyProperty.RegisterAttached(
                    "IsMultipleSelection",
                    typeof(bool),
                    typeof(TreeViewMultipleSelectionBehavior),
                    new PropertyMetadata(false, OnMultipleSelectionPropertyChanged));

        public static bool GetIsMultipleSelection(TreeView element)
        {
            return (bool)element.GetValue(IsMultipleSelectionProperty);
        }

        public static void SetIsMultipleSelection(TreeView element, bool value)
        {
            element.SetValue(IsMultipleSelectionProperty, value);
        }

        private static void OnMultipleSelectionPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TreeView treeView)
            {
                if (e.NewValue is bool)
                {
                    if ((bool)e.NewValue)
                    {
                        treeView.AddHandler(TreeViewItem.MouseLeftButtonDownEvent,
                          new MouseButtonEventHandler(OnTreeViewItemClicked), true);

                        treeView.AddHandler(TreeViewItem.KeyDownEvent, 
                            new KeyEventHandler(OnTreeViewKeyDown), true);

                        //treeView.AddHandler(TreeView.SelectedItemChangedEvent, )
                    }
                    else
                    {
                        treeView.RemoveHandler(TreeViewItem.MouseLeftButtonDownEvent,
                             new MouseButtonEventHandler(OnTreeViewItemClicked));

                        treeView.RemoveHandler(TreeViewItem.KeyDownEvent,
                            new KeyEventHandler(OnTreeViewKeyDown));
                    }
                }
            }
        }

        private static void OnTreeViewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.A && (e.KeyboardDevice.Modifiers & ModifierKeys.Control) != 0 && sender is TreeView treeView)
                SelectAllItems(treeView);
        }

        private static void OnTreeViewItemClicked(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem TreeViewItem = FindTreeViewItem(e.OriginalSource as DependencyObject);

            if (TreeViewItem != null && sender is TreeView treeView)
            {
                if (Keyboard.Modifiers == ModifierKeys.Control)
                {
                    SelectMultipleItemsRandomly(treeView, TreeViewItem);
                }
                else if (Keyboard.Modifiers == ModifierKeys.Shift)
                {
                    SelectMultipleItemsContinuously(treeView, TreeViewItem);
                }
                else
                {
                    SelectSingleItem(treeView, TreeViewItem);
                }
            }
        }

        private static TreeViewItem FindTreeViewItem(DependencyObject dependencyObject)
        {
            if (dependencyObject == null)
            {
                return null;
            }

            TreeViewItem TreeViewItem = dependencyObject as TreeViewItem;
            if (TreeViewItem != null)
            {
                return TreeViewItem;
            }

            return FindTreeViewItem(VisualTreeHelper.GetParent(dependencyObject));
        }

        private static void SelectSingleItem(TreeView treeView, TreeViewItem TreeViewItem)
        {
            // first deselect all items
            DeSelectAllItems(treeView, null);
            var selectedItems = GetSelectedItems(treeView);
            selectedItems.Clear();

            SetIsItemSelected(TreeViewItem, true);
            SetStartItem(treeView, TreeViewItem);
        }

        private static void DeSelectAllItems(TreeView treeView, TreeViewItem TreeViewItem)
        {
            if (treeView != null)
            {
                for (int i = 0; i < treeView.Items.Count; i++)
                {
                    if (treeView.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                    {
                        SetIsItemSelected(item, false);
                        DeSelectAllItems(null, item);
                    }
                }
            }
            else
            {
                for (int i = 0; i < TreeViewItem.Items.Count; i++)
                {
                    if (TreeViewItem.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                    {
                        SetIsItemSelected(item, false);
                        DeSelectAllItems(null, item);
                    }
                }
            }
        }

        private static void SelectAllItems(TreeView treeView, TreeViewItem TreeViewItem)
        {
            if (treeView != null)
            {
                for (int i = 0; i < treeView.Items.Count; i++)
                {
                    if (treeView.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                    {
                        SetIsItemSelected(item, true);
                        SelectAllItems(null, item);
                    }
                }
            }
            else
            {
                for (int i = 0; i < TreeViewItem.Items.Count; i++)
                {
                    if (TreeViewItem.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                    {
                        SetIsItemSelected(item, true);
                        SelectAllItems(null, item);
                    }
                }
            }
        }

        public static readonly DependencyProperty IsItemSelectedProperty =
        DependencyProperty.RegisterAttached(
            "IsItemSelected",
            typeof(bool),
            typeof(TreeViewMultipleSelectionBehavior),
            new PropertyMetadata(false, OnIsItemSelectedPropertyChanged));

        public static void DeSelectAllItems(TreeView treeView)
        {
            for (int i = 0; i < treeView.Items.Count; i++)
            {
                if (treeView.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                {
                    SetIsItemSelected(item, false);
                    DeSelectAllItems(null, item);
                }
            }
        }

        public static void SelectAllItems(TreeView treeView)
        {
            DeSelectAllItems(treeView);
            var selectedItems = GetSelectedItems(treeView);
            selectedItems.Clear();

            for (int i = 0; i < treeView.Items.Count; i++)
            {
                if (treeView.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                {
                    SetIsItemSelected(item, true);
                    SelectAllItems(null, item);
                }
            }
        }


        public static bool GetIsItemSelected(TreeViewItem element)
        {
            var result = (bool)element.GetValue(IsItemSelectedProperty);
            return result;
        }

        public static void SetIsItemSelected(TreeViewItem element, bool value)
        {
            element.SetValue(IsItemSelectedProperty, value);
        }

        private static void OnIsItemSelectedPropertyChanged(DependencyObject d,
                                           DependencyPropertyChangedEventArgs e)
        {
            TreeViewItem TreeViewItem = d as TreeViewItem;
            TreeView treeView = FindTreeView(TreeViewItem);

            if (TreeViewItem != null && treeView != null)
            {
                var selectedItems = GetSelectedItems(treeView);
                if (selectedItems != null)
                {
                    if (GetIsItemSelected(TreeViewItem))
                    {
                        selectedItems.Add(TreeViewItem.Header);
                    }
                    else
                    {
                        selectedItems.Remove(TreeViewItem.Header);
                    }
                }

            }
        }

        private static TreeView FindTreeView(DependencyObject dependencyObject)
        {
            if (dependencyObject == null)
            {
                return null;
            }

            if (dependencyObject is TreeView treeView)
            {
                return treeView;
            }

            return FindTreeView(VisualTreeHelper.GetParent(dependencyObject));
        }

        public static readonly DependencyProperty SelectedItemsProperty =
        DependencyProperty.RegisterAttached(
            "SelectedItems",
            typeof(IList),
            typeof(TreeViewMultipleSelectionBehavior),
            new PropertyMetadata());

        public static IList GetSelectedItems(TreeView element)
        {
            IList list = (IList)element.GetValue(SelectedItemsProperty);

            if (list == null)
                element.SetValue(SelectedItemsProperty, new List<object>());

            return (IList)element.GetValue(SelectedItemsProperty);
        }

        public static void SetSelectedItems(TreeView element, IList value)
        {
            element.SetValue(SelectedItemsProperty, value);
        }

        private static readonly DependencyProperty StartItemProperty =
        DependencyProperty.RegisterAttached(
            "StartItem",
            typeof(TreeViewItem),
            typeof(TreeViewMultipleSelectionBehavior),
            new PropertyMetadata());

        private static TreeViewItem GetStartItem(TreeView element)
        {
            return (TreeViewItem)element.GetValue(StartItemProperty);
        }

        private static void SetStartItem(TreeView element, TreeViewItem value)
        {
            element.SetValue(StartItemProperty, value);
        }

        private static void SelectMultipleItemsRandomly(TreeView treeView,
                                                    TreeViewItem TreeViewItem)
        {
            SetIsItemSelected(TreeViewItem, !GetIsItemSelected(TreeViewItem));
            if (GetStartItem(treeView) == null)
            {
                if (GetIsItemSelected(TreeViewItem))
                {
                    SetStartItem(treeView, TreeViewItem);
                }
            }
            else
            {
                if (GetSelectedItems(treeView).Count == 0)
                {
                    SetStartItem(treeView, null);
                }
            }
        }

        private static void SelectMultipleItemsContinuously(TreeView treeView,
                                                     TreeViewItem TreeViewItem)
        {
            TreeViewItem startItem = GetStartItem(treeView);
            if (startItem != null)
            {
                if (startItem == TreeViewItem)
                {
                    SelectSingleItem(treeView, TreeViewItem);
                    return;
                }

                ICollection<TreeViewItem> allItems = new List<TreeViewItem>();
                GetAllItems(treeView, null, allItems);
                DeSelectAllItems(treeView, null);
                bool isBetween = false;
                foreach (var item in allItems)
                {
                    if (item == TreeViewItem || item == startItem)
                    {
                        // toggle to true if first element is found and
                        // back to false if last element is found
                        isBetween = !isBetween;

                        // set boundary element
                        SetIsItemSelected(item, true);
                        continue;
                    }

                    if (isBetween)
                    {
                        SetIsItemSelected(item, true);
                    }
                }
            }
        }

        private static void GetAllItems(TreeView treeView, TreeViewItem TreeViewItem,
                                    ICollection<TreeViewItem> allItems)
        {
            if (treeView != null)
            {
                for (int i = 0; i < treeView.Items.Count; i++)
                {
                    if (treeView.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                    {
                        allItems.Add(item);
                        GetAllItems(null, item, allItems);
                    }
                }
            }
            else
            {
                for (int i = 0; i < TreeViewItem.Items.Count; i++)
                {
                    if (TreeViewItem.ItemContainerGenerator.ContainerFromIndex(i) is TreeViewItem item)
                    {
                        allItems.Add(item);
                        GetAllItems(null, item, allItems);
                    }
                }
            }
        }
    }
}
