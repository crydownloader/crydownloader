﻿using CryDownloader.Plugin;
using System;
using System.Threading;
using System.Windows;

namespace CryDownloader
{
    /// <summary>
    /// Interaktionslogik für Browser.xaml
    /// </summary>
    public partial class Browser : Window, IManualResolveElement
    {
        public bool IsCancelled { get; private set; }

        public ManualResetEvent ConditionVariable { get; set; }

        public bool IsOpen { get; private set; }

        private bool m_bValidated;

        public Browser(FrameworkElement element, bool needAction)
        {
            InitializeComponent();
            m_Grid.Children.Add(element);

            if (!needAction)
                HideActionBar();
        }

        void IManualResolveElement.ShowDialog()
        {
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            IsOpen = true;
            ShowDialog();
        }

        public void HideActionBar()
        {
            m_actionBar.Height = new GridLength(0, GridUnitType.Pixel);
            m_actionGrid.IsEnabled = false;
        }

        void IManualResolveElement.Show(Visibility visibility)
        {
            Dispatcher.Invoke(() =>
            {
                WindowStartupLocation = WindowStartupLocation.CenterOwner;

                IsOpen = true;
                Visibility = visibility;

                if (Visibility != Visibility.Visible)
                    WindowState = WindowState.Minimized;

                Show();
            });
        }

        void IManualResolveElement.Close()
        {
            m_bValidated = true;
            IsCancelled = false;
            Dispatcher.Invoke(() => Close());
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (!m_bValidated)
                IsCancelled = true;

            if (ConditionVariable != null)
                ConditionVariable.Set();
        }

        private void Validate_Click(object sender, RoutedEventArgs e)
        {
            m_bValidated = true;
            IsCancelled = false;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            m_bValidated = true;
            IsCancelled = true;
            Close();
        }
    }
}
