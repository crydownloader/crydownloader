﻿using System;

namespace CryDownloader
{
    static class TimeSpanConverter
    {
        public static string GetString(TimeSpan timeSpan, bool showMilliseconds)
        {
            if (timeSpan.Hours > 0)
            {
                if (showMilliseconds)
                    return $"{timeSpan.Hours} h {timeSpan.Minutes} min {timeSpan.Seconds} sec {timeSpan.Milliseconds} ms";
                else
                    return $"{timeSpan.Hours} h {timeSpan.Minutes} min {timeSpan.Seconds} sec";
            }
            else if (timeSpan.Minutes > 0)
            {
                if (showMilliseconds)
                    return $"{timeSpan.Minutes} min {timeSpan.Seconds} sec {timeSpan.Milliseconds} ms";
                else
                    return $"{timeSpan.Minutes} min {timeSpan.Seconds} sec";
            }
            else if (timeSpan.Seconds > 0)
            {
                if (showMilliseconds)
                    return $"{timeSpan.Seconds} sec {timeSpan.Milliseconds} ms";
                else
                    return $"{timeSpan.Seconds} sec";
            }
            else if (timeSpan.Seconds > 0 && showMilliseconds)
                return $"{timeSpan.Milliseconds} ms";

            return string.Empty;
        }
    }
}
