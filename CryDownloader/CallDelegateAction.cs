﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Xaml.Behaviors;
using System.Windows.Markup;

namespace CryDownloader
{
    [ContentProperty("Parameters")]
    class CallDelegateAction : TriggerAction<FrameworkElement>
    {
        public static readonly DependencyProperty DelegateNameProperty
              = DependencyProperty.Register("DelegateName", typeof(string),
              typeof(CallDelegateAction));

        public static readonly DependencyProperty TargetObjectProperty
              = DependencyProperty.Register("TargetObject", typeof(object),
              typeof(CallDelegateAction));

        private static readonly DependencyProperty ParametersProperty
              = DependencyProperty.RegisterAttached("Parameters", typeof(Collection<Parameter>),
                  typeof(CallDelegateAction));

        public string DelegateName
        {
            get { return (string)GetValue(DelegateNameProperty); }
            set { SetValue(DelegateNameProperty, value); }
        }

        public object TargetObject
        {
            get { return GetValue(TargetObjectProperty); }
            set { SetValue(TargetObjectProperty, value); }
        }

        public Collection<Parameter> Parameters
        {
            get { return (Collection<Parameter>)GetValue(ParametersProperty); }
            set { SetValue(ParametersProperty, value); }
        }

        public CallDelegateAction()
        {
            Parameters = new Collection<Parameter>();
        }

        protected override void Invoke(object parameter)
        {
            object target = TargetObject ?? AssociatedObject;
            PropertyInfo propertyInfo = target.GetType().GetProperty(
                DelegateName,
                BindingFlags.Instance | BindingFlags.Public
                | BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Static);

            var methodParams = Parameters;

            foreach (var param in methodParams)
            {
                if (param.Value == null)
                    param.Value = AssociatedObject;
            }

            if (propertyInfo.PropertyType.IsAssignableFrom(typeof(Delegate)))
            {
                Delegate dele = (Delegate)propertyInfo.GetValue(TargetObject);
                dele.DynamicInvoke(methodParams.Select((x) => x.Value).ToArray());
            }
        }
    }
}
